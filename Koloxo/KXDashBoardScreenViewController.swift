//
//  KXDashBoardScreenViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 12/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXDashBoardScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Humberger_Menu(_ sender: Any) {
        let sideMenuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
        self.present(sideMenuVC, animated: true, completion: nil)
    }
    @IBAction func My_property(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(2, forKey: "Tab_position")
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func MYRequest_Button(_ sender: Any) {
        let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "KXRequestViewController") as! KXRequestViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Tab_Home(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Tab_Button(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func Back_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
}
