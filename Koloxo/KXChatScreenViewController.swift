//
//  KXChatScreenViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 12/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu

class KXChatScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Back_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    @IBAction func Humberger_Button(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    @IBAction func Make_Deal(_ sender: Any) {
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func Decline(_ sender: Any) {
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Tab_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Home_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
    @IBAction func My_Profile(_ sender: Any) {
    }


}
