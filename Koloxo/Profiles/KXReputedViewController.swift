//
//  KXReputedViewController.swift
//  kxtrail
//
//  Created by admin on 27/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SideMenu

class KXReputedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,KXAddTitlePopupDelegate {
    

    @IBOutlet weak var img_propic: UIImageView!
    
    private var deselectedCell:KXReputedTableViewCell?
    private var isRotatedArrowIMG: Bool = false
    private var selectedIndexPath: IndexPath?
    private var isExpandedCell: Bool = false
    
    var numberOfDocument = 0
    var numberofDataPerDocument = 1
    var count = 4
    
    var documentData = [Array<Data>()]
    var titleArray:[String] = []
    
    
    var  model = [[UIColor]]()
    
    var data = [[UIColor]]()
    
    var storedOffsets = [Int: CGFloat]()
    
    @IBOutlet weak var tableRef: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AddDocument(_ sender: IPButton) {
        let popVC = storyboardSearch!.instantiateViewController(withIdentifier: "KXAddTitilePopupViewController") as! KXAddTitilePopupViewController
        popVC.delegate = self
        self.present(popVC, animated: true, completion: nil)
    }
    
    @IBAction func AddPopUp_Btn(_ sender: Any) {
        let popVC = storyboardSearch!.instantiateViewController(withIdentifier: "KXAddTitilePopupViewController") as! KXAddTitilePopupViewController
        popVC.delegate = self
        self.present(popVC, animated: true, completion: nil)
        
    }
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
    }
    
    @IBAction func verifyTapped(_ sender: Any) {
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- Delegate Function
    func setTitle(name: String, documentData: Data, docURL:URL) {
        
        tableRef.reloadData()
    }
    
    
    
    func generateDocument(index:IndexPath) -> [[UIColor]]{
        return (0..<index.section).map({ _ in
            return (0..<index.item).map({ _ in UIColor.randomColor() })
        })
    }
    
    func generateRandomData() -> [[UIColor]] {
        let numberOfRows = 20
        let numberOfItemsPerRow = 15
        
        return (0..<numberOfRows).map { _ in
            return (0..<numberOfItemsPerRow).map { _ in UIColor.randomColor() }
        }
    }
    
    @objc func openCloseCellBTNTapped(_ sender: IPButton) {
        
        let indexPath = sender.indexPath!
        guard let selectedCell = tableRef.cellForRow(at: indexPath) as? KXReputedTableViewCell else { return }
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
        //updateContent(of: selectedCell, byIndexPath: selectedIndexPath!)
        tableRef.beginUpdates()
        self.rotateArrowIMG(of: selectedCell)
        tableRef.endUpdates()
        
    }
    
    private func rotateArrowIMG(of selectedCell: KXReputedTableViewCell){
        let towardsDown = CGFloat(Double.pi)
        let towardsRight = CGFloat(-Double.pi)
        
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.3, animations: {
        
        if self.deselectedCell == nil {
            /// tapping on the cell first time
             selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsDown)
            self.isRotatedArrowIMG = true
        }else if self.deselectedCell == selectedCell {
            /// tapping on already selected cell
            if self.isRotatedArrowIMG {
                selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsRight)
                self.isRotatedArrowIMG = false
            }else{
                selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsDown)
                self.isRotatedArrowIMG = true
            }
        }else{
            /// tapping on other cell not already selected cell
            selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsDown)
            guard let dCell = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
            if self.isRotatedArrowIMG {
                dCell.arrowIMG.transform = dCell.arrowIMG.transform.rotated(by: towardsRight)
            }
            self.isRotatedArrowIMG = true
        }
        self.deselectedCell = selectedCell
            })
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
        return selectedIndexPath == indexPath ? (isExpandedCell ? 284 :  60) :  60
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentData[section].count
//       return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KXReputedTableViewCell") as? KXReputedTableViewCell else { return UITableViewCell() }
        //print(cell.expandCollapseButton)
         cell.expandCollapseButton.indexPath = indexPath
         cell.expandCollapseButton.addTarget(self, action:  #selector(openCloseCellBTNTapped(_:)), for: .touchDown)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? KXReputedTableViewCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? KXReputedTableViewCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
}

extension KXReputedViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return 2//data[collectionView.tag].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXReputedCollectionViewCell", for: indexPath) as! KXReputedCollectionViewCell

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

}





extension UIColor {
    
    class func randomColor() -> UIColor {
        
        let hue = CGFloat(arc4random() % 100) / 100
        let saturation = CGFloat(arc4random() % 100) / 100
        let brightness = CGFloat(arc4random() % 100) / 100
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
}
