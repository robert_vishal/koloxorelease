//
//  KXAddTitilePopupViewController.swift
//  kxtrail
//
//  Created by admin on 30/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol KXAddTitlePopupDelegate: class {
    func setTitle(name: String, documentData:Data, docURL:URL)
}

class KXAddTitilePopupViewController: UIViewController {

    
    @IBOutlet weak var documentChosen: UILabel!
    @IBOutlet weak var titleNameField: UITextField!
    weak var delegate: KXAddTitlePopupDelegate?
    
    var documentChosemLink:URL?
    var documentChosenData:Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func DissmissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        if let _ = documentChosenData, let text = titleNameField.text, text.isNotEmpty, let _ = documentChosemLink{
            delegate?.setTitle(name: titleNameField.text!, documentData: documentChosenData!, docURL: documentChosemLink!)
            self.dismiss(animated: true, completion: nil)
        }else {
            
            if !BaseValidator.isNotEmpty(string: titleNameField.text) {
                showBanner(message: "Please enter Title")

            }else {
                showBanner(message: "Please choose Document")
            }
            
            
        }
    }
    
    
    @IBAction func chooseDocumentTapped(_ sender: RoundedButton) {
        getDataFromICloud()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension KXAddTitilePopupViewController: UIDocumentPickerDelegate, UIDocumentMenuDelegate{
    
    func getDataFromICloud(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        //        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        //importMenu.modalPresentationStyle = .formSheet
        present(importMenu, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        do{
            let data = try Data(contentsOf: url)
            self.documentChosenData = data
            self.documentChosen.text = "Document-1"
            self.documentChosemLink = url
        }catch{
            print(error)
        }
    }
    
}
