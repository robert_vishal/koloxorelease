//
//  KXReputedTV.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 24/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class IPButton: UIButton {
    var indexPath:IndexPath?
    convenience init(indexPath: IndexPath) {
        self.init(frame:CGRect.zero)
        self.indexPath = indexPath
    }
}

class KXReputedTV: UITableViewCell {
    
    @IBOutlet var imageview: UIImageView!
    @IBOutlet var TitleLabel_Furniture: UILabel!
    
    @IBOutlet var headerLBL: UILabel!
    @IBOutlet var TitleLabel: UILabel!
    //    @IBOutlet var slotTitleLBL: UILabel!
//    @IBOutlet var arrowIMG: UIImageView!
    @IBOutlet var openCloseCellBTN: IPButton!
    
//    @IBOutlet var sizeTF: UITextField!
//    @IBOutlet var priceTF: UITextField!
//    @IBOutlet var imageNameLBL: UILabel!
//    @IBOutlet var addImageBTN: IPButton!
//    @IBOutlet var saveDetailsBTN: IPButton!
//    @IBOutlet var showImageBTN: IPButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
