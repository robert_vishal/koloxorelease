//
//  KXReputedTableViewCell.swift
//  kxtrail
//
//  Created by admin on 27/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
class IPButton1: UIButton {
    var indexPath:IndexPath?
    convenience init(indexPath: IndexPath) {
        self.init(frame:CGRect.zero)
        self.indexPath = indexPath
    }
}


class KXReputedTableViewCell: UITableViewCell {

    
    @IBOutlet weak var col_reputed: UICollectionView!
    @IBOutlet weak var expandCollapseButton: IPButton1!
    @IBOutlet weak var arrowIMG: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    var collectionViewOffset: CGFloat {
        get {
            return col_reputed.contentOffset.x
        }
        
        set {
            col_reputed.contentOffset.x = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        col_reputed.delegate = dataSourceDelegate
        col_reputed.dataSource = dataSourceDelegate
        col_reputed.tag = row
        
        print(row)
        
        col_reputed.reloadData()
    }

}
