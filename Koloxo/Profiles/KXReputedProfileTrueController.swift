//
//  KXReputedProfileTrueController.swift
//  Koloxo
//
//  Created by Arun Induchoodan on 18/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos

class KXReputedProfileTrueController: UIViewController, KXAddTitlePopupDelegate {
   
    @IBOutlet weak var tableRef: UITableView!
    @IBOutlet var profileImage: StandardImageView!
    @IBOutlet var profileImageEditView: StandardView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    var selectedSection = 99
    var documentDataArray:[Array<Data>] = []
    var sectionTitleArray:[String] = []
    var insertIndex:IndexPath?
    
    fileprivate var passingUserID: String = Credentials.shared.userId
    fileprivate var isProfileImageSelected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewDidLayoutSubviews() {
        tableHeight.constant = tableRef.contentSize.height
        self.tableRef.estimatedRowHeight = 169
    }
    
    func fillWithDummyData(){
        for _ in 0...3{
          documentDataArray.append(Array<Data>())
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skipBTNTapped(_ sender: UIButton) {
        let vc = self.storyboardDashboard!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func profileImageTapped(_ sender: UIButton) {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        presentOpalImagePickerController(imagePicker, animated: true,
                                         select: { (assets) in
                                            for item in assets{
                                                let OPImage = self.getAssetThumbnail(asset: item)
                                                print(OPImage,"image")
                                                DispatchQueue.main.async {
                                                    self.profileImage.image = OPImage
                                                    self.isProfileImageSelected = true
                                                }
                                            }
                                            self.dismiss(animated: true, completion: {
                                                
                                            })
        }, cancel: {
            //self.dismiss(animated: true, completion: nil)
        })

    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 500, height: 500), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    

    @IBAction func addDocumentAsNewSection(_ sender: UIButton) {
        insertIndex = nil
        let popVC = storyboardSearch!.instantiateViewController(withIdentifier: "KXAddTitilePopupViewController") as! KXAddTitilePopupViewController
        popVC.delegate = self
        self.present(popVC, animated: true, completion: nil)
    }
    
    @IBAction func headerBTNTapped(_ sender: UIButton) {
        self.selectedSection = sender.tag
        tableRef.reloadData()
        DispatchQueue.main.async {
         self.tableHeight.constant = self.tableRef.contentSize.height
        }
       
    }
    
    
    @IBAction func deleteDocument(_ sender: StandardButton) {
        documentDataArray[sender.indexPath!.section].remove(at:sender.indexPath!.row)
        tableRef.reloadData()
    }
    
    
    @IBAction func addDocumentToSection(_ sender: StandardButton) {
        insertIndex = sender.indexPath
        let popVC = self.storyboardSearch!.instantiateViewController(withIdentifier: "KXAddTitilePopupViewController") as! KXAddTitilePopupViewController
        popVC.delegate = self
        self.present(popVC, animated: true, completion: nil)
    }
    
    
    @IBAction func postProfileData(_ sender: UIButton) {
        postProfileData()
        if isProfileImageSelected {
            uploadUserProfileImage()
            isProfileImageSelected = false
        }
    }
    
    // Delegate from POP-Up
    func setTitle(name: String, documentData: Data, docURL:URL) {
        if let index = insertIndex{
            documentDataArray[index.section].append(documentData)
        }else{
            sectionTitleArray.append(name)
            documentDataArray.append([documentData])
        }
        tableRef.reloadData()
        DispatchQueue.main.async {
            self.tableHeight.constant = self.tableRef.contentSize.height
            if !self.documentDataArray.isEmpty {
                self.tableRef.scrollToRow(at: IndexPath.init(row: 0, section: self.documentDataArray.count-1), at: .top, animated: true)
            }
        }
        
    }
    
    func postProfileData(){
        let unixTime = SecureSocket.getTimeStamp()
        _ = SecureSocket.getToken(with: unixTime)
        var param:[String:String] = ["user_id":"\(Credentials.shared.userId)"]
        var dataParam:[String:Data] = [:]
        for (index,item) in documentDataArray.enumerated(){
            param.updateValue("\(sectionTitleArray[index])", forKey: "document[\(index)][0]")
            for (subIndex,doc) in item.enumerated(){
                dataParam.updateValue(doc, forKey: "document[\(index)][\(subIndex+1)]")
            }
        }
        Webservices.postMethodMultiPartData(url: "add_reputed_profile", parameter: param, dataParameter: dataParam) { (isComplete, json) in
            print(json)
            if isComplete {
                BaseThread.asyncMain {
                    let vc = self.storyboardDashboard!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
                    self.navigationController?.pushViewController(vc, animated: true)

                }
            }
            
        }
    }
    
    func uploadUserProfileImage(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
        //        var param:[String:Any] = ["id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime
        //        ]
        let param:[String:Any] = ["id":"\(passingUserID)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime
        ]
        
        var dataParam:[String:Data] = [:]
        if let proImage = self.profileImage.image{
            dataParam.updateValue(UIImageJPEGRepresentation(proImage, 1)!, forKey: "image")
        }
        //print("Data Param",dataParam)
        Webservices.postMethodMultiPartImage(url: "updateUserimage",
                                             parameter:param,
                                             dataParameter:dataParam ) { (isCompleted, json) in
                                                //print(json)
                                                if isCompleted {
                                                    let data = json["Data"] as? [String:Any]
                                                    if let image = data!["image"] as? String{
                                                        Credentials.shared.profileImage = image
                                                    }
                                                }else{
                                                    print("Call Failed")
                                                }
        }
    }

}

extension KXReputedProfileTrueController:UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return documentDataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 169
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == selectedSection{
            return 1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReputedProfileTableCell") as! ReputedProfileTableCell
        cell.displayData = documentDataArray[indexPath.section]
        cell.currentSection = indexPath.section
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! header
        cell.headerLBL.text = sectionTitleArray[section]
        cell.headerBTN.tag = section
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "footer")
        return cell
    }

}

class ReputedProfileColBTNCell: UICollectionViewCell{
    
    @IBOutlet weak var addBTN: StandardButton!
    
}

class ReputedProfileDocumentColCell: UICollectionViewCell{
    
    @IBOutlet weak var documentLabel: UILabel!
    @IBOutlet weak var documentImage: BaseImageView!
    
    @IBOutlet weak var closeBTNRef: StandardButton!
    
}

class ReputedProfileTableCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionRef: UICollectionView!
    var currentSection = 0
    
    override func awakeFromNib() {
        collectionRef.delegate = self
        collectionRef.dataSource = self
    }
    
    var displayData:[Data] = []{didSet{collectionRef.reloadData()}}
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayData.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellData = collectionView.dequeueReusableCell(withReuseIdentifier: "ReputedProfileDocumentColCell", for: indexPath) as! ReputedProfileDocumentColCell
        let cellBTN = collectionView.dequeueReusableCell(withReuseIdentifier: "ReputedProfileColBTNCell", for: indexPath) as! ReputedProfileColBTNCell
        if indexPath.row >= displayData.count{
            cellBTN.addBTN.indexPath = IndexPath(row: indexPath.row, section: currentSection)
            return cellBTN
        }else{
            cellData.closeBTNRef.indexPath = IndexPath(row: indexPath.row, section: currentSection)
            cellData.documentLabel.text = "Document-\(indexPath.row+1)"
            return cellData
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 122, height: 142)
    }
    
}

extension UITableView{
    func reloadWithCompletion(completion:@escaping()->()){
        self.reloadData()
        completion()
    }
}
