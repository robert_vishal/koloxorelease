//
//  KXMyRequestTableViewCell.swift
//  Koloxo
//
//  Created by Appzoc on 22/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class KXMyRequestTableViewCell: UITableViewCell {
    
    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var propertyStatusLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
