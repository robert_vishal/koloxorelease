//
//  KXSearchListModel.swift
//  Koloxo
//
//  Created by Appzoc on 14/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

class KXSearchListModel
{
    var buildings:[KXSearchBuildingModel] = []
    var availables:[KXSearchAvailableModel] = []
    var almost_availables:[KXSearchAvailableModel] = []
    var not_availables:[KXSearchAvailableModel] = []
    
    init(dictionary: Json) {
        buildings = KXSearchBuildingModel.getValue(fromArray: dictionary["building"] as! JsonArray)
        
        availables = KXSearchAvailableModel.getValue(fromArray: dictionary["available"] as! JsonArray)

        almost_availables = KXSearchAvailableModel.getValue(fromArray: dictionary["almost_availables"] as! JsonArray)
        
        not_availables = KXSearchAvailableModel.getValue(fromArray: dictionary["not_availables"] as! JsonArray)

    }
    
    init(){
    }
    
    class func getValue(fromObject: Json?) -> KXSearchListModel
    {
        guard let object = fromObject else { return KXSearchListModel() }
        return KXSearchListModel(dictionary: object)
    }
    
}

public enum BuildingAvailability: String {
    case available = "Available"
    case almostAvailable = "Almost Available"
    case notAvailable = "Not Available"
}

class KXSearchBuildingModel
{
    var id:Int = 0
    var name:String = ""
    var description:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var spec:String = ""
    var location:String = ""
    var file_upload:String = ""
    var status:Int = 0
    var updated_at:String = ""
    var created_at:String = ""
    var deleted_at:String = ""
    var property_building_count:Int = 0
    var property_buildings:[KXPropertyBuildingModel] = []
    // additional property used for determining building availability -  in the list view,
    var buildingAvailability: BuildingAvailability = .available
    var propertySourceInBuilding: [KXSearchAvailableModel] = []
    var propertyAvailability: BuildingAvailability = .available
    
    // additional property for all search result list view
    var isProperty: Bool = false
    init(dictionary: Json) {
        
        id = dictionary["id"] as? Int ?? 0
        name = dictionary["name"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        latitude = dictionary["latitude"] as? String ?? ""
        longitude = dictionary["longitude"] as? String ?? ""
        spec = dictionary["spec"] as? String ?? ""
        location = dictionary["location"] as? String ?? ""
        file_upload = dictionary["file_upload"] as? String ?? ""
        status = dictionary["status"] as? Int ?? 0
        updated_at = dictionary["updated_at"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        property_building_count = dictionary["property_building_count"] as? Int ?? 0
        deleted_at = dictionary["deleted_at"] as? String ?? ""
        
        property_buildings = KXPropertyBuildingModel.getValue(fromArray: dictionary["property_building"] as! JsonArray)
    }
    
    
    init() {
        
    }
    
    class func getValue(fromObject: Json?)-> KXSearchBuildingModel {
        guard let object = fromObject else { return KXSearchBuildingModel() }
        return KXSearchBuildingModel(dictionary: object)
    }
    
    
    class func getValue(fromArray: JsonArray) -> [KXSearchBuildingModel] {
        var modelArray: [KXSearchBuildingModel] = []
        for item in fromArray {
            modelArray.append(KXSearchBuildingModel(dictionary: item))
        }
        return modelArray
    }
    
    
}


class KXPropertyBuildingModel
{
    var id:Int = 0
    var building_id:Int = 0
    
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? 0
        building_id = dictionary["building_id"] as? Int ?? 0
    }
    
    init() {
        
    }
    class func getValue(fromObject: Json?)-> KXPropertyBuildingModel {
        guard let object = fromObject else { return KXPropertyBuildingModel() }
        return KXPropertyBuildingModel(dictionary: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXPropertyBuildingModel]
    {
        var result:[KXPropertyBuildingModel] = []
        for item in fromArray
        {
            result.append(KXPropertyBuildingModel.getValue(fromObject: item))
        }
        return result
    }
    
}

struct KXSearchAvailableModel
{
    var id:Int = 0
    var property_name:String = ""
    var usd_price:Double = 0.0
    var bathroom_count:Int = 0
    var bedroom_count:Int = 0
    var building_id:Int = 0
    
    var property_images:[KXSearchPropertyImageModel] = []
    var property_facilitys:[KXDetailPropertyFacilityModel] = []
    var property_bedrooms:KXSearchPropertyBedroomsModel?
    var property_bathrooms:KXSearchPropertyBedroomsModel?
    var property_ratings_totals:[KXSearchPropertyRatingsTotalModel] = []
    
    var wish_list: Int = 0
    var property_owner: Int = 0
    
    // additional variables for listing
    var numberOfProperties: Int = 2
    var buildingAvailability: BuildingAvailability = .available
    var location: String = ""
    
    init(dictionary: Json) {
        id = dictionary["id"] as? Int ?? 0
        property_name = dictionary["property_name"] as? String ?? ""
        usd_price = dictionary["usd_price"] as? Double ?? 0.0
        bathroom_count = dictionary["bathroom_count"] as? Int ?? 0
        bedroom_count = dictionary["bedroom_count"] as? Int ?? 0
        building_id = dictionary["building_id"] as? Int ?? 0
        property_owner = dictionary["property_owner"] as? Int ?? 0
        
        property_images = KXSearchPropertyImageModel.getValue(fromArray: dictionary["property_images"] as! JsonArray)
        property_facilitys = KXDetailPropertyFacilityModel.getValue(fromArray: dictionary["property_facility"] as! JsonArray)
        property_bedrooms = KXSearchPropertyBedroomsModel.getValue(fromObject: dictionary["property_bedrooms"] as? Json)
        property_bathrooms = KXSearchPropertyBedroomsModel.getValue(fromObject: dictionary["property_bathrooms"] as? Json)
        property_ratings_totals = KXSearchPropertyRatingsTotalModel.getValue(fromArray: dictionary["property_ratings_total"] as! JsonArray)
        
        if let wishListArray = dictionary["wish_list"] as? JsonArray {
            wish_list = wishListArray.isEmpty ? 0 : 1
           // print("Wishlist---",wish_list)
        }
        
    }
    
    init(){}
    
    static func getValue(fromObject: Json?) -> KXSearchAvailableModel {
        guard let object = fromObject else { return KXSearchAvailableModel() }
        return KXSearchAvailableModel(dictionary: object)
    }
    
    static func getValue(fromArray: JsonArray) -> [KXSearchAvailableModel] {
        var modelArray: [KXSearchAvailableModel] = []
        for item in fromArray {
            modelArray.append(KXSearchAvailableModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
}


class KXSearchPropertyBedroomsModel
{
    var id:Int = 0
    var data:String = ""
    
    init(dictionary: Json) {
        id = dictionary["id"] as! Int
        data = dictionary["data"] as! String
    }
    init(){}
    class func getValue(fromObject: Json?) -> KXSearchPropertyBedroomsModel {
        guard let object = fromObject else { return KXSearchPropertyBedroomsModel() }
        return KXSearchPropertyBedroomsModel(dictionary: object)
    }
    
    class func getValue(fromArray:JsonArray) -> [KXSearchPropertyBedroomsModel]
    {
        var result:[KXSearchPropertyBedroomsModel] = []
        for item in fromArray
        {
            result.append(KXSearchPropertyBedroomsModel.getValue(fromObject: item))
        }
        return result
    }
    
}



class KXSearchTypesModel
{
    var id:Int = 0
    var type:String = ""
    
    init(){}
    
    init(dictionary: Json) {
        
        id = dictionary["id"] as! Int
        type = dictionary["type"] as! String
    }
    
    class func getValue(fromObject: Json?) ->KXSearchTypesModel {
        guard let object = fromObject else { return KXSearchTypesModel() }
        return KXSearchTypesModel(dictionary: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXSearchTypesModel]
    {
        var result:[KXSearchTypesModel] = []
        for item in fromArray
        {
            result.append(KXSearchTypesModel.getValue(fromObject: item))
        }
        return result
    }
    
}

class KXSearchPropertyImageModel
{
    var id:Int = 0
    var property_id:Int = 0
    var type_id:Int = 0
    var image:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var types:KXSearchTypesModel?
    
    init(dictionary: Json) {
        
        id = dictionary["id"] as! Int
        property_id = dictionary["property_id"] as! Int
        type_id = dictionary["type_id"] as! Int
        image = dictionary["image"] as! String
        created_at = dictionary["created_at"] as! String
        updated_at = dictionary["updated_at"] as! String
        types = KXSearchTypesModel.getValue(fromObject: dictionary["types"] as? Json)
    }
    
    init(){}
    
    class func getValue(fromObject: Json?) -> KXSearchPropertyImageModel {
        guard let object = fromObject else { return KXSearchPropertyImageModel()}
        return KXSearchPropertyImageModel(dictionary: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXSearchPropertyImageModel]
    {
        var result:[KXSearchPropertyImageModel] = []
        for item in fromArray
        {
            result.append(KXSearchPropertyImageModel.getValue(fromObject: item))
        }
        return result
    }
    
}



class KXSearchPropertyRatingsTotalModel
{
    var aggregate:String = ""
    var property_id:Int = 0
    
    init(){}
    init(dictionary: Json) {
        aggregate = dictionary["aggregate"] as! String
        property_id = dictionary["property_id"] as! Int
    }
    class func getValue(fromObject: Json?) -> KXSearchPropertyRatingsTotalModel {
        guard let object = fromObject else { return KXSearchPropertyRatingsTotalModel() }
        return KXSearchPropertyRatingsTotalModel(dictionary: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXSearchPropertyRatingsTotalModel]
    {
        var result:[KXSearchPropertyRatingsTotalModel] = []
        for item in fromArray
        {
            result.append(KXSearchPropertyRatingsTotalModel.getValue(fromObject: item))
        }
        return result
    }
    
}



//public class KXSearchModelPropertyImage {
//    var id:Int = 0
//    var property_id:Int = 0
//    var type_id:Int = 0
//    var image:String = ""
//
//    init() {
//
//    }
//
//    class func getValue(from object: Json?) -> KXRequestModelPropertyImage {
//        guard let object = object else { return KXRequestModelPropertyImage() }
//        return KXRequestModelPropertyImage(object)
//    }
//
//    class func getValue(from array: JsonArray?) -> [KXRequestModelPropertyImage] {
//        var modelArray : [KXRequestModelPropertyImage] = []
//        guard let array = array else { return modelArray }
//        for item in array{
//            let object = KXRequestModelPropertyImage.getValue(from: item)
//            modelArray.append(object)
//        }
//        return modelArray
//    }
//
//
//    required public init(_ object: Json) {
//        id = object["id"] as? Int ?? 0
//        property_id = object["property_id"] as? Int ?? 0
//        type_id = object["type_id"] as? Int ?? 0
//        image = object["image"] as? String ?? ""
//    }
//
//    private init(){}
//
//}



//class AlmostAvailable
//{
//    var id:Int = 0
//    var property_name:String = ""
//    var usd_price:Int = 0
//    var bathroom_count:Int = 0
//    var bedroom_count:Int = 0
//    var building_id:Int = 0
//    var property_images:[AnyObject] = []
//    var property_facilitys:[AnyObject] = []
//    var property_bedrooms:PropertyBedrooms2 = 0
//    var property_bathrooms:PropertyBathrooms2 = 0
//    var property_ratings_totals:[AnyObject] = []
//    //var property_rentals:[AnyObject] = []
//
//    func Populate(dictionary:NSDictionary) {
//
//        id = dictionary["id"] as! Int
//        property_name = dictionary["property_name"] as! String
//        usd_price = dictionary["usd_price"] as! Int
//        bathroom_count = dictionary["bathroom_count"] as! Int
//        bedroom_count = dictionary["bedroom_count"] as! Int
//        building_id = dictionary["building_id"] as! Int
//        property_images = AnyObject.PopulateArray(dictionary["property_images"] as! [NSArray])
//        property_facilitys = AnyObject.PopulateArray(dictionary["property_facility"] as! [NSArray])
//        property_bedrooms = dictionary["property_bedrooms"] as! PropertyBedrooms2
//        property_bathrooms = dictionary["property_bathrooms"] as! PropertyBathrooms2
//        property_ratings_totals = AnyObject.PopulateArray(dictionary["property_ratings_total"] as! [NSArray])
//        property_rentals = AnyObject.PopulateArray(dictionary["property_rental"] as! [NSArray])
//    }
//    class func PopulateArray(array:NSArray) -> [AlmostAvailable]
//    {
//        var result:[AlmostAvailable] = []
//        for item in array
//        {
//            var newItem = AlmostAvailable()
//            newItem.Populate(item as! NSDictionary)
//            result.append(newItem)
//        }
//        return result
//    }
//
//}



