//
//  KXWishListVC.swift
//  KoloxoScreens
//
//  Created by Appzoc on 13/06/18.
//  Copyright © 2018 AppZoc. All rights reserved.
//

import UIKit

class wishListCell:UITableViewCell
{
    @IBOutlet var propertyImage: UIImageView!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var deleteBTN: UIButton!
    @IBOutlet weak var propertyTypeView: UIView!
    @IBOutlet weak var propertyTypeLBL: UILabel!
    
    override func awakeFromNib() {
        
        propertyTypeView.transform = CGAffineTransform.identity
        propertyTypeView.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
    }

}

class KXWishListVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    
    var contentArray = [[String:Any]]()
    var userID = Credentials.shared.userId
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getDataWeb()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    
    fileprivate func getDataWeb(){
        Webservices.postMethod(url: "wishList", parameter: ["user_id":"\(userID)"])
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.contentArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                self.contentTV.reloadData()
            }
        }

    }
    
    @IBAction func deleteBTNTapped(_ sender: UIButton)
    {
        
        
        
        let otherAlert = UIAlertController(title: nil, message: "Are you sure you want to delete this property?", preferredStyle: UIAlertControllerStyle.alert)
        
        let callFunction1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
            
            (alert: UIAlertAction!) -> Void in
            
            
            print(sender.tag)
            let propertyId = (self.contentArray[sender.tag])["property_id"] as? Int ?? 0
            
            let url = "deleteWishlist"
            var param = Json()
            param.updateValue(self.userID, forKey: "user_id")
            param.updateValue(propertyId, forKey: "propertyid")
            
            Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
                print("deleted from wishlist")
                guard isSucceeded else { return }
                BaseThread.asyncMain {
                    if self.contentArray.count > sender.tag {
                        self.contentArray.remove(at: sender.tag)
                        self.contentTV.reloadData()
                    }
                }
            }
         
            
        })
        
        let callFunction2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        
        otherAlert.addAction(callFunction1)
        otherAlert.addAction(callFunction2)
        
        present(otherAlert, animated: true, completion: nil)
        
  
    }
    
    
    @IBAction func Back_Button(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any)
    {
        showSideMenu()
        //        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
        //        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    // Tab Actions
    @IBAction func tabHomeTapped(_ sender: UIButton)
    {
        
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton)
    {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton)
    {
        
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                 vc.segueType = .search
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton)
    {
        
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                 vc.segueType = .chat
                isLoadedTabSearchChatNotify = .chat

                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton)
    {
        
                let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                vc.segueType = .notification
                self.navigationController?.pushViewController(vc, animated: true)
    }
}

// TableView Delegates
extension KXWishListVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wishListCell") as! wishListCell
        
        let propertyModel = (contentArray[indexPath.row])["properties"] as? [String:Any] ?? [String:Any]()
        cell.propertyTitleLBL.text = propertyModel["property_name"] as? String ?? "No Title".uppercased()
        cell.propertyLocationLBL.text = propertyModel["address"] as? String ?? "No Address"
        let price = propertyModel["usd_price"] as? Double ?? 0.0
        cell.propertyPriceLBL.text = price.showPriceInDollar() //price.showPriceInKilo()
        cell.deleteBTN.tag = indexPath.row
        
        let PropertyImageArray = propertyModel["property_images_thumb"] as? [[String:Any]] ?? [[String:Any]]()
        
        if PropertyImageArray.count > 0
        {
            let PropertyImageAtZeroIndex = PropertyImageArray[0]["image"] as? String ?? ""
            let imageURL = URL(string: baseImageURL + PropertyImageAtZeroIndex)
            cell.propertyImage.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"),
                                           options: [.requestModifier(getImageAuthorization())],
                                           progressBlock: nil,
                                           completionHandler: nil)
        }
        else
        {
            cell.propertyImage.image = #imageLiteral(resourceName: "sampleProperty.png")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
        vc.propertyId = contentArray[indexPath.row]["property_id"] as! Int
      //  vc.propertyType = contentArray[indexPath.row]["property_type"] as! Int
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
