//
//  KXHistoryPendingRequestsController.swift
//  Koloxo
//
//  Created by Appzoc on 26/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXHistoryPendingRequestsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableRef: UITableView!
    
    var questionSource:[ReviewQuestionModel] = []
    
    var requestedUsersData:[RequestedUsers] = []
    var passingPropertyID:Int = 0
    
    @IBOutlet var notificationCountLBL: UILabel!
    @IBOutlet var unreadChatCountLBL: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpWebcall()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
   
    @IBAction func postReviewTapped(_ sender: UIButton) {
        let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXRateUserPopUpController") as! KXRateUserPopUpController
        vc.questionsSource = self.questionSource
       
        vc.passingPropertyID = self.passingPropertyID
        vc.toUserID = requestedUsersData[sender.tag].id
        vc.ratingType = .tenent
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func setUpWebcall(){
        Webservices.getMethodWith(url: "RequestedUsers", parameter: ["property_id":self.passingPropertyID,"history":1,"user_id": Credentials.shared.userId]) { (isComplete, json) in
            if isComplete{
                do{
                    if let trueData = json["Data"] as? [[String:Any]]{
                        let data = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                        let parsedData = try JSONDecoder().decode([RequestedUsers].self, from: data)
                        self.requestedUsersData = parsedData
                        self.tableRef.reloadData()
                        //print("Requested User Data Sample rental details",parsedData.map({$0.requestDetails.map({$0.rentalDetails})}))
                    }else{
                        print("Failed conversion")
                    }
                }catch{
                    print(error)
                }
            }

        }
    }
    
    //MARK: Table Delegate functions
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 142
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestedUsersData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXPendingRequestsTableCell") as! KXPendingRequestsTableCell
        cell.profileName.text = requestedUsersData[indexPath.row].name
        cell.countryLBL.text = requestedUsersData[indexPath.row].countryDetails.name
        
       
        if let periodData = requestedUsersData[indexPath.row].requestDetails[0].rentalDetails {
            if let inDate = periodData.movingIn.components(separatedBy: " ").first, let outDate = periodData.movingOut.components(separatedBy: " ").first {
                if  inDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy") != "", outDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy") != "" {
                    
                    cell.periodOfStayLBL.text = inDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy") + " to " + outDate.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                }else{
                    cell.periodOfStayLBL.text = "Not Available"
                }
                
            }else {
                cell.periodOfStayLBL.text = "Not Available"
            }
        }else {
            cell.periodOfStayLBL.text = "Not Available"
        }
            
        cell.profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(requestedUsersData[indexPath.row].image)"), placeholder: UIImage(named: "userPlaceholder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    //MARK: Tab Actions
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuTapped(_ sender: UIButton) {
        showSideMenu()
    }
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    struct RequestedUsers: Codable{
        let id:Int
        let name:String
        let image:String
        let countryDetails:CountryDetails
        let requestDetails:[PropertyRequest]
        
        private enum CodingKeys: String, CodingKey{
            case id,name,image,countryDetails = "country_detail", requestDetails = "property_request"
        }
        
        
        struct CountryDetails:Codable{
            let id:Int
            let name:String
        }
        
        struct PropertyRequest: Codable{
            let id:Int
            let rentalDetails:RentalDetails?
            
            private enum CodingKeys: String, CodingKey{
                case rentalDetails = "rental_details",id
            }
            
        }
        
        struct RentalDetails: Codable {
            let propertyID:Int
            let userID:Int
            let requestID:Int
            let movingIn:String
            let movingOut:String
            
            private enum CodingKeys: String, CodingKey{
                case propertyID = "property_id", userID = "user_id", requestID = "request_id", movingIn = "moving_in", movingOut = "moving_out"
            }
            
        }
    }
    
}



class KXPendingRequestsTableCell: UITableViewCell{
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var countryLBL: UILabel!
    @IBOutlet var periodOfStayLBL: UILabel!
    @IBOutlet var rateAndReviewBTNRef: UIButton!
    
    
}
