//
//  KXRateUserPopUpController.swift
//  Koloxo
//
//  Created by Appzoc on 26/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import FloatRatingView
enum RatingType: Int {
    case owner
    case tenent
    case property
}

class KXRateUserPopUpController: UIViewController, UITableViewDelegate, UITableViewDataSource, FloatRatingViewDelegate {
    
    @IBOutlet var tableRef: UITableView!
    @IBOutlet var ratingTableHeight: NSLayoutConstraint!
    
    var questionsSource:[ReviewQuestionModel] = []
    var commentData:String?
    var passingPropertyID = 0
    var toUserID = 0

    final var ratingType: RatingType = .property
    
    override func viewDidLoad() {
        super.viewDidLoad()
    //   print(questionsSource[indexPath.row].question)
        tableRef.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        // Do any additional setup after loading the view.
//        for var item in questionsSource{
//            item.rating = "0"
//        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(KXRateUserPopUpController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KXRateUserPopUpController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableRef.reloadData()
        print("befoer")
        print(self.ratingTableHeight.constant)
        //updateTableHeight()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0//keyboardSize.height
            }
        }
    }
    
    func updateTableHeight(){
        DispatchQueue.main.async {
            self.ratingTableHeight.constant = self.tableRef.contentSize.height
            print("after",self.ratingTableHeight.constant)

        }
        
    }
    
    //MARK: Float rating View Delegate
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        questionsSource[ratingView.tag].rating = "\(rating)"
    }
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //http://koloxo-homes.dev.webcastle.in/api/rateUser?property_id=413&from_user_id=13&to_user_id=203&comment=jkkjhkjh&ratings[0][id]=1&ratings[0][rating]=1
    
    //MARK: Post Data
    @IBAction func postDataTapped(_ sender: StandardButton) {
        guard validateEntry() else{
            showBanner(title: "", message: "Please enter a comment", style: .danger)
            return
        }
        var param = Json()
        var url = "rateProperty"
        switch ratingType {
        case .owner:
            url = "rateUser"
            param = ["property_id":"\(passingPropertyID)",
                "from_user_id":"\(Credentials.shared.userId)",
                "to_user_id":"\(toUserID)",
                "comment":"\(commentData!)"
            ]
            param["owner_type"] = 1

        case .tenent:
            url = "rateUser"

            param = ["property_id":"\(passingPropertyID)",
                "from_user_id":"\(Credentials.shared.userId)",
                "to_user_id":"\(toUserID)",
                "comment":"\(commentData!)"
            ]

            param["owner_type"] = 0

        case .property:
            param = ["property_id":"\(passingPropertyID)",
                "from_user_id":"\(Credentials.shared.userId)",
                "to_user_id":"\(toUserID)",
                "comment":"\(commentData!)"
            ]

            
        }
        
        
        for (index,object) in questionsSource.enumerated(){
            param.updateValue("\(object.id)", forKey: "ratings[\(index)][id]")
            param.updateValue("\(object.rating ?? "1")", forKey: "ratings[\(index)][rating]")
        }
        print("url",url)
        print(":Param:",param)
        Webservices.postMethod(url: url, parameter: param) { (isComplete, json) in
                if isComplete{
                    print("Success")
                    let message = json["Message"] as? String ?? "Rated Successfully"
                    showBanner(title: "", message: message, style: .success)
                }
            BaseThread.asyncMain {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func validateEntry()->Bool{
        if let _ = commentData{
            return true
        }else{
            return false
        }
    }
    
    //MARK: TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionsSource.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let starCell = tableView.dequeueReusableCell(withIdentifier: "KXRateUserPopupStarCell") as! KXRateUserPopupStarCell
        let textViewCell = tableView.dequeueReusableCell(withIdentifier: "KXRateUserPopupTextViewCell") as! KXRateUserPopupTextViewCell
        
        if indexPath.row < questionsSource.count{
            starCell.question.text = self.questionsSource[indexPath.row].question
          //  print(starCell.question.text)
            starCell.ratingView.tag = indexPath.row
            starCell.ratingView.delegate = self
            return starCell
        }else{
            return textViewCell
        }
        
//        return starCell
    }
    
    
    //MARK: Tab Actions
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func menuBTNTapped(_ sender: UIButton) {
        showSideMenu()
    }
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

}

extension KXRateUserPopUpController: UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty{
            textView.text = "Type your answer"
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.commentData = textView.text
    }
}


class KXRateUserPopupStarCell: UITableViewCell{
    @IBOutlet var question: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    
}

class KXRateUserPopupTextViewCell: UITableViewCell{
    @IBOutlet var question: UILabel!
    @IBOutlet var textViewReply: UITextView!
    
    
}
