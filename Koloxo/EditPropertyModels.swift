//
//  EditPropertyModels.swift
//  Koloxo
//
//  Created by Appzoc on 07/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

struct PropertyDetailModel:Codable {
    
    let propertyType:Int //Buy or Sell
    let propertyName:String?
    let price:Int
    let floorSize:String
    let requiredDeposit:String
    let currencyID:Int
    let priceNegotiable:Int
    let reputationRequired:Int
    let buildingType:BuildingType
    let buildingAge:Int
    let elevator:Int
    let floorNumber:Int
    let bedroomCount:Int
    let bathroomCount:Int
    let propertyDescription:String
    let stepsCompleted:Int
    let brochure:[Brochure]
    let deed:[TitleDeed]
    let dateMoving:String?
    
    /// add
    let energy_type: String?
    let architecture_type: String?
    let water_heating: String?
    
    private enum CodingKeys : String, CodingKey {
        case propertyName = "property_name",price,requiredDeposit = "deposite", currencyID = "currency_id", priceNegotiable = "pirce_negotiable", reputationRequired = "reputation", buildingType = "building_type", propertyType = "property_type", buildingAge = "age_building", elevator, floorNumber = "floor_no", bedroomCount = "bedroom_count", bathroomCount = "bathroom_count", propertyDescription = "property_description", stepsCompleted = "states_completed", brochure = "property_brochure", deed = "property_deed", floorSize = "floor_size", dateMoving = "date_moving",energy_type,architecture_type,water_heating
        
    }
    
    
    struct Brochure:Codable{
        let id:Int
        let brochure:String
        let file_name_bro: String

    }
    
    struct TitleDeed:Codable {
        let id:Int
        let titleDeed:String
        let file_name_deed: String

    }
    
    struct BuildingType:Codable{
        let id:Int
        let data:String
    }
    
    struct propertyBathroom:Codable{
        let id:Int
        let data:String
    }
    
    struct propertyBedroom:Codable {
        let id:Int
        let data:String
    }
}



struct PropertyFloorPlanModel:Codable{
    let propertyFacility:[FacilitySelected]
    let floorPlan:[FloorPlanData]
    
    struct FacilitySelected:Codable {
        let facilityID:Int
        private enum CodingKeys: String, CodingKey{
            case facilityID = "facility_id"
        }
    }
    
    struct FloorPlanData:Codable {
        let id:Int
        let floorPlan:String
        private enum CodingKeys: String, CodingKey{
            case floorPlan = "floor_plans",id
        }
    }
    
    private enum CodingKeys: String, CodingKey{
        case propertyFacility = "property_facility", floorPlan = "floor_plan"
    }
}

struct PropertyFurnitureDetails:Codable {
    let id:Int
    let category:String
    let furniture:[Furniture]
    
    struct Furniture:Codable {
        let id:Int
        let title:String
        let description:String
    }
    
    private enum CodingKeys: String, CodingKey{
        case id, category, furniture = "property_furniture"
    }
}



struct PropertyPlaceModel: Codable {
    let state:String
    let country:Int
    let city:String
    let address:String
    let latitude:String
    let longitude:String
    let building_id:Int?
}


struct PropertyImagesModel:Codable{
    let id:Int
    let propertyName:String
    let propertyImages:[Images]
    
    private enum CodingKeys: String, CodingKey {
        case propertyName = "property_name", id, propertyImages = "property_images"
    }
    
    struct Images:Codable {
        let id:Int
        let image:String
        let type_id:Int
    }
}



