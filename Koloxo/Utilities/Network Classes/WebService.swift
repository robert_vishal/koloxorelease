//
//  Webservices.swift
//  Exhibiz
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import Alamofire
import NotificationBannerSwift
import FLAnimatedImage

let baseURL = "http://koloxo-homes.dev.webcastle.in/api/"
let baseImageURL = "http://koloxo-homes.dev.webcastle.in/"
let baseDocURL = "http://koloxo-homes.dev.webcastle.in/"


// LIVE URL
//let baseURL = "http://app.koloxohome.com/api/"
//let baseImageURL = "http://app.koloxohome.com/"
//let baseDocURL = "http://app.koloxohome.com/"


///Local Server URLs - http://192.168.1.209/koloxo/
//let baseURL = "http://192.168.1.209/koloxo/api/"
//let baseImageURL = "http://192.168.1.209/koloxo/"
//let baseDocURL = "http://192.168.1.209/koloxo/"


let authUserName = "dev"
let authPassword = "dev"


final class Webservices
{
    
    private init(){}
    class func getMethod(url:String, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url)")
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                activity.stop()
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }

                    CompletionHandler(false, ["":""])
                    break
                }
        }
    }
    
    class func getMethodWithoutLoading(url:String,CompletionHandler:@escaping ((Bool,[String:Any])->())){
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        print("\(baseURL)\(url)")
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }

                case .failure(let error):
                    
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    CompletionHandler(false, ["":""])
                    break
                }
        }
    }
    
    // Parameterised GET Method
    class func getMethodWith(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url)")
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .get,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    activity.stop()
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        debugPrint(result)
                        //print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    break
                }
        }
    }
    
    
    //Mark:- POST Methods
    class func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()

                    CompletionHandler(false, ["":""])
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                }
        }
    }
    
    class func postMethodForPush(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
//        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
//        let base64Credentials = credentialData.base64EncodedString(options: [])
//        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        let headers = ["Accept": "application/json","Content-Type":"application/json"]
        Alamofire.request("\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                        }
                    }
                    
                    break
                    
                case .failure(let error):

                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    CompletionHandler(false, ["":""])
                }
        }
    }
    
    class func postMethodWithoutAlert(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    CompletionHandler(false, ["":""])
                }
        }
    }
    
    
    // Mark:- Muiltipart data web call
    
    class func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , timeOutSeconds: TimeInterval = 300, CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityWithProgress()
        activity.start()
        print("\(baseURL)\(url)")
        
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        let manager = Alamofire.SessionManager()
        manager.session.configuration.timeoutIntervalForRequest = timeOutSeconds

        manager.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(baseURL)\(url)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.uploadProgress{
                        progress in
                        activity.updateWith(progress: progress)
                        print("Progress is ",progress.fractionCompleted)
                    }

                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            activity.stop()
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["ErrorCode"] as? String ?? "1"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                showErrorBanner(result: result)
                            }
                        }
                    })
                    
                case .failure(let error):
                    activity.stop()
                    
                    if error._code == NSURLErrorTimedOut {
                        
                        showBanner(title: "Upload Time Out", message: error.localizedDescription)
                        
                    }else {
                        showBanner(title: "Network Error", message: "Server Not Available...")
                    }

                    CompletionHandler(false, ["":""])

                }
        }
        )
        
    }
    
    
    class func postMethodMultiPartData(url:String, parameter:[String:Any], dataParameter: [String:Data] ,timeOutSeconds: TimeInterval = 300, CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityWithProgress()//ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url)")

        
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = timeOutSeconds

        
        manager.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                //"application/pdf"
                //                if let brochure = dataParameter["brochure"], let deed = dataParameter["deed"]{
                //                    multipartFormData.append(brochure, withName: "brochure", fileName: "brochure.pdf", mimeType: "text/plain")
                //                    multipartFormData.append(deed, withName: "deed", fileName: "deed.pdf", mimeType: "text/plain")
                //                }
                for (key, value) in dataParameter{
                    //multipartFormData.append(value, withName: key, mimeType: "text/plain")
                    multipartFormData.append(value, withName: "\(key)", fileName: "\(key).pdf", mimeType: "text/plain")
                }
                //multipartFormData.append(pdfData, withName: "pdfDocuments", fileName: namePDF, mimeType:"application/pdf")
                
        },
                         to: "\(baseURL)\(url)",
            method:.post,
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    print("Resopnse",upload.response as Any)
                    
                    upload.uploadProgress{
                        progress in
                        activity.updateWith(progress: progress)
                        print("Progress is ",progress.fractionCompleted)
                    }

                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        activity.stop()
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["ErrorCode"] as? String ?? "1"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                showErrorBanner(result: result)
                            }
                        }else {
                            //let result = response.result.value as! [String:Any]
                            showBanner(title: "Network Error", message: "Server Not Available...")
                            CompletionHandler(false, ["":""])

                        }
                    }
                    )
                    
                case .failure(let error):
                    activity.stop()
                    
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "Upload Size Exceeded", message: error.localizedDescription)
                    }else {
                        showBanner(title: "Network Error", message: "Server Not Available...")
                    }
                    
                    CompletionHandler(false, ["":""])


                }
        }
        )
        
    }
    
    //MARK:- Progress Bar Implemented
    class func postMethodMultiPartImage(url:String, parameter:[String:Any], dataParameter: [String:Data] , timeOutSeconds: TimeInterval = 300, CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityWithProgress()//ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url)")
        
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = timeOutSeconds

        
        manager.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                for (key, value) in dataParameter{
                    //multipartFormData.append(value, withName: key, mimeType: "text/plain")
                    multipartFormData.append(value, withName: "\(key)", fileName: "\(key).jpg", mimeType: "image/jpg")
                }
                
        },
                         to: "\(baseURL)\(url)",
            method:.post,
            headers : headers,
            encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress{
                        progress in
                        activity.updateWith(progress: progress)
                        print("Progress is ",progress.fractionCompleted)
                    }
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        activity.stop()
                       
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["ErrorCode"] as? String ?? "1"
                            //print(Errorcode)
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["ErrorCode":"\(Errorcode)"])
                                showErrorBanner(result: result)
                            }
                        }else{
                            showBanner(title: "Upload Time Out", message: "Please reduce uploadig images count or size")
                        }
                    }
                    )
                    
                case .failure(let error):
                    activity.stop()

                    if error._code == NSURLErrorTimedOut {
                      
                        showBanner(title: "Upload Time Out", message: error.localizedDescription)
                        
                    }else {
                        showBanner(title: "Network Error", message: "Server Not Available...")
                    }
                    
                    CompletionHandler(false, ["":""])

                    

                }
        }
        )
        
    }
    
    
    class func getMethodWithoutLoading(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        print("\(baseURL)\(url)")
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url)",
            method: .get,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        debugPrint(result)
                        //print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):

                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    CompletionHandler(false, ["":""])
                    //  showBanner(title: "Network Error", message: "Server Not Available...")
                    break
                }
        }
    }
    
}

class MapWebService{
    class func getMethod(url:String,address:String, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        
        if !NetworkReachabilityManager()!.isReachable {
            showBanner(title: "Network Error", message: "No internet connectivity")
            CompletionHandler(false, [:])
        }
        
        let activity = ActivityIndicator()
        activity.start()
        let credentialData = "\(authUserName):\(authPassword)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(url)",
            method: .get,
            parameters: ["address":"\(address)","key":"AIzaSyAfnrRFEF-6n8dQfVdHZ8fIkTfcFAc2byk"],
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["ErrorCode"] as? String ?? "1"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            //showBanner(title: "", message: Message)
                            showErrorBanner(result: result)
                        }
                    }
                    
                    break
                    
                case .failure(let error):
                    activity.stop()
                    if error._code == NSURLErrorTimedOut {
                        showBanner(title: "", message: error.localizedDescription)
                    }
                    CompletionHandler(false, ["":""])
                    break
                }
        }
    }
}

class ActivityIndicator
{
    
    var view = UIView(frame: UIScreen.main.bounds)
    
    func start()
    {
        let gifImageUrl = Bundle.main.path(forResource: "loader-2", ofType: "gif")
        let gifData = try? Data(contentsOf: URL(fileURLWithPath: gifImageUrl!))
        let animGIFImage = FLAnimatedImage(animatedGIFData: gifData)
        let gifContainer = FLAnimatedImageView(frame: CGRect(x: (view.bounds.maxX/2)-40,
                                                     y: (view.bounds.maxY/2)-40,
                                                     width: 80, height: 80))
        gifContainer.contentMode = .scaleAspectFit
        gifContainer.animatedImage = animGIFImage
        self.view.addSubview(gifContainer)
        self.view.backgroundColor = UIColor.clear
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.addSubview(self.view)
        }
        
//        if activityView == nil
//        { activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
//            activityView.frame = CGRect(x: (view.bounds.maxX/2)-20,
//                                        y: (view.bounds.maxY/2)-20,
//                                        width: 40, height: 40)
//            self.view.addSubview(activityView)
//            self.view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
//            UIApplication.shared.keyWindow?.addSubview(self.view)
//            activityView.startAnimating()
//        }
//        else if activityView.isAnimating == true
//        {
//        }
//        else
//        { UIApplication.shared.keyWindow?.addSubview(self.view)
//            activityView.startAnimating()
//        }
    }
    
    func stop()
    {
        DispatchQueue.main.async {
            self.view.removeFromSuperview()
        }
        
//        DispatchQueue.main.async {
//        if self.activityView == nil || self.activityView.isAnimating == false
//            {
//            }
//            else
//        { self.activityView.stopAnimating()
//                self.view.removeFromSuperview()
//            }
//        }
    }
}

class Credentials {
    var emailId = ""
    var deviceTocken = ""
    var deviceType = "iOS"
    var userId = ""
    var firstName = ""
    var lastName = ""
    var mobile = ""
    var country = BaseMemmory.read(forKey: "location") as? String ?? ""
    var profileImage = ""
    var reputed = (BaseMemmory.read(forKey: "reputed") as? Int ?? 0).description
    
    static let shared = Credentials()
    private init() {
//        country = BaseMemmory.read(forKey: "location") as! String
    }
    
    
}


