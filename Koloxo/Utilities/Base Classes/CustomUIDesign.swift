//
//  gradientColor.swift
//  Koloxo
//
//  Created by Janaina on 13/03/18.
//  Copyright © 2018 Janaina. All rights reserved.
//

import Foundation
import UIKit


 final class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear
    
    override func draw(_ rect: CGRect) {
      //  layer.sublayers?.first?.removeFromSuperlayer()
        
       let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
     //   gradient.frame = CGRect(origin: CGPoint.zero, size: self.bounds.size)
      //  gradient.colors = [startColor.cgColor, endColor.cgColor]
      //  layer.insertSublayer(gradient, at: 0)
    }
   
}


 final class GradientButton: UIButton {
    
    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear
    
    override func draw(_ rect: CGRect) {
      
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
      //  gradient.startPoint = CGPoint(x: 0.4, y: 0.7)
      //  gradient.endPoint = CGPoint(x: 0.8, y: 0.6)
      //  gradient.colors = [startColor.cgColor, endColor.cgColor]
        layer.insertSublayer(gradient, at: 0)
    }
}

 class RoundedImage: UIImageView
{
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
   //
//    @IBInspectable var rounded: Bool = false {
//        didSet {
//            updateCornerRadius()
//        }
//    }
//
//    @IBInspectable var cornerRadius: CGFloat = 0.1 {
//        didSet {
//            updateCornerRadius()
//        }
//    }
    
    func updateCornerRadius() {
        layer.cornerRadius = frame.size.height/2
        layer.masksToBounds = true
    }
}

 class RoundedView: UIView
{
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? cornerRadius : 0
        layer.masksToBounds = rounded ? true : false
    }
}


 class PopUpView: UIView
{
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }

    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = frame.size.height/30
        layer.masksToBounds = true
    }
}


 class RoundedButton: UIButton
{
    var index: IndexPath?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
//    @IBInspectable var rounded: Bool = false {
//        didSet {
//            updateCornerRadius()
//        }
//    }
//
//    @IBInspectable var cornerRadius: CGFloat = 0.1 {
//        didSet {
//            updateCornerRadius()
//        }
//    }
    
    func updateCornerRadius() {
        layer.cornerRadius = frame.size.height/2
        layer.masksToBounds = true
    }
    
    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear

    override func draw(_ rect: CGRect) {
      
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.8, y: 0.5)
     //   gradient.colors = [startColor.cgColor, endColor.cgColor]
     //   layer.insertSublayer(gradient, at: 0)
        
    }
    
    
}





