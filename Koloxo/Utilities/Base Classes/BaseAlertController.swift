//
//  BaseAlertController.swift
//  Follow
//
//  Created by Appzoc on 10/01/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit

public class BaseAlert {
    
    fileprivate init(){}
    
    // show normal alert with "OK" button.

    class func alert(withTitle title: String, message: String, tintColor: UIColor, andPresentOn viewContoller: UIViewController)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.view.tintColor = tintColor
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    // show automatically dismissing alert.
    
    class func autoDismissAlert(withTitle: String, message: String, dismissTime: Double,  andPresentOn viewController: UIViewController){
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
            viewController.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dismissTime){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    // show alert with controll go back to the view controller which it is presented on
    
    class func completionAlert(withTitle title: String, message: String, OkTitle:String = "Ok", CancelTitle:String = "Cancel" ,tintColor: UIColor, andPresentOn viewContoller: UIViewController, completion: @escaping () -> Void)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.view.tintColor = tintColor
            
            alert.addAction(UIAlertAction(title: CancelTitle, style: UIAlertActionStyle.default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: OkTitle, style: UIAlertActionStyle.default, handler: { (action) in
                completion()
            }))
            
            
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }
}



