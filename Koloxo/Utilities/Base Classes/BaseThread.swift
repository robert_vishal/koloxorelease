//
//  BaseThread.swift
//  Follow
//
//  Created by Appzoc on 19/01/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation

typealias ThreadHandler = () -> Void

class BaseThread{
    
    private init(){}
    
    static func asyncMain(_ completion : @escaping ThreadHandler) -> Void{
        if Thread.isMainThread{
            completion()
        }else{
            DispatchQueue.main.async(execute: { () -> Void in
                completion()
            })
        }
    }
    
    static func asyncMainDelay(seconds: Double, _ completion : @escaping ThreadHandler) -> Void{
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
            completion()
        })
    }
    
    
    static func syncMain(_ completion: @escaping ThreadHandler) -> Void{
        if Thread.isMainThread{
            completion()
        }else{
            DispatchQueue.main.sync(execute: { () -> Void in
                completion()
            })
        }
    }

    
    static func asyncConcurrent(withIdentifier: String, _ completion: @escaping ThreadHandler) -> Void{
        let concurrentQueue = DispatchQueue(label: withIdentifier , attributes: .concurrent)
        concurrentQueue.async {
            completion()
        }
    }
    
    
    static func syncConcurrent(withIdentifier: String, _ completion: @escaping ThreadHandler) -> Void{
        let concurrentQueue = DispatchQueue(label: withIdentifier , attributes: .concurrent)
        concurrentQueue.sync {
            completion()
        }
    }


//    class func async(group: DispatchGroup? = nil, qos: DispatchQoS? = nil, flags: DispatchWorkItemFlags? = nil, execute work: @escaping @convention(block) () -> Swift.Void) {
////
//    }
//    func async(_ work: @escaping @convention(block) () -> Swift.Void) {
//
//    }

        static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
            DispatchQueue.global(qos: .background).async {
                background?()
                if let completion = completion {
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                        completion()
                    })
                }
            }
        }

    

}



