//
//  DateFormatting.swift
//  Koloxo
//
//  Created by Appzoc on 05/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import Foundation

class DateFormatting{
    
    static func getDifferenceBetweenTwoDates(date1:Date, date2:Date) -> String{
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day,.month,.hour,.minute,.second], from: date1, to: date2)
        if let monthCount = components.month, monthCount > 0{
            return monthCount.description + " Months ago"
        }else if let dayCount = components.day, dayCount > 0{
            return dayCount.description + " Days ago"
        }else if let hourCount = components.hour, hourCount > 0{
            return hourCount.description + " Hours ago"
        }else if let minuteCount = components.minute, minuteCount > 0{
            return minuteCount.description + " Minutes ago"
        }else if let secondCount = components.second {
            return secondCount.description + " Seconds ago"
        }else {
            return ""
        }
    }
    
}
