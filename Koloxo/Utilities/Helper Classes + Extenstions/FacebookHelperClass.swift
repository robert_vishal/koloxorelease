/*
 https://developers.facebook.com/
 Log in
 Add App
 Dont forget to Make Your Project.(App Review - Your app is currently live and available to the public)
 */

/*  Pod Install
 pod 'FacebookCore'
 pod 'FacebookLogin'
 pod 'FacebookShare'
 */

/*  Appdelegate.swift
 
 import FBSDKLoginKit
 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
 {
 return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
 }
 
 
 func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
 {
 return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
 }
 
 
 func applicationWillResignActive(_ application: UIApplication)
 {
 FBSDKAppEvents.activateApp()
 }
 */

import Foundation
import UIKit
import FBSDKLoginKit
import FacebookLogin

public struct facebookDictionary {
    public var name : String = ""
    public var firstName: String = ""
    public var lastName: String = ""
    public var imagedata : Any? = nil
    public var facebookId : String = ""
    public var email : String = ""
}



public class Facebook
{
    static let facebookLoginManager = LoginManager()
    static var profilename = String()
    
    class func LoginRequest(viewcontroller: UIViewController, completion : @escaping(Bool,facebookDictionary) -> Void)
    {
        var imageData = Data()
        var facebookId = String()
        var email = String()
        var firstName = String()
        var lastName = String()
        
        facebookLoginManager.logIn(readPermissions: [.publicProfile, .email, .userHometown], viewController: viewcontroller, completion: {
            (LoginResult) in
            
            switch LoginResult{
            case .success(_, _, _):
                
                if((FBSDKAccessToken.current()) != nil)
                {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, first_name, last_name, location, hometown "]).start(completionHandler: { (connection, result, error) -> Void in
                        if (error == nil)
                        {
                            guard let userInfo = result as? [String: Any] else { return } //handle the error
                            if let profileName = userInfo["name"]
                            {
                                profilename = (profileName as? String)!
                            }
                            if let useriD = userInfo["id"]
                            {
                                facebookId = (useriD as? String)!
                            }
                            if let firstname = userInfo["first_name"]
                            {
                                firstName = (firstname as? String)!
                            }
                            if let lasttname = userInfo["last_name"]
                            {
                                lastName = (lasttname as? String)!
                            }
                            if let useremail = userInfo["email"]
                            {
                                email = (useremail as? String)!
                            }
                            if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String
                            {
                                //Download image from imageURL
                                if let url = NSURL(string: imageURL) {
                                    if let data = NSData(contentsOf: url as URL) {
                                        imageData = data as Data
                                    }
                                }
                            }
                            
                            
                            let dict = facebookDictionary(name: profilename,firstName: firstName, lastName: lastName, imagedata: imageData, facebookId: facebookId, email: email)
                            completion(true, dict)
                        }
                    })
                }
                
            case .failed(let ErrorRecvd):
                print("error",ErrorRecvd)
                let dict = facebookDictionary(name: "", firstName: "", lastName: "", imagedata: nil, facebookId: "", email: "")
                completion(false, dict)
                
            case .cancelled:
                print("cancelled")
                let dict = facebookDictionary(name: "", firstName: "", lastName: "", imagedata: nil, facebookId: "", email: "")
                completion(false, dict)
            }
            
        })
        
        
    }
    
    
    class func LogoutRequest()
    {
        facebookLoginManager.logOut()
        facebookLoginManager.loginBehavior = .web
//        facebookLoginManager.loginBehavior = 
    }
    
    
    
}





// Your ViewController
/*
 @IBAction func LoginButton(_ sender: Any)
 {
 Facebook.LoginRequest(viewcontroller: self, completion: { (isFinished, dic) in
 if isFinished
 {
 print(dic)
 self.profileLabel.text = dic.name
 self.imageview.image = UIImage(data: dic.imagedata as! Data)
 }
 
 })
 
 }
 
 @IBAction func LogoutButton(_ sender: Any)
 {
 Facebook.LogoutRequest()
 imageview.image = nil
 profileLabel.text = ""
 }
 
 */

