//
//  KXAddPropertyImagesCell.swift
//  Koloxo
//
//  Created by Appzoc on 28/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXAddPropertyImagesCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    @IBOutlet weak var collectionHeightRef: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var addImagesBTNRef: StandardButton!
    
    var imageData:[UIImage]? = []{
        didSet{
            
                self.collectionRef.reloadData()
        }
    }
    
    override func awakeFromNib() {
        collectionRef.delegate = self
        collectionRef.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let data = imageData{
            return data.count
        }else{
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXAddedImageCollectionCell", for: indexPath) as! KXAddedImageCollectionCell
        cell.deleteImageBTNRef.indexPath = IndexPath(row: indexPath.row, section: addImagesBTNRef.indexPath!.section)
        if let data = imageData{
            cell.propertyImage.image = data[indexPath.row]
        }
        return cell
    }

}
