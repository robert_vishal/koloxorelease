//
//  KXPropertyDetailsVC.swift
//  KoloxoDetail
//
//  Created by Appzoc on 27/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import SideMenu

class KXPropertyDetailsVC: UIViewController {
    
    @IBOutlet var sliderImageCountLBL: UILabel!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var ratingCountLBL: UILabel!
    @IBOutlet var availabilityLBL: UILabel!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var bedroomCountLBL: UILabel!
    @IBOutlet var bathroomCountLBL: UILabel!
    @IBOutlet var parkingCountLBL: UILabel!
    @IBOutlet var aboutPropertyLBL: UILabel!
    @IBOutlet var aboutPropertyMoreBTN: UIButton!
    @IBOutlet var propertyAgeLBL: UILabel!
    @IBOutlet var propertyAddressLBL: UILabel!
    @IBOutlet var facilitiesCV: UICollectionView!
    @IBOutlet var facilitiesMoreView: UIView!
    @IBOutlet var spacesCV: UICollectionView!
    @IBOutlet var spacesMoreView: UIView!
    @IBOutlet var furnituresTV: UITableView!
    @IBOutlet var newsfeedsCV: UICollectionView!
    @IBOutlet var newsfeedsPC: UIPageControl!
    @IBOutlet var reviewTimeLBL: UILabel!
    @IBOutlet var ratingProfileImage: BaseImageView!
    @IBOutlet var reviewProfileNameLBL: UILabel!
    @IBOutlet var reviewProfileLocationLBL: UILabel!
    @IBOutlet var reviewRatingCountLBL: UILabel!
    @IBOutlet var reviewTitleLBL: UILabel!
    @IBOutlet var reviewDescriptionLBL: UILabel!
    @IBOutlet var BasehideView: BaseView!
    
    @IBOutlet var facilitiesMoreLBL: UILabel!
    @IBOutlet var facilitiesMoreImage: UIImageView!
    
    @IBOutlet var spacesMoreImage: UIImageView!
    @IBOutlet var spacesMoreLBL: UILabel!
    @IBOutlet var heightFacilitiesCV: NSLayoutConstraint!
    @IBOutlet var heightSpacesCV: NSLayoutConstraint!
    @IBOutlet var heightFurnitureTV: NSLayoutConstraint!
    
    fileprivate var isAbovePropertyMoreTapped: Bool = false
    fileprivate var isFacilitiesMoreTapped: Bool = false
    fileprivate var isSpacesMoreTapped: Bool = false
    fileprivate var furnitureDataSource: [String] = ["Bedroom","Kitchen","Guest Room","Balcony","Other Furniture"]
    fileprivate var facilitiesDataSource: [String] = ["Tennis","Fire Extinguisher","Gym"]
    fileprivate var spacesDataSource: [String] = ["Bedroom","Kitchen","Guest Room","Balcony"]
    fileprivate var newsFeedCount = 3
    private var deselectedCell:KXPropertyDetailsFurnitureTVC?
    private var isRotatedArrowIMG: Bool = false
    private var selectedIndexPath: IndexPath?
    private var isExpandedCell: Bool = false
    
    fileprivate var newsfeedTimer = Timer()
    fileprivate var currentIndexPath = IndexPath(item: 0, section: 0)
    
    
    let aboutMoreBTNAttributes : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Medium", size: 12) as Any,
        NSAttributedStringKey.foregroundColor : UIColor.blue,
        NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        newsfeedTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(updateNewsfeed), userInfo: nil, repeats: true)
        newsfeedsPC.currentPage = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.furnituresTV.layoutIfNeeded()
        self.heightFurnitureTV.constant = self.furnituresTV.contentSize.height
        heightFacilitiesCV.constant = self.facilitiesCV.contentSize.height
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        newsfeedTimer.invalidate()
    }
    
    // Actions
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
    }
    
    @IBAction func favouriteTapped(_ sender: UIButton) {
    }
    
    @IBAction func slideMenuTapped(_ sender: UIButton) {
         let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    @IBAction func sliderNextTapped(_ sender: UIButton) {
    }
    @IBAction func sliderPreviousTapped(_ sender: UIButton) {
    }
    @IBAction func callAssistanceTapped(_ sender: UIButton) {
    }
    
    @IBAction func moreAboutPropertyTapped(_ sender: UIButton) {
        
        if isAbovePropertyMoreTapped {
            aboutPropertyLBL.numberOfLines = 3
            isAbovePropertyMoreTapped = false
            let attributeString = NSMutableAttributedString(string: "More", attributes: aboutMoreBTNAttributes)
            aboutPropertyMoreBTN.setAttributedTitle(attributeString, for: .normal)
        }else{
            aboutPropertyLBL.numberOfLines = 0
            isAbovePropertyMoreTapped = true
            let attributeString = NSMutableAttributedString(string: "Less", attributes: aboutMoreBTNAttributes)
            aboutPropertyMoreBTN.setAttributedTitle(attributeString, for: .normal)
        }
        
    }
    
    @IBAction func facilitiesMoreTapped(_ sender: UIButton) {
        if isFacilitiesMoreTapped {
            facilitiesMoreLBL.text = "More"
            heightFacilitiesCV.constant = facilitiesCV.contentSize.height
            isFacilitiesMoreTapped = false
            facilitiesMoreImage.transform = CGAffineTransform.identity
        }else{
            facilitiesMoreLBL.text = "Less"
            heightFacilitiesCV.constant = facilitiesCV.contentSize.height
            isFacilitiesMoreTapped = true
            facilitiesMoreImage.transform = facilitiesMoreImage.transform.rotated(by: .pi)
        }
    }
    @IBAction func spacesMoreTapped(_ sender: UIButton) {
        if isSpacesMoreTapped {
            spacesMoreLBL.text = "More"
            heightSpacesCV.constant = 120
            isSpacesMoreTapped = false
            spacesMoreImage.transform = CGAffineTransform.identity
        }else{
            spacesMoreLBL.text = "Less"
            heightSpacesCV.constant = 120
            
            isSpacesMoreTapped = true
            spacesMoreImage.transform = facilitiesMoreImage.transform.rotated(by: .pi)
            
        }
    }
    
    @IBAction func expandCollapseTVTapped(_ sender: BaseButton) {
        print("expandsss")
        let indexPath = sender.indexPath!
        guard let selectedCell = furnituresTV.cellForRow(at: indexPath) as? KXPropertyDetailsFurnitureTVC else { return }
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
        //updateContent(of: selectedCell, byIndexPath: selectedIndexPath!)
        furnituresTV.beginUpdates()
        self.rotateArrowIMG(of: selectedCell)
        furnituresTV.endUpdates()
        self.heightFurnitureTV.constant = self.furnituresTV.contentSize.height
    }
    
    private func rotateArrowIMG(of selectedCell: KXPropertyDetailsFurnitureTVC){
        let towardsDown = CGFloat(Double.pi/2)
        let towardsRight = CGFloat(-Double.pi/2)
        
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                if self.deselectedCell == nil {
                    /// tapping on the cell first time
                    selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                    self.isRotatedArrowIMG = true
                }else if self.deselectedCell == selectedCell {
                    /// tapping on already selected cell
                    if self.isRotatedArrowIMG {
                        selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsRight)
                        self.isRotatedArrowIMG = false
                    }else{
                        selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                        self.isRotatedArrowIMG = true
                    }
                }else{
                    /// tapping on other cell not already selected cell
                    selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                    guard let dCell = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
                    if self.isRotatedArrowIMG {
                        dCell.arrowImage.transform = dCell.arrowImage.transform.rotated(by: towardsRight)
                    }
                    self.isRotatedArrowIMG = true
                }
                self.deselectedCell = selectedCell
            })
        }
    }
    
}


// table view data source
extension KXPropertyDetailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return furnitureDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
        return selectedIndexPath == indexPath ? (isExpandedCell ? 253 :  50) :  50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KXPropertyDetailsFurnitureTVC", for: indexPath) as? KXPropertyDetailsFurnitureTVC else { return UITableViewCell() }
        cell.expandCollapseBTN.indexPath = indexPath
        cell.furnituresCV.tag = indexPath.item
        cell.furnituresCV.reloadData()
        cell.furnituesPC.currentPage = 0
        return cell
    }
    
}

// table view delegate
extension KXPropertyDetailsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectedTableCell")
    }
}

// collection view data source
extension KXPropertyDetailsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == facilitiesCV {
            return facilitiesDataSource.count
        }else if collectionView ==  spacesCV {
            return spacesDataSource.count
        }else if collectionView == newsfeedsCV {
            return newsFeedCount
        }else {
            return 4
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == facilitiesCV {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsFacilitiesCVC", for: indexPath) as? KXPropertyDetailsFacilitiesCVC else { return UICollectionViewCell() }
            cell.titleLBL.text = facilitiesDataSource[indexPath.item]
            
            return cell
            
        }else if collectionView == spacesCV {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsSpacesCVC", for: indexPath) as? KXPropertyDetailsSpacesCVC else { return UICollectionViewCell() }
            
            cell.spaceTitleLBL.text = spacesDataSource[indexPath.item]
            
            return cell
            
        }else if collectionView == newsfeedsCV {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsNewsFeedsCVC", for: indexPath) as? KXPropertyDetailsNewsFeedsCVC else { return UICollectionViewCell() }
            
            return cell
            
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsFurnituresCVC", for: indexPath) as? KXPropertyDetailsFurnituresCVC else { return UICollectionViewCell() }
            cell.tag = indexPath.item
            return cell
            
        }
        
    }
    
    
}

// collection view delegate
extension KXPropertyDetailsVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == facilitiesCV {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 3
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            let height =  55
            
            return CGSize(width: size, height: height)
            
        }else if collectionView == spacesCV {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 2
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            let height = 120
            
            return CGSize(width: size, height: height)
            
        }else{
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == newsfeedsCV {
            var visibleRect = CGRect()
            visibleRect.origin = newsfeedsCV.contentOffset
            visibleRect.size = newsfeedsCV.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = newsfeedsCV.indexPathForItem(at: visiblePoint) else { return }
            currentIndexPath = indexPath
            newsfeedsPC.currentPage = currentIndexPath.item
        }else{
            print("Tag",scrollView.tag)
            guard let _ = scrollView as? UICollectionView else { return }
            guard let cell = furnituresTV.cellForRow(at: IndexPath(row: scrollView.tag, section: 0)) as? KXPropertyDetailsFurnitureTVC else { return }
            var visibleRect = CGRect()
            visibleRect.origin = cell.furnituresCV.contentOffset
            visibleRect.size = cell.furnituresCV.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = cell.furnituresCV.indexPathForItem(at: visiblePoint) else { return }
            cell.furnituesPC.currentPage = indexPath.item
            
        }
    }
    
    @objc func updateNewsfeed(){
        guard newsFeedCount > 0 else { return }
        if newsFeedCount > currentIndexPath.item + 1{
            let nextIndexpath = IndexPath(item: currentIndexPath.item + 1, section: 0)
            newsfeedsCV.scrollToItem(at: nextIndexpath, at: .centeredHorizontally, animated: true)
            currentIndexPath = nextIndexpath
            newsfeedsPC.currentPage = currentIndexPath.item
        }else{
            currentIndexPath = IndexPath(item: 0, section: 0)
            newsfeedsCV.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: true)
            newsfeedsPC.currentPage = 0
        }
    }
    
    
}


