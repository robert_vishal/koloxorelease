//
//  KXChatMemberTableViewCell.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 09/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXChatMemberTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chatImage: BaseImageView!
    
    @IBOutlet weak var profileName: UILabel!
    
    @IBOutlet weak var lastMessage: UILabel!
    
    @IBOutlet weak var lastSeen: UILabel!
    
    @IBOutlet weak var unreadCount: UILabel!
    
    
    @IBOutlet var TimeView: StandardView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
