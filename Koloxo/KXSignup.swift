//
//  KXSignup.swift
//  Koloxo
//
//  Created by appzoc on 04/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import DropDown
import SideMenu

class KXSignup: UIViewController, InterfaceSettable, CategorySelectionDelegate {
    
    //////Outlets
    @IBOutlet var First_NameTF: UITextField!
    @IBOutlet var Last_NameTF: UITextField!
    @IBOutlet var EmailTF: UITextField!
    @IBOutlet var MobileTF: UITextField!
    @IBOutlet var telephoneCodeLBL: UILabel!
    @IBOutlet var PasswordTF: UITextField!
    @IBOutlet var CPasswordTF: UITextField!
    @IBOutlet var countryTF: UITextField!
    @IBOutlet var CheckBox: UIButton!
    @IBOutlet var Dropdown_Label: UILabel!
    @IBOutlet var countryFlagImage: UIImageView!
    @IBOutlet var dropview: UIView!
    
    var btn_click = 0
    fileprivate var countrySource :[KXCountryListModel] = []
    fileprivate var countryIndexPath: IndexPath?
    fileprivate var countryId: Int?
    final var registerSource :[KXSignupModel] = []
    final var Otp : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getCountryWeb()
        setUpInterface()
    }
    
    func setUpInterface() {
        countryFlagImage.image = nil
        telephoneCodeLBL.text = ""
        
    }
    
    // delegate from category selection vc(side dropdown)
    func selectedValue(index: IndexPath) {
        let country = countrySource[index.row]
        telephoneCodeLBL.text = country.country_code
        countryTF.text = country.name
        countryId = country.id
        telephoneCodeLBL.text = country.telephone_code
        let imageURL = URL(string: country.flag_image)
        countryFlagImage.kf.setImage(with: imageURL, placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        //countryFlagImage.kf.setImage(with: imageURL)
    }

    @IBAction func CountryDropdown(_ sender: Any) {
        
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.setTitle(title: "Select Country")
        categorySelectionViewController.tableSource = countrySource.map({$0.name})
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = countryIndexPath
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func TandCAction(_ sender: Any) {
    }
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    @IBAction func Register_now(_ sender: Any) {
        //        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXVerificationController") as! KXVerificationController
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.postData()
        
    }
    
    @IBAction func Sign_in(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Check_Action(_ sender: Any) {
        if(btn_click == 0)
        {
            if let image = UIImage(named: "black-check-box-with-white-check.png") {
                CheckBox.setImage(image, for: .normal)
                btn_click += 1
            }
        }
        else
        {
            if let image = UIImage(named: "basic-square") {
                CheckBox.setImage(image, for: .normal)
                btn_click -= 1
            }
            
        }
    }
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func TermsandConditions(_ sender: Any) {
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXTCViewController") as! KXTCViewController
        vc.isFromRegistration = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension KXSignup
{
    fileprivate func getCountryWeb(){
        getCountryListWeb { (list) in
            self.countrySource = list
        }
    }

    fileprivate func postData() {
        guard validateData() else { return }
        let registrationURL = "registration"

        var parameter = Json()
        parameter.updateValue(EmailTF.text!, forKey: "email")
        parameter.updateValue(PasswordTF.text!, forKey: "password")
        parameter.updateValue(First_NameTF.text!, forKey: "first_name")
        parameter.updateValue(Last_NameTF.text!, forKey: "last_name")
        parameter.updateValue(MobileTF.text!, forKey: "mobile_number")
        parameter.updateValue(countryId!, forKey: "country_residence")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
        
        Webservices2.postMethod(url: registrationURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            let signupdetails = KXSignupModel.getValue(fromObject: data)
            print(signupdetails.email, signupdetails.otp)
            
            BaseThread.asyncMain {
                let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXVerificationController") as! KXVerificationController
                vc.otpNumber = signupdetails.otp.description
                vc.emailId = signupdetails.email
                vc.user_id = signupdetails.user_id
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        }
        
    }
    
    fileprivate func validateData() -> Bool {
        
        
        if BaseValidator.isNotEmpty(string: EmailTF.text) &&
            BaseValidator.isNotEmpty(string: PasswordTF.text) &&
            BaseValidator.isNotEmpty(string: First_NameTF.text) &&
            BaseValidator.isNotEmpty(string: Last_NameTF.text) &&
            BaseValidator.isNotEmpty(string: MobileTF.text) &&
            BaseValidator.isNotEmpty(string: countryTF.text) &&
            BaseValidator.isNotEmpty(string: CPasswordTF.text) &&
            btn_click == 1{
            
            if BaseValidator.isValid(email: EmailTF.text)
            {
                print("Email Valid")
            }
            else
            {
                showBanner(message: "Invalid Email")
                return false
                
            }
            if BaseValidator.isValid(digits: MobileTF.text!), MobileTF.text!.count >= 7
            {
                print("Phone Valid")
            }
            else
            {
                showBanner(message: "Invalid MobileNumber")
                return false
                
            }
            if PasswordTF.text == CPasswordTF.text {
                print("Password  Valid")
                
                if PasswordTF.text!.count < 8 && PasswordTF.text!.count > 16 {
                    showBanner(message: "Password should contain characters between 8 and 16")
                    return false
                }
                
                var isContainUpperCase: Bool = false
                for item in PasswordTF.text! {
                    let str = String(item)
                    if str.uppercased() == str {
                        isContainUpperCase = true
                        break
                    }else {
                        isContainUpperCase = false
                    }
                }

                if !isContainUpperCase {
                    showBanner(message: "Password should contain altleast 1 upper case letter")
                    return false
                }
                
                
            }
            else
            {
                showBanner(message: "Password and Confirm Password Not Correct")
                return false
                
            }
            return true
            
        }
        else
        {
            
            if  First_NameTF.text == ""{
                showBanner(message: "Please Enter FirstName")
                
            }
            else if  Last_NameTF.text == ""{
                showBanner(message: "Please Enter LastName")
                
            }
            else if  countryTF.text == ""{
                showBanner(message: "Please Select CountryName")
                
            }
            else if EmailTF.text == ""{
                showBanner(message: "Please Enter Email Id")
                
            }
            else if  MobileTF.text == ""{
                showBanner(message: "Please Enter MobileNumber")
                
            }
            else if MobileTF.text == ""{
                showBanner(message: "Please Enter MobileNumber")
                
            }
            else if  PasswordTF.text == ""{
                showBanner(message: "Please Enter Password")
                
            }
            else if  CPasswordTF.text == ""{
                showBanner(message: "Please Enter Password")
                
            }
            else if btn_click == 0
            {
                showBanner(message: "Please Select Terms and Condition")
            }
            return false
            
        }
        
    }
    

}














