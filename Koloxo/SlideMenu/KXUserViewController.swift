//
//  KXUserViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 17/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXUserViewController: UIViewController, InterfaceSettable {

    @IBOutlet var profileImage: RoundedImage!
    @IBOutlet var profileNameLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var locationIconImage: UIImageView!
    @IBOutlet var tickMarkIconImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpInterface()
    }
    
    func setUpInterface() {
        profileImage.kf.setImage(with: URL(string: baseImageURL + Credentials.shared.profileImage), placeholder: #imageLiteral(resourceName: "userPlaceholder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        
        profileNameLBL.text = Credentials.shared.firstName + " " + Credentials.shared.lastName

        locationLBL.text = Credentials.shared.country
        
        if BaseValidator.isNotEmpty(string: locationLBL.text) {
            locationIconImage.isHidden = false
        }else {
            locationIconImage.isHidden = true
        }
        
        if BaseValidator.isNotEmpty(string: profileNameLBL.text) {
            tickMarkIconImage.isHidden = false
        }else {
            tickMarkIconImage.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back_button(_ sender: Any)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Profile_change(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier:    "KXProfileEditVC") as! KXProfileEditVC
        vc.passingUserID = Credentials.shared.userId
        vc.profileReviewType = .owner
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Home_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func My_dashboard_Button(_ sender: Any) {
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXDashBoardScreenViewController") as! KXDashBoardScreenViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func My_request_Button(_ sender: Any) {
        
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXMyRequestVC") as! KXMyRequestVC
        self.navigationController?.pushViewController(vc, animated: true)

//        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXRequestViewController") as! KXRequestViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func contactUSTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXContactUsVC") as! KXContactUsVC
        self.navigationController?.pushViewController(vc, animated: true)

    
    
    }

    
    
    @IBAction func My_history_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHistory") as! KXHistory
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func Notification_Button(_ sender: Any) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)

        
//        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func My_wishlist_Button(_ sender: Any) {
        
        let vc = self.storyboardDashboard!.instantiateViewController(withIdentifier: "KXWishListVC") as! KXWishListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func myRatingBTNTapped(_ sender: UIButton) {
        let vc = storyboardBooking?.instantiateViewController(withIdentifier: "KXCompleteRatingDisplayVC") as! KXCompleteRatingDisplayVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func but_CallButton(_ sender: UIButton) {
        
        
        let alertController = UIAlertController(title: "Koloxo", message: "Do you want to call ? +34 662 121 468", preferredStyle: .alert)
        let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            
            let ownerPhoneNumber = "+34662121468"
            ownerPhoneNumber.makeACall()
            alertController.dismiss(animated: true, completion: nil)
        })
        let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesPressed)
        alertController.addAction(noPressed)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func My_Search_Button(_ sender: Any) {
        let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KXMySearchesController") as! KXMySearchesController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func LogOut(_ sender: Any) {
        let param = ["user_id":"\(Credentials.shared.userId)","device_token":"\(Credentials.shared.deviceTocken)","device_type":"\(Credentials.shared.deviceType)"]
        Webservices.postMethod(url: "user/logout", parameter: param) { (isComplete, json) in
            if isComplete{
                BaseMemmory.write(object: "", forKey: "userId")
                BaseMemmory.write(object: "", forKey: "firstName")
                BaseMemmory.write(object: "", forKey: "lastName")
                BaseMemmory.write(object: "", forKey: "location")
                Credentials.shared.userId = ""
                Credentials.shared.emailId = ""
                Credentials.shared.firstName = ""
                Credentials.shared.lastName = ""
                Credentials.shared.mobile = ""
               // Credentials.shared.country = ""
                Credentials.shared.profileImage = ""
                
                MySearchesManager.instance.removeDataFromStore()
                //Facebook.LogoutRequest()
                
                let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                print("Logout failed!")
            }
        }
    }
}
