//
//  KXFbViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 02/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu


class KXFbViewController: UIViewController, InterfaceSettable,CategorySelectionDelegate {
    
    //outlets
    @IBOutlet var firstNameLB: UILabel!
    @IBOutlet var lastNameLB: UILabel!
    @IBOutlet var emailLB: UILabel!
    @IBOutlet var telephoneCodeLBL: UILabel!
    @IBOutlet var mobileTF: UITextField!
    
    @IBOutlet var countryTF: UITextField!
    @IBOutlet var countryFlagImage: UIImageView!
    // data receiving properties
    final var email:String?
    final var firstName:String?
    final var lastName:String?
    final var country: String?
    final var facebookId: String?
    
    // vc properties
    fileprivate var countrySource :[KXCountryListModel] = []
    fileprivate var countryIndexPath: IndexPath?
    fileprivate var countryId: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpInterface()
        
    }
    
    func setUpInterface() {
        if let name = firstName {
            firstNameLB.text = name
        }
        if let name = lastName {
            lastNameLB.text = name
        }
        if let email = email {
            emailLB.text = email
        }
        countryFlagImage.image = nil
        telephoneCodeLBL.text = ""
        self.getCountryData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // delegate from county selection
    func selectedValue(index: IndexPath) {
        let country = countrySource[index.row]
        telephoneCodeLBL.text = country.country_code
        countryTF.text = country.name
        countryId = country.id
        telephoneCodeLBL.text = country.telephone_code
        let imageURL = URL(string: country.flag_image)
        countryFlagImage.kf.setImage(with: imageURL)
        countryIndexPath = index
    }

    
    @IBAction func Next_Button(_ sender: Any) {
        self.postData()
        
        //        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXVerificationController") as! KXVerificationController
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Back_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func DropdownSlider(_ sender: Any) {
//        slideDropDownSelected = .country
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.tableSource = countrySource.map{$0.name}
        categorySelectionViewController.setTitle(title: "Select Country")
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = countryIndexPath
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
}

extension KXFbViewController {
    
    fileprivate func getCountryData(){

        getCountryListWeb { (list) in
            self.countrySource = list

        }
    }
    
    fileprivate func postData() {
        guard validateData() else { return }
        let facebookRegisterURL = "registration"

        var parameter = Json()
        parameter.updateValue(firstNameLB.text!, forKey: "first_name")
        parameter.updateValue(lastNameLB.text!, forKey: "last_name")
        parameter.updateValue(mobileTF.text!, forKey: "mobile_number")
        parameter.updateValue(emailLB.text!, forKey: "email")
        parameter.updateValue(randomIntFrom(start: 1000, to: 9999), forKey: "password")
        parameter.updateValue(countryId as Any, forKey: "country_residence")
        parameter.updateValue(facebookId!, forKey: "facebook_id")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
        
        Webservices2.postMethod(url: facebookRegisterURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            let signupdetails = KXFbVerificationModel.getValue(fromObject: data)
            BaseMemmory.write(object: signupdetails.user_id.description, forKey: "userId")
            BaseMemmory.write(object: signupdetails.country_residence, forKey: "location")

            
            Credentials.shared.userId = signupdetails.user_id.description
            Chat.registerForPush()
            Credentials.shared.emailId = signupdetails.email.description
            Credentials.shared.firstName = signupdetails.first_name
            Credentials.shared.lastName = signupdetails.last_name
            Credentials.shared.mobile = signupdetails.mobile_number.description
//            Credentials.shared.country = signupdetails.country_residence
            Credentials.shared.profileImage = signupdetails.image

            BaseThread.asyncMain {
                let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXVerificationController") as! KXVerificationController
                vc.emailId = signupdetails.email
                vc.otpNumber = signupdetails.otp.description
                vc.user_id = signupdetails.user_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    }
    
    fileprivate func validateData() -> Bool {
        if BaseValidator.isNotEmpty(string: countryTF.text) &&
            BaseValidator.isNotEmpty(string: mobileTF.text) {
            return true
        }else{
 
            if !BaseValidator.isNotEmpty(string: countryTF.text) {
                showBanner(message: "Please select a country")
                return false
            }
            if !BaseValidator.isNotEmpty(string: mobileTF.text) {
                showBanner(message: "Please enter valid phone number")
                return false
            }
            return false
        }
    }
    
    fileprivate func segueHome(){
        let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
}










