//
//  NotificationManager.swift
//  Koloxo
//
//  Created by Appzoc on 05/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class NotificationManager{
    
    private init(){}
    
    static let shared:NotificationManager = NotificationManager()
    
    var currentCount:Int{
        if let count = UserDefaults.standard.value(forKey: "NotificationCount") as? Int{
            return count
        }else{
            return 99
        }
    }
    
    func getNotificationCount(){
        if isLoggedIn(){
            let params = ["user_id":Credentials.shared.userId]
            Webservices.getMethodWithoutLoading(url: "getNotifications_Count", parameter: params) { (isComplete, json) in
                if isComplete{
                    if let jsonData = json["Data"] as? [String:Any], let count = jsonData["notification_count"] as? Int{
                        UserDefaults.standard.set(count, forKey: "NotificationCount")
                        print("CurrentCount:** ",count.description)
                    }
                }
            }
        }
    }
    
}
