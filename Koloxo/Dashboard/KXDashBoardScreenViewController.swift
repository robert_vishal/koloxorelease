//
//  KXDashBoardScreenViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 12/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher

class KXDashBoardScreenViewController: UIViewController {
    
    @IBOutlet var profileImage: BaseImageView!
    @IBOutlet var profileNameLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var myPropertyView: BaseView!
    @IBOutlet var receivedRequestView: BaseView!
    @IBOutlet var myRequestView: BaseView!
    @IBOutlet var myPropertiesCountLBL: UILabel!
    @IBOutlet var receivedRequestCountLBL: UILabel!
    @IBOutlet var myRequestCountLBL: UILabel!
    @IBOutlet var chatBadgeLBL: UILabel!
    @IBOutlet var notificationBadgeLBL: UILabel!
    
    fileprivate var countryList: [KXCountryListModel] = []
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setUpInterface()
        getCountryData()
        self.getDataWeb()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton) {
        showSideMenu()
//
//        let sideMenuVC = storyboardMain!.instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    @IBAction func myPropertiesTapped(_ sender: UIButton) {
        let vc = self.storyboardDashboard?.instantiateViewController(withIdentifier: "KXMyPropertiesVC") as! KXMyPropertiesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func receivedRequestTapped(_ sender: UIButton) {
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXRequestViewController") as! KXRequestViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func myRequestTapped(_ sender: UIButton) {
        //
        let vc = storyboardDashboard!.instantiateViewController(withIdentifier: "KXMyRequestVC") as! KXMyRequestVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func profileTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXProfileEditVC") as! KXProfileEditVC
        vc.passingUserID = Credentials.shared.userId
        vc.profileReviewType = .owner
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            isLoadedTabSearchChatNotify = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        if let vcArray = navigationController?.viewControllers.reversed() {
            print("VCArray",vcArray)
            for item in vcArray {
                if let _ = item as? KXDashBoardScreenViewController {
                    
                }else {
                    print("PopingTo",item)
                    navigationController?.popToViewController(item, animated: true)
                    break;
                }
                
            }
            
            
        }else {
            navigationController?.popViewController(animated: true)

        }
    }
    
}


// Handling Webservice
extension KXDashBoardScreenViewController {
    
    fileprivate func getCountryData(){
        getCountryListWeb { (list) in
            self.countryList = list
        }
    }

    
    fileprivate func getDataWeb(){
        let dashboardURL = "dashboard"
        var parameter = Json()
        parameter.updateValue(Credentials.shared.userId, forKey: "user_id")
        Webservices2.getMethodWith(url: dashboardURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Json,!data.isEmpty else { return }
            let value = KXDashboardModel.getValue(from: data)
            BaseThread.asyncMain {
                self.profileImage.kf.setImage(with: URL(string: baseImageURL + value.image), placeholder: #imageLiteral(resourceName: "userPlaceholder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
                self.profileNameLBL.text = value.first_name + " " + value.last_name
                self.locationLBL.text = self.countryList.filter{ $0.id == value.country_residence}.first?.name ?? ""
                self.myPropertiesCountLBL.text = value.myproperty_count.description
                self.receivedRequestCountLBL.text = value.receivedRequest_count.description
                self.myRequestCountLBL.text = value.myRequestBuyRent_count.description
                
                Credentials.shared.firstName = value.first_name
                Credentials.shared.lastName = value.last_name
                Credentials.shared.country = self.countryList.filter{ $0.id == value.country_residence}.first?.name ?? ""
                Credentials.shared.profileImage = value.image
                
            }
            
        }
    }
    
}






