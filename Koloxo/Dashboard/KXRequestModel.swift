
//  KXRequestListModel.swift
//  Koloxo
//
//  Created by Appzoc on 22/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

class KXRequestModel {
    
    var id:Int = 0
    var property_id:Int = 0
    var user_id:Int = 0
    var status:statusType = .Pending
    var request_count:Int = 0
    var property:KXRequestModelProperty?
    
    enum statusType: String {
        case Pending
        case Accepted
    }
    
    class func getValue(from object: Json?) -> KXRequestModel? {
        guard let object = object else { return KXRequestModel()}
        return KXRequestModel(object)
    }
    
    class func getValue(from array: JsonArray?) -> [KXRequestModel] {
        var modelArray : [KXRequestModel] = []
        guard let array = array else { return modelArray }
        for item in array{
            let object = KXRequestModel.getValue(from: item)
            modelArray.append(object!)
        }
        return modelArray
    }
    
    required public init(_ object: Json) {
        id = object["id"] as? Int ?? 0
        property_id = object["property_id"] as? Int ?? 0
        user_id = object["user_id"] as? Int ?? 0
        if object["status"] as? Int ?? 0 == 0 {
            status = .Pending
        }else{
            status = .Accepted
        }
        request_count = object["request_count"] as? Int ?? 0
        property = KXRequestModelProperty.getValue(from: object["property"] as? Json)
    }
    
    private init(){}
    
    
}

//////

class KXRequestModelProperty
{
    var id:Int = 0
    var property_name:String = ""
    var price:Int = 0
    var city:String = ""
    var state:String = ""
    var country:String = ""
    var property_images:[KXRequestModelPropertyImage] = []
    
    class func getValue(from object: Json?) -> KXRequestModelProperty {
        guard let object = object else { return KXRequestModelProperty() }
        return KXRequestModelProperty(object)
    }
    //
    //    class func getValue(from array: JsonArray?) -> [KXRequestModelProperty] {
    //        var modelArray : [KXRequestModelProperty] = []
    //        guard let array = array else { return modelArray }
    //        for item in array{
    //            let object = KXRequestModelProperty.getValue(from: item)
    //            modelArray.append(object)
    //        }
    //        return modelArray
    //    }
    //
    required public init(_ object: Json) {
        id = object["id"] as? Int ?? 0
        property_name = object["property_name"] as? String ?? ""
        price = object["price"] as? Int ?? 0
        city = object["city"] as? String ?? ""
        state = object["state"] as? String ?? ""
        country = object["country"] as? String ?? ""
        //        property_images = KXRequestModelPropertyImage.getValue(from: object["property_images"] as? JsonArray)
    }
    
    private init(){}
    
}


public class KXRequestModelPropertyImage {
    var id:Int = 0
    var property_id:Int = 0
    var type_id:Int = 0
    var image:String = ""
    
    class func getValue(from object: Json?) -> KXRequestModelPropertyImage {
        guard let object = object else { return KXRequestModelPropertyImage() }
        return KXRequestModelPropertyImage(object)
    }
    
    class func getValue(from array: JsonArray?) -> [KXRequestModelPropertyImage] {
        var modelArray : [KXRequestModelPropertyImage] = []
        guard let array = array else { return modelArray }
        for item in array{
            let object = KXRequestModelPropertyImage.getValue(from: item)
            modelArray.append(object)
        }
        return modelArray
    }
    
    
    required public init(_ object: Json) {
        id = object["id"] as? Int ?? 0
        property_id = object["property_id"] as? Int ?? 0
        type_id = object["type_id"] as? Int ?? 0
        image = object["image"] as? String ?? ""
    }
    
    private init(){}
    
}



