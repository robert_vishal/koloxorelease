//
//  KXDashboardModel.swift
//  Koloxo
//
//  Created by Appzoc on 18/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

public class KXDashboardModel
{
    var first_name:String = ""
    var last_name:String = ""
    var country_residence:Int = 0
    var email:String = ""
    var mobile_number:String = ""
    var image:String = ""
    var myproperty_count:Int = 0
    var receivedRequest_count:Int = 0
    var myRequestBuyRent_count:Int = 0
    
    private init(){}
    
    required public init(dictionary: Json) {
        
        first_name = dictionary["first_name"] as? String ?? ""
        last_name = dictionary["last_name"] as? String ?? ""
        country_residence = dictionary["country_residence"] as? Int ?? 0
        email = dictionary["email"] as? String ?? ""
        mobile_number = dictionary["mobile_number"] as? String ?? ""
        image = dictionary["image"] as? String ?? ""
        myproperty_count = dictionary["myproperty_count"] as? Int ?? 0
        receivedRequest_count = dictionary["receivedRequest_count"] as? Int ?? 0
        myRequestBuyRent_count = dictionary["myRequestBuyRent_count"] as? Int ?? 0
    }
    
    class func getValue(from object: Json?) -> KXDashboardModel {
        guard let object = object else { return KXDashboardModel() }
        return KXDashboardModel(dictionary: object)
    }
    
    
}

