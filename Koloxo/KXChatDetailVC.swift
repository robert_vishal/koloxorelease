//
//  KXChatDetailVC.swift
//  KoloxoScreens
//
//  Created by Appzoc on 08/06/18.
//  Copyright © 2018 AppZoc. All rights reserved.
//

import UIKit
import TwilioChatClient

class profileCellS: UITableViewCell
{
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var profileImage: BaseImageView!
    
    @IBOutlet var message: UILabel!
    
    @IBOutlet var timeLBL: UILabel!
    
    
    func configure(with message: TCHMessage, user: KXChatDetailVC.UserData.Users){
        if let _ = message.author{
            name.text = user.profileName
            self.message.text = message.body!
            self.timeLBL.text = FormatDate.formatDateForChat(date: message.dateUpdatedAsDate!)
        }
        profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(user.profileImage)"), placeholder: UIImage(named: "userPlaceholder"),
                                 options: [.requestModifier(getImageAuthorization())],
                                 progressBlock: nil,
                                 completionHandler: nil)
    }
    
    func hideTimeLabel(){
        timeLBL.isHidden = true
    }
    
    func showTime(){
        timeLBL.isHidden = false
    }
    
}

class messageCellS: UITableViewCell
{
    
    @IBOutlet weak var message: UILabel!
    
    @IBOutlet weak var timeLBL: UILabel!
    
    func configure(with messageX:TCHMessage){
        if let _ = messageX.body{
            message.text = messageX.body!
            timeLBL.text = FormatDate.formatDateForChat(date: messageX.dateUpdatedAsDate!)
        }
    }
    
    func hideTimeLabel(){
        timeLBL.isHidden = true
    }
    
    func showTime(){
        timeLBL.isHidden = false
    }
    
}

class profileCellO: UITableViewCell
{
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var profileImage: BaseImageView!
    
    @IBOutlet var message: UILabel!
    
    @IBOutlet var timeLBL: UILabel!
    
    func configure(with message: TCHMessage, user: KXChatDetailVC.UserData.Users){
        if let _ = message.author{
            name.text = user.profileName
            self.message.text = message.body!
            self.timeLBL.text = FormatDate.formatDateForChat(date: message.dateUpdatedAsDate!)
        }
        profileImage.kf.setImage(with: URL(string:"\(baseImageURL)\(user.profileImage)"), placeholder: UIImage(named: "userPlaceholder"),
                                 options: [.requestModifier(getImageAuthorization())],
                                 progressBlock: nil,
                                 completionHandler: nil)
    }
    
    func hideTimeLabel(){
        timeLBL.isHidden = true
    }
    
    func showTime(){
        timeLBL.isHidden = false
    }
}

class messageCellO: UITableViewCell
{
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var timeLBL: UILabel!
    
    func configure(with messageX:TCHMessage){
        if let _ = messageX.body{
            message.text = messageX.body!
            timeLBL.text = FormatDate.formatDateForChat(date: messageX.dateUpdatedAsDate!)
        }
    }
    
    func hideTimeLabel(){
        timeLBL.isHidden = true
    }
    
    func showTime(){
        timeLBL.isHidden = false
    }
    
}


class KXChatDetailVC: UIViewController
{
    
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var tableRef: UITableView!
    
    @IBOutlet var MakeDealView: BaseView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var inviteUserBTNRef: UIButton!
    
    // properties requied if navigated from property details Page
    final var propertyOwnerId: Int = 0
    final var requestId: Int = 0
    final var isRentProperty: Bool = false
    
    var propertyName  = ""
    
    var chatClient:TwilioChatClient? = Chat.instance.chatClient
    var channel:TCHChannel?
    var messages:[TCHMessage] = []
    
    var channelName:String = "Koloxo_193_60_120"
    var channelID:Int = 0
    var propertyID:Int = 0
    
    let messageRetrieveCount = 40
    
    let chatManager = ChatManager()
    let indicator = ActivityIndicator()
    
    let loginIdentity:String = "\(Credentials.shared.userId)"
    
    var chatUsers:[UserData.Users] = []
    
    @IBOutlet weak var makeDealHeight: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(getChannelUsers), name: .userInvitedToChat, object: nil)
        
        if "\(propertyOwnerId)" == Credentials.shared.userId && isRentProperty{
            inviteUserBTNRef.isHidden = false
        }else{
            inviteUserBTNRef.isHidden = true
        }
        profileName.text = propertyName
        if Credentials.shared.userId == propertyOwnerId.description {
           // MakeDealView.isHidden = true
            makeDealHeight.constant = 0
        }else {
            makeDealHeight.constant = 70
           // MakeDealView.isHidden = false
        }
        self.view.layoutIfNeeded()
        print("Started")

    }
    
    override func viewWillAppear(_ animated: Bool) {
         login(with: loginIdentity)
         getChannelUsers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        logout()
    }
    
    @IBAction func makeDealTapped(_ sender: UIButton) {
        let vc = self.storyboardSearch!.instantiateViewController(withIdentifier: "KXConfirmDealVC") as! KXConfirmDealVC
        vc.isRentProperty = self.isRentProperty
        vc.requestId = self.requestId
//        vc.propertyData = self.propertyData
        vc.propertyId = self.propertyID
        vc.isFromPropertyDetails = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func declineRequestTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Back_Button(_ sender: Any)
    {
        //        navigationController?.popViewController(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any)
    {
        showSideMenu()
    }
    
    
    @IBAction func sendBTNTapped(_ sender: UIButton) {
        sendMessage()
    }
    
    @IBAction func addUserToChannel(_ sender: UIButton) {
        
        let vc = storyboardChat!.instantiateViewController(withIdentifier: "KXInviteChatViewController") as! KXInviteChatViewController
        vc.passingPropertyID = self.propertyID
        vc.propertyOwnerID = Int(Credentials.shared.userId)!
        vc.channelID = self.channelID
        self.present(vc, animated: true, completion: nil)
        
//        let vc = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "KXChatMembersViewController") as! KXChatMembersViewController
//        vc.passingPropertyID = self.propertyID
//        vc.propertyOwnerId = Int(Credentials.shared.userId)!
//        vc.isDelegated = true
//        vc.delegate = self
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func getChannelUsers() {
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        let param = ["chat_room_id":"\(channelID)","tokenkey":tokenKey,"unix_time":unixTime]
       //  let param = ["chat_room_id":"\(channelID)"]
        Webservices.postMethod(url: "getChannelUsers", parameter:param ) { (isComplete, json) in
            if let trueData = json["Data"] as? [[String:Any]]{
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                    let parsedData = try decoder.decode([UserData].self, from: jsonData)
                    self.chatUsers = parsedData.map({$0.users})
                    self.tableRef.reloadData()
                }catch{
                    print(error)
                }
            }else{
                print("No Data for Chat/Troubleshoot")
            }
        }
    }
    
    func addUserToChannel(user:Int){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        Webservices.postMethod(url: "addUserToChannel", parameter: ["channelID":"\(channelID)","userID":"\(user)","tokenkey":tokenKey,"unix_time":unixTime]) { (isComplete, json) in
            print(json)
        }
    }
    
    /*
     channel:"Koloxo_193_59_120"
     channelID:2
     message:"How are you?"
 */
    
    //MARK:- Save Last Chat
    func saveLastChat(message:String){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        Webservices.getMethodWith(url: "saveLastChat", parameter: ["channel":self.channelName,"channelID":self.channelID,"message":message,"tokenkey":tokenKey,"unix_time":unixTime]) { (isComplete, json) in
            print(json)
        }
    }
    
    func login(with id:String){
        indicator.start()
        chatManager.getAccessToken(identity: "\(id)") { (token) in
            self.chatManager.createChatClient(with: token, delegate: self) { (clientR) in
                
                if clientR != nil{
                    self.chatClient = clientR
                    //self.indicator.stop()
                }else{
                    self.indicator.stop()
                    showBanner(message: "Error initializing Chat")
                }
            }
        }
    }
    
    func logout()
    {
        if let _ = chatClient
        {
            chatClient!.delegate = nil
            chatClient!.shutdown()
            self.chatClient = nil
        }
    }
    
    func sendMessage(){
        if let _ = inputField.text,inputField.text!.isNotEmpty{
            if let messages = self.channel?.messages{
                _ = inputField.text!
                
                let messageOptions = TCHMessageOptions().withBody("\(inputField.text!)")
                messages.sendMessage(with: messageOptions, completion: { (result, message) in
                    print("Sent message: \(self.inputField.text ?? "123") ",result.isSuccessful())
                    let messageIndex = message?.index ?? 0
                    messages.setAllMessagesConsumedWithCompletion(nil)
                    
                    self.scrollToBottomOfTable()
                    //self.saveLastChat(message: messageSave)
                })
                inputField.text = ""
            }
        }
    }
    
    func loadMessages(){
        messages.removeAll()
        if channel?.synchronizationStatus == .all
        {   //UInt(self.messageCount)
            
            channel?.messages?.getLastWithCount(UInt(messageRetrieveCount))
            { (result, items) in
                
                self.messages = items ?? [TCHMessage]()
                self.indicator.stop()
                //TODO: TableView reload
                //TODO:scroll to bottom of table
                self.tableRef.reloadData()
                self.scrollToBottomOfTable()
            }
            channel?.messages?.setAllMessagesConsumedWithCompletion(nil)
        }else{
            self.indicator.stop()
            print("Failed fetching messages")
        }
    }
    
    func scrollToBottomOfTable(){
        if messages.count > 0{
            self.tableRef.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
    
    struct UserData:Codable{
        let users:Users
        
        struct Users:Codable{
            let identity:Int
            let profileImage:String
            let profileName:String
            
            private enum CodingKeys: CodingKey, String{
                case identity = "id", profileImage = "image", profileName = "first_name"
            }
        }
    }
    
}

extension KXChatDetailVC: TwilioChatClientDelegate{
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        if status == .completed{
            print("Delegate ",self.chatClient?.delegate as Any)
            //indicator.start()
            chatManager.createOrJoinChannel(channelSend: channelName, client: self.chatClient!, channelR: { (channelR) in
                self.channel = channelR
                self.loadMessages()
            })
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel,
                    messageAdded message: TCHMessage) {
        self.messages.append(message)
        channel.messages?.setAllMessagesConsumedWithCompletion(nil)
        //TODO: Table reload
        //TODO: scroll to bottom of table
        self.tableRef.reloadData()
        self.scrollToBottomOfTable()
    }
}


//TableView Delegates
extension KXChatDetailVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let profileSender = tableView.dequeueReusableCell(withIdentifier: "profileCellS") as! profileCellS
        let messageSender = tableView.dequeueReusableCell(withIdentifier: "messageCellS") as! messageCellS
        let profileOther = tableView.dequeueReusableCell(withIdentifier: "profileCellO") as! profileCellO
        let messageOther = tableView.dequeueReusableCell(withIdentifier: "messageCellO") as! messageCellO
        
        
        
        if let _ = messages[indexPath.row].author{
            if messages[indexPath.row].author!.contains(loginIdentity){
                if indexPath.row-1 >= 0{
                    if messages[indexPath.row-1].author!.contains(loginIdentity){
                        messageSender.configure(with: messages[indexPath.row])
                        return messageSender
                    }else{
                        profileSender.configure(with: messages[indexPath.row], user: chatUsers.filter({Double($0.identity) == Double(loginIdentity)}).first ?? chatUsers[0])
                        return profileSender
                    }
                }else{
                    profileSender.configure(with: messages[indexPath.row], user: chatUsers.filter({Double($0.identity) == Double(loginIdentity)}).first ?? chatUsers[0])
                    return profileSender
                }
            }else{
                for user in chatUsers{
                    if messages[indexPath.row].author!.contains("\(user.identity)"){
                        if indexPath.row-1 >= 0{
                            if messages[indexPath.row-1].author!.contains("\(user.identity)"){
                                messageOther.configure(with: messages[indexPath.row])
                                return messageOther
                            }else{
                                profileOther.configure(with: messages[indexPath.row], user: user)
                                return profileOther
                            }
                        }else{
                            profileOther.configure(with: messages[indexPath.row], user: user)
                            return profileOther
                        }
                    }
                }
            }
        }
        
        return messageOther
    }
    
    
}


class FormatDate{
    static func formatDateForChat(date:Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let dateRetrieve = formatter.string(from: date)
        return dateRetrieve
    }
}

















