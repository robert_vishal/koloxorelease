//
//  KXAddplacemarker.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 07/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import MapKit
import SideMenu
import GoogleMaps
import GooglePlaces

class KXAddplacemarker: UIViewController, InterfaceSettable, CategorySelectionDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet var heightContainerView: NSLayoutConstraint!
    @IBOutlet var heightMapView: NSLayoutConstraint!
    @IBOutlet weak var mapViewRef: GMSMapView!
    @IBOutlet weak var placeAddressLBL: UILabel!
    @IBOutlet weak var latitudeLBL: UILabel!
    @IBOutlet weak var longitudeLBL: UILabel!
    @IBOutlet var countryLBL: UILabel!
    @IBOutlet var stateLBL: UILabel!
    @IBOutlet var cityTF: UITextField!
    @IBOutlet var addressTextView: UITextView!
    @IBOutlet var arrowBTNRef: UIButton!
    
    fileprivate var countrySource: [KXCountryListModel] = []
    fileprivate var countryIndexPath: IndexPath?
    fileprivate var countryId: Int?
    fileprivate let marker = GMSMarker()
    fileprivate let locationManager = CLLocationManager()
    fileprivate var markerLocation: CLLocationCoordinate2D?
    fileprivate var parameter: Json = Json()
    fileprivate var isMarkerCreated: Bool = false
    fileprivate var isFromAutoComplete: Bool = false
    fileprivate var buildingId: Int?
    private enum ColorChanger: String {
        case Country
        case State
        case City
        case Address
        case none
    }
    private let placeHolderColor = UIColor.init(red:120/255, green:131/255, blue:157/255, alpha: 1)
    private var colorType: ColorChanger = .none
    var editingMode:Bool = false
    var passingPropertyID:Int = 0
    var shouldSkipBuildingAddingStep: Bool = false
    fileprivate var isExpanded: Bool = false
    fileprivate var geocoder:GMSGeocoder?
    fileprivate var latitude: CLLocationDegrees = 25.2048{
        didSet{
            DispatchQueue.main.async {
                self.latitudeLBL.text = "\(self.latitude)"
            }
        }
    }
    fileprivate var longitude: CLLocationDegrees = 55.2708{
        didSet{
            DispatchQueue.main.async {
                self.longitudeLBL.text = "\(self.longitude)"
            }
        }
    }
    fileprivate var locationCordinates:CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //editingMode = true
        getCountryData()
        geocoder = GMSGeocoder()
        //self.mapViewRef?.isMyLocationEnabled = true
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        //setUpInterface()
        
    }
    
    
    
    func setUpInterface() {
        let camera = GMSCameraPosition.camera(withLatitude: self.latitude,
                                              longitude: self.longitude,
                                              zoom: 14.0)
        mapViewRef.camera = camera
       // showMaker(at: CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude))
    }
    
    func changeLabelTextColor(label: UILabel) {
        
    }
    //MARK: Track Current Location
    
    @IBAction func trackCurrentLocation(_ sender: UIButton) {
        //self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    
    // Naviagation Actions
    @IBAction func Back_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == cityTF {
            if cityTF.text == "City" {
                cityTF.text = nil
            }else if cityTF.text == nil {
                cityTF.text = "City"
                if self.cityTF.text == ColorChanger.City.rawValue {
                    self.cityTF.textColor = self.placeHolderColor
                }else {
                    self.cityTF.textColor = .black
                }

            }
        }
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == nil {
            textView.text = ""
        }else if textView.text == "Address" {
            textView.text = ""
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == nil || textView.text == "" {
            textView.text = "Address"
            if self.addressTextView.text == ColorChanger.Address.rawValue {
                self.addressTextView.textColor = self.placeHolderColor
            }else {
                self.addressTextView.textColor = .black
            }

        }
    }
    
    // Custom Actions
    @IBAction func changeMapHightTapped(_ sender: UIButton) {
        if isExpanded {
            isExpanded = false
            arrowBTNRef.setImage(UIImage(named: "down-arrow-angle-set"), for: [])
            heightMapView.constant = 200
            heightContainerView.constant = 720
        }else{
            isExpanded = true
            heightMapView.constant = 315
            arrowBTNRef.setImage(UIImage(named: "up-arrow-angle-set"), for: [])
            heightContainerView.constant = 835
        }
        UIView.animate(withDuration: 0.15) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func Next_Button(_ sender: Any) {
        
        guard validateData() else { return }
        
        if shouldSkipBuildingAddingStep {
            self.postAddbuildingCreated()
        }else {
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXAddedPlaceTable") as! KXAddedPlaceTable
            vc.passingPropertyID = self.passingPropertyID
            vc.editingMode = self.editingMode
            
            parameter.updateValue(countryId!, forKey: "country_id")
            parameter.updateValue(self.stateLBL.text!, forKey: "stateid")
            parameter.updateValue(self.cityTF.text!, forKey: "city_id")
            parameter.updateValue(self.addressTextView.text, forKey: "address")
            parameter.updateValue(self.latitudeLBL.text!, forKey: "latitude")
            parameter.updateValue(self.longitudeLBL.text!, forKey: "longitude")
            parameter.updateValue(self.passingPropertyID , forKey: "property_id")
            parameter.updateValue(self.countryLBL.text!, forKey: "description")
            
            vc.lat = Double(self.latitudeLBL.text!)! // self.latitude
            vc.lon = Double(self.longitudeLBL.text!)!
            print(vc.lat, vc.lon,"LATLONG")
            vc.loca = self.cityTF.text!
            vc.params = parameter
            self.navigationController?.pushViewController(vc, animated: true)

        }
        
        
    }
    
    fileprivate func postAddbuildingCreated() {
        let url = "addBuildingList"
        var parameterBuildingList = Json()
        let name = "Untitled" + (self.latitudeLBL.text!.double * 10000).int64.description
        
        parameterBuildingList.updateValue(name, forKey: "name")
        parameterBuildingList.updateValue(self.latitudeLBL.text!, forKey: "latitude")
        parameterBuildingList.updateValue(self.longitudeLBL.text!, forKey: "longitude")
        parameterBuildingList.updateValue(self.cityTF.text!, forKey: "location")
        parameterBuildingList.updateValue(name, forKey: "description")
        parameterBuildingList.updateValue(3.description, forKey: "building_type") // this for excluding building in the the list if Villa/Indipendant House

        let image = #imageLiteral(resourceName: "buildingPlaceHolder") //buildingPlaceholder sending
        let dataParam:[String:Data] = ["building":UIImageJPEGRepresentation(image, 0.8)!]
        
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_tocken")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")

        let unixTime = Date().millisecondsSince1970
        let hashKeyString = "appzoc-koloxohomes-appwebcastle" + unixTime.description
        let tockenKey = generateMD5(from: hashKeyString)

        parameter.updateValue(tockenKey, forKey: "tokenkey")
        parameter.updateValue("\(unixTime)", forKey: "unix_time")

        print("parameterBuildingList:",parameterBuildingList)
        Webservices.postMethodMultiPartImage(url: url, parameter: parameterBuildingList, dataParameter: dataParam) { (isSucceeded, response) in
            print(response)
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Int else { return }
            self.buildingId = data
            self.postDataPlaceMarkWeb()
        }

        
    }
    
    
    fileprivate func postDataPlaceMarkWeb(){
        let url = "AddplaceMark"
        parameter.updateValue(countryId!, forKey: "country_id")
        parameter.updateValue(self.stateLBL.text!, forKey: "stateid")
        parameter.updateValue(self.cityTF.text!, forKey: "city_id")
        parameter.updateValue(self.addressTextView.text, forKey: "address")
        parameter.updateValue(self.latitudeLBL.text!, forKey: "latitude")
        parameter.updateValue(self.longitudeLBL.text!, forKey: "longitude")
        parameter.updateValue(self.passingPropertyID , forKey: "property_id")
        parameter.updateValue(self.countryLBL.text!, forKey: "description")

        parameter.updateValue(buildingId!, forKey: "building_id")
        parameter.updateValue("\(passingPropertyID)", forKey: "property_id")
        parameter.updateValue("\(Credentials.shared.userId)", forKey: "user_id")
        parameter.updateValue("1", forKey: "edit")
        
        Webservices2.postMethod(url: url, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            
            BaseThread.asyncMain {
                let vc = self.storyboardProperty!.instantiateViewController(withIdentifier: "KXAddRoomImagesController") as! KXAddRoomImagesController
                vc.passingPropertyID = self.passingPropertyID
                vc.editingMode = self.editingMode
                self.navigationController?.pushViewController(vc, animated: true)

            }
        }
        
        
    }
    
    
    fileprivate func validateData() -> Bool {
        if countryId != nil && BaseValidator.isNotEmpty(string: countryLBL.text) && BaseValidator.isNotEmpty(string: stateLBL.text) && BaseValidator.isNotEmpty(string: cityTF.text) && BaseValidator.isNotEmpty(string: latitudeLBL.text)
            && BaseValidator.isNotEmpty(string: longitudeLBL.text) && BaseValidator.isNotEmpty(string: addressTextView.text) && stateLBL.text != "State" && countryLBL.text != "Country" {
            
            return true
        }else {
            if countryId == nil {
                showBanner(message: "Please select country")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: stateLBL.text) || stateLBL.text == "State" {
                showBanner(message: "Please select state")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: cityTF.text) {
                showBanner(message: "Please enter city")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: addressTextView.text) {
                showBanner(message: "Please enter address")
                return false
            }
            return false
        }
    }
    
    @IBAction func DropDown(_ sender: Any) {
        let categorySelectionViewController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        categorySelectionViewController.setTitle(title: "Select Country")
        categorySelectionViewController.tableSource = countrySource.map{$0.name}
        categorySelectionViewController.delegate = self
        categorySelectionViewController.selectedIndexPath = countryIndexPath
        
        let sideMenuController = UISideMenuNavigationController(rootViewController: categorySelectionViewController)
        sideMenuController.leftSide = false
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    
    // delegate from county selection
    func selectedValue(index: IndexPath) {
        let country = countrySource[index.row]
       
            self.getPlaceLocation(place: country.name) { (coordinates) in
                 DispatchQueue.main.async {
                    let cameraSnap = GMSCameraPosition.camera(withTarget: coordinates, zoom: 14.0)
                    self.mapViewRef.camera = cameraSnap
                 }
            }
        DispatchQueue.main.async {
            self.countryLBL.text = country.name
            if self.countryLBL.text == ColorChanger.Country.rawValue {
                self.countryLBL.textColor = self.placeHolderColor
            }else {
                self.countryLBL.textColor = .black
            }

            self.countryId = country.id
            self.countryIndexPath = index
            self.stateLBL.text = ""
            self.cityTF.text = ""
            self.addressTextView.text = ""
            self.latitudeLBL.text = ""
            self.longitudeLBL.text = ""
        }
        
    }
    
    @IBAction func cityNameChanged(_ sender: UITextField) {
        
    }
    
    @IBAction func cityNameTapped(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

    }
    
}

// Common map functions
extension KXAddplacemarker {
    fileprivate func getCenterLocationMapView() -> CLLocationCoordinate2D{
        let lat =  mapViewRef.projection.coordinate(for: CGPoint(x: mapViewRef.frame.midX, y: mapViewRef.frame.midY)).latitude
        let lon = mapViewRef.projection.coordinate(for: CGPoint(x: mapViewRef.frame.midX, y: mapViewRef.frame.midY)).longitude
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    
    fileprivate func showMaker(at location: CLLocationCoordinate2D){
        if isMarkerCreated {
            marker.position = location
        }else{
            let markerImage = UIImage(named: "maps-and-flags (1)") //.withRenderingMode(.alwaysTemplate)
            let markerView = UIImageView(image: markerImage)
            markerView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            marker.position = location
            marker.iconView = markerView
            marker.map = mapViewRef
            isMarkerCreated = true
        }
    }
    
}

// Handling map events
extension KXAddplacemarker: GMSMapViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate, GMSAutocompleteViewControllerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocations-locationManager")
        let currentLocation = locations.last?.coordinate
        let camera = GMSCameraPosition.camera(withLatitude: (currentLocation?.latitude)!, longitude: (currentLocation?.longitude)!, zoom: 14.0)
        
        self.mapViewRef.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
        //showMaker(at: currentLocation!)
    }
    

    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
//        self.latitude = mapView.projection.coordinate(for: CGPoint(x: mapViewRef.frame.midX, y: mapViewRef.frame.midY)).latitude
//        self.longitude = mapView.projection.coordinate(for: CGPoint(x: mapViewRef.frame.midX, y: mapViewRef.frame.midY)).longitude
        self.latitude = position.target.latitude
        self.longitude = position.target.longitude
        let coordinate = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        print("coordinate",coordinate)
        //showMaker(at: coordinate)
        if isFromAutoComplete{
            isFromAutoComplete = false
        }else{
            didTapAtMap(coordinate: coordinate)
        }
//        geocoder!.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: latitude, longitude: longitude)) { (response, err) in
//            if err == nil{
//                if let result = response?.firstResult()?.lines{
//                    self.placeAddressLBL.text = ""
//                    for item in result{
//                        self.placeAddressLBL.text == nil ? self.placeAddressLBL.text = item : self.placeAddressLBL.text?.append(" \(item)")
//                    }
//                    self.latitude = response!.firstResult()!.coordinate.latitude
//                    self.longitude = response!.firstResult()!.coordinate.longitude
//                }
//            }else{
//                print(err as Any)
//            }
//        }
        
    }
    // [{"furniture_id":15,"data":{"title":"adfs","description":"bedromdescription"}},{"furniture_id":15,"data":{"title":"adfs","description":"bedromdescription"}}]
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        //didTapAtMap(coordinate: coordinate)
    }
    
    fileprivate func didTapAtMap(coordinate: CLLocationCoordinate2D) {
        //marker.position = coordinate
        geocoder!.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)) { (response, err) in
            if err == nil{
                if let result = response?.firstResult(){
                    
                    self.countryLBL.text = result.country ?? ""
                    if self.countryLBL.text == ColorChanger.Country.rawValue {
                        self.countryLBL.textColor = self.placeHolderColor
                    }else {
                        self.countryLBL.textColor = .black
                    }

                    self.stateLBL.text = result.administrativeArea ?? result.locality ?? ""
                    if self.stateLBL.text == ColorChanger.State.rawValue {
                        self.stateLBL.textColor = self.placeHolderColor
                    }else {
                        self.stateLBL.textColor = .black
                    }

                    self.cityTF.text = result.locality
                    if self.cityTF.text == ColorChanger.City.rawValue {
                        self.cityTF.textColor = self.placeHolderColor
                    }else {
                        self.cityTF.textColor = .black
                    }

                    var addressText = ""
                    if let _ = result.lines{
                        for line in result.lines!{
                            addressText += line
                        }
                    }
                    self.addressTextView.text = addressText
                    if self.addressTextView.text == ColorChanger.Address.rawValue {
                        self.addressTextView.textColor = self.placeHolderColor
                    }else {
                        self.addressTextView.textColor = .black
                    }

                    self.latitudeLBL.text = coordinate.latitude.description
                    self.longitudeLBL.text = coordinate.longitude.description
                    
                    print("Sublocality : ",result.subLocality as Any)
                    print("Lines : ",result.lines as AnyObject)
                    //  $0.id == value.country_residence
                    self.countryId = self.countrySource.filter{ $0.name == self.countryLBL.text}.first?.id
                    //marker.position = coordinate
                    
                }
            }else{
                print(err as Any)
            }
        }
    }
    
    
    // autocomplete delegates
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress!.description)")
       
        geocoder!.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude:  place.coordinate.latitude, longitude:  place.coordinate.longitude)) { (response, err) in
            if err == nil{
                if let result = response?.firstResult(){
                    
                    
                    self.isFromAutoComplete = true
                    self.countryLBL.text = result.country ?? ""
                    if self.countryLBL.text == ColorChanger.Country.rawValue {
                        self.countryLBL.textColor = self.placeHolderColor
                    }else {
                        self.countryLBL.textColor = .black
                    }

                    self.stateLBL.text = result.administrativeArea ?? result.locality ?? ""
                    if self.stateLBL.text == ColorChanger.State.rawValue {
                        self.stateLBL.textColor = self.placeHolderColor
                    }else {
                        self.stateLBL.textColor = .black
                    }

                    self.cityTF.text = result.subLocality ?? result.locality ?? result.administrativeArea ?? ""
                    if self.cityTF.text == ColorChanger.City.rawValue {
                        self.cityTF.textColor = self.placeHolderColor
                    }else {
                        self.cityTF.textColor = .black
                    }

                    self.latitudeLBL.text = place.coordinate.latitude.description
                    self.longitudeLBL.text = place.coordinate.longitude.description
                    let address: String? = place.formattedAddress?.description //result.lines?.joined(separator: ", ")
                    
                    self.addressTextView.text = address ?? ""
                    if self.addressTextView.text == ColorChanger.Address.rawValue {
                        self.addressTextView.textColor = self.placeHolderColor
                    }else {
                        self.addressTextView.textColor = .black
                    }

                    print("Adresssss",address as Any)

                    print(result.subLocality as Any)
                    print(result.lines as AnyObject)
                    self.countryId = self.countrySource.filter{ $0.name == self.countryLBL.text}.first?.id
                    
                    self.marker.position = place.coordinate
                    let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude,
                                                          longitude: place.coordinate.longitude,
                                                          zoom: 14)
                    self.mapViewRef.camera = camera
                    
                }
            }else{
                print(err as Any)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    
    
    func getPlaceLocation(place:String, completion: @escaping(CLLocationCoordinate2D)->()){
        let passingPlace = place.replacingOccurrences(of: " ", with: "")
        let request = URLRequest(url: URL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(passingPlace)&key=AIzaSyAfnrRFEF-6n8dQfVdHZ8fIkTfcFAc2byk")!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            
            if let trueData = data{
                let decoder = JSONDecoder()
                do{
                    let parsedData = try decoder.decode(GoogleMapLocationParse.self, from: trueData)
                    //parsedData.results.first!.geometry.location.lat
                    if let coordinateData = parsedData.results as? [Results]{
                        for place in coordinateData{
                           // print(place.geometry.location)
                            completion(CLLocationCoordinate2D(latitude: place.geometry.location.lat, longitude: place.geometry.location.lng))
                        }
                    }
                }catch{
                    print(error)
                }
                
            }
        }
        task.resume()
    }
}

// Handling Web service
extension KXAddplacemarker {
    
    fileprivate func getCountryData(){
        getCountryListWeb { (list) in
            self.countrySource = list
            if self.editingMode {
                self.editDataWebcall()
            }
        }
    }
    
    fileprivate func editDataWebcall(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"132"]
                let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"\(passingPropertyID)"]
        
        Webservices.getMethodWith(url: "getPropertyDetail", parameter: param) { (isComplete, json) in
            print(json)
            if isComplete{
                do{
                    let trueData = json["Data"] as? [String:Any]
                    let tempData = trueData!["property"]
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: tempData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode(PropertyPlaceModel.self, from: jsonData)
                    self.populateWithData(data: parsedData)
                    print(parsedData)
                }catch{
                    print(error)
                }
            }
        }
    }
    
    fileprivate func populateWithData(data: PropertyPlaceModel){
        DispatchQueue.main.async {
            self.latitude = Double(data.latitude)!
            self.longitude = Double(data.longitude)!
            self.latitudeLBL.text = "\(self.latitude)"
            self.longitudeLBL.text = "\(self.longitude)"
            self.countryId = data.country
            self.cityTF.text! = data.city
            if self.cityTF.text == ColorChanger.City.rawValue {
                self.cityTF.textColor = self.placeHolderColor
            }else {
                self.cityTF.textColor = .black
            }

            self.countryLBL.text = self.countrySource.flatMap{value in
                if value.id == self.countryId{
                    return value.name
                }
                return nil
                }.filter({$0 != nil})[0]
            print("Flat Map + Filter Result",self.countryLBL.text!)
            if self.countryLBL.text == ColorChanger.Country.rawValue {
                self.countryLBL.textColor = self.placeHolderColor
            }else {
                self.countryLBL.textColor = .black
            }

            self.stateLBL.text = data.state
            if self.stateLBL.text == ColorChanger.State.rawValue {
                self.stateLBL.textColor = self.placeHolderColor
            }else {
                self.stateLBL.textColor = .black
            }

            self.addressTextView.text = data.address
            if self.addressTextView.text == ColorChanger.Address.rawValue {
                self.addressTextView.textColor = self.placeHolderColor
            }else {
                self.addressTextView.textColor = .black
            }

            let camera = GMSCameraPosition.camera(withLatitude: self.latitude,
                                                  longitude: self.longitude,
                                                  zoom: 14)
            self.mapViewRef.camera = camera
        }
        
    }
}


