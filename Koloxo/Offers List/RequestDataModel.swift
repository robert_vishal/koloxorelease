//
//  RequestDataModel.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 08/02/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import Foundation

/*
 {
 "id": 508,
 "property_id": 1272,
 "user_id": 325,
 "status": 1,
 "deal_confirmation": 10,
 "request_amount": null,
 "offer_amount": null,
 "moving_in": "2019-02-04 00:00:00",
 "moving_out": "2019-02-19 00:00:00",
 "created_at": "2019-02-07 09:43:55",
 "updated_at": "2019-02-07 09:43:55",
 "deleted_at": null
 }
 
 */

class RequestDataModel {
    public var id : Int?
    public var property_id : Int?
    public var user_id : Int?
    public var status : Int?
    public var deal_confirmation : Int?
    public var request_amount : String?
    public var offer_amount : String?
    public var moving_in : String?
    public var moving_out : String?
    public var created_at : String?
    public var updated_at : String?
    public var deleted_at : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = RequestDataModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of RequestDataModel Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [RequestDataModel]
    {
        var models:[RequestDataModel] = []
        for item in array
        {
            models.append(RequestDataModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let RequestDataModel = RequestDataModel(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: RequestDataModel Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? Int
        property_id = dictionary["property_id"] as? Int
        user_id = dictionary["user_id"] as? Int
        status = dictionary["status"] as? Int
        deal_confirmation = dictionary["deal_confirmation"] as? Int
        request_amount = dictionary["request_amount"] as? String
        offer_amount = dictionary["offer_amount"] as? String
        moving_in = dictionary["moving_in"] as? String
        moving_out = dictionary["moving_out"] as? String
        created_at = dictionary["created_at"] as? String
        updated_at = dictionary["updated_at"] as? String
        deleted_at = dictionary["deleted_at"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.property_id, forKey: "property_id")
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.deal_confirmation, forKey: "deal_confirmation")
        dictionary.setValue(self.request_amount, forKey: "request_amount")
        dictionary.setValue(self.offer_amount, forKey: "offer_amount")
        dictionary.setValue(self.moving_in, forKey: "moving_in")
        dictionary.setValue(self.moving_out, forKey: "moving_out")
        dictionary.setValue(self.created_at, forKey: "created_at")
        dictionary.setValue(self.updated_at, forKey: "updated_at")
        dictionary.setValue(self.deleted_at, forKey: "deleted_at")
        
        return dictionary
    }
    
}
