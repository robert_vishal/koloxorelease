//
//  KX_ConfirmDealPopUp.swift
//  Koloxo
//
//  Created by Appzoc on 20/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class KX_ConfirmDealPopUp_Rent: UIViewController {
    @IBOutlet var but_reject: UIButton!
    @IBOutlet var but_Accept: UIButton!
    
    @IBOutlet var fromNameLB: UILabel!
    @IBOutlet var dealAmountLB: UILabel!
    @IBOutlet var offerAmountLB: UILabel!
    @IBOutlet var moveingInLB: UILabel!
    @IBOutlet var moveingOutLB: UILabel!
    
    var delegate:ReloadOffers?
    
    var PropID = ""
    var ReqID = ""
    var UserID = ""
    var Typeof = ""
    var status:Int = -1
    
     var fromNameLB_From = ""
    var dealAmountLB_From = ""
     var offerAmountLB_From = ""
    var moveingInLB_From = ""
    var moveingOutLB_From = ""
    
    override func viewDidLoad() {
        
        fromNameLB.text = fromNameLB_From
        dealAmountLB.text = dealAmountLB_From
        offerAmountLB.text = offerAmountLB_From
        moveingInLB.text = moveingInLB_From
        moveingOutLB.text = moveingOutLB_From
        
        //MARK: Check Status
        but_reject.isHidden = false
        
        but_Accept.isHidden = false
        
        if status == 0
        {
            but_reject.isHidden = false
            
            but_Accept.isHidden = false
        }
        else
        {
            but_reject.isHidden = true
            
            but_Accept.isHidden = true
        }
        
        
        
        self.view.backgroundColor = UIColor(red:0.30, green:0.30, blue:0.30, alpha:0.6)
        
  
        
        self.view.isUserInteractionEnabled = true
       
        
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(tapD(recognizer:)))
        
       self.view.addGestureRecognizer(doubleTapGest)
       
        
    }
    
    @objc func tapD(recognizer: UITapGestureRecognizer)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

    

    
    @IBAction func Reject_Action(_ sender: UIButton) {
        
//        Webservices2.postMethod(url: "confirmDeal", parameter: ["property_id":self.PropID,"request_id":self.ReqID,"user_id":self.UserID]) { (isSucceeded, response) in
//            guard isSucceeded else { return }
//            guard let data = response["Data"] as? JsonArray, !data.isEmpty else { return }
//            //self.dismiss(animated: true, completion: nil)
//
//
//
//        }
        var parameter = Json()
        parameter["user_id"] = self.UserID
        parameter["property_id"] = self.PropID
        parameter["request_id"] = self.ReqID
       // parameter["Typeof"] = self.Typeof
        print(self.Typeof)
        Webservices2.postMethod(url: "declineDeal", parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            
           // if let dealStatus = data["deal_confirmation"] as? Int {
              //  if dealStatus == 2 {
                    BaseThread.asyncMain {
                        showBanner(message: "Deal Rejected")
                        //self.delegate?.reloadOfferList()
                        self.dismiss(animated: true, completion: {
                            self.delegate?.reloadOfferList()
                        })
                    }
              //  }
          //  }
            

        }
        
        

        

        
    }
    
    @IBAction func Accept_Action(_ sender: UIButton) {
        print(self.PropID,self.ReqID,self.UserID)

        var parameter = Json()
        parameter["user_id"] = self.UserID
        parameter["property_id"] = self.PropID
        parameter["request_id"] = self.ReqID
      //  parameter["Typeof"] = self.Typeof
        print(self.Typeof)
        Webservices2.postMethod(url: "confirmDeal", parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            
            BaseThread.asyncMain {                               
                showBanner(message: "Deal Confirmed", style: .success)
                //self.delegate?.reloadOfferList()
                self.dismiss(animated: true, completion: {
                    self.delegate?.reloadOfferList()
                })
            }

        }
        
        
//        Webservices2.getMethodWith(url: "confirmDeal?property_id=\(self.PropID)&request_id=\(self.ReqID)&user_id=\(self.UserID)", parameter:["":""]) { (Succeeded, response) in
//
//
//            guard let data = response["Data"] as? JsonArray, !data.isEmpty else { return }
//            self.dismiss(animated: true, completion: nil)
//        }
        
    }
}


//Confirm Deal : http://koloxo-homes.dev.webcastle.in/api/confirmDeal
//fields : property_id,request_id,user_id
//Request Method : POST
//Confirm Deal : http://koloxo-homes.dev.webcastle.in/api/declineDeal
//fields : property_id,request_id,user_id
//Request Method : POST




class KX_ConfirmDealPopUp_Buy: UIViewController {
    
    @IBOutlet var but_Reject: UIButton!
    
    @IBOutlet var but_Accept: UIButton!
    
    var PropID = ""
    var ReqID = ""
    var UserID = ""
    var status = -1
    var Typeof = ""
    var fromNameLB_From = ""
    var dealAmountLB_From = ""
    var offerAmountLB_From = ""
    
    var delegate:ReloadOffers?

    
    @IBOutlet var fromNameLB: UILabel!
    @IBOutlet var offerAmountLB: UILabel!
    @IBOutlet var dealAmountLB: UILabel!
    override func viewDidLoad() {
        
        
        fromNameLB.text = fromNameLB_From
        dealAmountLB.text = dealAmountLB_From
        offerAmountLB.text = offerAmountLB_From

        
        but_Reject.isHidden = false
        
        but_Accept.isHidden = false
        
        if status == 0
        {
            but_Reject.isHidden = false
            
            but_Accept.isHidden = false
        }
        else
        {
            but_Reject.isHidden = true
            
            but_Accept.isHidden = true
        }
        
        
        self.view.backgroundColor = UIColor(red:0.30, green:0.30, blue:0.30, alpha:0.6)
        
        
        self.view.isUserInteractionEnabled = true
        
        
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(tapD(recognizer:)))
        
        self.view.addGestureRecognizer(doubleTapGest)
    }
    
    @objc func tapD(recognizer: UITapGestureRecognizer)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Accept_Action(_ sender: UIButton) {
        
        var parameter = Json()
        parameter["user_id"] = self.UserID
        parameter["property_id"] = self.PropID
        parameter["request_id"] = self.ReqID
        //parameter["Typeof"] = "2"
      //  parameter["Typeof"] = self.Typeof
        print(self.Typeof)
        Webservices2.postMethod(url: "confirmDeal", parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            
            BaseThread.asyncMain {
                showBanner(message: "Deal Confirmed", style: .success)

                self.dismiss(animated: true, completion: {
                    self.delegate?.reloadOfferList()
                })
            }
            
        }

    }
    
    @IBAction func Reject_Action(_ sender: UIButton) {
        
        print(self.PropID,self.ReqID,self.UserID)
        var parameter = Json()
        parameter["user_id"] = self.UserID
        parameter["property_id"] = self.PropID
        parameter["request_id"] = self.ReqID
      //  parameter["Typeof"] = self.Typeof
        print(self.Typeof)
        Webservices2.postMethod(url: "declineDeal", parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            
          //  if let dealStatus = data["deal_confirmation"] as? Int {
            //    if dealStatus == 2 {
                    BaseThread.asyncMain {
                        showBanner(message: "Deal Rejected")
                        //self.delegate?.reloadOfferList()
                        self.dismiss(animated: true, completion: {
                            self.delegate?.reloadOfferList()
                        })
                    }
          
        }
    }
}
