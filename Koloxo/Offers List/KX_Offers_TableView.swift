//
//  KX_Offers_TableView.swift
//  Offers_Koloxo
//
//  Created by Appzoc on 19/06/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

class viewOffersTableviewCell: UITableViewCell
{
    @IBOutlet var nameLb: UILabel!
    
    
    @IBOutlet var dateLb: UILabel!
    @IBOutlet var priceLb: UILabel!
    @IBOutlet var viewOfferBt: UIButton!
    
    @IBOutlet var cont_VIew: StandardView!
    
}

class viewOffersTableviewCell_Accepted: UITableViewCell
{
    @IBOutlet var nameLb: UILabel!
    @IBOutlet var dateLb: UILabel!
    @IBOutlet var priceLb: UILabel!
    @IBOutlet var viewOfferBt: UIButton!
    @IBOutlet var cont_VIew: StandardView!
    
}

class viewOffersTableviewCell_Rejected: UITableViewCell
{
    @IBOutlet var nameLb: UILabel!
    @IBOutlet var dateLb: UILabel!
    @IBOutlet var priceLb: UILabel!
    @IBOutlet var viewOfferBt: UIButton!
    @IBOutlet var cont_VIew: StandardView!
    
    
}

