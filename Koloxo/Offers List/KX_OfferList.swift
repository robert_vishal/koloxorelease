//
//  KX_OfferList.swift
//  Koloxo
//
//  Created by Appzoc on 19/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class KX_OfferList: UIViewController, ReloadOffers {
    
    @IBOutlet var propertyTittle_Lb: UILabel!
    
    @IBOutlet var listTableView: UITableView!
    
    var offerListdata = [Any]()
    var propetyTittle = ""//"No tittle"
    var propetyTyp = "No tittle"
    var propetyId = "No tittle"
    var userID = Credentials.shared.userId
    
    var isFromPushNotification:Bool = false
    final var moving_inMyrequest: String = ""
    final var durationMyRequest: String = ""
    final var typeOfProperty: Int = 1
    final var propertyId: Int = 210
    override func viewDidLoad() {
        super.viewDidLoad()
        
        propertyTittle_Lb.text = "\(propetyTittle)"
        
        print("UserId:",Credentials.shared.userId)
        print("PropertyType:",self.propetyTyp)
        
        self.listTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        self.listTableView.separatorColor = UIColor.clear
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //viewOffersTableviewCell
    
    override func viewWillAppear(_ animated: Bool)
    {
        reloadOfferList()
//        Webservices2.getMethod(url: "viewOfferRequestList?property_id=\(propetyId)") { (isSuccess, Respo) in
//
//            guard isSuccess else { return }
//            let dataArray = Respo["Data"] as! [Any]
//
//            self.offerListdata = dataArray
//            self.listTableView.reloadData()
//            // self.setdata()
//            self.listTableView.tableFooterView = UIView()
//        }
    }
    
    func reloadOfferList() {
        print("viewOfferRequestList:",propetyId)
        Webservices2.getMethod(url: "viewOfferRequestList?property_id=\(propetyId)") { (isSuccess, Respo) in
            
            guard isSuccess else { return }
            let dataArray = Respo["Data"] as! [Any]
            //dump(dataArray)
            
            
            self.offerListdata = dataArray
            self.listTableView.reloadData()
            // self.setdata()
            self.listTableView.tableFooterView = UIView()
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        if self.isFromPushNotification {
            let vc = self.storyboardMain?.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            navigationController?.popViewController(animated: true)
        }
    }
    
}

extension KX_OfferList: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.offerListdata.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //0 = Pending , 1 = Accepted , 2 = rejected

        let da = self.offerListdata[indexPath.row] as! [String:Any]
        let Users = da["users"] as! [String:Any]
        let name = "\(Users["first_name"] as! String) \(Users["last_name"] as! String)"
        let stat = da["deal_confirmation"] as! Int
        
        
       
        
        if stat == 0
        { // view offers
            let cell = tableView.dequeueReusableCell(withIdentifier: "viewOffersTableviewCell") as! viewOffersTableviewCell
            
            cell.nameLb.text = "\(name)".uppercased()
            cell.dateLb.text = "\(Users["date"] as? String ?? "No Date")"
            cell.priceLb.text = " \(da["offer_amount"] as! Int)"
            cell.viewOfferBt.tag = indexPath.row
            cell.viewOfferBt.addTarget(self, action:  #selector(openCloseCellBTNTapped(_:)), for: .allEvents)

            
            if indexPath.row == 0
            {
                cell.cont_VIew.setSpecificRoundCorners([.topLeft,.topRight], radius: 10)
            }
            
            if indexPath.row == self.offerListdata.count - 1
            {
                cell.cont_VIew.setSpecificRoundCorners([.bottomLeft,.bottomRight], radius: 10)
            }
            
            
            
            return cell
        }
        else if stat == 1
        { // accepted
            let cell = tableView.dequeueReusableCell(withIdentifier: "viewOffersTableviewCell_Accepted") as! viewOffersTableviewCell_Accepted
            
            cell.nameLb.text = "\(name)".uppercased()
            cell.dateLb.text = "\(Users["date"] as? String ?? "No Date")"
            cell.priceLb.text = " \(da["offer_amount"] as! Int)"
            //cell.viewOfferBt.text = ""
            if indexPath.row == 0
            {
                cell.cont_VIew.setSpecificRoundCorners([.topLeft,.topRight], radius: 10)
            }
            
            if indexPath.row == self.offerListdata.count - 1
            {
                cell.cont_VIew.setSpecificRoundCorners([.bottomLeft,.bottomRight], radius: 10)
            }
            
            return cell
            
            
        }
        else if stat == 2
        { // rejected = 2
            let cell = tableView.dequeueReusableCell(withIdentifier: "viewOffersTableviewCell_Rejected") as! viewOffersTableviewCell_Rejected
            
            cell.nameLb.text = "\(name)".uppercased()
            cell.dateLb.text = "\(Users["date"] as? String ?? "No Date")"
            cell.priceLb.text = " \(da["offer_amount"] as! Int)"
            //cell.viewOfferBt.text = ""
            
            if indexPath.row == 0
            {
                cell.cont_VIew.setSpecificRoundCorners([.topLeft,.topRight], radius: 10)
            }
            
            if indexPath.row == self.offerListdata.count - 1
            {
                cell.cont_VIew.setSpecificRoundCorners([.bottomLeft,.bottomRight], radius: 10)
            }
            
            return cell
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
        
    {
        return 87
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("oko",self.propetyTyp)
        
        self.listTableView.deselectRow(at: indexPath, animated: true)
        
        let da = self.offerListdata[indexPath.row] as! [String:Any]
        let Users = da["users"] as! [String:Any]
        let Ofamt = da["offer_amount"] as? Int ?? 0
        let Deamt = da["listing_amount"] as? Int ?? 0
        let name = "\(Users["first_name"] as! String) \(Users["last_name"] as! String)"
        let reqID = da["id"] as? Int ?? 0
        let tenentId = Users["id"] as? Int ?? 0
        
        
       
        let stat = da["deal_confirmation"] as! Int
        
        if self.propetyTyp == "1" //sell
        {

            let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KX_ConfirmDealPopUp_Buy") as! KX_ConfirmDealPopUp_Buy
            
            vc.fromNameLB_From = name
            vc.dealAmountLB_From = "\(Deamt)"
            vc.offerAmountLB_From = "\(Ofamt)"
            
            
            vc.PropID = propetyId
            vc.ReqID = "\(reqID)"
            vc.UserID = "\(tenentId)"
            vc.status = stat
            vc.delegate = self
            vc.Typeof = "1"
            self.present(vc, animated: true, completion: nil)

        }
        else
        {
            let DateDetails = da["property_rental_details"] as? [String:Any] ?? [String:Any]()
            print(DateDetails)

            let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KX_ConfirmDealPopUp_Rent") as! KX_ConfirmDealPopUp_Rent
            
            vc.fromNameLB_From = name
            vc.dealAmountLB_From = "\(Deamt)"
            vc.offerAmountLB_From = "\(Ofamt)"
            
            if let movingIn = DateDetails["moving_in"] as? String, movingIn != "" {
                if let movingInFirst = movingIn.components(separatedBy: " ").first{
                    vc.moveingInLB_From = movingInFirst.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                }
            }
            
            if let movingOut = DateDetails["moving_out"] as? String, movingOut != "" {
                if let movingOutFirst = movingOut.components(separatedBy: " ").first{
                    vc.moveingOutLB_From = movingOutFirst.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                }
            }

            
            
            
            vc.delegate = self
            vc.status = stat
            
            vc.PropID = propetyId
            vc.ReqID = "\(reqID)"
            vc.UserID = "\(tenentId)"
            vc.Typeof = "2"
            
            self.present(vc, animated: true, completion: nil)
        }
        
        
             
        
        
    }
    
    //KX_ConfirmDealPopUp
    
    
    @objc func openCloseCellBTNTapped(_ sender:UIButton )
    {
        
        let indexPa = sender.tag
        
        
        let da = self.offerListdata[indexPa] as! [String:Any]
        let Users = da["users"] as! [String:Any]
        let Ofamt = da["offer_amount"] as? Int ?? 0
        let Deamt = da["listing_amount"] as? Int ?? 0
        let name = "\(Users["first_name"] as! String) \(Users["last_name"] as! String)"
        let DateDetails = da["property_rental_details"] as? [String:Any] ?? [String:Any]()
        let reqID = da["id"] as? Int ?? 0
        let tenentId = Users["id"] as? Int ?? 0
        let passingStatus = da["deal_confirmation"] as! Int
        
        if self.propetyTyp == "1" //sell
        {
            
            let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KX_ConfirmDealPopUp_Buy") as! KX_ConfirmDealPopUp_Buy
            
            vc.fromNameLB_From = name
            vc.dealAmountLB_From = "\(Deamt)"
            vc.offerAmountLB_From = "\(Ofamt)"
            
            
            vc.PropID = propetyId
            vc.ReqID = "\(reqID)"
            vc.UserID = "\(tenentId)"
            vc.status = passingStatus
            vc.delegate = self
            vc.Typeof = "1"
            self.present(vc, animated: true, completion: nil)
            
        }
        else
        {
            
            let vc = storyboardOfferList!.instantiateViewController(withIdentifier: "KX_ConfirmDealPopUp_Rent") as! KX_ConfirmDealPopUp_Rent
            
            vc.fromNameLB_From = name
            vc.dealAmountLB_From = "\(Deamt)"
            vc.offerAmountLB_From = "\(Ofamt)"
            
            if let movingIn = DateDetails["moving_in"] as? String, movingIn != "" {
                if let movingInFirst = movingIn.components(separatedBy: " ").first{
                    vc.moveingInLB_From = movingInFirst.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                    print(vc.moveingInLB_From)
                }
            }
            
            if let movingOut = DateDetails["moving_out"] as? String, movingOut != "" {
                if let movingOutFirst = movingOut.components(separatedBy: " ").first{
                    vc.moveingOutLB_From = movingOutFirst.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                    print(vc.moveingInLB_From)
                }
            }

            
//            vc.moveingInLB_From = DateDetails["moving_in"] as? String ?? ""
//            vc.moveingOutLB_From = DateDetails["moving_out"] as? String ?? ""
            
            vc.delegate = self
            vc.status = passingStatus
            
            vc.PropID = propetyId
            vc.ReqID = "\(reqID)"
            vc.UserID = "\(tenentId)"
           // vc.type =
            vc.Typeof = "2"
            self.present(vc, animated: true, completion: nil)
        }
        
    }
}

protocol ReloadOffers{
    func reloadOfferList()
}
