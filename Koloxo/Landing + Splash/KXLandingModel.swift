//
//  LandingModel.swift
//  Koloxo
//
//  Created by Appzoc on 01/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

final public class KXLandingModel {
     var imageURL: String = ""
     var title: String = ""
    
    private init() {
    }
    
    class func getValue(fromObject object: Json) -> KXLandingModel {
        return KXLandingModel.init(object: object)
    }
    
    class func getValue(fromArray array: JsonArray) -> [KXLandingModel] {
        var modelArray: [KXLandingModel] = []
        for item in array {
            modelArray.append(KXLandingModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
    required public init(object: Json){
        imageURL = object["image"] as? String ?? ""
        title = object["title"] as? String ?? ""
    }
}
