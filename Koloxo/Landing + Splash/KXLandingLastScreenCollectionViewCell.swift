//
//  KXLandingLastScreenCollectionViewCell.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 13/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXLandingLastScreenCollectionViewCell: UICollectionViewCell
{
     @IBOutlet var imageview: UIImageView!
    
    @IBOutlet var informationTextView: UITextView!
    
//    override func awakeFromNib() {
//        flashScrollView()
//    }
//
//    func flashScrollView(){
//         let scroll = self.informationTextView as UIScrollView
//         scroll.flashScrollIndicators()
//    }
}
