//
//  StandardUIElements.swift
//  StandardClasses
//
//  Created by Appzoc on 04/01/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

/* For UIButton, UIImageview, UIVIew.
 A - To set currentCornerRadius,borderWidth,borderColor,roundCorners, shadowOffset, shadowColor, shadowOpacity and shadowRadius as Inspectable properties.
 B - To set corner radius for specific corners through function.
 */
/// button


/// view
 class BaseView:UIView{
    var currentCornerRadius:Float?
    
    //MARK:- A - UIView
    
    @IBInspectable var cornerRadius:Float{
        get{
            return Float(self.layer.cornerRadius)
        }
        set{
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderWidth:Float{
        get{
            return Float(self.layer.borderWidth)
        }
        set{
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderColor:UIColor{
        get{
            if let color = self.layer.borderColor{
                return UIColor(cgColor: color)
            }else{
                return UIColor.clear
            }
        }
        set{
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    
    @IBInspectable var roundCorners:Bool{
        get{
            return self.roundCorners
        }
        set{
            self.currentCornerRadius = Float(self.layer.cornerRadius)
            if newValue{
                self.layer.cornerRadius = self.frame.width/2
            }else{
                self.layer.cornerRadius = CGFloat(currentCornerRadius ?? 0)
            }
        }
    }
    
    @IBInspectable var shadowOffset:CGSize{
        get{
            let currentOffset = self.layer.shadowOffset
            // let offset:(width:Int,height:Int) = (Int(currentOffset.width),Int(currentOffset.height))
            return currentOffset
        }
        set{
            self.layer.shadowOffset = newValue
            
        }
    }
    
    @IBInspectable var shadowColor:UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor ?? UIColor.clear.cgColor)
        }
        
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity:Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
        
    }
    
    @IBInspectable var shadowRadius:CGFloat{
        get{
            return self.layer.shadowRadius
        }
        set{
            self.layer.shadowRadius = newValue
        }
    }
    
    
    func setUpShadows(mask: CAShapeLayer){
        self.layer.masksToBounds = false
        self.layer.shadowPath = mask.path
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        layoutSubviews()

    }
    
    //MARK:- B - UIView
    //MARK:- Usage -> view.setSpecificRoundCorners([.topLeft, .bottomLeft], radius: 10)
    //Default Border Color and Width are set.
    func setSpecificRoundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.frame = self.bounds
        borderLayer.path = mask.path
        borderLayer.lineWidth = CGFloat(self.borderWidth)
        borderLayer.strokeColor = self.borderColor.cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(borderLayer)
        setUpShadows(mask: mask)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}


//MARK:- Gradient View

 class BaseGradientView: BaseView {
    
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var centerColor:   UIColor = .gray { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var centerLocation: Double =   0.5 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber,centerLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, centerColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}

// TextView with no edge insets
 class UITextViewFixed: UITextView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}

//MARK:- A - UILabel

class BaseLabel:UILabel{
    var currentCornerRadius:Float?
    
    //MARK:- A - UIView
    
    @IBInspectable var cornerRadius:Float{
        get{
            return Float(self.layer.cornerRadius)
        }
        set{
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderWidth:Float{
        get{
            return Float(self.layer.borderWidth)
        }
        set{
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderColor:UIColor{
        get{
            if let color = self.layer.borderColor{
                return UIColor(cgColor: color)
            }else{
                return UIColor.clear
            }
        }
        set{
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    
    @IBInspectable var roundCorners:Bool{
        get{
            return self.roundCorners
        }
        set{
            self.currentCornerRadius = Float(self.layer.cornerRadius)
            if newValue{
                self.layer.cornerRadius = self.frame.width/2
            }else{
                self.layer.cornerRadius = CGFloat(currentCornerRadius ?? 0)
            }
        }
    }
    
    func setUpShadows(mask: CAShapeLayer){
        self.layer.masksToBounds = false
        self.layer.shadowPath = mask.path
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
    
    func setSpecificRoundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.frame = self.bounds
        borderLayer.path = mask.path
        borderLayer.lineWidth = CGFloat(self.borderWidth)
        borderLayer.strokeColor = self.borderColor.cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(borderLayer)
        setUpShadows(mask: mask)
        
    }
    
}

// Mark: - Textfield
 class BaseTextField: UITextField {
    
    @IBInspectable var isSetBottomBorder: Bool = false {
        didSet {
            setBottomBorder()
            self.borderStyle = .none
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.masksToBounds = false
            self.layer.shadowColor = BottomBorderColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: BottomBorderThick)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }
    
    @IBInspectable var BottomBorderColor: UIColor = .lightGray {
        didSet {
            setBottomBorder()
            self.borderStyle = .none
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.masksToBounds = false
            self.layer.shadowColor = BottomBorderColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: BottomBorderThick)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }

    @IBInspectable var BottomBorderThick: CGFloat = 1.0 {
        didSet {
            //setBottomBorder(color: BottomBorderColor, thickness: newValue)
            self.borderStyle = .none
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.masksToBounds = false
            self.layer.shadowColor = BottomBorderColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: BottomBorderThick)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }

    func setBottomBorder() {
        if isSetBottomBorder {
            self.borderStyle = .none
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.masksToBounds = false
            self.layer.shadowColor = BottomBorderColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: BottomBorderThick)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setBottomBorder()
    }
}


public class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override public func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    override public func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= rightPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            self.layer.borderWidth = 1
            self.borderStyle = .none
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderCornerRadius: CGFloat = 4 {
        didSet {
            self.layer.borderWidth = 1
            self.borderStyle = .none
            self.layer.cornerRadius = borderCornerRadius
        }
    }
    
    @IBInspectable var textPadding: CGFloat = 0 {
        didSet {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: textPadding, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        if let image = rightImage {
            rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            imageView.tintColor = color
            rightView = imageView
        } else {
            rightViewMode = UITextFieldViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
}



