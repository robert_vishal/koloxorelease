//

//  KXNotificationListVC.swift

//  KoloxoScreens

//

//  Created by Appzoc on 13/06/18.

//  Copyright © 2018 AppZoc. All rights reserved.

//



import UIKit

class notificationListCell:UITableViewCell
{
    @IBOutlet var TitleLBL: UILabel!
    @IBOutlet var SubTitleLBL: UILabel!
    @IBOutlet var HourLBL: UILabel!
    @IBOutlet var statusView: UIView!
}

class KXNotificationListVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    var contentArray = [Any]()
    
    var userID = Credentials.shared.userId
    
    
    //Constants:
    var REQUEST_RECEIVED:String = "Request Received"
    var ACCEPT_OFFER:String = "Offer Accept"
    var CONFIRM_DEAL:String = "Confirm Deal"
    var OFFER_RECEIVED:String = "Offer Received"
    var PROPERTY_REJECTED:String = "Property Rejected"
    var PROPERTY_PUBLISHED:String = "Property Published"
    
    
    override func viewDidLoad()
        
    {
        
        print(userID)
        contentTV.rowHeight = UITableViewAutomaticDimension
        contentTV.estimatedRowHeight = 100
        
        guard isLoggedIn() else {return}
        getWebData()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
        
    {
       // guard isLoadedTabSearchChatNotify == .notification else { return }
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(currentTabSet), name: .notificationTabActive, object: nil)
       // guard isLoggedIn() else {return}
       // if isLoadedTabSearchChatNotify == .notification {
            
           // getWebData()
            
      //  }
        
    }
    
    @objc func currentTabSet(){
        getWebData()
    }
    
    
    fileprivate func getWebData(){
        Webservices2.postMethod(url: "NotificatonList", parameter: ["user_id":userID])
        { (isFetched, resultData) in
            
            if isFetched
            {
                print(resultData)
                self.contentArray = [Any]()
                self.contentArray = resultData["Data"] as! [Any]
              //  self.contentArray.reverse()
                self.setData()
            }
        }
    }
    
    func checkNotificationTypeAndSegue(data:[String:Any]){
        
        print(data)
        
        if let event = data["event"] as? [String:Any], let eventName = event["name"] as? String, let eventId = event["id"] as? Int,
            let passingID = data["property_id"] as? Int,let property = data["property"] as? [String:Any],let proprtyType = property["property_type"] as? Int{
            switch eventId{
            case 1:
                print("Request Received")
                let vc = self.storyboardDashboard?.instantiateViewController(withIdentifier: "KXPropertyListViewController") as! KXPropertyListViewController
                //print("PassingID: ",passingID)
                vc.passingPropertyID = passingID
                
                        if let propertyModel = data["property"] as? [String:Any] {
                            if let typeof = propertyModel["property_type"] as? Int {
                                vc.typeOfProperty = typeof // 1-sell, rent - 2
                                print("typeof",typeof)
                            }else {
                                vc.typeOfProperty = 1 // sell
                            }
                            
                }

                self.navigationController?.pushViewController(vc, animated: true)
                
            case 2:
                print("Accept Offer")
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as!KXPropertyDetailsVC
                //print("PassingID: ",passingID)
                   vc.propertyId = passingID
                if let property = data["property"] as? [String:Any],let proprtyType = property["property_type"] as? Int{
                    vc.typeOfProperty = proprtyType
                            if let propertyModel = data["property"] as? [String:Any] {
                                if let typeof = propertyModel["property_type"] as? Int {
                                    vc.typeOfProperty = typeof // 1-sell, rent - 2
                                    print("typeof",typeof)
                                }else {
                                    vc.typeOfProperty = 1 // sell
                                }
                            }
                    
                }
                self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                print("Confirm Deal")
        let vc = self.storyboardOfferList!.instantiateViewController(withIdentifier: "KX_OfferList") as! KX_OfferList
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
                vc.propetyId = passingID.description
                if let property = data["property"] as? [String:Any], let propertyName = property["property_name"] as? String{
                    vc.propetyTittle = propertyName
                }
                self.navigationController?.pushViewController(vc, animated: true)
               
            case 4:
                print("Offer Received")
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
                    if let movingIn = data["moving_in"] as? String, let movingOut = data["moving_out"] as? String {
                        if let inDate = movingIn.components(separatedBy: " ").first, let outDate = movingOut.components(separatedBy: " ").first {
                           print(inDate)
                            print(outDate)
                            vc.moving_inMyrequest = inDate
                            vc.durationMyRequest = outDate
                            if let propertyModel = data["property"] as? [String:Any] {
                                if let typeof = propertyModel["property_type"] as? Int {
                                    vc.typeOfProperty = typeof // 1-sell, rent - 2
                                    print("typeof",typeof)
                                }else {
                                    vc.typeOfProperty = 1 // sell
                                }
                            }
                            
                            print("vc.moving_inMyrequest",inDate)
                            print("vc.durationMyRequest",outDate)
                            
                        }
                        
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            case 5:
                print("Property Rejected")
                let vc = self.storyboardMain!.instantiateViewController(withIdentifier:"KXHistory") as! KXHistory
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
                vc.typeOfProperty = proprtyType
                        if let propertyModel = data["property"] as? [String:Any] {
                            if let typeof = propertyModel["property_type"] as? Int {
                                vc.typeOfProperty = typeof // 1-sell, rent - 2
                                print("typeof",typeof)
                            }else {
                                vc.typeOfProperty = 1 // sell
                            }
                        }
                
                self.navigationController?.pushViewController(vc, animated: true)
            case 6:
                print("Property Published")
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
                        if let propertyModel = data["property"] as? [String:Any] {
                            if let typeof = propertyModel["property_type"] as? Int {
                                vc.typeOfProperty = typeof // 1-sell, rent - 2
                                print("typeof",typeof)
                            }else {
                                vc.typeOfProperty = 1 // sell
                            }
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            case 7:
                print("Property Published")
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
 
                        if let propertyModel = data["property"] as? [String:Any] {
                            if let typeof = propertyModel["property_type"] as? Int {
                                vc.typeOfProperty = typeof // 1-sell, rent - 2
                                print("typeof",typeof)
                            }else {
                                vc.typeOfProperty = 1 // sell
                            }
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            case 8:
                print("Property Published")
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
                
                        if let propertyModel = data["property"] as? [String:Any] {
                            if let typeof = propertyModel["property_type"] as? Int {
                                vc.typeOfProperty = typeof // 1-sell, rent - 2
                                print("typeof",typeof)
                            }else {
                                vc.typeOfProperty = 1 // sell
                            }
                        }
                
                self.navigationController?.pushViewController(vc, animated: true)
               
            default:
                print("Default Case")
                let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                //print("PassingID: ",passingID)
                vc.propertyId = passingID
             
                        if let propertyModel = data["property"] as? [String:Any] {
                            if let typeof = propertyModel["property_type"] as? Int {
                                vc.typeOfProperty = typeof // 1-sell, rent - 2
                                print("typeof",typeof)
                            }else {
                                vc.typeOfProperty = 1 // sell
                            }
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func setData()
    {
        print(self.contentArray.count)
        self.contentTV.reloadData()
    }
    
    
    
    @IBAction func Back_Button(_ sender: Any)
    {
        if isFromNotification {
            isFromNotification = false
            let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
            navigationController?.pushViewController(vc, animated: true)
        }else {
            navigationController?.popViewController(animated: true)
        }
        //self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func Humberger_menu(_ sender: Any)
    {
        showSideMenu()
    }
    
    
    
    
    
    
    
    
    
    
    
    func hrsFromdate(dateStr:String) -> String
        
    {
        
        
        
        
        
        let dateString = "\(dateStr) +0000"
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss +zzzz"
        
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss +zzzz"
        
        //print("Dateobj: \(dateFormatter.string(from: dateObj!))")
        
        let date = dateObj
        
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date!)
        
        let minutes = calendar.component(.minute, from: date!)
        
        let seconds = calendar.component(.second, from: date!)
        
        print("hours = \(hour):\(minutes):\(seconds)")
        
        
        
        return "\(hour)"
        
        
        
    }
    
    
    
    func calculateDaysBetweenTwoDates(start: Date, end: Date) -> Int {
        
        
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: start) else {
            
            return 0
            
        }
        
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: end) else {
            
            return 0
            
        }
        
        return end - start
        
    }
    
}



// TableView Delegates

extension KXNotificationListVC:UITableViewDelegate,UITableViewDataSource
    
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationListCell") as! notificationListCell
        if let propertyModel = self.contentArray[indexPath.row] as? [String:Any] {
            if let property = propertyModel["property"] as? [String:Any] {
                var createdDate = propertyModel["created_at"] as? String ?? "2018-06-16 11:21:21"
                //createdDate = getDateString(from: createdDate, format: "yyyy-mm-dd HH:mm:ss", outputFormat: "yyyy-mm-dd HH:mm:ss")
                let today = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC") //TimeZone(abbreviation: "GMT+0:00") //Current time zone
                
                if let date1 = dateFormatter.date(from: createdDate){
                    
                    print("**^ Created Date ",createdDate," Date 1 : ",date1.description," Date 2 : ",Date().description)
                    let daycont = DateFormatting.getDifferenceBetweenTwoDates(date1: date1, date2: Date())//self.calculateDaysBetweenTwoDates(start: date1!, end: today) * 24
                    cell.HourLBL.text = "\(daycont)" //self.hrsFromdate(dateStr: createdDate)
                }
                cell.TitleLBL.text = property["property_name"] as? String ?? "No Property Name"
                cell.SubTitleLBL.text = propertyModel["notification_message"] as? String ?? "No Property Name"
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let propertyModel = self.contentArray[indexPath.row] as? [String:Any], let passingID = propertyModel["property_id"] as? Int {
            checkNotificationTypeAndSegue(data: propertyModel)
            /*
            let vc = self.storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
            //print("PassingID: ",passingID)
            vc.propertyId = passingID
            self.navigationController?.pushViewController(vc, animated: true)
            */
            
            
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 87
//    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            print("ok")
            let propertyModel = self.contentArray[indexPath.row] as! [String:Any]
            let id = propertyModel["id"] as? Int ?? 0
            //print(id)
            Webservices.postMethod(url: "NotificatonDelete", parameter: ["notification_id":id])
            { (isFetched, resultData) in
                if isFetched
                {
                    self.getWebData()
                }
            }
        }
    }
}

