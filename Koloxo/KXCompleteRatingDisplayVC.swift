//
//  KXCompleteRatingDisplayVC.swift
//  Koloxo
//
//  Created by Appzoc on 17/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import UIKit
import FloatRatingView

class KXCompleteRatingDisplayVC: UIViewController {
    
    
    @IBOutlet var tableRef: UITableView!
    
    @IBOutlet var myRatingAggregateView: FloatRatingView!
    @IBOutlet var myPropertyAggregateView: FloatRatingView!
    
    @IBOutlet var myRatingLBL: UILabel!
    @IBOutlet var myPropertyRatingLBL: UILabel!
    
    @IBOutlet var sourceEmptyIndicatorLBL: UILabel!
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!
    
    
    var myRatingSource:[KXProfileEditVC.DocData.ReviewData] = []
    var myPropertySource:[KXProfileEditVC.DocData.ReviewData] = []
    var currentSource:[KXProfileEditVC.DocData.ReviewData] = []{
        didSet{
            if currentSource.count == 0{
                DispatchQueue.main.async {
                    self.sourceEmptyIndicatorLBL.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.sourceEmptyIndicatorLBL.isHidden = true
                }
            }
        }
    }
    
    var state:ProfileReviewSectionType = .owner
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpWebcall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
  // to setup chatcount and notification count
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
    
    
    func setUpWebcall(){
        Webservices.getMethodWith(url: "getAllReviews", parameter: ["user_id":"\(Credentials.shared.userId)"]) { (isComplete, json) in
            if isComplete{
                do{
                    let trueData = json["Data"] as? [String:Any]
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode(ReviewDataModel.self, from: jsonData)
                    self.setData(data: parsedData)
                }
                catch{
                    print(error)
                }
            }
        }
    }
    
    func setData(data:ReviewDataModel){
        myRatingAggregateView.rating = (data.ownerAverageRating.first?.average ?? "").getRatingInDouble() ?? 0
        myPropertyAggregateView.rating = (data.tenantAverageRating.first?.average ?? "").getRatingInDouble() ?? 0
        myRatingLBL.text = (data.ownerAverageRating.first?.average ?? "0").formatRatingToDisplay()
        myPropertyRatingLBL.text = (data.tenantAverageRating.first?.average ?? "0").formatRatingToDisplay()
        self.myRatingSource = data.ownerReview
        self.myPropertySource = data.tenantReview
        self.currentSource = self.myRatingSource
        DispatchQueue.main.async{
            self.tableRef.reloadData()
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myReviewTabSelected(_ sender: UIButton) {
        currentSource = myRatingSource
        state = .owner
        DispatchQueue.main.async {
            self.tableRef.reloadData()
        }
    }
    
    @IBAction func myPropertiesTabSelected(_ sender: UIButton) {
        currentSource = myPropertySource
        state = .tenant
        DispatchQueue.main.async {
            self.tableRef.reloadData()
        }
    }
    
  // to add action on tabbar
    
//    @IBAction func Humberger_menu(_ sender: Any)
//    {
//        showSideMenu()
//        //        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        //        self.present(sideMenuVC, animated: true, completion: nil)
//    }
//
//    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton)
    {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        isLoadedTabSearchChatNotify = .chat
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton)
    {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    
    
    
    
    struct ReviewDataModel:Codable{
        let ownerReview:[KXProfileEditVC.DocData.ReviewData]
        let tenantReview:[KXProfileEditVC.DocData.ReviewData]
        let ownerAverageRating:[RatingAverage]
        let tenantAverageRating:[RatingAverage]
        
        private enum CodingKeys: String, CodingKey{
            case ownerReview = "reviews_as_owner", tenantReview = "reviews_as_tenant", ownerAverageRating = "owner_total_average", tenantAverageRating = "tenant_total_average"
        }
        
        struct RatingAverage: Codable{
            let average:String?
            private enum CodingKeys: String, CodingKey{
                case average = "total_average"
            }
        }
    }
    
    
}

extension KXCompleteRatingDisplayVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXRatingAndReviewsTVC") as!  KXRatingAndReviewsTVC
        cell.configureWith(data: currentSource[indexPath.row], state: self.state)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 219
    }
    
}
