//
//  AppDelegate.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 12/03/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import GoogleMaps
import DropDown
import IQKeyboardManagerSwift
import FacebookCore
import UserNotifications
import GooglePlaces
import FacebookLogin
import FBSDKLoginKit
import SideMenu
// newnew
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate
{
    var window: UIWindow?
    enum NotificationType: String {
        case request_contact
        case request_offer
        case confirm_deal
        case decline_contact_request
        case rate_review
        case common
        
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // for clearing all the badge numbers - added this code for temparary usage, can be commented
        application.applicationIconBadgeNumber = 0
        
        DropDown.startListeningToKeyboard()
        IQKeyboardManager.sharedManager().enable = true
        application.isIdleTimerDisabled = true
        //application.isStatusBarHidden = true
        
        SideMenuManager.default.menuFadeStatusBar = false
        
        Chat.initializeChat()
        setupForDevice()
        GMSServices.provideAPIKey("AIzaSyA83Iv-F8dt9XcvTyPdxuqN4jcOi1MOKQk")
        GMSPlacesClient.provideAPIKey("AIzaSyDYSe0A9O1BBINQQ6kA5vVCgUf5GzRbmxA")
        GMSServices.provideAPIKey("AIzaSyDYSe0A9O1BBINQQ6kA5vVCgUf5GzRbmxA")

        if #available(iOS 10, *)
        {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else if #available(iOS 9, *)
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
        if let info = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            
            segueToNotificationInBackground(info: info)
        }
        
      return  SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
    }
    
     func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let appId = SDKSettings.appId
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" { // facebook
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return false
    }
    

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        BaseMemmory.write(object: deviceTokenString, forKey: "deviceTocken")
        Credentials.shared.deviceTocken = deviceTokenString
        print("APNs device token: \(deviceTokenString)")
        
    }
    
    internal func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return SDKApplicationDelegate.shared.application(application, open: url)
    }


    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        BaseMemmory.write(object: "123456", forKey: "deviceTocken")
        if let deviceTockenTemp = BaseMemmory.read(forKey: "deviceTocken") as? String {
            Credentials.shared.deviceTocken = deviceTockenTemp
        }else{
            Credentials.shared.deviceTocken = "123456"
        }
        print("APNs device token Error: \(error)")
        
    }
    
    func setupForDevice(){
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            SessionStore.current.currentDevice = .iPhone
           print("Running on Ipad")
        }else
        {
            SessionStore.current.currentDevice = .iPhone
            print("Running on IPhone")
        }
    }
    
    
    fileprivate func segueToNotificationInBackground(info:[String: AnyObject]){
        //showBanner(message: "Notificaiton Received")
        guard isLoggedIn() else { return }
        
        if let type = info["notification_type"] as? String, let notificationType = NotificationType(rawValue: type) {
            
            switch notificationType{
            case .common:
                print("common")
                segueToMainAsFallback()
            case .confirm_deal:
                print("Confirm-deal")
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                        vc.propertyId = propertyId
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }else {
                    // if property_id key is not present
                    segueToMainAsFallback()
                }
            case .decline_contact_request:
                print("Decline-contact-request")
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                        vc.propertyId = propertyId
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }else {
                    // if property_id key is not present
                    segueToMainAsFallback()
                }
            case .rate_review:
                print("Rate-review")
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                        vc.propertyId = propertyId
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }else {
                    // if property_id key is not present
                    segueToMainAsFallback()
                }
            case .request_contact:
                print("Request Contact")
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXPropertyListViewController") as! KXPropertyListViewController
                        vc.passingPropertyID = propertyId
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }else {
                    // if property_id key is not present
                    segueToMainAsFallback()
                }
            case .request_offer:
                print("Request offer")
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Offerlist", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KX_OfferList") as! KX_OfferList
                        //TODO: ADD Property Name and Property Type
                        vc.propetyId = propertyId.description
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }else {
                    // if property_id key is not present
                    segueToMainAsFallback()
                }
            }
            
            /// added and admin approved a new property
            /*
            if type != NotificationType.common.rawValue {
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                        vc.propertyId = propertyId
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                        
                    }

                }else {
                    // if property_id key is not present
                    
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        // below code for ipad support.currently it is only of iphone support
                        
                        //        if SessionStore.current.currentDevice == .iPad {
                        //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
                        //        }
                        
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                        vc.segueType = .notification
                        isFromNotification = true
                        isLoadedTabSearchChatNotify = .notification
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()

                    }

                }

            }else {
                
                /// notification_type is "common"
                
                DispatchQueue.main.async {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    //// below code for ipad support.currently it is only of iphone support
                    
                    //        if SessionStore.current.currentDevice == .iPad {
                    //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
                    //        }
                    
                    let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                    vc.segueType = .notification
                    isFromNotification = true
                    isLoadedTabSearchChatNotify = .notification
                    let navigationController = UINavigationController.init(rootViewController: vc)
                    navigationController.isNavigationBarHidden = true
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                    
                }

            }
 */
        }else{
            
            /// not present notification_type key in notification data
            
            DispatchQueue.main.async {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //// below code for ipad support.currently it is only of iphone support
                
                //        if SessionStore.current.currentDevice == .iPad {
                //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
                //        }
                
                let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                vc.segueType = .notification
                isFromNotification = true
                isLoadedTabSearchChatNotify = .notification
                let navigationController = UINavigationController.init(rootViewController: vc)
                navigationController.isNavigationBarHidden = true
                self.window?.rootViewController = navigationController
                self.window?.makeKeyAndVisible()

            }

        }
    }
    
    
    func segueToMainAsFallback(){
        DispatchQueue.main.async {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            // below code for ipad support.currently it is only of iphone support
            
            //        if SessionStore.current.currentDevice == .iPad {
            //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
            //        }
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            isFromNotification = true
            isLoadedTabSearchChatNotify = .notification
            let navigationController = UINavigationController.init(rootViewController: vc)
            navigationController.isNavigationBarHidden = true
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            
        }
    }
    
    
    fileprivate func segueToNotificaion(info:[String: AnyObject]){
        guard isLoggedIn() else { return }
        
        if let type = info["type"] as? String {
            /// added and admin approved a new property
            if type != NotificationType.common.rawValue {
                if let propertyId = info["property_id"] as? Int {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Search", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                        vc.propertyId = propertyId
                        vc.isFromPushNotification = true
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                }else {
                    DispatchQueue.main.async {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        // below code for ipad support.currently it is only of iphone support
                        
                        //        if SessionStore.current.currentDevice == .iPad {
                        //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
                        //        }
                        
                        let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                        vc.segueType = .notification
                        isFromNotification = true
//                        isLoadedTabSearchChatNotify = .notification
                        let navigationController = UINavigationController.init(rootViewController: vc)
                        navigationController.isNavigationBarHidden = true
                        self.window?.rootViewController = navigationController
                        self.window?.makeKeyAndVisible()
                    }
                    
                }
                
            }else {
                DispatchQueue.main.async {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    // below code for ipad support.currently it is only of iphone support
                    
                    //        if SessionStore.current.currentDevice == .iPad {
                    //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
                    //        }
                    
                    let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                    vc.segueType = .notification
                    isFromNotification = true
                    //isLoadedTabSearchChatNotify = .notification
                    let navigationController = UINavigationController.init(rootViewController: vc)
                    navigationController.isNavigationBarHidden = true
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }

            }
        }else {
            DispatchQueue.main.async {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //// below code for ipad support.currently it is only of iphone support
                
                //        if SessionStore.current.currentDevice == .iPad {
                //            storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
                //        }
                
                let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
                vc.segueType = .notification
                isFromNotification = true
//                isLoadedTabSearchChatNotify = .notification
                let navigationController = UINavigationController.init(rootViewController: vc)
                navigationController.isNavigationBarHidden = true
                self.window?.rootViewController = navigationController
                self.window?.makeKeyAndVisible()
                
            }
        }
        /*
        DispatchQueue.main.async {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            //// below code for ipad support.currently it is only of iphone support
//            if SessionStore.current.currentDevice == .iPad {
//                storyBoard = UIStoryboard(name: "Main-iPad", bundle: nil)
//            }
            let vc = storyBoard.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            isFromNotification = true
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let navigation = self.window?.rootViewController as! UINavigationController
            navigation.pushViewController(vc, animated: true)

        }*/

    
    }
    
    fileprivate func showNotificaionBanner(newData: [String: AnyObject]){
    
        //let newData:[String: AnyObject] = notification.request.content.userInfo as! [String: AnyObject]
        //print("NewData",newData as Any)
        let defaultMessage = "Notification Received"
        if let userInfo = newData["aps"] as? [String:AnyObject] {
            if let alertBody = userInfo["alert"] as? String {
                showBanner(title: "", message: alertBody, style: .success)
            }else{
                showBanner(title: "", message: defaultMessage, style: .success)
            }
        }else {
            showBanner(title: "", message: defaultMessage, style: .success)
        }
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Push notification received: willPresent")
        if let userInfo = notification.request.content.userInfo as? [String: AnyObject] {
            showNotificaionBanner(newData: userInfo )
        }else {
            segueToNotificaion(info: [String: AnyObject]())
        }
        NotificationManager.shared.getNotificationCount()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Push notification received: didReceive")
        //showBanner(message: "Push notification received: didReceive")
        if let userInfo = response.notification.request.content.userInfo as? [String: AnyObject] {
            segueToNotificationInBackground(info: userInfo)
            
        }
        
        NotificationManager.shared.getNotificationCount()
     }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Push notification received:")
        
//        let newData:[String: AnyObject] = userInfo as! [String: AnyObject]
//        let details = newData["aps"] as! [String:AnyObject]
//        let alertBody = userInfo["alert"] as! [String:AnyObject]
//        let alertTitle = alertBody["title"] as! String
//        let alertMessage = alertBody["body"] as? String
//        var alertPropertyId: String? = nil
//
//        if let propertyId = newData["property_id"]{
//            alertPropertyId = propertyId as? String
//        }
        
        if ( application.applicationState == UIApplicationState.active)
        {
            // App is foreground and notification is recieved,
            if let newData:[String: AnyObject] = userInfo as? [String: AnyObject] {
                showNotificaionBanner(newData: newData)
            }else {
                
            }
           // segueToNotificaion()
        }
            
        else if( application.applicationState == UIApplicationState.background)
        {
            // App is in background and notification is received,
            // You can fetch required data here don't do anything with UI.
            
            //print("Background")
            if let info = userInfo as? [String: AnyObject] {
                segueToNotificationInBackground(info: info)

            }

        }
        else if( application.applicationState == UIApplicationState.inactive)
        {
            // App came in foreground by used clicking on notification,
            // Use userinfo for redirecting to specific view controller.
            
            //print("Inactive")
            
            if let info = userInfo as? [String: AnyObject] {
                segueToNotificationInBackground(info: info)
                
            }
            
            
        }

        NotificationManager.shared.getNotificationCount()

    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationManager.shared.getNotificationCount()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    

}

