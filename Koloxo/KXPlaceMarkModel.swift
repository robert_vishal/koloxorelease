//
//  KXPlaceMarkModel.swift
//  Koloxo
//
//  Created by Appzoc on 05/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//


import Foundation

public class KXPlaceMarkerModel
{
    var id:Int = 0
    var name:String = ""
    var description:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var spec:String = ""
    var location:String = ""
    var file_upload:String = ""
    var updated_at:String = ""
    var created_at:String = ""
    
    class func getValue(fromObject: Json) -> KXPlaceMarkerModel {
        return KXPlaceMarkerModel(dictionary: fromObject)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXPlaceMarkerModel] {
        var modelArray: [KXPlaceMarkerModel] = []
        for item in fromArray {
            modelArray.append(KXPlaceMarkerModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
    required public init(dictionary: Json) {
        
        id = dictionary["id"] as? Int ?? 0
        name = dictionary["name"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        latitude = dictionary["latitude"] as? String ?? ""
        longitude = dictionary["longitude"] as? String ?? ""
        spec = dictionary["spec"] as? String ?? ""
        location = dictionary["location"] as? String ?? ""
        file_upload = dictionary["file_upload"] as? String ?? ""
        updated_at = dictionary["updated_at"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
    }
    

    
}

