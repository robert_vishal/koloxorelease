//
//  KXSearchModels.swift
//  Koloxo
//
//  Created by Appzoc on 14/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation

class KXMonthsModel {
    var id: Int = 0
    var data: String = ""
    var display_order = ""
    
    init() {
    }
    
    init(object: Json) {
        id = object["id"] as? Int ?? 0
        data = object["data"] as? String ?? ""
        display_order = object["display_order"] as? String ?? ""
    }
    
    class func getValue(fromObject: Json?) -> KXMonthsModel {
        guard let object = fromObject else { return KXMonthsModel() }
        return KXMonthsModel(object: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXMonthsModel] {
        var modelArray: [KXMonthsModel] = []
        for item in fromArray {
            modelArray.append(KXMonthsModel(object: item))
        }
        return modelArray
    }
    
}

