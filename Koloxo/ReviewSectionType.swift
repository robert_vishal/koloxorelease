//
//  ReviewSectionType.swift
//  Koloxo
//
//  Created by Appzoc on 19/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

enum ProfileReviewSectionType:Int{
    case owner = 1
    case tenant = 2
}
