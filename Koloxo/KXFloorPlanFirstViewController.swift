//
//  KXFloorPlanFirstViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 21/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import Photos
import Kingfisher

class Collection_Btn: UICollectionViewCell
{
    @IBOutlet var bgGradientView: BaseGradientView!
    @IBOutlet var facilityLBL: UILabel!
    @IBOutlet var bgGrayView: BaseView!
    
}

class KXFloorPlanFirstViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var selectedIndexPath: IndexPath?
    private var isExpandedCell: Bool = false
    @IBOutlet var gradientView: BaseGradientView!
    
    var Selected_Index = Bool()
    var index = 0
    var passingPropertyID:Int = 0
    var editingMode:Bool = false
    var floorPlanData:PropertyFloorPlanModel?
    var shouldSkipBuildingAddingStep: Bool = false
    
    @IBOutlet weak var facilitiesCollectionRef: UICollectionView!
    @IBOutlet weak var floorPlanCollectionRef: UICollectionView!
    
    @IBOutlet weak var facilitiesCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var floorPlanCollectionHeight: NSLayoutConstraint!
    
    
    fileprivate var selectedIndexPaths: [IndexPath] = []
    fileprivate var imagePicker = UIImagePickerController()
    
    fileprivate var facilitiesData:[KXFacilities] = []
    final var facilityArray = ["AC","Heaters","Playground","Security Guards","Gym","Land","Swimming pool","Fire Extinguisher"]{
        didSet{
            arraySize.removeAll()
            for _ in facilityArray{
                arraySize.append(CGSize.zero)
            }
        }
    }
    
    fileprivate var floorPlanSource:[UIImage] = []
    fileprivate var downloadedSource:[UIImage] = []
    
    fileprivate var arraySize:[CGSize] = [CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39), CGSize(width: 80, height: 39)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //editingMode = true
        imagePicker.delegate = self
        floorPlanCollectionHeight.constant = floorPlanCollectionRef.contentSize.height
        setupWebCall()
        //setUpCollectionLayout()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpCollectionLayout(){
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 12
        layout.minimumInteritemSpacing = 1
        layout.itemSize = CGSize(width: 160, height: 98)
        floorPlanCollectionRef.collectionViewLayout = layout
    }
    
    func setupWebCall(){
        
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let param:[String:String] = ["user_id":"193",
//            "device_type":"ios",
//            "device_token":"123456",
//            "tokenkey":tokenKey,
//            "unix_time":unixTime]
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime]
        
        
        
        Webservices.getMethodWith(url: "getFacilities", parameter:param ) { (isComplete, json) in
            do{
                let tempData = json["Data"] as? [String:Any]
                let trueData = tempData!["facilities"]
                let parsedData = try JSONSerialization.data(withJSONObject: trueData!, options: .prettyPrinted)
                let decoder = JSONDecoder()
                let facilityData = try decoder.decode([KXFacilities].self, from: parsedData)
                self.facilitiesData = facilityData
                self.facilityArray.removeAll()
                for facility in facilityData{
                    self.facilityArray.append(facility.facility)
                }
                self.setUpLabelSize()
                self.facilitiesCollectionRef.reloadData()
                DispatchQueue.main.async {
                    self.facilitiesCollectionHeight.constant = self.facilitiesCollectionRef.contentSize.height
                }
                if self.editingMode{
                    self.editWebCall()
                }
            }catch{
                print(error)
            }
        }
        
//        Webservices.getMethod(url: "get_facilities") { (isCompleted, response) in
//            do{
//                let trueData = response["Data"]
//                let parsedData = try JSONSerialization.data(withJSONObject: trueData!, options: .prettyPrinted)
//                let decoder = JSONDecoder()
//                let facilityData = try decoder.decode([KXFacilities].self, from: parsedData)
//                self.facilityArray.removeAll()
//                for facility in facilityData{
//                    self.facilityArray.append(facility.facilities)
//                }
//                self.setUpLabelSize()
//                self.facilitiesCollectionRef.reloadData()
//                DispatchQueue.main.async {
//                    self.facilitiesCollectionHeight.constant = self.facilitiesCollectionRef.contentSize.height
//                }
//
//            }catch{
//                print(error)
//            }
//        }
        
    }
    
    //MARK:- Edit Webcall
    
    func editWebCall(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let param:[String:String] = ["user_id":"193",
//                                     "device_type":"ios",
//                                     "device_token":"123456",
//                                     "tokenkey":tokenKey,
//                                     "unix_time":unixTime,"propertyid":"132"]
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"\(passingPropertyID)"]
        Webservices.getMethodWith(url: "getFloorplanDetails", parameter: param) { (isComplete, json) in
            debugPrint(json)
            if isComplete{
                do{
                    let tempData = json["Data"] as? [String:Any]
                    let parsedData = try JSONSerialization.data(withJSONObject: tempData, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    let selectedData = try decoder.decode(PropertyFloorPlanModel.self, from: parsedData)
                    self.populateWithData(data: selectedData)
                    self.floorPlanData = selectedData
                }catch{
                    print(error)
                }
            }
        }
        
    }
    
    func populateWithData(data:PropertyFloorPlanModel){
        //Floor plan appendin string - http://koloxo-homes.dev.webcastle.in/
        for item in data.floorPlan{
            if let url = URL(string: baseImageURL+"\(item.floorPlan)"){
//                if let a = UIImage(data: try! Data(contentsOf: url)){
//                    floorPlanSource.append(a)
//                }
                let activityA = ActivityIndicator()
                activityA.start()
                var imageView = UIImageView()
                imageView.kf.setImage(with: url, options: [.requestModifier(getImageAuthorization())]) { (image, error, type, url) in
                    if error == nil && image != nil {
                        if let appendingImage = imageView.image{
                            self.floorPlanSource.append(appendingImage)
                            self.downloadedSource.append(appendingImage)
                            print("Floor Plan Source",self.floorPlanSource)
                        }
                        DispatchQueue.main.async {
                            self.floorPlanCollectionRef.reloadData()
                             self.floorPlanCollectionHeight.constant = self.floorPlanCollectionRef.collectionViewLayout.collectionViewContentSize.height
                        }
                    } else {
                        print(error)
                    }
                    activityA.stop()
                }
            }
        }
        floorPlanCollectionRef.reloadData()
        for (index,item) in facilitiesData.enumerated(){
            for (subIndex,obj) in data.propertyFacility.enumerated(){
                if item.id == obj.facilityID{
                    selectedIndexPaths.append(IndexPath(item: index, section: 0))
                }
            }
        }
        facilitiesCollectionRef.reloadData()
    }
    
    func setUpLabelSize(){
        for item in 0..<facilityArray.count{
            let label = UILabel(frame: CGRect.zero)
            label.text = facilityArray[item]
            var size = label.intrinsicContentSize
            if size.width < 40{
                size.width = 40
            }
            let modifiedSize = CGSize(width: size.width + 20, height: size.height + 20)
            arraySize[item] = modifiedSize
            self.facilitiesCollectionRef.reloadData()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            floorPlanSource.append(pickedImage)
            print(floorPlanSource.count)
            DispatchQueue.main.async {
                self.floorPlanCollectionRef.reloadData()
                self.floorPlanCollectionHeight.constant = self.floorPlanCollectionRef.collectionViewLayout.collectionViewContentSize.height
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Add Image Action
    
    @IBAction func addImageTapped(_ sender: UIButton) {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status{
                case .authorized:
                    print("Authorised")
                case .restricted:
                    print("Restricted")
                case .denied:
                    print("Denied")
                case .notDetermined:
                    print("Not Determined")
            }
        }
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    //MARK:- Validation
    fileprivate func validateData() -> Bool {
        if !floorPlanSource.isEmpty && !selectedIndexPaths.isEmpty{
            return true
        }else {
            
            if floorPlanSource.isEmpty {
                showBanner(message: "Add floor plans")
                return false
            }
            
            if selectedIndexPaths.isEmpty {
                showBanner(message: "Select facilities provided")
                return false
            }
            return false
        }
        
    }
    
    func removeImageBackend(tag:Int){
        let id = self.floorPlanData!.floorPlan[tag].id
        print("ImageID",id)
        Webservices.postMethod(url: "deleteImage/\(id)", parameter: ["slug":"floorPlan"]) { (isCompleted, json) in
            print(json)
        }
    }
    
    //MARK: Remove Image Action
    
    @IBAction func removeButton(_ sender: UIButton) {
        if downloadedSource.contains(floorPlanSource[sender.tag]){
            removeImageBackend(tag: sender.tag)
        }
        floorPlanSource.remove(at: sender.tag)
        floorPlanCollectionRef.reloadData()
        floorPlanCollectionHeight.constant = floorPlanCollectionRef.collectionViewLayout.collectionViewContentSize.height
        
        
    }
    
    //MARK:- Next Button Tapped
    @IBAction func Next_Button(_ sender: Any) {
        
        guard validateData() else { return }
        
        //let homeScreen = storyboardProperty!.instantiateViewController(withIdentifier: "KXFurnitureDetails") as! KXFurnitureDetails
        //KNRoomDescriptionController
        postData { (isComplete) in
            if isComplete{
                let nextScreen = self.storyboardProperty!.instantiateViewController(withIdentifier: "KNRoomDescriptionController") as! KNRoomDescriptionController
                nextScreen.editingMode = self.editingMode
                nextScreen.passingPropertyID = self.passingPropertyID
                nextScreen.shouldSkipBuildingAddingStep = self.shouldSkipBuildingAddingStep

                self.navigationController?.pushViewController(nextScreen, animated: true)
            }
        }
       
    
    }
    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
    }
    
    @IBAction func Back_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func postData(completion:@escaping (Bool)->()){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
//        var param:[String:String] = ["user_id":"193",
//            "device_type":"ios",
//            "device_token":"123456",
//            "tokenkey":tokenKey,
//            "unix_time":unixTime,
//            "property_id":"132"
//        ]
         var param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime, "property_id":"\(passingPropertyID)"]
        
        if editingMode{
            param.updateValue("1", forKey: "edit")
        }
//        var param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,
//                                     "property_id":"\(passingPropertyID)"
//        ]
        
        var dataParam:[String:Data] = [:]
        //dataParam = ["brochure":brochureRent,"deed":titleDeedRent]
        let selectedFacilities = selectedIndexPaths.map({$0.row})
        print("Selected Facilities",selectedFacilities)
        
        for (index,value) in selectedFacilities.enumerated(){
            param.updateValue("\(facilitiesData[value].id)", forKey: "facilities[\(index)]")
        }
        
        print("Param",param)
        var count = 0
        for value in floorPlanSource{
            if downloadedSource.contains(value){
                
            }else{
                dataParam.updateValue(UIImageJPEGRepresentation(value, 0.6)!, forKey: "floorplan_img[\(count)]")
                count += 1
            }
        }
        
        print("Passsing Param",param, "\n Passing Data",dataParam)
        Webservices.postMethodMultiPartImage(url: "addpropertySecondStep",
                                            parameter:param,
                                            dataParameter:dataParam ) { (isCompleted, json) in
                                                print(json)
                                                if isCompleted {
                                                    completion(true)
                                                }else{

                                                    print("Call Failed")
                                                    completion(false)
                                                }
        }
    }
    
}


extension KXFloorPlanFirstViewController: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1{
            return floorPlanSource.count
        }else{
            return facilityArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXFloorPlanCollectionCell", for: indexPath) as! KXFloorPlanCollectionCell
            cell.floorPlanImage.image = floorPlanSource[indexPath.row]
            cell.removeImageButtonRef.tag = indexPath.row
            cell.removeImageButtonRef.clipsToBounds = true
            //cell.removeImageButtonRef.layer.cornerRadius = 15
            cell.contentView.frame = cell.bounds
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Collection_Btn", for: indexPath) as! Collection_Btn
            cell.facilityLBL.text = facilityArray[indexPath.item]
            if selectedIndexPaths.contains(indexPath) {
                cell.facilityLBL.textColor = .white
                cell.bgGradientView.isHidden = false
                cell.bgGrayView.isHidden = true
            }else{
                cell.facilityLBL.textColor = .black
                cell.bgGradientView.isHidden = true
                cell.bgGrayView.isHidden = false
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1{
            
        }else{
            let cell = collectionView.cellForItem(at: indexPath) as! Collection_Btn
            if selectedIndexPaths.contains(indexPath) {
                cell.facilityLBL.textColor = .black
                cell.bgGradientView.isHidden = true
                cell.bgGrayView.isHidden = false
                if let index = selectedIndexPaths.index(of: indexPath) {
                    selectedIndexPaths.remove(at: index)
                }
            }else{
                cell.facilityLBL.textColor = .white
                cell.bgGradientView.isHidden = false
                cell.bgGrayView.isHidden = true
                selectedIndexPaths.append(indexPath)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1{
            return CGSize(width: (collectionView.frame.width-24)/2, height: 98)
        }else{
//            let size = arraySize[indexPath.row]
//            let returnSize = CGSize(width: size.width , height: size.height )
//            return returnSize
            let collectionSize = facilitiesCollectionRef.frame
            return CGSize(width: collectionSize.width/2 - 8, height: 40)  // w:120 h:40
        }
    }
    
    
    

}


