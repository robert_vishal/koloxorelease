//
//  KXMySearchesController.swift
//  Koloxo
//
//  Created by Appzoc on 10/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXMySearchesTableCell: UITableViewCell{
    
    @IBOutlet var buildingImage: UIImageView!
    
    @IBOutlet var propertyName: UILabel!
    
    @IBOutlet var propertyLocation: UILabel!
    
    @IBOutlet var price: UILabel!
    
    @IBOutlet var currencyLBL: UILabel!
    
    func configureWithData(data:[String:Any]){
        propertyName.text = data["name"] as? String
        propertyLocation.text = data["location"] as? String
        price.text = data["price"] as? String
        if let bImage = data["image"] as? String{
            buildingImage.kf.setImage(with: URL(string: "\(baseImageURL)\(bImage)"), placeholder: UIImage(named: "logo-1x"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
//        if let bImage = MySearchesManager.instance.getImageFromDisk(name: data["image"] as! String){
//            self.buildingImage.image = bImage
//        }
    }
    
}

class KXMySearchesController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var dataDictionary:[[String:Any]] = []
    
    @IBOutlet var tableRef: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let fetchedData = MySearchesManager.instance.fetchDataFromStore(){
            dataDictionary = fetchedData
            tableRef.reloadData()
            print("Dictionary Count: \(dataDictionary.count)")
        }
        tableRef.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDictionary.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXMySearchesTableCell") as! KXMySearchesTableCell
        cell.configureWithData(data: dataDictionary[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let selectedData =  dataDictionary[indexPath.row]
        let vc = storyboardSearch?.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
        vc.propertyId = Int(selectedData["id"] as! String)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Tab Bar Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        if isLoggedIn() {
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        if isLoggedIn() {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        if isLoggedIn() {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuBTNTapped(_ sender: UIButton) {
        showSideMenu()
    }
    
}
