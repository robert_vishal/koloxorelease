//
//  KXVerificationSplash.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 05/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXVerificationSplash: UIViewController {
    var timer = Timer()
    var count = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        setTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    @objc func autoScroll()
    {
            timer.invalidate()
            let registerScreen = storyboardMain!.instantiateViewController(withIdentifier: "KXRegisterProfileViewController") as! KXRegisterProfileViewController
            self.navigationController?.pushViewController(registerScreen, animated: true)
        
        
    }
    
    @IBAction func Back_Button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
