//
//  ChatManager.swift
//  KoloxoChat
//
//  Created by Arun Induchoodan on 10/06/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import TwilioChatClient

class ChatManager{
    
    func getAccessToken(identity:String,tokenR: @escaping(String)->()){
        let tokenURL:String = "https://chamoisee-discus-6804.twil.io/chat-token"
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let urlString = "\(tokenURL)?identity=\(identity)&device=\(deviceId)"
        //print("URLString",urlString)
            self.retrieveToken(url: urlString) { (token, identity, error) in
                if let _ = token{
                    tokenR(token!)
                }else{
                    tokenR("")
                }
            }
    }
    
    func createChatClient(with token:String, delegate:TwilioChatClientDelegate?, clientR: @escaping (TwilioChatClient?)->()){
            // Set up Twilio Chat client
            TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: delegate) {
                (result, chatClient) in
                if result.isSuccessful(){
                    clientR(chatClient)
                }else{
                    clientR(nil)
                }
            }
    }
    
    func createOrJoinChannel(channelSend:String, client:TwilioChatClient, channelR: @escaping (TCHChannel?)->()) {
        let channel = "\(channelSend)"
        var returnChannel:TCHChannel? = nil
        
        // Join (or create) the general channel
        let defaultChannel = channel
        if let channelsList = client.channelsList() {
            channelsList.channel(withSidOrUniqueName: defaultChannel, completion: { (result, channel) in
                if let checkedChannel = channel {
//                    self.generalChannel = channel
                    
                    checkedChannel.join(completion: { result in
                        print("Channel -> \(channelSend) joined with result \(result.isSuccessful())")
                        returnChannel = checkedChannel
                        channelR(returnChannel)
                    })
                } else {
                    // Create the general channel (for public use) if it hasn't been created yet
                    channelsList.createChannel(options: [TCHChannelOptionFriendlyName: "\(defaultChannel)", TCHChannelOptionType: TCHChannelType.public.rawValue], completion: { (result, channel) -> Void in
                        //if result.isSuccessful() {
                        if let checkedChannel = channel{
                            checkedChannel.join(completion: { result in
                                checkedChannel.setUniqueName(defaultChannel, completion: { (result) in
                                    print("Channel Unique name set")
                                    returnChannel = checkedChannel
                                    channelR(returnChannel)
                                })
                            })
                        }else{
                            print("Channel creation failed")
                            channelR(nil)
                        }
                       // }
                    })
                }
            })
        }else{
            print("Fetching Channels failed. Client Error.")
            channelR(nil)
        }
    }
    
    
    //MARK:- Private Functions
    fileprivate func retrieveToken(url: String, completion: @escaping (String?, String?, Error?) -> Void) {
        if let requestURL = URL(string: url) {
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: requestURL, completionHandler: { (data, response, error) in
                if let data = data {
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:String]
                        let token = json["token"]
                        let identity = json["identity"]
                        completion(token, identity, error)
                    }
                    catch let error as NSError {
                        completion(nil, nil, error)
                    }
                    
                } else {
                    completion(nil, nil, error)
                }
            })
            task.resume()
        }
        
    }
    
    static func goToChatController(userID:Int, propertyID:Int, requestID:Int, channelID:Int, navigation:UINavigationController){
        var storyBoard = UIStoryboard(name: "Chat", bundle: nil)
        if SessionStore.current.currentDevice == .iPad {
            storyBoard = UIStoryboard(name: "Chat-iPad", bundle: nil)
        }
        let chatVC = storyBoard.instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
        chatVC.channelName = "Koloxo_\(userID)_\(propertyID)_\(requestID)"
        chatVC.channelID = channelID
        chatVC.propertyID = propertyID
        navigation.pushViewController(chatVC, animated: true)
    }
    
    static func goToChatController(channel:String, propertyID:Int, channelID:Int, navigation:UINavigationController){
        var storyBoard = UIStoryboard(name: "Chat", bundle: nil)
        if SessionStore.current.currentDevice == .iPad {
            storyBoard = UIStoryboard(name: "Chat-iPad", bundle: nil)
        }
        let chatVC = storyBoard.instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
        chatVC.channelName = channel
        chatVC.channelID = channelID
        chatVC.propertyID = propertyID
        navigation.pushViewController(chatVC, animated: true)
    }
    
}

class Chat{
    private init(){}
    
    static var instance:Chat = Chat()
    
    var chatClient:TwilioChatClient?
    var unreadCount:Int = 0
    
    static func registerForPush(){
        //https://chamoisee-discus-6804.twil.io/register-binding
        let urlString = "https://chamoisee-discus-6804.twil.io/register-binding"
        let param = ["identity": Credentials.shared.userId,
                     "BindingType" : "apn",
                     "Address" : Credentials.shared.deviceTocken]
        Webservices.postMethodForPush(url: urlString, parameter: param) { (isComplete, json) in
           // if isComplete{
                print("Registered for Twilio Chat Notification \n",json )
          //  }
        }
    }
    
    static func initializeChat(){
        let chatManager = ChatManager()
        if isLoggedIn(), Chat.instance.chatClient == nil{
            print("Chat instance Identity:",Credentials.shared.userId)
            chatManager.getAccessToken(identity: "\(Credentials.shared.userId)") { (token) in
                chatManager.createChatClient(with: token, delegate: nil) { (clientR) in
                    if clientR != nil{
                        Chat.instance.chatClient = clientR
                        
                        print("Chat initialize success.")
                        Chat.getTotalUnreadCount()
                    }else{
                        print("Chat initialize error.")
                        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 5, execute: {
                            Chat.initializeChat()
                        })
                    }
                }
            }
        }
    }
    
    static func getTotalUnreadCount(){   //completion: @escaping (String) -> Void
        
        //getChannels?user_id=155
        if isLoggedIn(){
            let params = ["user_id":"\(Credentials.shared.userId)"]
            Webservices.getMethodWithoutLoading(url: "getChannels", parameter: params, CompletionHandler: { (isComplete, json) in
                if isComplete{
                    print("Channnel Data : ",json)
                    var count:Int = 0
                    if let _client = Chat.instance.chatClient, let channelList = _client.channelsList(){
                        if let channelData = json["Data"] as? [[String:Any]]{
                            for item in channelData{
                                if let channel = item["channel"] as? String{
                                    //print("Channel** ",channel)
                                    channelList.channel(withSidOrUniqueName: channel, completion: { (result, _channel) in
                                        print("Result : ",result.isSuccessful())
                                        if let _ = _channel{
                                            _channel!.getUnconsumedMessagesCount(completion: { (_result, _count) in
                                                count += Int(_count)
                                                self.instance.unreadCount = count
                                                print("Count for Channel : ",channel," = ",_count.description)
                                            })
                                        }
                                    })
                                }
                            }
                        }
                    }
                    
                }
            })
        }
//            for channel in channelList.subscribedChannels(){
//                //print("Channel Name : ",channel.sid)
//                channel.getUnconsumedMessagesCount(completion: { (result, count) in
//                    completion("\(count)")
//                })
//            }
        
        
//            _ = channelList.channel(withSidOrUniqueName: dataForCell.channel, completion: { (result, channel) in
//                if let _ = channel{
//                    channel!.getUnconsumedMessagesCount(completion: { (result, count) in
//                        cell.unreadCount.text = "\(count)"
//                    })
//                }
//            })
        
        //completion("")
    }
    
}
