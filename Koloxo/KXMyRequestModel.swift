////
////  KXRequestListModel.swift
////  Koloxo
////
////  Created by Appzoc on 22/05/18.
////  Copyright © 2018 Appzoc-Macmini. All rights reserved.
////
//
//import Foundation
//
//class KXMyRequestModel {
//    
//    var id:Int = 0
//    var property_id:Int = 0
//    var user_id:Int = 0
//    var status:statusType = .Pending
//    var property:KXRequestModelProperty?
//
//    enum statusType: String {
//        case Pending
//        case Accepted
//    }
//    
//    class func getValue(from object: Json?) -> KXMyRequestModel? {
//        guard let object = object else { return KXMyRequestModel()}
//        return KXMyRequestModel(object)
//    }
//    
////    class func getValue(from array: JsonArray?) -> [KXMyRequestModel] {
////        var modelArray : [KXMyRequestModel] = []
////        guard let array = array else { return modelArray }
////        for item in array{
////            let object = KXMyRequestModel.getValue(from: item)
////            modelArray.append(object!)
////        }
////        return modelArray
////    }
//    
//    required public init(_ object: Json) {
//        id = object["id"] as? Int ?? 0
//        property_id = object["property_id"] as? Int ?? 0
//        user_id = object["user_id"] as? Int ?? 0
//        if object["status"] as? Int ?? 0 == 0 {
//            status = .Pending
//        }else{
//            status = .Accepted
//        }
//        property = KXRequestModelProperty.getValue(from: object["property"] as? Json)
//    }
//    
//    private init(){}
//    
//}
//
//
//
