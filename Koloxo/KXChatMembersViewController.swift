//
//  KXChatMembersViewController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 09/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import TwilioChatClient

class KXChatMembersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var Chatmember_tab: UITableView!
    @IBOutlet var propertyNameLB: UILabel!
    
    @IBOutlet var unreadChatCountLBL: UILabel!
    @IBOutlet var notificationCountLBL: UILabel!


    var chatUsers:[ChannelData] = []
    var passingPropertyID:Int = 59
    
    final var propertyOwnerId: Int = 0
    
    var isDelegated:Bool = false
    var delegate:KXChatDetailVC?
    var propertyName = ""
    
    var ChatListdata = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addFillerdData()
        
        // Do any additional setup after loading the view.
        
        self.propertyNameLB.text = propertyName
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpWebCall()
        self.setUpNotificationLBL()
        self.setUpChatCount()
    }
    
    
    func setUpChatCount(){
        DispatchQueue.main.async {
            self.unreadChatCountLBL.layer.cornerRadius = 9
            self.unreadChatCountLBL.clipsToBounds = true
            //self.unreadChatCountLBL.layer.masksToBounds = true
        }
        
        if Chat.instance.unreadCount > 0{
            unreadChatCountLBL.isHidden = false
            unreadChatCountLBL.text = Chat.instance.unreadCount.description
        }else{
            unreadChatCountLBL.isHidden = true
        }
    }
    
    func setUpNotificationLBL(){
        DispatchQueue.main.async {
            self.notificationCountLBL.layer.cornerRadius = 9
            self.notificationCountLBL.layer.masksToBounds = true
        }
        
        if NotificationManager.shared.currentCount > 0{
            self.notificationCountLBL.isHidden = false
            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
        }else{
            self.notificationCountLBL.isHidden = true
        }
    }
    
//    func addFillerdData(){
//        let nameList = ["Tom","John","Harry"]
//        for i in 0...2{
//            let user = ChatUserModel(userid: i, name: nameList[i], channel: "Koloxo_193_60_120", lastChat: "Hello, how are you?", profileImage: "", unreadChatCount: 3, lastSeen: "Now", channelID: i)
//            chatUsers.append(user)
//        }
//        Chatmember_tab.reloadData()
//    }
    
    //MARK: Previous WebCall
//    func setUpWebcall(){
//        let unixTime = SecureSocket.getTimeStamp()
//        let tokenKey = SecureSocket.getToken(with: unixTime)
//
//        let param = ["propertyID":"\(passingPropertyID)","tokenkey":tokenKey,"unix_time":unixTime]
//        Webservices.getMethodWith(url: "propertyRequestedUsers", parameter: param) { (isComplete,json) in
//            let trueData = json["Data"] as? [String:Any]
//            let tempData = trueData!["requestedUsers"]
//            let decoder = JSONDecoder()
//            do{
//                let jsonData = try JSONSerialization.data(withJSONObject: tempData, options: .prettyPrinted)
//                let parsedData = try decoder.decode([ChatUserModel].self, from: jsonData)
//                self.chatUsers = parsedData
//                self.Chatmember_tab.reloadData()
//            }catch{
//                print(error)
//            }
//        }
//    }
    
    func setUpWebCall(){
        //RequestedUsers?property_id=226
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
//        Webservices.getMethodWith(url: "RequestedUsers", parameter: ["property_id":"\(passingPropertyID)","tokenkey":tokenKey,"unix_time":unixTime]) { (isComplete, json) in
//            let trueData = json["Data"] as? [Any]
//
//            self.ChatListdata = trueData!
//            self.Chatmember_tab.reloadData()
//
//        }
        
        
        Webservices.postMethod(url: "getChatRoom", parameter: ["property_id":passingPropertyID,"tokenkey":tokenKey,"unix_time":unixTime,"owner_id":propertyOwnerId]) { (isComplete, json) in
            print(json)
            if isComplete{
               // let trueData = json["Data"] as? [Any]
                
                let trueData = json["Data"] as? [[String:Any]]
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode([ChannelData].self, from: jsonData)
                    self.chatUsers = parsedData
                    self.Chatmember_tab.reloadData()
                }catch{
                    print(error)
                }
                
                //self.ChatListdata = trueData!
                //self.Chatmember_tab.reloadData()
                self.parseDownloadedData()
            }
        }
    }
    
    func parseDownloadedData(){
        
       
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatUsers.filter({$0.chat_room_members.count > 0}).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! KXChatMemberTableViewCell
        let dataForCell = chatUsers[indexPath.row]
        if dataForCell.chat_room_members.count > 0 {
            
            cell.profileName.text = dataForCell.chat_room_members[1].users.first_name + " " + dataForCell.chat_room_members[1].users.last_name
            cell.chatImage.kf.setImage(with: URL(string:"\(baseImageURL)\(dataForCell.chat_room_members[1].users.image)"), placeholder: UIImage(named: "userPlaceholder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            
            if let channelList = Chat.instance.chatClient?.channelsList() {
                
                _ = channelList.channel(withSidOrUniqueName: dataForCell.channel, completion: { (result, channel) in
                    print("Channel Fetch : ",result.isSuccessful())
                    if let _ = channel{
                        channel!.getUnconsumedMessagesCount(completion: { (result, count) in
                            cell.unreadCount.text = "\(count)"
                            print("Channel  unread count : ",count)
                        })
                        channel!.messages?.getLastWithCount(1, completion: { (result, messages) in
                            if let _ = messages, messages!.count > 0 {
                                cell.lastMessage.text = "\(messages!.first!.body!)"
                                cell.lastSeen.text = "\(FormatDate.formatDateForChat(date: messages!.first!.dateUpdatedAsDate!))"
                            }
                        })
                    }
                })
                
            }
//            cell.lastMessage.text = ""
//            cell.lastSeen.text = ""
//            cell.unreadCount.text = ""
//            cell.TimeView.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isDelegated{
            delegate?.addUserToChannel(user:chatUsers[indexPath.row].chat_room_members[1].users.id)
            print("Added User :\(chatUsers[indexPath.row].chat_room_members[1].users.first_name) ID :\(chatUsers[indexPath.row].chat_room_members[1].users.id)")
            self.navigationController?.popViewController(animated: true)
        }else{
            let vc = self.storyboardChat!.instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
            let dataForCell = chatUsers[indexPath.row]
            vc.channelName = dataForCell.channel
            vc.channelID = dataForCell.id
            vc.propertyID = dataForCell.property_id
            vc.propertyOwnerId = dataForCell.owner_id
            vc.propertyName = self.propertyName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func Back_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_Menu(_ sender: Any) {
        showSideMenu()
    }
    
    @IBAction func Tab_Home(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Tab_Button(_ sender: Any) {
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    struct RequestedUsers: Codable{
        let id:Int
        let name:String
        let image:String
        let mobileNumber:String
        let propertyRequest:[PropertyRequest]
        
        private enum CodingKeys: String, CodingKey{
            case id, name, image, mobileNumber = "mobile_number", propertyRequest = "property_request"
        }
        
        struct PropertyRequest: Codable {
            let propertyID:Int
            let status:Int
            let id:Int
            
            private enum CodingKeys:  String, CodingKey{
                case propertyID = "property_id",status,id
            }
        }
    }
    
}
/*
"id":7,
"owner_id":13,
"property_id":229,
"request_id":5,
"channel":"Taskmario_229_92_118",
"created_at":"2018-03-21 10:13:49",
"updated_at":"2018-03-21 10:13:49",
"property":null,
"chat_room_members":[
{
"id":3,
"chat_room_id":7,
"customer_id":13,
"created_at":"2018-06-07 13:37:48",
"updated_at":"2018-06-07 13:37:48",
"users":{
"first_name":"abc",
"id":13,
"last_name":"def",
"image":"/storage/app/images/user_image/download.jpeg"
}
},
*/

struct ChannelData:Codable{
    let id:Int
    let owner_id:Int
    let property_id:Int
    let channel:String
    let chat_room_members:[Members]
    
    struct Members:Codable {
        let id:Int
        let chat_room_id:Int
        let customer_id:Int
        let users:Users
        
        struct Users:Codable {
            let first_name:String
            let id:Int
            let last_name:String
            let image:String
        }
    }
}

class ChatUserModel:Codable{
    var userid:Int = 0
    var name:String = ""
    var property_id: Int = 0
    var owner_id: Int = 0
    var channelID:Int = 0
    var channel:String = ""
    
    var lastChat:String = ""
    var profileImage:String = ""
    var unreadChatCount:Int = 0
    var lastSeen:String = ""
    
    init(){}
    init(object: Json){
        
    }
    
    class func getValue(fromObject: Json?) -> ChatUserModel {
        guard let object = fromObject else { return ChatUserModel() }
        return ChatUserModel(object: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [ChatUserModel] {
        var modelArray: [ChatUserModel] = []
        for item in fromArray {
            modelArray.append(ChatUserModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
}
