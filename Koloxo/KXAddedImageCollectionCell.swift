//
//  KXAddedImageCollectionCell.swift
//  Koloxo
//
//  Created by Appzoc on 28/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXAddedImageCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var propertyImage: BaseImageView!
    
    
    @IBOutlet weak var deleteImageBTNRef: StandardButton!
    
    
    @IBOutlet weak var documentLabel: UILabel!
    
}
