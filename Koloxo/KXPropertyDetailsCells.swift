//
//  KXPropertyDetailsCells.swift
//  Koloxo
//
//  Created by Appzoc on 30/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit

class KXPropertyDetailsSpacesCVC: UICollectionViewCell {
    @IBOutlet var spaceImage: UIImageView!
    @IBOutlet var spaceTitleLBL: UILabel!
}

class KXPropertyDetailsNewsFeedsCVC: UICollectionViewCell {
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var detailsTextView: UITextView!
    @IBOutlet var timeStampLBL: UILabel!
    
}

class KXPropertyDetailsFacilitiesCVC: UICollectionViewCell {
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var facilitiesImage: UIImageView!
}





class KXPropertyDetailsFurnituresCVC: UICollectionViewCell {
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionTXV: UITextView!
}
class KXPropertyDetailsFurnitureTVC: UITableViewCell {
    
    @IBOutlet var expandCollapseBTN: BaseButton!
    
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var arrowImage: UIImageView!
    @IBOutlet var furnituresCV: UICollectionView!
    @IBOutlet var furnituesPC: UIPageControl!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

extension KXPropertyDetailsFurnituresCVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsFurnituresCVC", for: indexPath) as? KXPropertyDetailsFurnituresCVC else { return UICollectionViewCell() }
        
        return cell
    }
    
    
}

extension KXPropertyDetailsFurnituresCVC: UICollectionViewDelegate, UIScrollViewDelegate {
    
}
