//
//  KXPropertyOwnerProfile.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 26/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXPropertyOwnerProfile: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var PropertyOwnerTable: UITableView!
    var Titlearray = ["Bedroom","Kitchen","Guest Room","Balcony"]
    private var deselectedCell:KXPropertyOwnerProfile?
    //
    private var selectedIndexPath: IndexPath?
    private var isExpandedCell: Bool = false
    
    @IBOutlet var AddProperty_Tab: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! KXReputedTV
       cell.openCloseCellBTN.indexPath = indexPath
        //cell.TitleLabel_Furniture.text = Titlearray[indexPath.row]
        
        cell.openCloseCellBTN.addTarget(self, action:  #selector(openCloseCellBTNTapped(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
      // print(selectedIndexPath == indexPath ? (isExpandedCell ? 285 :  74) :  99)
        return selectedIndexPath == indexPath ? (isExpandedCell ? 300 : 58) : 60
        
    }
    @objc func openCloseCellBTNTapped(_ sender: IPButton) {
        let indexPath = sender.indexPath!
        //        guard let selectedCell = Reputed_table.cellForRow(at: indexPath!) as? KXReputedProfile else { return }
        //        guard let sele
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
        // Reputed_table.reloadData()
        //updateContent(of: selectedCell, byIndexPath: selectedIndexPath!)
        PropertyOwnerTable.beginUpdates()
        //self.rotateArrowIMG(of: selectedCell)
        PropertyOwnerTable.endUpdates()
        
    }
//    @IBAction func Back_Button(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
//
//    }
//    @IBAction func Humberger_Button(_ sender: Any) {
//        let sideMenuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
//
//    }
//    @IBAction func PostProperty_button(_ sender: Any) {
//        let defaults = UserDefaults.standard
//        defaults.set(2, forKey: "Tab_position")
//
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
//        self.navigationController?.pushViewController(vc, animated: true)
//    }

}
