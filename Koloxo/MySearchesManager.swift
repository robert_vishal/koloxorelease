//
//  MySearchesManager.swift
//  Koloxo
//
//  Created by Appzoc on 10/07/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

//Implemented in Dashboard StoryBoard
import Foundation
import UIKit

struct MySearchesManager{
    private init(){}
    
    static var instance:MySearchesManager = MySearchesManager()
    
    var key:String = "MySearches"
    
    func fetchDataFromStore() -> [Dictionary<String,Any>]?{
        if let dictionary = UserDefaults.standard.array(forKey: key) as? [[String:Any]]{
            return dictionary
        }else{
            return nil
        }
    }
    
//    func saveDataToStore(data:[Dictionary<String,Any>]){
//        UserDefaults.standard.setValue(data, forKey: key)
//    }
    
    func removeDataFromStore(){
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    //MARK: Operations
    
    func addDataToDictionary(data:Dictionary<String,Any>){
        if var dictionary = UserDefaults.standard.array(forKey: key) as? [Dictionary<String,Any>]{
            let compareDictionary = dictionary
            if compareDictionary.filter({ let a = $0["id"] as? String
                let b = data["id"] as? String
                return a == b
            }).count == 0{
                dictionary.append(data)
                if dictionary.count > 10{
                    dictionary.remove(at: 0)
                }
                UserDefaults.standard.setValue(dictionary, forKey: key)
            }
        }else{
            var newDictionary = [data]
           // newDictionary.append(data)
            UserDefaults.standard.setValue(newDictionary, forKey: key)
        }
        print("Number of saved searches: \(UserDefaults.standard.array(forKey: key)?.count)")
    }
    
    func saveImageDocumentDirectory(name:String){
        let fileManager = FileManager.default
        let paths =
            (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
        let image = UIImage(named: "\(name)")
        //print(paths)
        let imageData = UIImageJPEGRepresentation(image!, 0.9)
        fileManager.createFile(atPath: paths, contents: imageData, attributes: nil)
    }
    
    private func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImageFromDisk(name:String)->UIImage?{
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(name)
        if fileManager.fileExists(atPath: imagePAth){
            return UIImage(contentsOfFile: imagePAth)
        }else{
            return nil
        }
    }
    
}
