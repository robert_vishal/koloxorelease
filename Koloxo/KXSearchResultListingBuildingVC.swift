//
//  KXSearchResultListingBuildingVC.swift
//  Koloxo
//
//  Created by Appzoc on 15/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
import UIKit
import FloatRatingView

class KXSearchResultListingBuildingTVCHeader: UITableViewCell {
    
    @IBOutlet var iconImage: UIImageView!
    @IBOutlet var favouriteBTN: UIButton!
    @IBOutlet var propertyNameLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var unitCountLBL: UILabel!
    @IBOutlet var availabilityStatusLBL: UILabel!
    @IBOutlet var availabilityIconView: BaseView!
    @IBOutlet var ratingCountLBL: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet weak var propertyView: StandardView!
    @IBOutlet weak var buildingView: BaseView!
    
    // for propertyview
    
    @IBOutlet var PVAvailableView: StandardView!
    @IBOutlet weak var PVpropertyImage: StandardImageView!
    @IBOutlet weak var PVpriceLBL: UILabel!
    @IBOutlet weak var PVAvailableLBL: UILabel!
    @IBOutlet weak var PVratingView: FloatRatingView!
    @IBOutlet weak var PVbedroomLBL: UILabel!
    @IBOutlet weak var PVbathroomLBL: UILabel!
    @IBOutlet weak var PVfavouriteBTN: UIButton!
    @IBOutlet weak var PVpropertyNameLBL: UILabel!
    @IBOutlet weak var PVratingCountLBL: UILabel!
    @IBOutlet weak var PVlocationLBL: UILabel!
    @IBOutlet weak var PVparkingLBL: UILabel!
    @IBOutlet weak var PVparkingImage: UIImageView!
    
    // identifier for property - true for property
    final var isProperty: Bool = false
    
}

class KXSearchResultListingBuildingTVC: UITableViewCell {
    
    
  
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet var propertyImage: BaseImageView!
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet var bedroomCountLBL: UILabel!
    @IBOutlet var bathroomCountLBL: UILabel!
    @IBOutlet var parkingLBL: UILabel!
    @IBOutlet var parkingImage: UIImageView!
}

enum SearchDetailsSeguePropertyType: Int {
    case isProperty
    case isPropertyInsideBuilding
    case none
}


class KXSearchResultListingBuildingVC: KLBaseViewController,ExpyTableViewDataSource,ExpyTableViewDelegate, UITextFieldDelegate
{
    @IBOutlet var contentTV: ExpyTableView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet var cityTF: UITextField!
    
    var expandedSection: Int?
    
    final var isRentalProperty: Bool = false
    final var isFromMap: Bool = true
    
    final var dataSource: [KXSearchBuildingModel] = []
    final var buildingAvailability: BuildingAvailability = .available
    final var parameterReceived: Json = Json()
    final var citySelected: String = ""
    
    fileprivate var dataSourceModified: [KXSearchBuildingModel] = []
    
    fileprivate var availableSource: [KXSearchAvailableModel] = []
    fileprivate var notAvailableSource: [KXSearchAvailableModel] = []
    fileprivate var almostAvailableSource: [KXSearchAvailableModel] = []
    fileprivate var builidngSource: [KXSearchBuildingModel] = []
    // for different types building handling
    fileprivate var availableBuilding:[KXSearchBuildingModel] = []
    fileprivate var notAvailableBuilding:[KXSearchBuildingModel] = []
    fileprivate var almostAvailableBuilding:[KXSearchBuildingModel] = []
    
    // for all properties listing
    fileprivate var buildingListViewAllSource: [KXSearchBuildingModel] = []

    
    private var detailSegueIndexPath: IndexPath?  // used for identifying which property_data/property_id is passed to details page
    private var detailSegueIsProperty: SearchDetailsSeguePropertyType = .none // used for identifying property/property_inside_building type data is passed to details page
    private var searchText: String? = nil
    private var searchIndexPath: IndexPath?
    private var shouldExpandAlways: Bool = true // this property is for showing all the building cell will be in expanded state by default
    private var expandableSections: [Int] = []
    private var isExpandingFirstTime: Bool = true //used for controlling scroll if expandable cell is clicked both in first time/regularly

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        titleLBL.text = citySelected
        cityTF.text = nil
        cityTF.delegate = self
        expandableSections.removeAll()
        
        if isFromMap {
            for item in dataSource {
                if item.property_building_count == 1 {
                    item.isProperty = true
                }else {
                    item.isProperty = false
                    if item.propertySourceInBuilding.first?.id == 0 {
                        //print("AlreadyAppednedZeroth")
                    }else {
                        //print("appendingZeroth")
                        item.propertySourceInBuilding.insert(KXSearchAvailableModel(), at: 0)
                    }
                    

                }
                //print("Multicount",item.propertySourceInBuilding.count)
                dataSourceModified.append(item)
                //print("datasourceCount",dataSource.count)
            }
        }else {
            getSearchDataWeb()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateWithChangesFromDetails), name: .searchSourceInfoChanged, object: nil)


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("willAppear-searchresultbuildingvc")
        if isFromMap {  // no need of reload if came from details page without changing source data
            view.layoutIfNeeded()
            contentTV.reloadData()
            view.layoutIfNeeded()
        }else {
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        if isFromMap {  // no need of reload if came from details page without changing source data
            view.layoutIfNeeded()
            contentTV.reloadData()
            view.layoutIfNeeded()
            isFromMap = false
        }else {
            
        }
        
        expandTableAlways()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //NotificationCenter.default.removeObserver(self)
    }
    
    @inline(__always) func expandTableAlways() {
        BaseThread.asyncMainDelay(seconds: 0.5) {
            if self.shouldExpandAlways {
                if !self.expandableSections.isEmpty {
                    self.expandableSections.forEach({ (currentSection) in
                        self.contentTV.expand(currentSection)
                    })
                }
            }
        }
    }
    
    @objc private func updateWithChangesFromDetails(notification: Notification) {
        print("updateWithChangesFromDetails-KXSearchResultListingBuildingVC")
        
        if let info = notification.userInfo as? Json {
            let propertyId = info["property_id"] as? Int ?? 0
            let wishList = info["wish_list"] as? Int ?? 0
            
            let segueType = info["segue_type"] as? SearchDetailsSeguePropertyType ?? .none
            let buildingId = info["building_id"] as? Int ?? 0
            
            //print("KXSearchResultListingBuildingVC-updateWithChangesFromDetails:",propertyId,":::",wishList,":::",segueType.rawValue,":::",buildingId,":::",detailSegueIndexPath as Any)
            
            // check wheather the selectedIndexpath id is equal to property_id which is from notificaion from details page, if else
            // find the matching property_id in the whole datasource and do the change(if wish_list changed)
            if let indexPath = detailSegueIndexPath {
                if segueType == .isProperty {
                    if dataSourceModified[indexPath.section].propertySourceInBuilding[0].id == propertyId {
                        dataSourceModified[indexPath.section].propertySourceInBuilding[0].wish_list = wishList
                        print("corrected 1")
                    }else {
                        if let currentBuilding = dataSourceModified.filter({$0.id == buildingId}).first {
                            if currentBuilding.propertySourceInBuilding[0].id == propertyId {
                                dataSourceModified.filter({$0.id == buildingId}).first?.propertySourceInBuilding[0].wish_list = wishList
                                print("corrected 1 else case 1")

                            }else {
                                if var currentProperty = currentBuilding.propertySourceInBuilding.filter({$0.id == propertyId}).first {
                                    currentProperty.wish_list = wishList
                                    print("corrected 1 else case 2")

                                }else {
                                    print("corrected 1 else case 3")

                                }
                            }
                        }else {
                            // iterate fulll
                            print("corrected 1 else case 3")

                        }
                    }
                }else if segueType == .isPropertyInsideBuilding {
                    if dataSourceModified[indexPath.section].propertySourceInBuilding[indexPath.row].id == propertyId {
                        dataSourceModified[indexPath.section].propertySourceInBuilding[indexPath.row].wish_list = wishList
                        print("corrected2")

                    }else {
                        if var currentProperty = dataSourceModified[indexPath.section].propertySourceInBuilding.filter({$0.id == propertyId}).first {
                            currentProperty.wish_list = wishList
                            print("corected 2 else case 1")
                        }else {
                            print("corected 2 else case 2")

                        }
                        
                    }
                }else {
                    print("corected 3")

                }
            BaseThread.asyncMain {
                self.contentTV.reloadData()
                self.expandTableAlways()

            }
            
        }
      }
    }
    
    
    @IBAction func searchEditingChanged(_ sender: UITextField) {
        print("searchEditingChanged-KXSearchResultListingBuildingVC:",sender.text as Any)
        searchText = sender.text
        contentTV.reloadData()
        self.view.layoutIfNeeded()
        self.contentTV.setContentOffset(.zero, animated: true)
        expandTableAlways()

    }
    
    
    // generating list from web service if the user comes from filter and not from direct map
    fileprivate func getSearchDataWeb(){
        
        let url = "searchProperty"
        var params = parameterReceived
        if isLoggedIn() {
            params["user_id"] = Credentials.shared.userId
        }
        
        debugPrint("SearchParams",params as AnyObject)
        Webservices2.getMethodWith(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            print("SearchResponse:-",response)
            BaseThread.asyncMain {
                let searchResult = KXSearchListModel.getValue(fromObject: data)
                self.availableSource = searchResult.availables
                self.notAvailableSource = searchResult.not_availables
                self.almostAvailableSource = searchResult.almost_availables
                self.builidngSource = searchResult.buildings
                
                for itemBuilding in self.builidngSource {
                    for itemProperty in itemBuilding.property_buildings {
                        if self.availableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .available
                            self.availableBuilding.append(itemBuilding)
                            break
                        }else if self.almostAvailableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .almostAvailable
                            self.almostAvailableBuilding.append(itemBuilding)
                            break
                        }else if self.notAvailableSource.contains(where: {$0.id == itemProperty.id  }) {
                            itemBuilding.propertyAvailability = .notAvailable
                            self.notAvailableBuilding.append(itemBuilding)
                            break
                        }
                    }
                }
                self.listGenerator()
                
            }
            
        }
        
    }
    
    // generating list from web service if the user comes from filter and not from direct map
    fileprivate func listGenerator() {
        print(" listGenerator")
        buildingListViewAllSource.removeAll()
        for item in availableBuilding {
            item.buildingAvailability = .available
            buildingListViewAllSource.append(item)
            print(item.longitude,"available:Lat",item.latitude)
        }
        
        for item in almostAvailableBuilding {
            item.buildingAvailability = .almostAvailable
            buildingListViewAllSource.append(item)
            print(item.longitude,"almostAvailableBuilding:Lat",item.latitude)
        }
        
        for item in notAvailableBuilding {
            item.buildingAvailability = .notAvailable
            buildingListViewAllSource.append(item)
            print(item.longitude,"notAvailableBuilding:Lat",item.latitude)
        }
        
        for item in buildingListViewAllSource {
            var propertiesInsideBuilding: [KXSearchAvailableModel] = []
            propertiesInsideBuilding.removeAll()
            for item1 in item.property_buildings {
                propertiesInsideBuilding.append(contentsOf: self.availableSource.filter({ $0.id == item1.id  }))
                propertiesInsideBuilding.append(contentsOf: self.almostAvailableSource.filter({ $0.id == item1.id  }))
                propertiesInsideBuilding.append(contentsOf: self.notAvailableSource.filter({ $0.id == item1.id  }))
            }
            item.propertySourceInBuilding = propertiesInsideBuilding
            print(item.latitude,"abc",item.longitude,"buildingLitVIewsouerce",buildingListViewAllSource.count)
        }
        
        self.dataSource.removeAll()
        self.dataSource = buildingListViewAllSource
        for item in dataSource {
        
            if item.property_building_count == 1 {
                item.isProperty = true
            }else {
                item.isProperty = false
                item.propertySourceInBuilding.insert(KXSearchAvailableModel(), at: 0)
            }
            print("Inside PropertyCount",item.propertySourceInBuilding.count)
            dataSourceModified.append(item)
        }

        contentTV.reloadData()
        expandTableAlways()

    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton) {
        showSideMenu()
    }
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    @IBAction func filterTapped(_ sender: UIButton) {
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXFilterViewController") as! KXFilterViewController
        
        vc.parameterReceived = self.parameterReceived
        vc.isMapFilter = false
        vc.isRentalProperty = self.isRentalProperty
        vc.citySelected = self.citySelected
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    @IBAction func PVfavouriteTapped(_ sender: UIButton) {
        if isLoggedIn() {
            if dataSourceModified[sender.tag].propertySourceInBuilding[0].wish_list == 1 {
                deleteWishlist(atIndex: sender.tag)
            }else {
                addToWishList(atIndex: sender.tag)
            }
        }else {
            let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    fileprivate func deleteWishlist(atIndex: Int){
        let url = "deleteWishlist"
        var param = Json()
        
        param.updateValue(Credentials.shared.userId, forKey: "user_id")
        param.updateValue(dataSource[atIndex].id, forKey: "propertyid")
        
        Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
            print("deleted from wishlist")
            guard isSucceeded else { return }
            BaseThread.asyncMain {
                self.dataSourceModified[atIndex].propertySourceInBuilding[0].wish_list = 0
                self.contentTV.reloadData()
                self.expandTableAlways()

               // self.SearchView_tab.reloadData()
                
            }
        }
    }
    
    fileprivate func addToWishList(atIndex: Int){
        let url = "addtoWishlist"
        var param = Json()
        
        param.updateValue(Credentials.shared.userId, forKey: "user_id")
        param.updateValue(dataSource[atIndex].id, forKey: "property_id")
        
        Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
            print("added to wishlist")
            guard isSucceeded else { return }
            BaseThread.asyncMain {
                //self.favouriteBTN.setImage(UIImage(named: "heartFiled"), for: [])
                self.dataSourceModified[atIndex].propertySourceInBuilding[0].wish_list = 1
                self.contentTV.reloadData()
                self.expandTableAlways()

            }
        }
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int
    {
       //print("dataSourceModified",dataSourceModified.count)
        return dataSourceModified.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //print(section,"numberOfRowsInSection",dataSourceModified[section].propertySourceInBuilding.count)
        
        if let searchKey = searchText, searchKey.isNotEmpty {
            
            if dataSourceModified[section].isProperty {
                
                return dataSourceModified[section].propertySourceInBuilding.filter { (propertyItem) -> Bool in
                    let isMatched = propertyItem.property_name.lowercased().range(of: searchKey.lowercased())
                    return isMatched != nil ? true : false
                    }.count
                
            }else {
                
                if dataSourceModified[section].name.lowercased().contains(searchKey.lowercased()) {
                    return dataSourceModified[section].propertySourceInBuilding.count
                }else {
                    return 0
                }
                
                
            }

        }else {
            if dataSourceModified[section].isProperty {
                return dataSourceModified[section].propertySourceInBuilding.count
                
            }else {
                return dataSourceModified[section].propertySourceInBuilding.count
                
            }

        }

        
    }
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        if dataSourceModified[section].isProperty {
            return false
        }else {
            if shouldExpandAlways { // if all sections opened directly(in the case of building)
                if !expandableSections.contains(section) {
                    expandableSections.append(section)
                }
            }
            return true
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if dataSourceModified[indexPath.section].isProperty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXSearchResultListingBuildingTVCHeader") as! KXSearchResultListingBuildingTVCHeader
            cell.buildingView.isHidden = true
            cell.propertyView.isHidden = false
            
            let source = dataSourceModified[indexPath.section].propertySourceInBuilding[0]
            cell.PVpropertyImage.setSpecificRoundCorners([.topLeft,.topRight], radius: 9)
            cell.PVfavouriteBTN.tag = indexPath.section
            
            if isLoggedIn() {
                if source.property_owner.description == Credentials.shared.userId {
                    cell.PVfavouriteBTN.isHidden = true
                }else {
                    cell.PVfavouriteBTN.isHidden = false
                    if source.wish_list == 1 {
                        cell.PVfavouriteBTN.setImage(#imageLiteral(resourceName: "heartFiled"), for: .normal)
                    }else {
                        cell.PVfavouriteBTN.setImage(#imageLiteral(resourceName: "heartCopy"), for: .normal)
                    }

                }
            }else {
                cell.PVfavouriteBTN.isHidden = true
            }
            
            
            
            
            cell.PVpriceLBL.text = " \(source.usd_price.showPriceInDollar())" //showPriceInKilo()

          //  cell.LBL.text =
            cell.PVpropertyNameLBL.text = source.property_name
            
            cell.PVlocationLBL.text = dataSourceModified[indexPath.section].location //source.location //self.propertyLocation
            
            if let bedroomCount = source.property_bedrooms?.data {
                let bedroomCountInt = Int(bedroomCount)!
                if bedroomCountInt > 1 {
                    cell.PVbedroomLBL.text = bedroomCountInt.description + " Bedrooms"
                }else {
                    cell.PVbedroomLBL.text = bedroomCountInt.description + " Bedroom"
                }
            }
            
            if let bathroomCount = source.property_bathrooms?.id {
                let bathroomCountInt = Int(bathroomCount)
                if bathroomCountInt > 1 {
                    cell.PVbathroomLBL.text = bathroomCountInt.description + " Bathrooms"
                }else {
                    cell.PVbathroomLBL.text = bathroomCountInt.description + " Bathroom"
                }
            }
            
            if source.property_ratings_totals.count > 0 {
                cell.PVratingCountLBL.text = source.property_ratings_totals[0].aggregate
                cell.PVratingView.rating = Double(source.property_ratings_totals[0].aggregate)!
            }else{
                cell.PVratingCountLBL.text = 0.0.description
                cell.PVratingView.rating = 0.0
            }
            if !source.property_images.isEmpty {
                let imageURL = source.property_images[0].image
                cell.PVpropertyImage.kf.setImage(with: URL(string: baseImageURLDetail + imageURL), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            }
            cell.PVAvailableLBL.text =  dataSourceModified[indexPath.section].buildingAvailability.rawValue// source.buildingAvailability.rawValue
            
            //switch source.buildingAvailability {
            switch dataSourceModified[indexPath.section].buildingAvailability {
            case .available:
                cell.PVAvailableView.backgroundColor = UIColor.availableGreen
            case .almostAvailable:
                cell.PVAvailableView.backgroundColor = UIColor.almostAvailableYellow
            case .notAvailable:
                cell.PVAvailableView.backgroundColor = UIColor.notAvailableRed
                
            }
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "KXSearchResultListingBuildingTVC") as! KXSearchResultListingBuildingTVC
            
            let source = dataSourceModified[indexPath.section].propertySourceInBuilding[indexPath.row]
           // print(source.property_name)
            if let bedroomCount = source.property_bedrooms?.data {
                let bedroomCountInt = Int(bedroomCount)!
                if bedroomCountInt > 1 {
                    cell.bedroomCountLBL.text = bedroomCountInt.description// + " Bedrooms"
                }else {
                    cell.bedroomCountLBL.text = bedroomCountInt.description// + " Bedroom"
                }
            }
            
            if let bathroomCount = source.property_bathrooms?.id {
                let bathroomCountInt = Int(bathroomCount)
                if bathroomCountInt > 1 {
                    cell.bathroomCountLBL.text = bathroomCountInt.description// + " Bathrooms"
                }else {
                    cell.bathroomCountLBL.text = bathroomCountInt.description// + " Bathroom"
                }
            }

            cell.priceLBL.text = " \(source.usd_price.showPriceInDollar())" //showPriceInKiloWithoutDollar()
            print(source.property_name)
            cell.nameLBL.text = source.property_name
            
            
            if !source.property_images.isEmpty {
                let imageURL = source.property_images[0].image
                cell.propertyImage.kf.setImage(with: URL(string: baseImageURLDetail + imageURL), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            }
            return cell

        }
        
    }
    
    
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell
    {

        let cell = tableView.dequeueReusableCell(withIdentifier: "KXSearchResultListingBuildingTVCHeader") as! KXSearchResultListingBuildingTVCHeader
        
        if !dataSourceModified[section].isProperty {
//            print("Building-expandableCellForSection")
            cell.buildingView.isHidden = false
            cell.propertyView.isHidden = true

            let source = dataSourceModified[section]
            dataSourceModified[section].isProperty = false
            cell.propertyNameLBL.text = source.name //source.propertySourceInBuilding[0].property_name
            cell.locationLBL.text = source.location
            cell.unitCountLBL.text = source.property_building_count.description
            cell.availabilityStatusLBL.text = source.buildingAvailability.rawValue
            
            switch source.buildingAvailability {
            case .available:
                cell.availabilityIconView.backgroundColor = UIColor.availableGreen
            case .almostAvailable:
                cell.availabilityIconView.backgroundColor = UIColor.almostAvailableYellow
            case .notAvailable:
                cell.availabilityIconView.backgroundColor = UIColor.notAvailableRed
                
            }

            if !source.propertySourceInBuilding.isEmpty {
                if !source.propertySourceInBuilding[0].property_ratings_totals.isEmpty {
                    cell.ratingCountLBL.text = source.propertySourceInBuilding[0].property_ratings_totals[0].aggregate
                    cell.ratingView.rating = Double(source.propertySourceInBuilding[0].property_ratings_totals[0].aggregate)!
                }else{
                    cell.ratingCountLBL.text = 0.0.description
                    cell.ratingView.rating = 0.0
                }
            }else{
                cell.ratingCountLBL.text = 0.0.description
                cell.ratingView.rating = 0.0
            }
            cell.iconImage.kf.setImage(with: URL(string: baseImageURLDetail + source.file_upload), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if dataSourceModified[indexPath.section].isProperty {
            detailSegueIsProperty = .isProperty
            detailSegueIndexPath = indexPath
            print("didSelect-Property")
            let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
            vc.propertyId = dataSourceModified[indexPath.section].propertySourceInBuilding[0].id
            vc.isFromSearch = true
            vc.detailSegueIsProperty = .isProperty
            self.navigationController?.pushViewController(vc, animated: true)

        }else {
            if indexPath.row != 0 {
                detailSegueIsProperty = .isPropertyInsideBuilding
                detailSegueIndexPath = indexPath
                print("didSelect-PropertyInside-building")

                let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
                vc.propertyId = dataSourceModified[indexPath.section].propertySourceInBuilding[indexPath.row].id
                vc.isFromSearch = true
                vc.detailSegueIsProperty = .isProperty

                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                detailSegueIsProperty = .none
                detailSegueIndexPath = nil
                isExpandingFirstTime = false
                print("didSelect")
            }

        }
        
        
    }
    
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {

        if shouldExpandAlways {
            if !isExpandingFirstTime {
                if state == .didExpand {
                    tableView.scrollToRow(at: IndexPath.init(row: 0, section: section), at: .top, animated: true)
                }
                isExpandingFirstTime = true
            }else {
                
            }

        }else {
            if state == .didExpand {
                tableView.scrollToRow(at: IndexPath.init(row: 0, section: section), at: .top, animated: true)
            }

        }
        

    }
    

    
}

/*print("Propertyyyyyyyyyy-expandableCellForSection")
 cell.buildingView.isHidden = true
 cell.propertyView.isHidden = false
 
 let source = dataSourceModified[section].propertySourceInBuilding[0]
 cell.PVpropertyImage.setSpecificRoundCorners([.topLeft,.topRight], radius: 9)
 cell.PVpriceLBL.text = source.usd_price.description
 cell.propertyNameLBL.text = source.property_name
 cell.PVlocationLBL.text = source.location //self.propertyLocation
 if source.bedroom_count > 1 {
 cell.PVbedroomLBL.text = source.bedroom_count.description + " Bedrooms"
 }else{
 cell.PVbedroomLBL.text = source.bedroom_count.description + " Bedroom"
 }
 
 if source.bathroom_count > 1 {
 cell.PVbathroomLBL.text = source.bathroom_count.description + " Bathrooms"
 }else{
 cell.PVbathroomLBL.text = source.bathroom_count.description + " Bathroom"
 }
 if !source.property_ratings_totals.isEmpty {
 cell.PVratingCountLBL.text = source.property_ratings_totals[0].aggregate
 cell.PVratingView.rating = Double(source.property_ratings_totals[0].aggregate)!
 }else{
 cell.PVratingCountLBL.text = 0.0.description
 cell.PVratingView.rating = 0.0
 }
 if !source.property_images.isEmpty {
 let imageURL = source.property_images[0].image
 cell.PVpropertyImage.kf.setImage(with: URL(string: baseImageURLDetail + imageURL), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
 }
 cell.PVAvailableLBL.text = source.buildingAvailability.rawValue */
