//
//  KXVerificationController.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 05/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit

class KXVerificationController: UIViewController, InterfaceSettable {
    
    //////// Outlets
    @IBOutlet var firstOtpTF: UITextField!
    @IBOutlet var secondOtpTF: UITextField!
    @IBOutlet var thirdOtpTF: UITextField!
    @IBOutlet var fourthOtpTF: UITextField!
    @IBOutlet var firstOtpView: UIView!
    @IBOutlet var secondOtpView: UIView!
    @IBOutlet var thirdOtpView: UIView!
    @IBOutlet var fourthOtpView: UIView!
    @IBOutlet var toMailLBL: UILabel!
    
    // data receiving properties
    final var otpNumber: String = ""
    final var emailId : String = ""
    final var user_id: Int = 0
    final var reputedStatus:Int = 0
    // vc properties
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(otpNumber.description)
        setUpInterface()
        
    }

    func setUpInterface() {
        firstOtpTF.text = ""
        secondOtpTF.text = ""
        thirdOtpTF.text = ""
        fourthOtpTF.text = ""
        firstOtpView.isHidden = false
        secondOtpView.isHidden = false
        thirdOtpView.isHidden = false
        fourthOtpView.isHidden = false
        toMailLBL.text = "to " + emailId
    }
    override func viewWillAppear(_ animated: Bool) {
        //showBanner(message: otpNumber)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Back_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOtpTapped(_ sender: UIButton) {
        self.postDataWebResend()
    }
    
    @IBAction func clearNumberTapped(_ sender: UIButton) {
        if (fourthOtpTF.text?.isNotEmpty)!{
            fourthOtpTF.text = ""
        }
        else if (thirdOtpTF.text?.isNotEmpty)!{
            thirdOtpTF.text = ""
        }
        else if (secondOtpTF.text?.isNotEmpty)!{
            secondOtpTF.text = ""
        }
        else if (firstOtpTF.text?.isNotEmpty)!{
            firstOtpTF.text = ""
        }
        
    }
    
    @IBAction func number_button(_ sender: Any) {
        if (firstOtpTF.text?.isEmpty)!{
            let tag = (sender as AnyObject).tag
            firstOtpTF.text = tag?.description
            firstOtpView.isHidden = true
        }
        else if (firstOtpTF.text?.isNotEmpty)! && (secondOtpTF.text?.isEmpty)! {
            let tag = (sender as AnyObject).tag
            secondOtpTF.text = tag?.description
            secondOtpView.isHidden = true

        }
        else if (firstOtpTF.text?.isNotEmpty)! && (secondOtpTF.text?.isNotEmpty)! && (thirdOtpTF.text?.isEmpty)! {
            let tag = (sender as AnyObject).tag
            thirdOtpTF.text = tag?.description
            thirdOtpView.isHidden = true

        }
        else if (firstOtpTF.text?.isNotEmpty)! && (secondOtpTF.text?.isNotEmpty)! && (thirdOtpTF.text?.isNotEmpty)! && (fourthOtpTF.text?.isEmpty)! {
            let tag = (sender as AnyObject).tag
            fourthOtpTF.text = tag?.description
            fourthOtpView.isHidden = true

            let otpFullText = firstOtpTF.text! + secondOtpTF.text! + thirdOtpTF.text! + fourthOtpTF.text!
            
            if otpFullText == otpNumber {
                self.postDataWeb()
            }
            else{
                showBanner(title: "Invalid OTP", message: "Please renter OTP")
                firstOtpTF.text = ""
                secondOtpTF.text = ""
                thirdOtpTF.text = ""
                fourthOtpTF.text = ""
                firstOtpView.isHidden = false
                secondOtpView.isHidden = false
                thirdOtpView.isHidden = false
                fourthOtpView.isHidden = false
            }
        }
    }
    
}

// Mark:- Handling textfield events
extension KXVerificationController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return true
    }
}

extension KXVerificationController
{
    fileprivate func postDataWeb() {
        let otpVerifyURL = "otp_verify"
        var parameter = Json()
        parameter.updateValue("\(self.user_id)", forKey: "user_id")
        parameter.updateValue(otpNumber, forKey: "otp")
        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
       
        let unixTime = Date().millisecondsSince1970
        print("UnixTime",unixTime.description)
        let hashKeyString = "appzoc-koloxohomes-appwebcastle" + unixTime.description
        print(hashKeyString)
        let tockenKey = generateMD5(from: hashKeyString)
        parameter.updateValue(tockenKey, forKey: "tokenkey")
        parameter.updateValue(unixTime, forKey: "unix_time")
        
        print(parameter)
        
        Webservices2.postMethod(url: otpVerifyURL, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["Data"] as? Json, !data.isEmpty else { return }
            let signupdetails = KXFbVerificationModel.getValue(fromObject: data)
            debugPrint("otp_verify response:",response as AnyObject)
            
            BaseMemmory.write(object: signupdetails.user_id.description, forKey: "userId")
            BaseMemmory.write(object: signupdetails.first_name, forKey: "firstName")
            BaseMemmory.write(object: signupdetails.last_name, forKey: "lastName")
            BaseMemmory.write(object: signupdetails.country_residence, forKey: "location")
            BaseMemmory.write(object: signupdetails.mobile_number, forKey: "mobile_number")
            BaseMemmory.write(object: self.reputedStatus, forKey: "reputed")

            Credentials.shared.userId = signupdetails.user_id.description
            Chat.registerForPush()
            Credentials.shared.emailId = signupdetails.email.description
            Credentials.shared.firstName = signupdetails.first_name
            Credentials.shared.lastName = signupdetails.last_name
            Credentials.shared.mobile = signupdetails.mobile_number.description
//            Credentials.shared.country = signupdetails.country_residence
            Credentials.shared.profileImage = signupdetails.image
            Credentials.shared.reputed = self.reputedStatus.description

            Chat.initializeChat()
            
            BaseThread.asyncMain {
                //self.segueHome()
                self.segueToVerificationSplash()
            }
        }
    }
    
    
    fileprivate func segueToVerificationSplash(){
        let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXVerificationSplash") as! KXVerificationSplash
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func segueHome(){
        let vc = self.storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    fileprivate func postDataWebResend() {
        let resendOtpUrl = "resend_otp"
        
        var parameter = Json()
        parameter.updateValue(emailId, forKey: "email")
        parameter.updateValue(otpNumber, forKey: "otp")
        parameter.updateValue(user_id, forKey: "user_id")

        parameter.updateValue(Credentials.shared.deviceType, forKey: "device_type")
        parameter.updateValue(Credentials.shared.deviceTocken, forKey: "device_token")
        let unixTime = Date().millisecondsSince1970
        let hashKeyString = "appzoc-koloxohomes-appwebcastle" + unixTime.description
        let tockenKey = generateMD5(from: hashKeyString)
        parameter.updateValue(tockenKey, forKey: "tokenkey")
        parameter.updateValue(unixTime, forKey: "unix_time")

        print("parameter:",parameter)
        Webservices2.postMethod(url: resendOtpUrl, parameter: parameter) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["Data"] as? Json, !dataArray.isEmpty else { return }
            let signupdetails = KXResendOtpmodel.getValue(fromObject: dataArray)
            self.otpNumber = signupdetails.otp
            
        }
    }
    
}




