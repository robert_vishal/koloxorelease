//
//  KXPreviousBookingData.swift
//  Koloxo
//
//  Created by Appzoc on 17/01/19.
//  Copyright © 2019 Appzoc-Macmini. All rights reserved.
//

import Foundation

struct KXPreviousBookingData{
    
    var movingIn:String = ""
    var movingOut:String = ""
    
    /*
     id: 286,
     property_id: 1034,
     user_id: 193,
     request_id: 353,
     moving_in: "0000-00-00 00:00:00",
     moving_out: "0000-00-00 00:00:00",
     status: 1,
     type: 0,
     created_at: "2019-01-05 11:48:14",
     updated_at: "2019-01-05 11:49:06",
     deleted_at: null
 */
    
    init(data:[String:Any]){
        if let _inDate = data["moving_in"] as? String{
            movingIn = formatDate(date: _inDate)
        }
        if let _outDate = data["moving_out"] as? String{
            movingOut = formatDate(date: _outDate)
        }
    }
    
    init(startDate:String,endDate:String){
        self.movingIn = startDate
        self.movingOut = endDate
    }
    
    func formatDate(date:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let dateObj = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dateString = dateFormatter.string(from: dateObj)
            return dateString
        }else{
            return ""
        }
    }
    
}
