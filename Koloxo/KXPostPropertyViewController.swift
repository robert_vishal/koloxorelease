//
//  ViewController.swift
//  collapasbleTbale
//
//  Created by admin on 27/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

//post-property-1

import UIKit
import SideMenu
import MobileCoreServices
import DatePickerDialog
import Foundation
import CoreMIDI

var availableDateGB: String = ""
var availableDateDisplayGB: String = ""
class KXPostPropertyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var questinLbl: UILabel!
    @IBOutlet var addPropertyTitleLBL: UILabel!
    
    var passingPropertyID:Int = 0
    var editingMode:Bool = false
    
    //MARK: Added for General
    var currentSourceType:SlideDropDownTypePostProperty = .currency
    //Source Model
    var currencySource:[KXCurrency] = []
    var propertySource:[KXPropertyType] = []
    var ageBuildingSource:[KXAgeBuilding] = []
    var elevatorSource:[KXElevator] = []
    var buildingFloorSource:[KXBuildingFloor] = []
    var buyRentType:[KXBuyRentType] = []        //Clarify
    var bedroomSource:[KXPostPropertyGeneral] = []
    var bathroomSource:[KXPostPropertyGeneral] = []
    
    // added new properties
    private var archetectureTypeSource:[KXPostPropertySideOptionModel] = []
    private lazy var selectedArchetectureTypeR: String = "Select"
    private lazy var selectedArchetectureTypeB: String = "Select"
    private var waterHeatingSource:[KXPostPropertySideOptionModel] = []
    private lazy var selectedWaterHeatingR: String = "Select"
    private lazy var selectedWaterHeatingB: String = "Select"
    private var energySource:[KXPostPropertySideOptionModel] = []
    private lazy var selectedEnergyR: String = "Select"
    private lazy var selectedEnergyB: String = "Select"

    //Send Data
//    var sendCurrency:KXCurrency = KXCurrency(toSend: "")
//    var sendProperty:KXPropertyType = KXPropertyType(toSend: "")
    var isBrochure:Bool = true
    var editingFillData:PropertyDetailModel?
    
    //MARK:- Selected Values
    var propertyTitleBuy = ""
    var addedPriceBuy = ""
    var requiredDepositBuy = ""
    var floorSizeBuy = ""
    var availableDate:String = ""
    var priceNegotiableBuy:Bool = false{ didSet{tableView.reloadData()}}
    var reuptedProfileBuy:Bool = false{ didSet{tableView.reloadData()}}
    var brochureBuy:Data = Data()
    var titleDeedBuy:Data = Data()
    
    var selectedCurrencyBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedPropertyTypeBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedBuildingAgeBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedElevatorBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedFloorNumberBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedBedroomBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedBathroomBuy:IndexPath?{ didSet{tableView.reloadData()}}
    var descriptionBuy:String = ""
    var brochureLinkBuy = ""
    var titleDeedLinkBuy = ""
    
    var fileNameBrochure = "Brochure"
    var fileNameTitleDeed = "Titledeed"

    var propertyTitleRent = ""
    var addedPriceRent = ""
    var requiredDepositRent = ""
    var floorSizeRent = ""
    var priceNegotiableRent:Bool = false{ didSet{tableView.reloadData()}}
    var reuptedProfileRent:Bool = false{ didSet{tableView.reloadData()}}
    var brochureRent:Data = Data()
    var titleDeedRent:Data = Data()
    
    var selectedCurrencyRent:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedPropertyTypeRent:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedBuildingAgeRent:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedElevatorRent:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedFloorNumberRent:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedBedroomRent:IndexPath?{ didSet{tableView.reloadData()}}
    var selectedBathroomRent:IndexPath?{ didSet{tableView.reloadData()}}
    var descriptionRent:String = ""
    var brochureLinkRent = ""
    var titleDeedLinkRent = ""
    
    var isFrist = false // true -> Buy, false -> Rent
    var isReputedYes = false
    
    var propertyCVData:Data = Data()
    var titleDeedData:Data = Data()
    
    let maxHeaderHeight: CGFloat = 130
    let minHeaderHeight: CGFloat = 80
    
    private lazy var fileNamePlaceHolder = "File Name"
    //private lazy var excludedPropertyTypeIds: [Int] = [2] // this is for hiding floorselection view if vill/Indipendat house is selected
    
    
    private var shouldSkipBuildingAddingStep: Bool = false
    private lazy var priceHint = "Price/Month*"
    private lazy var depositHint = "Required Deposit*"

    var previousScrollOffset:CGFloat = 0
    
    fileprivate var selectPlaceholder = "Select %^"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //editingMode = true
        tableView.dataSource = self
        tableView.delegate = self
        availableDate = showCurrentDate()
        availableDateGB = availableDate
        availableDateDisplayGB = showCurrentDate(outputFormat: "dd/MM/yyyy")
        setUpWebCall()
        if editingMode{
            DispatchQueue.main.async {
                self.addPropertyTitleLBL.text = "Edit Property"
            }
        }
    
    }
    
   
    func removeTitleDeed(){
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell
        if self.isFrist{
            DispatchQueue.main.async {
                cell.uploadedDeedIconBuy.isHidden = true
                cell.deedGradientViewBuy.isHidden = false
            }
            self.titleDeedLinkBuy = ""
            self.titleDeedBuy = Data()
        }else{
            DispatchQueue.main.async {
                cell.uploadedDeedIcon.isHidden = true
                cell.deedGradientViewRent.isHidden = false
            }
            self.titleDeedLinkRent = ""
            self.titleDeedRent = Data()
        }
        
        if let _ = editingFillData{
            let id = editingFillData!.deed.first!.id
//            let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell
            Webservices.postMethod(url: "deleteImage/\(id)", parameter: ["slug":"titleDeedImage"]) { (isCompleted, json) in
                print(json)
//                if self.isFrist{
//                    DispatchQueue.main.async {
//                        cell.uploadedDeedIconBuy.isHidden = true
//                    }
//                    self.titleDeedBuy = Data()
//                }else{
//                    DispatchQueue.main.async {
//                        cell.uploadedDeedIcon.isHidden = true
//                    }
//                    self.titleDeedRent = Data()
//                }
            }
        }
    }
    
    func removeBrochure(){
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell
        if self.isFrist{
            DispatchQueue.main.async {
                cell.uploadedBrochureIconBuy.isHidden = true
                cell.brochureGradientViewBuy.isHidden = false
            }
            self.brochureLinkBuy = ""
            self.brochureBuy = Data()
        }else{
            DispatchQueue.main.async {
                cell.uploadedBrochureIcon.isHidden = true
                cell.brochureGradientVIewRent.isHidden = false
            }
            self.brochureLinkRent = ""
            self.brochureRent = Data()
        }
        
        if let _ = editingFillData{
            let id = editingFillData!.brochure.first!.id
            //let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell
            Webservices.postMethod(url: "deleteImage/\(id)", parameter: ["slug":"propertyBrochure"]) { (isCompleted, json) in
                print(json)
                
//                if self.isFrist{
//                        DispatchQueue.main.async {
//                            cell.uploadedBrochureIconBuy.isHidden = true
//                        }
//                        self.brochureBuy = Data()
//                    }else{
//                        DispatchQueue.main.async {
//                            cell.uploadedBrochureIcon.isHidden = true
//                        }
//                        self.brochureRent = Data()
//                }
            }
        }
    }
    
    func showBrochure(){
        if isFrist{
            let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
            vc.link = brochureLinkBuy
            if editingMode{
                vc.isLocal = false
            }else{
                //vc.dataForLocal = brochureBuy
                vc.isLocal = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
            vc.link = brochureLinkRent
            
            if editingMode{
                vc.isLocal = false
            }else{
                //vc.dataForLocal = brochureRent
                vc.isLocal = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showTitleDeed(){
        if isFrist{
            let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
            vc.link = titleDeedLinkBuy
            if editingMode{
                vc.isLocal = false
            }else{
                vc.isLocal = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
            vc.link = titleDeedLinkRent
            if editingMode{
                vc.isLocal = false
            }else{
                vc.isLocal = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
  
    //MARK:- SetUpWebCall
    
    func setUpWebCall(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime]
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime]
        print("Parameters",param)
        
        Webservices.getMethodWith(url: "getPostPropertyAttributes", parameter:param ) { (isComplete, json) in
            if isComplete{
                let trueData = json["Data"]
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: trueData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode(PropertyDetailOne.self, from: jsonData)
                    print(parsedData.currency.first!)
                    self.currencySource = parsedData.currency
                    self.propertySource = parsedData.building_type
                    self.ageBuildingSource = parsedData.ages
                    self.elevatorSource = parsedData.elevators
                    self.buildingFloorSource = parsedData.floor
                    self.buyRentType = parsedData.saletype
                    self.bedroomSource = parsedData.Bedroom_counts
                    self.bathroomSource = parsedData.Bathroom_count
                    if self.editingMode{
                        self.editingWebCall()
                    }
                }catch{
                    print(error)
                }
            }
        }
        
        Webservices.getMethodWithoutLoading(url: "getDynamicValues") { (isComplete, json) in
            if isComplete {
                guard let trueData = json["Data"] as? Json else { return }
                
                if let energyData = trueData["property_energy_types"] as? JsonArray, !energyData.isEmpty {
                    self.energySource = KXPostPropertySideOptionModel.getValues(objectArray: energyData)
                }
                
                if let waterHeatingData = trueData["water_heating_types"] as? JsonArray, !waterHeatingData.isEmpty {
                    self.waterHeatingSource = KXPostPropertySideOptionModel.getValues(objectArray: waterHeatingData)

                }
                
                if let buildingTypeData = trueData["architecture_types"] as? JsonArray, !buildingTypeData.isEmpty {
                    self.archetectureTypeSource = KXPostPropertySideOptionModel.getValues(objectArray: buildingTypeData)
                }
                
            }else {
                print("Failure-API: getDynamicValues")
            }
        }
        
        
        
    }
    
    
    
    func editingWebCall(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
//        let param:[String:String] = ["user_id":"193","device_type":"ios","device_token":"123456","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"132"]
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime,"propertyid":"\(passingPropertyID)"]
        
        Webservices.getMethodWith(url: "getPropertyDetail", parameter: param) { (isComplete, json) in
            print(json)
            if isComplete{
                do{
                    let trueData = json["Data"] as? [String:Any]
                    let tempData = trueData!["property"]
                    let decoder = JSONDecoder()
                    let jsonData = try JSONSerialization.data(withJSONObject: tempData as Any, options: .prettyPrinted)
                    let parsedData = try decoder.decode(PropertyDetailModel.self, from: jsonData)
                    BaseThread.asyncMain {
                        self.populateWithData(data: parsedData)
                    }
                    
                    print(parsedData)
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func populateWithData(data:PropertyDetailModel){
        if data.propertyType == 1 {
            isFrist = true
            self.tableView.reloadData()
            propertyTitleBuy = data.propertyName ?? ""
            addedPriceBuy = "\(data.price)"
            requiredDepositBuy = data.requiredDeposit
            
//            selectedCurrencyBuy = currencySource.enumerated().map(
//                {(arg) in
//
//                    let (index, value) = arg
//                    if Double(value.id) == Double(data.currencyID){return IndexPath(row: index, section: 0)}
//                    return nil
//                }
//            ).first!
            
            if let brochureData = data.brochure.first?.brochure {
                brochureLinkBuy = brochureData
            }

            if let deedData = data.deed.first?.titleDeed {
                titleDeedLinkBuy = deedData

            }else {
                print("No Deed Data")
                
            }

            
            if let name = data.brochure.first?.file_name_bro {
                fileNameBrochure = name
            }
            if let name = data.deed.first?.file_name_deed {
                fileNameTitleDeed = name
            }

            selectedCurrencyBuy = IndexPath(row: abs(currencySource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.currencyID)
            })!.distance(to: 0)), section: 0)
            
            
            
            priceNegotiableBuy = data.priceNegotiable == 0 ? false : true
            reuptedProfileBuy = data.reputationRequired == 0 ? false : true
            
            selectedPropertyTypeBuy = IndexPath(row: abs(propertySource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.buildingType.id)
            })!.distance(to: 0)), section: 0)
            
            selectedBuildingAgeBuy = IndexPath(row: abs(ageBuildingSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.buildingAge)
            })!.distance(to: 0)), section: 0)
            

            if let indexValue = elevatorSource.index(where: { (item) -> Bool in
                Double(item.data) == Double(data.elevator)
            }) {
                
                selectedElevatorBuy = IndexPath(row: abs(indexValue) , section: 0)
                print("selectedElevatorBuy-Inside",selectedElevatorRent as Any)
            }
            
            
//            selectedElevatorBuy = IndexPath(row: abs(elevatorSource.index(where: { (item) -> Bool in
//                Double(item.id) == Double(data.elevator)
//            })!.distance(to: 0)), section: 0)
            
            print("buildingFloorSourceBuy",buildingFloorSource as AnyObject, buildingFloorSource.count,"aa",data.floorNumber)

            if let indexValue = buildingFloorSource.index(where: { (item) -> Bool in
                Double(item.data) == Double(data.floorNumber)
            }) {
                
                selectedFloorNumberBuy = IndexPath(row: abs(indexValue) , section: 0)
                print("buildingFloorSourceRent-Inside",selectedFloorNumberRent as Any)
            }
            
            selectedBedroomBuy = IndexPath(row: abs(bedroomSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.bedroomCount)
            })!.distance(to: 0)), section: 0)
            
            selectedBathroomBuy = IndexPath(row: abs(bathroomSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.bathroomCount)
            })!.distance(to: 0)), section: 0)
            
            descriptionBuy = data.propertyDescription
            floorSizeBuy = data.floorSize
            
            selectedEnergyB = data.energy_type ?? selectPlaceholder
            selectedWaterHeatingB = data.water_heating ?? selectPlaceholder
            selectedArchetectureTypeB = data.architecture_type ?? selectPlaceholder
            
        }else{
            isFrist = false
            self.tableView.reloadData()
            propertyTitleRent = data.propertyName ?? ""
            availableDate = data.dateMoving ?? showCurrentDate()
            availableDateDisplayGB = data.dateMoving ?? showCurrentDate(outputFormat: "dd/MM/yyyy")
            availableDateGB = availableDate
            addedPriceRent = "\(data.price)"
            requiredDepositRent = data.requiredDeposit
            
            if let brochureData = data.brochure.first?.brochure {
                brochureLinkRent = brochureData
            }else {
                print("No Brouchure Data")
            }
            
            if let deedData = data.deed.first?.titleDeed {
                titleDeedLinkRent = deedData

            }else {
                print("No Deed Data")

            }
            
            if let name = data.brochure.first?.file_name_bro {
                fileNameBrochure = name
            }
            if let name = data.deed.first?.file_name_deed {
                fileNameTitleDeed = name
            }

            selectedCurrencyRent = IndexPath(row: abs(currencySource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.currencyID)
            })!.distance(to: 0)), section: 0)

            priceNegotiableRent = data.priceNegotiable == 0 ? false : true
            reuptedProfileRent = data.reputationRequired == 0 ? false : true
            
            selectedPropertyTypeRent = IndexPath(row: abs(propertySource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.buildingType.id)
            })!.distance(to: 0)), section: 0)
            
            selectedBuildingAgeRent = IndexPath(row: abs(ageBuildingSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.buildingAge)
            })!.distance(to: 0)), section: 0)
            
            
            if let indexValue = elevatorSource.index(where: { (item) -> Bool in
                print("itemmmmm:",item,":",data.elevator)

                return (Double(item.id) == Double(data.elevator))
            }) {
                
                selectedElevatorRent = IndexPath(row: abs(indexValue) , section: 0)
                print("selectedElevatorRent-Inside",selectedElevatorRent as Any)
            }
            
print("=====Index")
            print(elevatorSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.elevator)
            }) as Any)

            if let indexValue = buildingFloorSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.floorNumber)
            }) {
                
                selectedFloorNumberRent = IndexPath(row: abs(indexValue) , section: 0)
                print("buildingFloorSourceRent-Inside",selectedFloorNumberRent as Any)
            }


            selectedBedroomRent = IndexPath(row: abs(bedroomSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.bedroomCount)
            })!.distance(to: 0)), section: 0)
            
            selectedBathroomRent = IndexPath(row: abs(bathroomSource.index(where: { (item) -> Bool in
                Double(item.id) == Double(data.bathroomCount)
            })!.distance(to: 0)), section: 0)
            
            descriptionRent = data.propertyDescription
            floorSizeRent = data.floorSize
            
            selectedEnergyR = data.energy_type ?? selectPlaceholder
            selectedWaterHeatingR = data.water_heating ?? selectPlaceholder
            selectedArchetectureTypeR = data.architecture_type ?? selectPlaceholder

        }
        tableView.reloadData()
    }
    
    //MARK:- Post Data
    
    func postData(completion: @escaping (Bool)->()){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell
        

        addedPriceBuy = cell.propertyPriceTFB.text!.replacingOccurrences(of: ",", with: "")
        requiredDepositBuy = cell.requiredDepositsTFB.text!.replacingOccurrences(of: ",", with: "")
        descriptionBuy = cell.buildingDescriptionB.text
        
        addedPriceRent = cell.propertyPriceTFR.text!.replacingOccurrences(of: ",", with: "")
        requiredDepositRent = cell.requiredDepositsTFR.text!.replacingOccurrences(of: ",", with: "")
        descriptionRent = cell.buildingDescriptionR.text
        floorSizeBuy = cell.floorSizeTFB.text!
        floorSizeRent = cell.floorSizeTFR.text!
        
        var param:[String:String] = [:]
        

        if isFrist {
         param = ["user_id":Credentials.shared.userId,
                                     "property_type":"\(propertySource[selectedPropertyTypeBuy!.row].id)",
                                     "device_type":Credentials.shared.deviceType,
                                     "device_token":Credentials.shared.deviceTocken,
                                     "tokenkey":tokenKey,
                                     "unix_time":unixTime,
            "building_age":"\(ageBuildingSource[selectedBuildingAgeBuy!.row].id)",
            /*"elevator":"\(elevatorSource[selectedElevatorBuy!.row].id)",*/
            /*"floor_no":"\(buildingFloorSource[selectedFloorNumberBuy!.row].id)",*/
            "bathroom":"\(bathroomSource[selectedBathroomBuy!.row].id)",
            "bedroom":"\(bedroomSource[selectedBedroomBuy!.row].id)",
            "description":"\(descriptionBuy)",
            "price":"\(self.addedPriceBuy.trimmingCharacters(in: .whitespacesAndNewlines))",
            "pirce_negotiable":"\(priceNegotiableBuy ? 1 : 0)",
            "deposite":"\(requiredDepositBuy.trimmingCharacters(in: .whitespacesAndNewlines))",
            "reputation":"\(reuptedProfileBuy ? 1 : 0)",
            "currency_id":"\(currencySource[selectedCurrencyBuy!.row].id)",
            "type":"\(buyRentType[0].id)",
            "floor_size":"\(floorSizeBuy)"
        ]
            
            param["architecture_type"] = selectedArchetectureTypeB
            param["water_heating"] = selectedWaterHeatingB
            param["energy_type"] = selectedEnergyB

            
            if let _ = selectedPropertyTypeBuy {
                if self.propertySource[self.selectedPropertyTypeBuy!.row].type == 1 {
                    param["elevator"] = "\(elevatorSource[selectedElevatorBuy!.row].data)"
                    print(param)
                    print()
                    param["floor_no"] = "\(buildingFloorSource[selectedFloorNumberBuy!.row].id)"
                }else {
                    print("Elevator,FloorNo-rNotPresent-Params-Buyyy")
                }
            }
            
            if let _ = cell.propertyTitleTFB.text {
                propertyTitleBuy = cell.propertyTitleTFB.text!
                param["property_name"] = propertyTitleBuy
            }else {
                
            }
            
            if let name = cell.titledeedFileNameLBLB.text {
                param["file_name_deed"] = name
            }

            
        }else{

            availableDate = availableDateGB
            print("availableDateGB:",availableDateGB)
            param = ["user_id":Credentials.shared.userId,
                     "property_type":"\(propertySource[selectedPropertyTypeRent!.row].id)",
                "device_type":Credentials.shared.deviceType,
                "device_token":Credentials.shared.deviceTocken,
                "tokenkey":tokenKey,
                "unix_time":unixTime,
            "building_age":"\(ageBuildingSource[selectedBuildingAgeRent!.row].id)",
            /*"elevator":"\(elevatorSource[selectedElevatorRent!.row].id)",
            "floor_no":"\(buildingFloorSource[selectedFloorNumberRent!.row].id)",*/
            "bathroom":"\(bathroomSource[selectedBathroomRent!.row].id)",
            "bedroom":"\(bedroomSource[selectedBedroomRent!.row].id)",
            "description":"\(descriptionRent)",
            "price":"\(addedPriceRent.trimmingCharacters(in: .whitespacesAndNewlines))",
            "pirce_negotiable":"\(priceNegotiableRent ? 1 : 0)",
            "deposite":"\(requiredDepositRent.trimmingCharacters(in: .whitespacesAndNewlines))",
            "reputation":"\(reuptedProfileRent ? 1 : 0)",
            "currency_id":"\(currencySource[selectedCurrencyRent!.row].id)",
            "type":"\(buyRentType[1].id)",
            "floor_size":"\(floorSizeRent)",
            "available_from":"\(availableDateGB)"
            ]
            
            param["architecture_type"] = selectedArchetectureTypeR
            param["water_heating"] = selectedWaterHeatingR
            param["energy_type"] = selectedEnergyR

            
            if self.propertySource[self.selectedPropertyTypeRent!.row].type == 1 {
                param["elevator"] = "\(elevatorSource[selectedElevatorRent!.row].data)"
                param["floor_no"] = "\(buildingFloorSource[selectedFloorNumberRent!.row].id)"
            }else {
                print("Elevator,FloorNo-rNotPresent-Params-Renttttt")
            }
            
            if let _ = cell.propertyTitleTFR.text {
                propertyTitleRent = cell.propertyTitleTFR.text!
                param["property_name"] = propertyTitleRent
            }else {
                
            }
            
            if let name = cell.titledeedFileNameLBLR.text {
                param["file_name_deed"] = name
            }

            
        }
        
        
        if editingMode{
            param.updateValue("\(passingPropertyID)", forKey: "property_id")
        }
        print("Passing Parameter",param)
        
        var dataParam:[String:Data] = [:]
        
        
        if isFrist{
            dataParam = ["deed":titleDeedBuy]
            
            if (brochureBuy != Data() || brochureLinkBuy != "") {
                dataParam["brochure"] = brochureBuy
                
                if let name = cell.brochureFileNameLBLB.text {
                    param["file_name_bro"] = name
                }

            }
            

            
        }else{
            dataParam = ["deed":titleDeedRent]
            
            if (brochureRent != Data() || brochureLinkRent != "") {
                dataParam["brochure"] = brochureRent
                
                if let name = cell.brochureFileNameLBLR.text {
                    param["file_name_bro"] = name
                }

            }

        }
        
        print("Dataaa Passing Parameter",dataParam["brochure"] as Any)
        
        Webservices.postMethodMultiPartData(url: "add_post_property", parameter:param, dataParameter:dataParam ) { (isCompleted, json) in
            print("Post result json",json)
            if isCompleted {
                let temp = json["Data"] as! [String:Any]
                self.passingPropertyID = temp["propertyid"] as! Int
                completion(true)
            }else{
                
                print("Call Failed")
                completion(false)
            }

        }
    }
    
    @IBAction func back_button(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
//        let sideMenuVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sideMenuNavigation") as! UISideMenuNavigationController
//        self.present(sideMenuVC, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.headerHeightConstraint.constant = self.maxHeaderHeight
        updateHeader()
        
        
    }
    
    //MARK: Set Available Date
    @IBAction func setAvailableDate(_ sender: UIButton) {
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/ yyyy"
                self.availableDate = formatter.string(from: dt)
            }
        }
    }
    
    
    @IBAction func uploadCVTapped(_ sender: UIButton) {
        isBrochure = true
        getDataFromICloud()
    }
    
    
    @IBAction func uploadTitleDeed(_ sender: UIButton) {
        isBrochure = false
        getDataFromICloud()
    }
    
    
    
    @IBAction func deleteUploadedBrochure(_ sender: UIButton) {
        if isFrist{
            brochureBuy = Data()
            removeBrochure()
        }else{
            brochureRent = Data()
            removeBrochure()
        }
        tableView.reloadData()
    }
    
    
    @IBAction func deleteUploadedDeed(_ sender: UIButton) {
        if isFrist{
            titleDeedBuy = Data()
            removeTitleDeed()
        }else{
            titleDeedRent = Data()
            removeTitleDeed()
        }
        tableView.reloadData()
    }
    
    @IBAction func first(_ sender: Any) {
        isFrist = true
        tableView.reloadData()
    }
    
    @IBAction func second(_ sender: Any) {
        isFrist = false
        tableView.reloadData()
    }
    
    //MARK:- Show PDF in WebView
    
    @IBAction func showBrochure(_ sender: UIButton) {
        showBrochure()
    }
    
    @IBAction func showTitleDeed(_ sender: UIButton) {
        showTitleDeed()
    }
    
    
    
    //MARK: Next Button
    @IBAction func nextAction(_ sender: Any) {
        if isFrist {
            guard validateDataBuy() else { return }
        }else{
            guard validateDataRent() else { return }
        }
        postData { (completed) in
            if completed{
                BaseThread.asyncMain {
                    let nextVC = self.storyboardProperty!.instantiateViewController(withIdentifier: "KXFloorPlanFirstViewController") as! KXFloorPlanFirstViewController
                    nextVC.passingPropertyID = self.passingPropertyID
                    nextVC.editingMode = self.editingMode
                    
                    if self.isFrist {
                        if self.propertySource[self.selectedPropertyTypeBuy!.row].type == 0 {
                            nextVC.shouldSkipBuildingAddingStep = true

                        }
                        
//                        if !self.excludedPropertyTypeIds.contains(self.propertySource[self.selectedPropertyTypeBuy!.row].id) {
//                            nextVC.shouldSkipBuildingAddingStep = true
//                        }
                    }else {
                        if self.propertySource[self.selectedPropertyTypeRent!.row].type == 0 {
                            nextVC.shouldSkipBuildingAddingStep = true
                            
                        }
                    }
                    
                    self.editingMode = true
                    self.navigationController?.pushViewController(nextVC, animated: true)

                }
                
            }
        }
    }
    
    @IBAction func CurrencyDrop(_ sender: Any) {
        //slideDropDownSelected = .currency
        
//        let SlideDropdown =  UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "UIsideMenuNavigation") as! UISideMenuNavigationController
//        self.present(SlideDropdown, animated: true, completion: nil)
        currentSourceType = .currency
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Currency")
        selectionController.tableSource = currencySource.map({$0.currency})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    
    @IBAction func buildingTypeTapped(_ sender: UIButton) {
        currentSourceType = .archetectureype
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Building Architecture")
        selectionController.tableSource = archetectureTypeSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    
    @IBAction func TypeDrop(_ sender: Any) {
        currentSourceType = .propertyType
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Property Type")
        selectionController.tableSource = propertySource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
      
    }
    
    @IBAction func TypeBuilding(_ sender: Any) {
        currentSourceType = .buildingAge
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Age of Building")
        selectionController.tableSource = ageBuildingSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func ElevatorsDrop(_ sender: Any) {
        currentSourceType = .elevators
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Number of Elevators")
        selectionController.tableSource = elevatorSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func FloorNumberDrop(_ sender: Any) {
        currentSourceType = .floorNumber
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Number of Floors")
        selectionController.tableSource = buildingFloorSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    
    @IBAction func bedroomDrop(_ sender: UIButton) {
        currentSourceType = .bedrooms
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Number of Bedrooms")
        selectionController.tableSource = bedroomSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func bathroomDrop(_ sender: UIButton) {
        currentSourceType = .bathrooms
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Number of Bathrooms")
        selectionController.tableSource = bathroomSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    
    @IBAction func waterHeatingTapped(_ sender: UIButton) {
        currentSourceType = .waterHeating
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Water Heating type")
        selectionController.tableSource = waterHeatingSource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    @IBAction func energyTapped(_ sender: UIButton) {
        
        currentSourceType = .energy
        let selectionController = self.storyboardChat?.instantiateViewController(withIdentifier: "FLCategorySelectionViewController") as! FLCategorySelectionViewController
        selectionController.setTitle(title: "Select Energy type")
        selectionController.tableSource = energySource.map({$0.data})
        selectionController.delegate = self
        let sideMenuController = UISideMenuNavigationController(rootViewController: selectionController)
        sideMenuController.navigationBar.isHidden = true
        SideMenuManager.default.menuRightNavigationController = sideMenuController
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)

    }
    
    @IBAction func priceNegotiableYes(_ sender: UIButton) {
        print("Hit priceNegotiable Yes")
        if isFrist{
            self.priceNegotiableBuy = true
        }else{
            self.priceNegotiableRent = true
        }
    }
    
    @IBAction func priceNegotiableNo(_ sender: UIButton) {
        if isFrist{
            self.priceNegotiableBuy = false
        }else{
            self.priceNegotiableRent = false
        }
    }
    
    @IBAction func reputedProfileYex(_ sender: UIButton) {
        if isFrist{
            self.reuptedProfileBuy = true
        }else{
            self.reuptedProfileRent = true
        }
        
    }
    
    
    @IBAction func reputedProfileNo(_ sender: UIButton) {
        if isFrist{
            self.reuptedProfileBuy = false
        }else{
            self.reuptedProfileRent = false
        }
    }
    
    //MARK:- Validation
    fileprivate func validateDataBuy() -> Bool {
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell
        if BaseValidator.isNotEmpty(string: cell.ageOfBuildingLBLB.text) && cell.ageOfBuildingLBLB.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.bathroomLBLB.text) && cell.bathroomLBLB.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.bedroomLBLB.text) && cell.bedroomLBLB.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.buildingDescriptionB.text) && cell.buildingDescriptionB.text != "Type a short description." &&
            /*BaseValidator.isNotEmpty(string: cell.elevatorsLBLB.text) && cell.elevatorsLBLB.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.floorNumberLBLB.text) && cell.floorNumberLBLB.text != "Select" && */
            BaseValidator.isNotEmpty(string: cell.propertyPriceTFB.text) &&
            /*BaseValidator.isNotEmpty(string: cell.propertyTitleTFB.text) &&*/ BaseValidator.isNotEmpty(string: cell.currencyBTNB.currentTitle) &&
            BaseValidator.isNotEmpty(string: cell.floorSizeTFB.text) &&
            BaseValidator.isNotEmpty(string: cell.energyTypeLBLB.text) && cell.energyTypeLBLB.text != "Select" && selectedEnergyB != "" &&
            BaseValidator.isNotEmpty(string: cell.buildingTypeLBLB.text) && cell.buildingTypeLBLB.text != "Select" && selectedArchetectureTypeB != "" &&
            BaseValidator.isNotEmpty(string: cell.waterHeatingLBLB.text) && cell.waterHeatingLBLB.text != "Select" && selectedWaterHeatingB != "" &&
            BaseValidator.isNotEmpty(string: cell.propertyTypeLBLB.text) && cell.propertyTypeLBLB.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.requiredDepositsTFB.text) /*&& (brochureBuy != Data() || brochureLinkBuy != "")*/ && (titleDeedBuy != Data() || titleDeedLinkBuy != "") //&& self.priceNegotiableBuy && self.reuptedProfileBuy
            
        {
            
            if self.propertySource[self.selectedPropertyTypeBuy!.row].type == 1 {
                
                if BaseValidator.isNotEmpty(string: cell.elevatorsLBLB.text) && cell.elevatorsLBLB.text != "Select" &&
                    BaseValidator.isNotEmpty(string: cell.floorNumberLBLB.text) && cell.floorNumberLBLB.text != "Select" {
                    return true
                }else {
                    
                    if !BaseValidator.isNotEmpty(string: cell.elevatorsLBLB.text) || cell.elevatorsLBLB.text == "Select" {
                        showBanner(message: "Please enter number of Elevators")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.floorNumberLBLB.text) || cell.floorNumberLBLB.text == "Select" {
                        showBanner(message: "Please enter number of Floors")
                        return false
                    }
                    showBanner(message: "All fields are mandatory")
                    return false
                }
            }else {
                return true
            }
            
        }else{
            if !BaseValidator.isNotEmpty(string: cell.propertyPriceTFB.text) {
                showBanner(message: "Please enter Price")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: cell.requiredDepositsTFB.text) {
                showBanner(message: "Please enter Required deposit")
                return false
            }

            if !BaseValidator.isNotEmpty(string: cell.floorSizeTFB.text) {
                showBanner(message: "Please enter Floor size")
                return false
            }
            
            if titleDeedBuy == Data() || titleDeedLinkBuy == "" {
                showBanner(message: "Please upload Title Deed")
                return false
            }

            if !BaseValidator.isNotEmpty(string: cell.buildingTypeLBLB.text) || cell.buildingTypeLBLB.text == "Select" || selectedArchetectureTypeB == "" {
                showBanner(message: "Please choose a Building Architecture type")
                return false
            }

            if !BaseValidator.isNotEmpty(string: cell.propertyTypeLBLB.text) || cell.propertyTypeLBLB.text == "Select" {
                showBanner(message: "Please choose a Property type")
                return false
            }

            if !BaseValidator.isNotEmpty(string: cell.ageOfBuildingLBLB.text) || cell.ageOfBuildingLBLB.text == "Select" {
                showBanner(message: "Please enter Age of property")
                return false
            }
            
            
            if self.propertySource[self.selectedPropertyTypeBuy!.row].type == 1 {
                
                if BaseValidator.isNotEmpty(string: cell.elevatorsLBLB.text) && cell.elevatorsLBLB.text != "Select" &&
                    BaseValidator.isNotEmpty(string: cell.floorNumberLBLB.text) && cell.floorNumberLBLB.text != "Select" {
                    
                    if !BaseValidator.isNotEmpty(string: cell.bedroomLBLB.text) || cell.bedroomLBLB.text == "Select" {
                        showBanner(message: "Please enter number of Bedrooms")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.bathroomLBLB.text) || cell.bathroomLBLB.text == "Select" {
                        showBanner(message: "Please enter number of Bathrooms")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.waterHeatingLBLB.text) || cell.waterHeatingLBLB.text == "Select" || selectedWaterHeatingB == "" {
                        showBanner(message: "Please choose a Water Heating type")
                        return false
                    }

                    if !BaseValidator.isNotEmpty(string: cell.energyTypeLBLB.text) || cell.energyTypeLBLB.text == "Select" || selectedEnergyB == "" {
                        showBanner(message: "Please choose an Energy type")
                        return false
                    }

                    
                    if !BaseValidator.isNotEmpty(string: cell.buildingDescriptionB.text) || cell.buildingDescriptionB.text == "Type a short description." {
                        showBanner(message: "Please enter Description")
                        return false
                    }

                    return true
                    
                }else {
                    
                    if !BaseValidator.isNotEmpty(string: cell.elevatorsLBLB.text) || cell.elevatorsLBLB.text == "Select" {
                        showBanner(message: "Please enter number of Elevators")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.floorNumberLBLB.text) || cell.floorNumberLBLB.text == "Select" {
                        showBanner(message: "Please enter number of Floors")
                        return false
                    }
                    
                    showBanner(message: "All fields are mandatory")
                    return false
                }
            }else {
                if !BaseValidator.isNotEmpty(string: cell.bedroomLBLB.text) || cell.bedroomLBLB.text == "Select" {
                    showBanner(message: "Please enter number of Bedrooms")
                    return false
                }
                
                if !BaseValidator.isNotEmpty(string: cell.bathroomLBLB.text) || cell.bathroomLBLB.text == "Select" {
                    showBanner(message: "Please enter number of Bathrooms")
                    return false
                }
                
                if !BaseValidator.isNotEmpty(string: cell.waterHeatingLBLB.text) || cell.waterHeatingLBLB.text == "Select" || selectedWaterHeatingB == "" {
                    showBanner(message: "Please choose a Water Heating type")
                    return false
                }
                
                if !BaseValidator.isNotEmpty(string: cell.energyTypeLBLB.text) || cell.energyTypeLBLB.text == "Select" || selectedEnergyB == "" {
                    showBanner(message: "Please choose an Energy type")
                    return false
                }

                
                if !BaseValidator.isNotEmpty(string: cell.buildingDescriptionB.text) || cell.buildingDescriptionB.text == "Type a short description." {
                    showBanner(message: "Please enter Description")
                    return false
                }
                
                showBanner(message: "All fields are mandatory")
                return false

            }

        }
        
    }
    
    fileprivate func validateDataRent() -> Bool {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell

        if BaseValidator.isNotEmpty(string: cell.ageOfBuildingLBLR.text) && cell.ageOfBuildingLBLR.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.bathroomLBLR.text) && cell.bathroomLBLR.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.bedroomLBLR.text) && cell.bedroomLBLR.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.buildingDescriptionR.text) && cell.buildingDescriptionR.text != "Type a short description." &&
            /*BaseValidator.isNotEmpty(string: cell.elevatorsLBLR.text) && cell.elevatorsLBLR.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.floorNumberLBLR.text) && cell.floorNumberLBLR.text != "Select" &&*/
            BaseValidator.isNotEmpty(string: cell.propertyPriceTFR.text) &&
            /*BaseValidator.isNotEmpty(string: cell.propertyTitleTFR.text) &&*/ BaseValidator.isNotEmpty(string: cell.currencyBTNR.currentTitle) && BaseValidator.isNotEmpty(string: cell.floorSizeTFR.text) &&
            
            BaseValidator.isNotEmpty(string: cell.energyTypeLBLR.text) && cell.energyTypeLBLR.text != "Select" && selectedEnergyR != "" &&
            BaseValidator.isNotEmpty(string: cell.buildingTypeLBLR.text) && cell.buildingTypeLBLR.text != "Select" && selectedArchetectureTypeR != "" &&
            BaseValidator.isNotEmpty(string: cell.waterHeatingLBLR.text) && cell.waterHeatingLBLR.text != "Select" && selectedWaterHeatingR != "" &&

            BaseValidator.isNotEmpty(string: cell.propertyTypeLBLR.text) && cell.propertyTypeLBLR.text != "Select" &&
            BaseValidator.isNotEmpty(string: cell.requiredDepositsTFR.text) /*&& (brochureRent != Data() || brochureLinkRent != "")*/ && (titleDeedRent != Data() || titleDeedLinkRent != "") //&& self.priceNegotiableBuy && self.reuptedProfileBuy
        {
            
            if self.propertySource[self.selectedPropertyTypeRent!.row].type == 1 {
                if BaseValidator.isNotEmpty(string: cell.elevatorsLBLR.text) && cell.elevatorsLBLR.text != "Select" &&
                    BaseValidator.isNotEmpty(string: cell.floorNumberLBLR.text) && cell.floorNumberLBLR.text != "Select" {
                    
                    return true
                }else {
                    if !BaseValidator.isNotEmpty(string: cell.elevatorsLBLR.text) || cell.elevatorsLBLR.text == "Select" {
                        showBanner(message: "Please enter number of Elevators")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.floorNumberLBLR.text) || cell.floorNumberLBLR.text == "Select" {
                        showBanner(message: "Please enter number of Floors")
                        return false
                    }

                    showBanner(message: "All fields are mandatory")
                    return false
                }
            }else {
                
                return true
            }

        }else{
            
            if !BaseValidator.isNotEmpty(string: cell.propertyPriceTFR.text) {
                showBanner(message: "Please enter Price")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: cell.requiredDepositsTFR.text) {
                showBanner(message: "Please enter Required deposit")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: cell.floorSizeTFR.text) {
                showBanner(message: "Please enter Floor size")
                return false
            }
            
            if titleDeedRent == Data() || titleDeedLinkRent == "" {
                showBanner(message: "Please upload Title deed")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: cell.buildingTypeLBLR.text) || cell.buildingTypeLBLR.text == "Select" || selectedArchetectureTypeR == "" {
                showBanner(message: "Please choose a Building Architecture type")
                return false
            }

            if !BaseValidator.isNotEmpty(string: cell.propertyTypeLBLR.text) || cell.propertyTypeLBLR.text == "Select" {
                showBanner(message: "Please choose a Property type")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: cell.ageOfBuildingLBLR.text) || cell.ageOfBuildingLBLR.text == "Select" {
                showBanner(message: "Please enter Age of property")
                return false
            }
            
            
            if self.propertySource[self.selectedPropertyTypeRent!.row].type == 1 {
                
                if BaseValidator.isNotEmpty(string: cell.elevatorsLBLR.text) && cell.elevatorsLBLR.text != "Select" &&
                    BaseValidator.isNotEmpty(string: cell.floorNumberLBLR.text) && cell.floorNumberLBLR.text != "Select" {
                    
                    if !BaseValidator.isNotEmpty(string: cell.bedroomLBLR.text) || cell.bedroomLBLR.text == "Select" {
                        showBanner(message: "Please enter number of Bedrooms")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.bathroomLBLR.text) || cell.bathroomLBLR.text == "Select" {
                        showBanner(message: "Please enter number of Bathrooms")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.waterHeatingLBLR.text) || cell.waterHeatingLBLR.text == "Select" || selectedWaterHeatingR == "" {
                        showBanner(message: "Please choose a Water Heating type")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.energyTypeLBLR.text) || cell.energyTypeLBLR.text == "Select" || selectedEnergyR == "" {
                        showBanner(message: "Please choose an Energy type")
                        return false
                    }

                    
                    if !BaseValidator.isNotEmpty(string: cell.buildingDescriptionR.text) || cell.buildingDescriptionR.text == "Type a short description." {
                        showBanner(message: "Please enter Description")
                        return false
                    }
                    
                    showBanner(message: "All fields are mandatory")
                    return false
                    
                }else {
                    
                    if !BaseValidator.isNotEmpty(string: cell.elevatorsLBLR.text) || cell.elevatorsLBLR.text == "Select" {
                        showBanner(message: "Please enter number of Elevators")
                        return false
                    }
                    
                    if !BaseValidator.isNotEmpty(string: cell.floorNumberLBLR.text) || cell.floorNumberLBLR.text == "Select" {
                        showBanner(message: "Please enter number of Floors")
                        return false
                    }
                    
                    showBanner(message: "All fields are mandatory")
                    return false
                }
            }else {
                if !BaseValidator.isNotEmpty(string: cell.bedroomLBLR.text) || cell.bedroomLBLR.text == "Select" {
                    showBanner(message: "Please enter number of Bedrooms")
                    return false
                }
                
                if !BaseValidator.isNotEmpty(string: cell.bathroomLBLR.text) || cell.bathroomLBLR.text == "Select" {
                    showBanner(message: "Please enter number of Bathrooms")
                    return false
                }
                
                if !BaseValidator.isNotEmpty(string: cell.waterHeatingLBLR.text) || cell.waterHeatingLBLR.text == "Select" || selectedWaterHeatingR == "" {
                    showBanner(message: "Please choose a Water Heating type")
                    return false
                }
                
                if !BaseValidator.isNotEmpty(string: cell.energyTypeLBLR.text) || cell.energyTypeLBLR.text == "Select" || selectedEnergyR == "" {
                    showBanner(message: "Please choose an Energy type")
                    return false
                }

                
                
                if !BaseValidator.isNotEmpty(string: cell.buildingDescriptionR.text) || cell.buildingDescriptionR.text == "Type a short description." {
                    showBanner(message: "Please enter Description")
                    return false
                }
                
                showBanner(message: "All fields are mandatory")
                return false
            
            }
            
        }
        
    }
    
    // Mark:- TextView delegates
    
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    
    func showCurrentDate(outputFormat: String = "MM/dd/yyyy") -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = outputFormat //"MM/dd/yyyy"
        return formatter.string(from: date)
    }
    
    //MARK:- TableView Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        if isFrist {
            cell.firstview.backgroundColor = UIColor(red: 87/255, green: 86/255, blue: 214/255, alpha: 1)
            
            cell.firstBtn.backgroundColor = UIColor.white
            
            cell.secondView.backgroundColor = .white//UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
            cell.secondBtn.backgroundColor = .white//UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        } else {
            cell.firstview.backgroundColor = .white//UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
            cell.firstBtn.backgroundColor = .white//UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
            cell.secondView.backgroundColor = UIColor(red: 87/255, green: 86/255, blue: 214/255, alpha: 1)
            cell.secondBtn.backgroundColor = UIColor.white

        }
        //cell.setNeedsDisplay()
        //cell.setNeedsLayout()
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //_ = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1445//704
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataTableViewCell") as! DataTableViewCell
        
        if isFrist {
            cell.firstViewContent.isHidden = false
            cell.secondViewContent.isHidden = true
            
            if editingMode{
                cell.propertyTitleTFB.text = propertyTitleBuy
                cell.requiredDepositsTFB.text = requiredDepositBuy
                if addedPriceBuy != "" {
                    cell.propertyPriceTFB.text = addedPriceBuy.trimmingCharacters(in: .whitespacesAndNewlines).double.showPriceInDollar()
                }

                cell.buildingDescriptionB.text = descriptionBuy
                cell.buildingDescriptionB.textColor = UIColor.typingTextColor
                cell.floorSizeTFB.text = floorSizeBuy
            }
            
            if brochureLinkBuy == ""{
                cell.uploadedBrochureIconBuy.isHidden = true
                cell.brochureGradientViewBuy.isHidden = false
            }else{
                cell.uploadedBrochureIconBuy.isHidden = false
                cell.brochureGradientViewBuy.isHidden = true
                cell.brochureFileNameLBLB.text = fileNameBrochure
                
            }
            
            if titleDeedLinkBuy == ""{
                cell.uploadedDeedIconBuy.isHidden = true
                cell.deedGradientViewBuy.isHidden = false
            }else{
                cell.uploadedDeedIconBuy.isHidden = false
                cell.deedGradientViewBuy.isHidden = true
                cell.titledeedFileNameLBLB.text = fileNameTitleDeed
            }
            
            if let _ = selectedCurrencyBuy{
                DispatchQueue.main.async {
                    //print(self.currencySource[self.selectedCurrencyBuy!.row])
                    cell.currencyBTNB.setTitle("\(self.currencySource[self.selectedCurrencyBuy!.row].currency_code)", for: [])
                    cell.priceHintLBLB.text = self.priceHint + " (\(cell.currencyBTNB.title(for: .normal) ?? ""))"
                    cell.depositHintLBLB.text = self.depositHint + " (\(cell.currencyBTNB.title(for: .normal) ?? ""))"
                }
            }else{
                DispatchQueue.main.async {
                   // var dollar = ""
                    for (index,item) in self.currencySource.enumerated(){
                        if item.currency_code == "USD"{
                            self.selectedCurrencyBuy = IndexPath(row: index, section: 0)
                           // dollar = item.currency_code
                        }
                    }
                    //cell.currencyBTNB.setTitle(dollar, for: [])
                    tableView.reloadData()
                   // cell.currencyBTNB.setTitle("\(self.currencySource.count > 0 ? self.currencySource[0].currency_code : )", for: [])
                }
            }
            
            if let _ = selectedPropertyTypeBuy {
                DispatchQueue.main.async {
                    cell.propertyTypeLBLB.text = self.propertySource[self.selectedPropertyTypeBuy!.row].data
                    
                    if self.propertySource[self.selectedPropertyTypeBuy!.row].type == 1 {
                        cell.buildingFloorViewB.setActive()
                        cell.elevatorViewB.setActive()
                        print("ELevators-Setactive")
                        //MARK:- Commented One
                        
                        if cell.elevatorsLBLB.text == 0.description {
                            cell.elevatorsLBLB.text = nil
                            print("ELevators-Setactive if")

                        }else {
                            print("ELevators-Setactive else")

                        }
                        if cell.floorNumberLBLB.text == 0.description {
                            cell.floorNumberLBLB.text = nil
                        }else {

                        }

                        // Till Here
                    }else {
                        cell.buildingFloorViewB.setInactive()
                        cell.elevatorViewB.setInactive()
                        cell.elevatorsLBLB.text = "Select"
                        cell.floorNumberLBLB.text = "Select"
                        print("Elevetoes-setInacto")
                    }

                    if cell.propertyTypeLBLB.text == self.selectPlaceholder {
                        cell.propertyTypeLBLB.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.propertyTypeLBLB.textColor = UIColor.typingTextColor
                    }
                }
            }
            
            if let _ = selectedBuildingAgeBuy{
                DispatchQueue.main.async {
                   cell.ageOfBuildingLBLB.text = self.ageBuildingSource[self.selectedBuildingAgeBuy!.row].data
                    if cell.ageOfBuildingLBLB.text == self.selectPlaceholder {
                        cell.ageOfBuildingLBLB.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.ageOfBuildingLBLB.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            
            if let _ = selectedElevatorBuy{
                DispatchQueue.main.async {
                    if cell.elevatorViewB.isUserInteractionEnabled {
                        if !self.elevatorSource.isEmpty, self.selectedElevatorBuy!.row >= 0 && self.selectedElevatorBuy!.row < self.elevatorSource.count {
                            cell.elevatorsLBLB.text = self.elevatorSource[self.selectedElevatorBuy!.row].data
                            
                        }
                        
                        
                    }else {
                        cell.elevatorsLBLB.text = "Select #$"
                        
                    }
                    if cell.elevatorsLBLB.text == self.selectPlaceholder {
                        cell.elevatorsLBLB.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.elevatorsLBLB.textColor = UIColor.typingTextColor
                    }
                    
                }
            }
            
            
            
            
            
            if let _ = selectedFloorNumberBuy{
                DispatchQueue.main.async {
                    if cell.buildingFloorViewB.isUserInteractionEnabled {
                        
                        if  !self.buildingFloorSource.isEmpty, self.selectedFloorNumberBuy!.row >= 0 && self.selectedFloorNumberBuy!.row < self.buildingFloorSource.count {
                            cell.floorNumberLBLB.text = self.buildingFloorSource[self.selectedFloorNumberBuy!.row].data

                        }
                        

                    }else {
                        cell.floorNumberLBLB.text = "Select"

                    }
                    if cell.floorNumberLBLB.text == self.selectPlaceholder {
                        cell.floorNumberLBLB.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.floorNumberLBLB.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedBedroomBuy{
                DispatchQueue.main.async {
                 cell.bedroomLBLB.text = self.bedroomSource[self.selectedBedroomBuy!.row].data
                    if cell.bedroomLBLB.text == self.selectPlaceholder {
                        cell.bedroomLBLB.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.bedroomLBLB.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedBathroomBuy{
                DispatchQueue.main.async {
                    cell.bathroomLBLB.text = self.bathroomSource[self.selectedBathroomBuy!.row].data
                    if cell.bathroomLBLB.text == self.selectPlaceholder {
                        cell.bathroomLBLB.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.bathroomLBLB.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if self.reuptedProfileBuy{
                cell.reputedProfileYes.image = UIImage(named: "tick_color")
                cell.reputedProfileNo.image = UIImage(named: "tick_grey")
            }else{
                cell.reputedProfileYes.image = UIImage(named: "tick_grey")
                cell.reputedProfileNo.image = UIImage(named: "tick_color")
            }
            
            if self.priceNegotiableBuy{
                cell.priceNegotiableYes.image = UIImage(named: "tick_color")
                cell.priceNegotiableNo.image = UIImage(named: "tick_grey")
            }else{
                cell.priceNegotiableYes.image = UIImage(named: "tick_grey")
                cell.priceNegotiableNo.image = UIImage(named: "tick_color")
            }
            
            cell.buildingTypeLBLB.text = selectedArchetectureTypeB
            cell.waterHeatingLBLB.text = selectedWaterHeatingB
            cell.energyTypeLBLB.text = selectedEnergyB
            
            if cell.buildingTypeLBLB.text == self.selectPlaceholder {
                cell.buildingTypeLBLB.textColor = UIColor.labelPlaceholderColor
            }else {
                cell.buildingTypeLBLB.textColor = UIColor.typingTextColor
            }

            if cell.waterHeatingLBLB.text == self.selectPlaceholder {
                cell.waterHeatingLBLB.textColor = UIColor.labelPlaceholderColor
            }else {
                cell.waterHeatingLBLB.textColor = UIColor.typingTextColor
            }

            if cell.energyTypeLBLB.text == self.selectPlaceholder {
                cell.energyTypeLBLB.textColor = UIColor.labelPlaceholderColor
            }else {
                cell.energyTypeLBLB.textColor = UIColor.typingTextColor
            }
            
        } else {
            
            cell.firstViewContent.isHidden = true
            cell.secondViewContent.isHidden = false
            cell.dateBTNRef.setTitle(availableDateDisplayGB, for: [])
            
            if editingMode{
                cell.propertyTitleTFR.text = propertyTitleRent
                cell.requiredDepositsTFR.text = requiredDepositRent
                if addedPriceRent != "" {
                    cell.propertyPriceTFR.text = addedPriceRent.trimmingCharacters(in: .whitespacesAndNewlines).double.showPriceInDollar()
                }
                cell.buildingDescriptionR.text = descriptionRent
                cell.buildingDescriptionR.textColor = UIColor.typingTextColor
                cell.floorSizeTFR.text = floorSizeRent
                //cell.dateBTNRef.setTitle(availableDate, for: [])
            }
            
            if brochureLinkRent == ""{
                cell.uploadedBrochureIcon.isHidden = true
                cell.brochureGradientVIewRent.isHidden = false
            }else{
                cell.uploadedBrochureIcon.isHidden = false
                cell.brochureGradientVIewRent.isHidden = true
                cell.brochureFileNameLBLR.text = fileNameBrochure

            }
            
            if titleDeedLinkRent == ""{
                cell.uploadedDeedIcon.isHidden = true
                cell.deedGradientViewRent.isHidden = false
            }else{
                cell.uploadedDeedIcon.isHidden = false
                cell.deedGradientViewRent.isHidden = true
                cell.titledeedFileNameLBLR.text = fileNameTitleDeed

            }
            
            if let _ = selectedCurrencyRent{
                DispatchQueue.main.async {
                    print(self.currencySource[self.selectedCurrencyRent!.row].currency)
                    cell.currencyBTNR.setTitle("\(self.currencySource[self.selectedCurrencyRent!.row].currency_code)", for: [])
                    cell.priceHintLBLR.text = self.priceHint + " (\(cell.currencyBTNR.title(for: .normal) ?? ""))"
                    cell.depositHintLBLR.text = self.depositHint + " (\(cell.currencyBTNR.title(for: .normal) ?? ""))"

                }
            //cell.secondViewContent.backgroundColor = UIColor.KXViolet
            }else{
                DispatchQueue.main.async {
//                    var dollar = ""
                    for (index,item) in self.currencySource.enumerated(){
                        if item.currency_code == "USD"{
                            self.selectedCurrencyRent = IndexPath(row: index, section: 0)
//                            dollar = item.currency_code
                        }
                    }
                    //cell.currencyBTNR.setTitle(dollar, for: [])
                    tableView.reloadData()
                }
//                DispatchQueue.main.async {
//                    cell.currencyBTNR.setTitle("\(self.currencySource.count > 0 ? self.currencySource[0].currency_code : "")", for: [])
//                }
            }
            
            if let _ = selectedPropertyTypeRent{
                DispatchQueue.main.async {
                    cell.propertyTypeLBLR.text = self.propertySource[self.selectedPropertyTypeRent!.row].data
                    
                    if self.propertySource[self.selectedPropertyTypeRent!.row].type == 1 {
                        cell.buildingFloorViewR.setActive()
                        cell.elevatorViewR.setActive()
//                        if cell.elevatorsLBLR.text == 0.description {
//                            cell.elevatorsLBLR.text = nil
//                        }else {
//
//                        }
//                        if cell.floorNumberLBLR.text == 0.description {
//                            cell.floorNumberLBLR.text = nil
//                        }else {
//
//                        }
                        
                    }else {

                        cell.buildingFloorViewR.setInactive()
                        cell.elevatorViewR.setInactive()
                        cell.elevatorsLBLR.text = "Select"
                        cell.floorNumberLBLR.text = "Select"

                    }

                    if cell.propertyTypeLBLR.text == self.selectPlaceholder {
                        cell.propertyTypeLBLR.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.propertyTypeLBLR.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedBuildingAgeRent{
                DispatchQueue.main.async {
                    cell.ageOfBuildingLBLR.text = self.ageBuildingSource[self.selectedBuildingAgeRent!.row].data
                    if cell.ageOfBuildingLBLR.text == self.selectPlaceholder {
                        cell.ageOfBuildingLBLR.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.ageOfBuildingLBLR.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedElevatorRent{
                DispatchQueue.main.async {
                    if cell.elevatorViewR.isUserInteractionEnabled {
                        if !self.elevatorSource.isEmpty, self.selectedElevatorRent!.row >= 0 && self.selectedElevatorRent!.row < self.elevatorSource.count {
                            cell.elevatorsLBLR.text = self.elevatorSource[self.selectedElevatorRent!.row].data

                        }
                        

                    }else {
                        cell.elevatorsLBLR.text = "Select"

                    }
                    if cell.elevatorsLBLR.text == self.selectPlaceholder {
                        cell.elevatorsLBLR.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.elevatorsLBLR.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedFloorNumberRent{
                DispatchQueue.main.async {
                    if cell.buildingFloorViewR.isUserInteractionEnabled {
                        if !self.buildingFloorSource.isEmpty, self.selectedFloorNumberRent!.row >= 0 && self.selectedFloorNumberRent!.row < self.buildingFloorSource.count {
                            cell.floorNumberLBLR.text = self.buildingFloorSource[self.selectedFloorNumberRent!.row].data

                        }

                    }else {
                        cell.floorNumberLBLR.text = "Select"

                    }
                    if cell.floorNumberLBLR.text == self.selectPlaceholder {
                        cell.floorNumberLBLR.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.floorNumberLBLR.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedBedroomRent{
                DispatchQueue.main.async {
                    cell.bedroomLBLR.text = self.bedroomSource[self.selectedBedroomRent!.row].data
                    if cell.bedroomLBLR.text == self.selectPlaceholder {
                        cell.bedroomLBLR.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.bedroomLBLR.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if let _ = selectedBathroomRent{
                DispatchQueue.main.async {
                    cell.bathroomLBLR.text = self.bathroomSource[self.selectedBathroomRent!.row].data
                    if cell.bathroomLBLR.text == self.selectPlaceholder {
                        cell.bathroomLBLR.textColor = UIColor.labelPlaceholderColor
                    }else {
                        cell.bathroomLBLR.textColor = UIColor.typingTextColor
                    }

                }
            }
            
            if self.reuptedProfileRent{
                cell.reputedProfileYesRent.image = UIImage(named: "tick_color")
                cell.reputedProfileNoRent.image = UIImage(named: "tick_grey")
            }else{
                cell.reputedProfileYesRent.image = UIImage(named: "tick_grey")
                cell.reputedProfileNoRent.image = UIImage(named: "tick_color")
            }
            
            if self.priceNegotiableRent{
                cell.priceNegotiableYesRent.image = UIImage(named: "tick_color")
                cell.priceNegotiableNoRent.image = UIImage(named: "tick_grey")
            }else{
                cell.priceNegotiableYesRent.image = UIImage(named: "tick_grey")
                cell.priceNegotiableNoRent.image = UIImage(named: "tick_color")
            }
            cell.buildingTypeLBLR.text = selectedArchetectureTypeR
            cell.waterHeatingLBLR.text = selectedWaterHeatingR
            cell.energyTypeLBLR.text = selectedEnergyR

            if cell.buildingTypeLBLR.text == self.selectPlaceholder {
                cell.buildingTypeLBLR.textColor = UIColor.labelPlaceholderColor
            }else {
                cell.buildingTypeLBLR.textColor = UIColor.typingTextColor
            }
            
            if cell.waterHeatingLBLR.text == self.selectPlaceholder {
                cell.waterHeatingLBLR.textColor = UIColor.labelPlaceholderColor
            }else {
                cell.waterHeatingLBLR.textColor = UIColor.typingTextColor
            }
            
            if cell.energyTypeLBLR.text == self.selectPlaceholder {
                cell.energyTypeLBLR.textColor = UIColor.labelPlaceholderColor
            }else {
                cell.energyTypeLBLR.textColor = UIColor.typingTextColor
            }

        }
        
        
        return cell
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = scrollView.contentOffset.y - self.previousScrollOffset
        
        let absoluteTop: CGFloat = 0;
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;
        
        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom
        
        if canAnimateHeader(scrollView) {
            
            // Calculate new header height
            var newHeight = self.headerHeightConstraint.constant
            if isScrollingDown {
                newHeight = max(self.minHeaderHeight, self.headerHeightConstraint.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(self.maxHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
            }
            
            // Header needs to animate
            if newHeight != self.headerHeightConstraint.constant {
                self.headerHeightConstraint.constant = newHeight
                self.updateHeader()
                self.setScrollPosition(position: self.previousScrollOffset)
            }
            
            self.previousScrollOffset = scrollView.contentOffset.y
        }
    }
    
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollViewDidStopScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            // scrolling has stopped
            scrollViewDidStopScrolling()
        }
    }
    
    func scrollViewDidStopScrolling() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)
        
        if self.headerHeightConstraint.constant > midPoint {
            // expand header
            //self.headerHeightConstraint.constant = self.maxHeaderHeight
            expandHeader()
            
        } else {
            // collapse header
            // self.headerHeightConstraint.constant = self.minHeaderHeight
            collapseHeader()
            
        }
    }
    
    
    
    
    
    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        // Calculate the size of the scrollView when header is collapsed
        let scrollViewMaxHeight = scrollView.frame.height + self.headerHeightConstraint.constant - minHeaderHeight
        
        // Make sure that when header is collapsed, there is still room to scroll
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func collapseHeader() {
        
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.minHeaderHeight
            // Manipulate UI elements within the header here
            self.updateHeader()
            self.view.layoutIfNeeded()
            
        })
    }
    
    func expandHeader() {
        
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.maxHeaderHeight
            // Manipulate UI elements within the header here
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    func setScrollPosition(position: CGFloat) {
        
        self.tableView.contentOffset = CGPoint(x: self.tableView.contentOffset.x, y: position)
        
    }
    
    
    func updateHeader() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.headerHeightConstraint.constant - self.minHeaderHeight
        let percentage = openAmount / range
        
        //self.titleTopConstraint.constant = -openAmount + 10
        self.questinLbl.alpha = percentage
    }
    
    
}

//MARK:- Side Menu Delegate Method
extension KXPostPropertyViewController: CategorySelectionDelegate {
    
    func selectedValue(index: IndexPath) {
        switch self.currentSourceType{
        case .currency:
            if self.isFrist{
                selectedCurrencyBuy  = index
            }else{
                selectedCurrencyRent = index
            }
            print("...Currency hit")
        case .propertyType:
            if self.isFrist{
                selectedPropertyTypeBuy  = index
            }else{
                selectedPropertyTypeRent = index
            }
        print("...Property Type Hit")
        case .buildingAge:
            if self.isFrist{
                selectedBuildingAgeBuy  = index
            }else{
                selectedBuildingAgeRent = index
            }
            print("...Building Age")
        case .elevators :
            if self.isFrist{
                selectedElevatorBuy  = index
            }else{
                selectedElevatorRent = index
            }
            print("...Elevator Hit")
        case .floorNumber:
            if self.isFrist{
                selectedFloorNumberBuy  = index
            }else{
                selectedFloorNumberRent = index
            }
          print("...Floor Number Hit")
        case .buyRentType:
            print("...BuyRentType Hit")
        case .bedrooms:
            if self.isFrist{
                selectedBedroomBuy  = index
            }else{
                selectedBedroomRent = index
            }
            print("Bedrooms Hit")
        case .bathrooms:
            if self.isFrist{
                selectedBathroomBuy  = index
            }else{
                selectedBathroomRent = index
            }
            print("Bathrooms Hit")
            
        case .energy:
            if self.isFrist {
                selectedEnergyB = energySource[index.row].data
            }else {
                selectedEnergyR = energySource[index.row].data

            }
            tableView.reloadData()

        case .waterHeating:
            if self.isFrist {
                selectedWaterHeatingB = waterHeatingSource[index.row].data
            }else {
                selectedWaterHeatingR = waterHeatingSource[index.row].data
            }
            tableView.reloadData()

        case .archetectureype:
            if self.isFrist {
                selectedArchetectureTypeB = archetectureTypeSource[index.row].data
            }else {
                selectedArchetectureTypeR = archetectureTypeSource[index.row].data
            }
            tableView.reloadData()

        default:
            print("...Not Selected")
        }
    }
}



extension KXPostPropertyViewController: UIDocumentPickerDelegate, UIDocumentMenuDelegate, UINavigationControllerDelegate{
    
    
    func getDataFromICloud(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
//        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        //importMenu.modalPresentationStyle = .formSheet
        present(importMenu, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("Filesize:",url.filesSize)
        if url.filesSize > 8000000 {
            showBanner(message: "File size exceeded!")
            return
        }
        
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DataTableViewCell

        do{
            let data = try Data(contentsOf: url)
            if isBrochure{
                if isFrist{
                    brochureLinkBuy = "\(url)"
                    DispatchQueue.main.async {
                        cell.brochureFileNameLBLB.text = url.fileName
                        self.fileNameBrochure = url.fileName
                        cell.uploadedBrochureIconBuy.isHidden = false
                        cell.brochureGradientViewBuy.isHidden = true
                        
                    }
                    brochureBuy = data
                    print("DocumentURL",url)
                    
                }else{
                    brochureLinkRent = "\(url)"
                    DispatchQueue.main.async {
                        cell.brochureFileNameLBLR.text = url.fileName
                        self.fileNameBrochure = url.fileName

                        cell.uploadedBrochureIcon.isHidden = false
                        cell.brochureGradientVIewRent.isHidden = true
                    }
                    brochureRent = data
                    print("DocumentURL",url)

                }
            }else{
                if isFrist{
                    titleDeedLinkBuy = "\(url)"
                    DispatchQueue.main.async {
                        cell.titledeedFileNameLBLB.text = url.fileName
                        self.fileNameTitleDeed = url.fileName

                        cell.uploadedDeedIconBuy.isHidden = false
                        cell.deedGradientViewBuy.isHidden = true
                    }
                    titleDeedBuy = data
                    print("DocumentURL",url)

                }else{
                    titleDeedLinkRent = "\(url)"
                    DispatchQueue.main.async {
                        cell.titledeedFileNameLBLR.text = url.fileName
                        self.fileNameTitleDeed = url.fileName

                        cell.uploadedDeedIcon.isHidden = false
                        cell.deedGradientViewRent.isHidden = true
                    }
                    titleDeedRent = data
                    print("DocumentURL",url)
                }
            }
        
        }catch{
            print("Error Fetching PDF",error)
        }
        
    }
    
    

    
}

struct KXPostPropertyGeneral: Codable {
    let id:Int
    let data:String
}

class KXPostPropertySideOptionModel {
    var id:Int = 0
    var data:String = ""
    
    private init(object: Json) {
        id = object["id"] as? Int ?? 0
        data = object["type"] as? String ?? ""
    }
    
    class func getValues(objectArray: JsonArray) -> [KXPostPropertySideOptionModel]{
        var resultArray: [KXPostPropertySideOptionModel] = []
        for item in objectArray {
            resultArray.append(KXPostPropertySideOptionModel.init(object: item))
        }
    
        return resultArray
    }
    
}


enum SlideDropDownTypePostProperty {
    case country
    case currency
    case propertyType
    case buildingAge
    case elevators
    case floorNumber
    case buyRentType
    case bedrooms
    case bathrooms
    case archetectureype
    case energy
    case waterHeating
}

