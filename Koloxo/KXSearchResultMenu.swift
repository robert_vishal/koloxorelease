//
//  UISearchResultMenu.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 10/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import FloatRatingView


class KXSearchResultMenuTVC: UITableViewCell {
    @IBOutlet var propertyImage: StandardImageView!
    
    @IBOutlet var propertyNameLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet var availableLBL: UILabel!
    @IBOutlet var availableView: StandardView!
    @IBOutlet var ratingView: FloatRatingView!
    
    @IBOutlet var ratingCountLBL: UILabel!
    @IBOutlet var bedroomCountLBL: UILabel!
    @IBOutlet var bathroomCountLBL: UILabel!
    @IBOutlet var parkingLBL: UILabel!
    
    @IBOutlet var favouriteBTN: UIButton!
    @IBOutlet var parkingImage: UIImageView!
}


class KXSearchResultMenu: KLBaseViewController, UITextFieldDelegate {

    @IBOutlet var cityTF: UITextField!
    @IBOutlet var SearchView_tab: UITableView!
    @IBOutlet var titleLBL: UILabel!
    
    final var dataSource: [KXSearchAvailableModel] = []
    final var buildingAvailability: BuildingAvailability = .available
    final var propertyLocation: String = ""
    final var citySelected: String = ""
    
    private var detailSegueIndexPath: IndexPath?
    private var searchText: String? = nil
    private var searchIndexPath: IndexPath?

    override func viewDidLoad() {
        super.viewDidLoad()
        SearchView_tab.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        titleLBL.text = citySelected
        NotificationCenter.default.addObserver(self, selector: #selector(updateWithChangesFromDetails), name: .searchSourceInfoChanged, object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        SearchView_tab.reloadData()
        view.layoutIfNeeded()
        print("detailSegueIndexPath-KXSearchResultMenu::",detailSegueIndexPath as Any)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func updateWithChangesFromDetails(notification: Notification) {
        print("KXSearchResultMenu-updateWithChangesFromDetails")
        if let info = notification.userInfo as? Json {
            let propertyId = info["property_id"] as? Int ?? 0
            let wishList = info["wish_list"] as? Int ?? 0
            
            print("KXSearchResultMenu-updateWithChangesFromDetails:",propertyId,":::",wishList)
            
            // check wheather the selectedIndexpath id is equal to property_id which is from notificaion from details page, if else
            // find the matching property_id in the whole datasource and do the change(if wish_list changed)
            if let indexPath = detailSegueIndexPath {
                if dataSource[indexPath.row].id == propertyId {
                    dataSource[indexPath.row].wish_list = wishList
                    print("corrected1")
                }else {
                    if var currentProperty = dataSource.filter({$0.id == propertyId}).first {
                        currentProperty.wish_list = wishList
                        print("corrected2")
                    }
                }
            }else {
                if var currentProperty = dataSource.filter({$0.id == propertyId}).first {
                    currentProperty.wish_list = wishList
                    print("corrected3")
                }
            }
            
            BaseThread.asyncMain {
                self.SearchView_tab.reloadData()
            }

        }
    }
    
    
    @IBAction func searchEditingChanged(_ sender: UITextField) {
        print("searchEditingChanged:",sender.text as Any)
        searchText = sender.text
        SearchView_tab.reloadData()
        self.view.layoutIfNeeded()
        self.SearchView_tab.setContentOffset(.zero, animated: true)

    }
    
    
    @IBAction func favouriteTapped(_ sender: UIButton) {
        if isLoggedIn() {
            if dataSource[sender.tag].wish_list == 1 {
                deleteWishlist(atIndex: sender.tag)
            }else {
                addToWishList(atIndex: sender.tag)
            }
        }else {
            let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    fileprivate func deleteWishlist(atIndex: Int){
        let url = "deleteWishlist"
        var param = Json()
        
        param.updateValue(Credentials.shared.userId, forKey: "user_id")
        param.updateValue(dataSource[atIndex].id, forKey: "propertyid")
        
        Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
            print("deleted from wishlist")
            guard isSucceeded else { return }
            BaseThread.asyncMain {
                self.dataSource[atIndex].wish_list = 0
                self.SearchView_tab.reloadData()

            }
        }
    }
    
    fileprivate func addToWishList(atIndex: Int){
        let url = "addtoWishlist"
        var param = Json()
        
        param.updateValue(Credentials.shared.userId, forKey: "user_id")
        param.updateValue(dataSource[atIndex].id, forKey: "property_id")

        Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
            print("added to wishlist")
            guard isSucceeded else { return }
            BaseThread.asyncMain {
                self.dataSource[atIndex].wish_list = 1
                self.SearchView_tab.reloadData()
            }
        }
        
    }

    
    @IBAction func Back_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    

    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        if isLoggedIn(){
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    @IBAction func Home_Button(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Humberger_Button(_ sender: Any) {
        showSideMenu()
    }

    @IBAction func Filter_Button(_ sender: Any) {
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXFilterViewController") as! KXFilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension KXSearchResultMenu: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let searchKey = searchText, searchKey.isNotEmpty {
            return dataSource.filter({ (propertyItem) -> Bool in
                let isMatched = propertyItem.property_name.lowercased().range(of: searchKey.lowercased())
                return isMatched != nil ? true : false
            }).count
        }else {
            return dataSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KXSearchResultMenuTVC") as! KXSearchResultMenuTVC
        cell.propertyImage.setSpecificRoundCorners([.topLeft,.topRight], radius: 9)
        let source = dataSource[indexPath.row]
        
        cell.priceLBL.text = " \(source.usd_price.showPriceInDollar())" //showPriceInKilo()
        cell.propertyNameLBL.text = source.property_name
        cell.propertyLocationLBL.text = source.location
        cell.favouriteBTN.tag = indexPath.row
        
        if isLoggedIn() {
            if source.property_owner.description == Credentials.shared.userId {
                cell.favouriteBTN.isHidden = true
            }else {
                cell.favouriteBTN.isHidden = false
                
                if source.wish_list == 1 {
                    cell.favouriteBTN.setImage(#imageLiteral(resourceName: "heartFiled"), for: .normal)
                }else {
                    cell.favouriteBTN.setImage(#imageLiteral(resourceName: "heartCopy"), for: .normal)
                    
                }
                
            }
        }else {
            cell.favouriteBTN.isHidden = true
        }
        
        
    // id changed to data
        if let bedroomCount = source.property_bedrooms?.data{
            let bedroomCountInt = Int(bedroomCount)!
            if bedroomCountInt > 1 {
                cell.bedroomCountLBL.text = bedroomCountInt.description + " Bedrooms"
            }else {
                cell.bedroomCountLBL.text = bedroomCountInt.description + " Bedroom"
            }
            
        }
     // data changed to id
        if let bathroomCount = source.property_bathrooms?.id{
            let bathroomCountInt = Int(bathroomCount)
            if bathroomCountInt > 1 {
                cell.bathroomCountLBL.text = bathroomCountInt.description + " Bathrooms"
            }else {
                cell.bathroomCountLBL.text = bathroomCountInt.description + " Bathroom"
            }
            
        }
        
        
        if !source.property_ratings_totals.isEmpty {
            cell.ratingCountLBL.text = source.property_ratings_totals[0].aggregate
            cell.ratingView.rating = Double(source.property_ratings_totals[0].aggregate)!
        }else{
            cell.ratingCountLBL.text = 0.0.description
            cell.ratingView.rating = 0.0
        }
        
        if !source.property_images.isEmpty {
            let imageURL = source.property_images[0].image
            cell.propertyImage.kf.setImage(with: URL(string: baseImageURLDetail + imageURL), placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
        
        
        cell.availableLBL.text = source.buildingAvailability.rawValue
        
        switch source.buildingAvailability {
        case .available:
            cell.availableView.backgroundColor = UIColor.availableGreen
        case .almostAvailable:
            cell.availableView.backgroundColor = UIColor.almostAvailableYellow
        case .notAvailable:
            cell.availableView.backgroundColor = UIColor.notAvailableRed
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 302
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        detailSegueIndexPath = indexPath
        
        let vc = storyboardSearch!.instantiateViewController(withIdentifier: "KXPropertyDetailsVC") as! KXPropertyDetailsVC
        vc.propertyId = dataSource[indexPath.row].id
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
