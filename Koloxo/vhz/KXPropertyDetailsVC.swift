//
//  KXPropertyDetailsVC.swift
//  KoloxoDetail
//
//  Created by Appzoc on 27/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import SideMenu
import GoogleMaps
import VisualEffectView
import FloatRatingView
import ImageSlideshow
import Kingfisher

class KXPropertyDetailsVC: UIViewController {
    
    @IBOutlet var sliderImageCountLBL: UILabel!
    @IBOutlet var favouriteBTN: UIButton!
    @IBOutlet var propertyTitleLBL: UILabel!
    @IBOutlet var propertyLocationLBL: UILabel!
    @IBOutlet var ratingCountLBL: UILabel!
    @IBOutlet var availabilityLBL: UILabel!
    @IBOutlet var availabilityIndicatorView: BaseView!
    @IBOutlet var propertyMapView: GMSMapView!
    @IBOutlet var propertyRatingView: FloatRatingView!
    @IBOutlet var propertyPriceLBL: UILabel!
    @IBOutlet var bedroomCountLBL: UILabel!
    @IBOutlet var bathroomCountLBL: UILabel!
    @IBOutlet var parkingCountLBL: UILabel!
    @IBOutlet var parkingAvailabilityImage: UIImageView!
    @IBOutlet var aboutPropertyLBL: UILabel!
    @IBOutlet var aboutPropertyMoreBTN: UIButton!
    @IBOutlet var propertyAgeLBL: UILabel!
    @IBOutlet var propertyAddressLBL: UILabel!
    @IBOutlet var facilitiesCV: UICollectionView!
    @IBOutlet var facilitiesMoreView: UIView!
    @IBOutlet var spacesCV: UICollectionView!
    @IBOutlet var spacesMoreView: UIView!
    @IBOutlet var furnituresTV: UITableView!
    @IBOutlet var newsfeedsCV: UICollectionView!
    @IBOutlet var newsfeedsPC: UIPageControl!
    @IBOutlet var reviewTimeLBL: UILabel!
    @IBOutlet var ratingProfileImage: BaseImageView!
    @IBOutlet var reviewProfileNameLBL: UILabel!
    @IBOutlet var reviewProfileLocationLBL: UILabel!
    @IBOutlet var reviewRatingCountLBL: UILabel!
    @IBOutlet var reviewTitleLBL: UILabel!
    @IBOutlet var reviewDescriptionLBL: UILabel!
    @IBOutlet var reviewerLastImage: BaseImageView!
    @IBOutlet var reviewerFirstImage: BaseImageView!
    @IBOutlet var reviewersCountLBL: UILabel!
    @IBOutlet var reviewProfileRatingVIew: FloatRatingView!
    @IBOutlet var facilitiesMoreLBL: UILabel!
    @IBOutlet var facilitiesMoreImage: UIImageView!
    @IBOutlet var spacesMoreImage: UIImageView!
    @IBOutlet var spacesMoreLBL: UILabel!
    @IBOutlet var ownerTimeStampLBL: UILabel!
    @IBOutlet var ownerImage: BaseImageView!
    @IBOutlet var ownerNameLBL: UILabel!
    @IBOutlet var ownerLocationLBL: UILabel!
    @IBOutlet var ownerMobileLBL: UILabel!
    @IBOutlet var ownerMailLBL: UILabel!
    @IBOutlet var heightFacilitiesCV: NSLayoutConstraint!
    @IBOutlet var heightSpacesCV: NSLayoutConstraint!
    @IBOutlet var heightFurnitureTV: NSLayoutConstraint!
    @IBOutlet var ratingReviewBlurView: VisualEffectView!
    @IBOutlet var separatorBlurView: VisualEffectView!
    @IBOutlet var ownerBlurView: VisualEffectView!
    @IBOutlet var parkingView: UIView!
    @IBOutlet var reviewersCountLBLView: BaseGradientView!
    @IBOutlet weak var propertySlidingImages: ImageSlideshow!
    @IBOutlet var confirmDealView: BaseGradientView!
    @IBOutlet var requestContactView: BaseGradientView!
    @IBOutlet var confrimDealBTN: UIButton!
    @IBOutlet var requestOnlineBTN: UIButton!
    @IBOutlet var heightRatingReviewView: NSLayoutConstraint!
    @IBOutlet var heightNewsFeedsView: NSLayoutConstraint!
    @IBOutlet var aboutPropertyOwnerView: BaseView!
    //Latest edit
    @IBOutlet weak var aboutPropertyOwnerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var confirmDealBTN: UIButton!
    @IBOutlet var heightFurnituresView: NSLayoutConstraint!
    @IBOutlet var floorSizeLBL: UILabel!
    @IBOutlet var buildingNameLBL: UILabel!
    @IBOutlet var availableDateLBL: UILabel!
    @IBOutlet var availableDateHeight: NSLayoutConstraint!
    @IBOutlet var callBTN: BaseButton!
    @IBOutlet var elevatorsLBL: UILabel!
    @IBOutlet var waterHeatingLBL: UILabel!
    @IBOutlet var energyLBL: UILabel!
    @IBOutlet var architectuteLBL: UILabel!
    @IBOutlet var floorNoLBL: UILabel!
    @IBOutlet var heightBuildingNameKey: NSLayoutConstraint!
    @IBOutlet var heightBuildingNameValue: NSLayoutConstraint!
    @IBOutlet var heightElevatorkey: NSLayoutConstraint!
    @IBOutlet var heightElevatorValue: NSLayoutConstraint!
    @IBOutlet var heightFloorNoKey: NSLayoutConstraint!
    @IBOutlet var heightFloorNoValue: NSLayoutConstraint!
    @IBOutlet var propTypeLB: UILabel!
    @IBOutlet var availabilityLBLBottom: UILabel!
    
    
    // Data passsing property
    var userID = Credentials.shared.userId
    final var propertyId: Int = 210//179//210
    final var propertyType : Int = 211
    final var isFromPushNotification: Bool = false //used for navigation/back if page is loaded directly from pushnotification
    final var isFromSearch:Bool = false
    final var availableStatus:Int = 0
    final var detailSegueIsProperty: SearchDetailsSeguePropertyType = .none // used for identifying property/property_inside_building type data is passed to details page

    /// the below properties used only for request contact->paramter sending -if came from dashboard->"my request" tab
    final var isFromMyRequest: Bool = false
    final var isFromMyProperty: Bool = false
    
    var passingConfirmationRequestID:Int = 0


    final var moving_inMyrequest: String = ""
    final var durationMyRequest: String = ""
    final var typeOfProperty: Int = 1
    //    final var saleType: Int = 2 // 2-> rent, 1-> buy
    
    // vc only properties
    private var buildingId: Int = 0 // this used for identify the building when come from search(update the current source, in search-list view)
    private var propertySlidingImagesSource: [UIImage] = []
    private var isAbovePropertyMoreTapped: Bool = false
    private var isFacilitiesMoreTapped: Bool = false
    private var isSpacesMoreTapped: Bool = false
    private var furnitureSource: [KXDetailFurnitureModel] = [] //["Bedroom","Kitchen","Guest Room","Balcony","Other Furniture"]
    private var facilitiesSource: [KXDetailPropertyFacilityModel] = [] //["Tennis","Fire Extinguisher","Gym"]
    private var facilitiesImageSource:[FacilitiesImageSource] = []
    
    private var spacesSource: [KXDetailsPropertyImageModel] = [] //["Bedroom","Kitchen","Guest Room","Balcony"]
    private var newsFeedSource: [KXDetailNewsFeedModel] = []
    private var furnitureCVSource: [[KXDetailPropertyFurnitureModel]] = []
    private var newsFeedCount = 0
    private var deselectedCell:KXPropertyDetailsFurnitureTVC?
    private var isRotatedArrowIMG: Bool = false
    private var selectedIndexPath: IndexPath?
    private var isExpandedCell: Bool = false
    private var newsfeedTimer = Timer()
    private var currentIndexPath = IndexPath(item: 0, section: 0)
    private let marker = GMSMarker()
    private var threadPropertyDetails: ThreadStatus = .progressing
    private var threadUserReview: ThreadStatus = .progressing
    private var threadRequestStatus: ThreadStatus = .progressing
    private var resultPropertyDetails: KXPropertyDetailModel?
    private var resultUserReview: KXDetailUserReviewModel?
    private var resultRequestStatus: KXDetailsRequestStatusModel?
    private var sliderImagesTotal: Int = 0
    private var countrySource: [KXCountryListModel] = []
    private var propertyImageSourceKF: [KingfisherSource] = []
    private var isRentProperty: Bool = false
    private var requestId: Int?
//    private var propertyData = Json()
    private var ownerID:Int = 0
    private var isFirst = true
    private var imageArray = [[String:[String]]]()
    private let aboutMoreBTNAttributes : [NSAttributedStringKey: Any] = [
        NSAttributedStringKey.font : UIFont(name: "Montserrat-SemiBold", size: 12) as Any,
        NSAttributedStringKey.foregroundColor : UIColor.init(red: 99/255, green: 142/255, blue: 255/255, alpha: 1.0),
        NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
    private enum ThreadStatus: Int {
        case progressing
        case completed
    }
    private var latitude:Double = 0
    private var longitude:Double = 0
    private lazy var blurRadius: CGFloat = 5
    private lazy var availabilityDateText = "You cannot request to contact now. Property is not available. Property will be available by "
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MySearchesManager.instance.removeDataFromStore()
        ratingReviewBlurView.blurRadius = blurRadius
        ownerBlurView.blurRadius = blurRadius
        separatorBlurView.blurRadius = blurRadius
        
        propertySlidingImages.contentScaleMode = .scaleAspectFill
        propertySlidingImages.draggingEnabled = true
        propertySlidingImages.slideshowInterval = 3
        
        self.sliderImageCountLBL.isHidden = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(availabilityLBLBottomTapped))
        gestureRecognizer.numberOfTapsRequired = 1
        self.availabilityLBLBottom.addGestureRecognizer(gestureRecognizer)
      //  facilitiesCV.reloadData()
       // setfacilityImage()
        getCountryData()
        getDataWeb()
        //Latest edit
        propertyMapView.settings.scrollGestures = false
        propertyMapView.settings.zoomGestures = false
        //             self?.sliderImageCountLBL.text = (self?.propertySlidingImages.currentPage + 1).description + "/" + self?.sliderImagesTotal.description

        propertySlidingImages.currentPageChanged = { [weak self] _ /*currentPage*/ in
            guard let _ = self else {
                print("SelfRetineedddddddddddd")
                return
            }
            self?.sliderImageCountLBL.text = ((self?.propertySlidingImages.currentPage)! + 1).description + "/" + (self?.sliderImagesTotal.description)!
            //print("sliding Continues",self.propertySlidingImages.currentPage)
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        newsfeedTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(updateNewsfeed), userInfo: nil, repeats: true)
        newsfeedsPC.currentPage = 0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.furnituresTV.layoutIfNeeded()
        self.heightFurnitureTV.constant = self.furnituresTV.contentSize.height
        heightFacilitiesCV.constant = 55//self.facilitiesCV.contentSize.height
        heightSpacesCV.constant = 120//self.spacesCV.contentSize.height
        availableDateHeight.constant = 0
        self.view.layoutIfNeeded()
       
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        newsfeedTimer.invalidate()
        propertySlidingImages.stopTimer()
        propertySlidingImages.removeImageInputs()
        //parameterContainerForRent.shared.clearParams()

    }
    
    
    func setfacilityImage(){
        let unixTime = SecureSocket.getTimeStamp()
        let tokenKey = SecureSocket.getToken(with: unixTime)
        let param:[String:String] = ["user_id":"\(Credentials.shared.userId)","device_type":"\(Credentials.shared.deviceType)","device_token":"\(Credentials.shared.deviceTocken)","tokenkey":tokenKey,"unix_time":unixTime]
       // print(param)


        Webservices.getMethodWith(url: "getFacilities", parameter:param ) { (isComplete, json) in
            do{
                let tempData = json["Data"] as? [String:Any]
                let trueData = tempData!["facilities"]

                let parsedData = try JSONSerialization.data(withJSONObject: trueData!, options: .prettyPrinted)
                let decoder = JSONDecoder()
                let facilityData = try decoder.decode([KXFacilities].self, from: parsedData)
                for item in facilityData{
                    let facilityItem = FacilitiesImageSource(name: item.facility, image: item.facility, id: item.id)
                    self.facilitiesImageSource.append(facilityItem)
                }
                DispatchQueue.main.async {
                    self.facilitiesCV.reloadData()
                }
            }catch{
                print(error)
            }
        }


    }
    private func setUpInterface(){
        
        guard let propertyDetails = resultPropertyDetails else { return }
        let property = propertyDetails.property
        
        self.buildingId = property?.building_id ?? 0
        
        if property?.property_type == 1
        {
            //print("SELL")
            self.propTypeLB.text = "SELL"
            //self.propTypeLB.sizeToFit()
        }
        else if property?.property_type == 2
        {
            //print("Rent")
            self.propTypeLB.text = "RENT"
            //self.propTypeLB.sizeToFit()
        }

        self.confrimDealBTN.isUserInteractionEnabled = true

        availableDateHeight.constant = 0

        
        if isLoggedIn() {
            if property!.user_id.description == Credentials.shared.userId {
                //print("same user")
                favouriteBTN.isHidden = true
                confirmDealView.isHidden = false
                confirmDealBTN.setTitle("My Property", for: .normal)
                confirmDealBTN.isHidden = false
                availabilityLBLBottom.isHidden = true
                callBTN.isHidden = true
                aboutPropertyOwnerHeightConstraint.constant = 0
                //self.view.layoutIfNeeded()
            }else {
                setUpInterfaceReviewStatus()

                if resultPropertyDetails?.wishlist == 1 {
                    favouriteBTN.setImage(#imageLiteral(resourceName: "heartFiledShadow"), for: [])
                }else {
                    favouriteBTN.setImage(#imageLiteral(resourceName: "heartShadow"), for: [])
                }
                favouriteBTN.isHidden = false
                confirmDealView.isHidden = false
                callBTN.isHidden = false
                aboutPropertyOwnerHeightConstraint.constant = 261
                
                if property!.availableStatus == 0 || property!.availableStatus == 2 {
                    //let availableDateReceived =  propertyDetails.date_available.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                    //availabilityLBLBottom.text = availabilityDateText + availableDateReceived
                    availabilityLBLBottom.isHidden = false
                    confirmDealBTN.isHidden = true

                }else {
                    availabilityLBLBottom.isHidden = true
                    confirmDealBTN.isHidden = false
                    ratingReviewBlurView.isHidden = true
                }

                
                //self.view.layoutIfNeeded()
            }
        }else {

            confirmDealBTN.setTitle("Request Contact", for: .normal)
            confirmDealView.isHidden = false
            ratingReviewBlurView.isHidden = false
            ownerBlurView.isHidden = false
            separatorBlurView.isHidden = false
            callBTN.isHidden = false
            
            ///
            if property!.availableStatus == 0 || property?.availableStatus == 2 {
                //let availableDateReceived =  propertyDetails.date_available.convertDate(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
                //availabilityLBLBottom.text = availabilityDateText + availableDateReceived
                availabilityLBLBottom.isHidden = false
                confirmDealBTN.isHidden = true

            }else{
                availabilityLBLBottom.isHidden = true
                confirmDealBTN.isHidden = false

            }


        }
        
        var propertyImagesInputs: [InputSource] = []
        
        self.sliderImageCountLBL.isHidden = true

        for item in (property?.property_images)! {
            /// for offer request
//            propertyData["image"] = property?.property_images[0].image
            
            let imageViewTemp = UIImageView()
            imageViewTemp.kf.setImage(with: URL(string: "\(baseImageURLDetail)\(item.image)"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
                if imageViewTemp.image != nil{
//                    guard let _ = self else {
//                        return
//                    }
                    propertyImagesInputs.append(ImageSource(image: image!))
                    self.sliderImagesTotal += 1
                    self.propertySlidingImages.setImageInputs(propertyImagesInputs)
                    
                    self.sliderImageCountLBL.text = ((self.propertySlidingImages.currentPage) + 1).description + "/" + (property?.property_images.count.description)!

                 }
            })
        }
        
        propertySlidingImages.pageControl.isHidden = true
        
        BaseThread.asyncMainDelay(seconds: 2.5) {
            self.sliderImageCountLBL.isHidden = false
        }
        
        //sliderImageCountLBL.text = (propertySlidingImages.currentPage + 1).description + "/" + sliderImagesTotal.description
        
        propertyTitleLBL.text = property?.property_name.description.uppercased()
        propertyLocationLBL.text = property?.city.description
        propertyPriceLBL.text = " \(property?.usd_price.showPriceInDollar() ?? "")"
        
        aboutPropertyLBL.text = property?.about_property
        
        isRentProperty = property?.property_type == 1 ? false : true  // 2- for rent
        parkingView.isHidden = false
        propertyAddressLBL.text = property?.address
        
        
        var availableString = String()
        //print("AvailabilityStatus:",property!.availableStatus)
        switch property!.availableStatus {
        case 1 :
            availableString = "Available"
            availabilityIndicatorView.backgroundColor = UIColor.availableGreen
        case 0 :
            availableString = "Not Available"
            availabilityIndicatorView.backgroundColor = UIColor.notAvailableRed
        default: //2 -> almost available
            availableString = "Almost Available"
            availabilityIndicatorView.backgroundColor = UIColor.almostAvailableYellow

        }
        availabilityLBL.text = availableString
        
        
        // Property Rating
        if let propertyRatingValue = property?.property_ratings_total, !propertyRatingValue.isEmpty {
            let rating = propertyRatingValue[0].aggregate
            ratingCountLBL.text = rating
            propertyRatingView.isUserInteractionEnabled = false
            propertyRatingView.rating = Double(rating)!
        }
        
        if let bedroomCount = property?.property_bedrooms?.data {
            let bedroomCountInt = Int(bedroomCount)!
            
            if bedroomCountInt > 1 {
                bedroomCountLBL.text = bedroomCountInt.description + " Bedrooms"
            }else {
                bedroomCountLBL.text = bedroomCountInt.description + " Bedroom"
            }
        }
        
        if let bathroomCount = property?.property_bathrooms?.id {
            let bathroomCount = Int(bathroomCount)
            if bathroomCount > 1 {
                bathroomCountLBL.text = bathroomCount.description + " Bathrooms"
            }else {
                bathroomCountLBL.text = bathroomCount.description + " Bathroom"
            }

        }
        
        if let parkingCount = property?.parking_count{
            //print(parkingCount)
            if parkingCount == 1{
                parkingCountLBL.text = "Parking"
            }else{
                parkingView.isHidden = true
            }
        }
        
        if let value = property?.architecture_type {
            architectuteLBL.text = value
        }
        
        if let age = property?.age_building {
            if age <= 1 {
                propertyAgeLBL.text = age.description + " Year"
            }else{
                propertyAgeLBL.text = age.description + " Years"
            }
        }
        
        if let value = property?.floorSize {
            floorSizeLBL.text = value + " sq m"
        }

        //// If building type = 0, then below properties optionals
        if let buildingType = property?.building_type, buildingType.type == 1 {
            
            if let value = property?.floor_no, value > 0 {
                floorNoLBL.text = value.description
            }else {
                heightFloorNoKey.constant = 0
                heightFloorNoValue.constant = 0
            }
            
            if let value = property?.buildingName, value.isNotEmpty {
                buildingNameLBL.text = value.uppercased()
            }else {
                heightBuildingNameKey.constant = 0
                heightBuildingNameValue.constant = 0
            }

            if let value = property?.elevator, value > 0 {
                elevatorsLBL.text = value.description
            }else {
                heightElevatorkey.constant = 0
                heightElevatorValue.constant = 0
            }
            
        }else {
            heightFloorNoKey.constant = 0
            heightFloorNoValue.constant = 0
            
            heightBuildingNameKey.constant = 0
            heightBuildingNameValue.constant = 0

            heightElevatorkey.constant = 0
            heightElevatorValue.constant = 0

        }
        
        if let waterHeating = property?.water_heating {
            self.waterHeatingLBL.text = waterHeating
        }
        
        if let energy = property?.energy_type {
            self.energyLBL.text = energy
        }
        
        
        //
        if let lat = property?.latitude, let lon = property?.longitude, lat.isNotEmpty, lon.isNotEmpty {
            if let latDouble = Double(lat), let lonDouble = Double(lon) {
                self.latitude = latDouble
                self.longitude = lonDouble
                createMarker(at: CLLocationCoordinate2D(latitude: latDouble, longitude: lonDouble))
                let camera = GMSCameraPosition.camera(withLatitude: latDouble,
                                                      longitude: lonDouble,
                                                      zoom: 20)
                propertyMapView.camera = camera
                
            }
            
        }
        
        // Updating Rating and Reviews
        //print("property?.property_ratings:",property?.property_ratings as AnyObject)
        if let userReviewRating = property?.property_ratings, !userReviewRating.isEmpty {
            let userReview = property!.property_ratings[0]
            reviewTimeLBL.text = userReview.created_at
            reviewProfileNameLBL.text = (userReview.users?.first_name)! + " " + (userReview.users?.last_name)!
            reviewProfileLocationLBL.text = getCountryName(ofId: (userReview.users?.country_residence)!)
            
            if !userReview.rating_averages.isEmpty {
                reviewRatingCountLBL.text = userReview.rating_averages[0].aggregate
                reviewProfileRatingVIew.rating = Double(userReview.rating_averages[0].aggregate)!
            }
            reviewDescriptionLBL.text = userReview.comment
            
            print(property?.property_ratings.count)
            if property!.property_ratings.count > 1{
                if let firstUserReview = property?.property_ratings[1] {
                    let imageURL = firstUserReview.users?.image
                    reviewerFirstImage.kf.setImage(with: URL(string: baseImageURL2 + imageURL!), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
                }

            }
            if property!.property_ratings.count > 2{
                if let secondUserReview = property?.property_ratings[2] {
                    let imageURL = secondUserReview.users?.image
                    reviewerLastImage.kf.setImage(with: URL(string: baseImageURL2 + imageURL!), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
                }

            }

            if let reviewUserCount = property?.property_ratings.count {
                if reviewUserCount > 3 {
                    reviewersCountLBL.text = (reviewUserCount - 3).description + "+"
                    reviewersCountLBLView.isHidden = false
                }else{
                    reviewersCountLBLView.isHidden = true
                }
            }else{
                reviewersCountLBLView.isHidden = true
            }
        }else {
            // no rating and reviews
            heightRatingReviewView.constant = 0
           // self.view.layoutIfNeeded()
        }
        
        // Updating owner details
        //ownerImage
        ownerTimeStampLBL.text = getDateString(from: property?.users?.created_at)
        ownerNameLBL.text = (property?.users?.first_name)! + " " + (property?.users?.last_name)!
        ownerLocationLBL.text = getCountryName(ofId: (property?.users?.country_residence)!)
        ownerMailLBL.text = property?.users?.email
        ownerMobileLBL.text = property?.users?.mobile_number
        if let passId = property?.users?.id{
            ownerID = passId
            //print("t",ownerID)
        }
        
        // assgning sources
        if let news = property?.news_feeds, !news.isEmpty {
            newsFeedSource = news
        }else{
            heightNewsFeedsView.constant = 0
            //self.view.layoutIfNeeded()
        }
        furnitureSource = propertyDetails.furnitures.sorted(by: {$0.display_order < $1.display_order })
        if let fac = property?.property_facilitys, !fac.isEmpty {
            facilitiesSource = fac
        }else{
            
        }
        
        if let spaces = property?.property_images, !spaces.isEmpty {
            
            var alreadyThere = Set<KXDetailsPropertyImageModel>()
            let uniqueSpaces = spaces.flatMap { (space) -> KXDetailsPropertyImageModel? in
                guard !alreadyThere.contains(space) else { return nil }
                alreadyThere.insert(space)
                return space
            }
            spacesSource = uniqueSpaces
            getImageID()
            
        }
        
        facilitiesCV.reloadData()
        spacesCV.reloadData()
        furnituresTV.reloadData()
        newsfeedsCV.reloadData()
        
        BaseThread.asyncMain {
            self.heightFacilitiesCV.constant = 55.0
            self.heightSpacesCV.constant = 120
            //self.view.layoutIfNeeded()
        }
        
        viewDidAppear(true)
        
    }
    
    //MARK: Open map in Browser
    @IBAction func openMapInBrowser(_ sender: UIButton) {
        //?center=%f,%f"
        //https://www.google.com/maps/@42.585444,13.007813,6z
        //comgooglemaps://?center=%f,%f
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = "https://www.google.com/maps/@\(latitude),\(longitude),18z"
        //UIApplication.shared.openURL(URL(string:"https://www.google.com/maps/@42.585444,13.007813,6z")!)
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            UIApplication.shared.openURL(URL(string: "\(mapAppURL)")!)
        }else if UIApplication.shared.canOpenURL(URL(string: "\(browserURL)")!){
            UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
        }
    }
    
    private func setUpInterfaceReviewStatus(){
        
        guard let requestStatus = resultRequestStatus else { return }
        let currentStatus = requestStatus.response!.status
        self.requestId = requestStatus.response?.request_id
        let dealStatus = requestStatus.response?.deal_confirmation ?? 10 // default status
        print("GETTTTTTcurrentStatus",currentStatus,"DealStatus",requestStatus.response?.deal_confirmation as Any)

        
        if currentStatus == 3 {
            confirmDealBTN.setTitle("Request Contact", for: .normal)
           // availabilityLBL.text = availableString

        }
        else if currentStatus == 0 {
            // pending
            ratingReviewBlurView.isHidden = false
            ownerBlurView.isHidden = false
            separatorBlurView.isHidden = false

            self.confirmDealBTN.setTitle("Pending", for: .normal)
            self.confrimDealBTN.isUserInteractionEnabled = false
        }else if currentStatus == 1 {
            //accepted hide blur views
            ratingReviewBlurView.isHidden = true
            ownerBlurView.isHidden = true
            separatorBlurView.isHidden = true
            self.confirmDealBTN.setTitle("Confirm Deal", for: .normal)
            self.confrimDealBTN.isUserInteractionEnabled = true

            if dealStatus == 10 {
                // not offer requested
            }else if dealStatus == 0 {
                // pending offer request
                self.confirmDealBTN.setTitle("Deal Pending", for: .normal)
                self.confrimDealBTN.isUserInteractionEnabled = false
            }else if dealStatus == 2 {
                // rejected previously requested offer
                self.confirmDealBTN.setTitle("Confirm Deal Again", for: .normal)
                self.confrimDealBTN.isUserInteractionEnabled = true

            }
            
        }else if currentStatus == 2 {
            // rejected
            ratingReviewBlurView.isHidden = false
            ownerBlurView.isHidden = false
            separatorBlurView.isHidden = false

            self.confirmDealBTN.setTitle("Rejected", for: .normal)
            self.confrimDealBTN.isUserInteractionEnabled = false

        }
    }
    
    
    private func createMarker(at location: CLLocationCoordinate2D){
        let markerImage = UIImage(named: "maps-and-flags (1)") //.withRenderingMode(.alwaysTemplate)
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        marker.position = location
        marker.iconView = markerView
        marker.map = propertyMapView
    }

    
    private func isThreadsCompleted() -> Bool {
        if isLoggedIn() {
            if threadPropertyDetails == .completed && threadUserReview == .completed && threadRequestStatus == .completed {
                return true
            }else {
                return false
            }
        }else {
            if threadPropertyDetails == .completed {
                return true
            }else {
                return false
            }
        }
    }
    
    //MARK:  Rating View All Action
    
    @IBAction func viewAllRatingTapped(_ sender: UIButton) {
        if let vc = storyboardBooking?.instantiateViewController(withIdentifier: "KXRatingAndReviewsVC") as? KXRatingAndReviewsVC{
            vc.passingUserID = userID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    private func goBack(){
        BaseThread.asyncMain {
            if self.isFromPushNotification {
                let vc = self.storyboardMain?.instantiateViewController(withIdentifier: "KXHomeViewController") as! KXHomeViewController
                self.navigationController?.pushViewController(vc, animated: true)

            }else {
                self.imageArray.removeAll()
                self.propertySlidingImagesSource.removeAll()
                self.furnitureSource.removeAll()
                self.facilitiesSource.removeAll()
                self.spacesSource.removeAll()
                self.newsFeedSource.removeAll()
                self.deselectedCell = nil
                self.resultPropertyDetails = nil
                self.resultUserReview = nil
                self.resultRequestStatus = nil
                self.countrySource.removeAll()
                self.propertyImageSourceKF.removeAll()
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }   
    
    
    
    private func getPropertyDetail() {
        let url = "getPropertyDetail"
        var parameter = Json()
        parameter.updateValue(propertyId , forKey: "propertyid")
        if isLoggedIn() {
            parameter.updateValue(Credentials.shared.userId , forKey: "user_id")
        }
        
        if isFromSearch, parameterContainerForRent.shared.saleType == 2 {
            if parameterContainerForRent.shared.movingInDate != "" {
                parameter.updateValue(parameterContainerForRent.shared.movingInDate, forKey: "moving_in")
            }
            if parameterContainerForRent.shared.movingOutDate != "" {
                parameter.updateValue(parameterContainerForRent.shared.movingOutDate, forKey: "moving_out")
            }

            parameter.updateValue(parameterContainerForRent.shared.saleType.description, forKey: "saletype")
        }else if isFromMyRequest {
            if typeOfProperty == 2 {
                //rent
                parameter.updateValue(self.moving_inMyrequest, forKey: "moving_in")
                parameter.updateValue(self.durationMyRequest, forKey: "moving_out_date")
                parameter.updateValue(2.description, forKey: "saletype")
                
            }
            
        }
        
        
        print("Paramssssssss",parameter)
        Webservices2.getMethodWith(url: url, parameter: parameter) { (isSucceeded, responseProperty) in
            guard isSucceeded else {
                self.goBack()
                return
            }
            guard let responseDataProperty = responseProperty["Data"] as? Json, !responseDataProperty.isEmpty else {
                let message = responseProperty["Message"] as? String ?? "Error"
                showBanner(message: message)
                //print("Call Failed and going back")
                self.goBack()
                return
            }
            
            
            self.resultPropertyDetails = KXPropertyDetailModel.getValue(fromObject: responseDataProperty)
            
            if self.isFromSearch {
                if let name = self.resultPropertyDetails?.property?.property_name, let location = self.resultPropertyDetails?.property?.address, let price = self.resultPropertyDetails?.property?.price, let propertyImage = self.resultPropertyDetails?.property?.property_images, let id = self.resultPropertyDetails?.property?.id{
                    var saveSearchDictionary = [
                        "name":name,
                        "location":location,
                        "price":"\(price)",
                        "id":"\(id)"
                    ]
                    
                    if propertyImage.filter({$0.type_id == 6}).count > 0{
                        saveSearchDictionary.updateValue(propertyImage.filter({$0.type_id == 6})[0].image, forKey: "image")
                        // MySearchesManager.instance.saveImageDocumentDirectory(name: propertyImage.filter({$0.type_id == 6})[0].image)
                    }else{
                        saveSearchDictionary.updateValue(propertyImage.map({$0.image}).first ?? "", forKey: "image")
                        //MySearchesManager.instance.saveImageDocumentDirectory(name: propertyImage.map({$0.image})[0])
                    }
                    //print("Image Dictionary : \(saveSearchDictionary)")
                    MySearchesManager.instance.addDataToDictionary(data: saveSearchDictionary)
                    
                }
            }
           //print(self.imageArray)
            self.threadPropertyDetails = .completed
            if self.isThreadsCompleted(){
                BaseThread.asyncMain {
                    self.setUpInterface()
                    
                }
            }

            
            
            
        }
        
    }
    
    
    private func getRequestStatus(){
        let url = "getRrequestStatus"
        var parameter = Json()
        parameter.updateValue(propertyId, forKey: "property_id")
        parameter.updateValue(Credentials.shared.userId, forKey: "user_id")
        
        if isFromSearch, parameterContainerForRent.shared.saleType == 1 {
            parameter.updateValue(1, forKey: "type") // sale type 1 is Buy and type 2 is Rent
        }else if isFromSearch, parameterContainerForRent.shared.saleType == 2 {
            parameter.updateValue(2, forKey: "type") // sale type 1 is Buy and type 2 is Rent
            parameter.updateValue(parameterContainerForRent.shared.movingInDate, forKey: "moving_in")
            parameter.updateValue(parameterContainerForRent.shared.movingOutDate, forKey: "duration")
        }else {
            if typeOfProperty == 1 {
                parameter.updateValue(typeOfProperty, forKey: "type") // sale type 1 is Buy and type 2 is Rent
            }else {
                parameter.updateValue(typeOfProperty, forKey: "type") // sale type 1 is Buy and type 2 is Rent
                parameter.updateValue(self.moving_inMyrequest, forKey: "moving_in")
                parameter.updateValue(self.durationMyRequest, forKey: "moving_out_date")

            }
            
        }
            
            
//            if isFromMyRequest {
//            print("getrequestStatus Not FromSearch")
//            if typeOfProperty == 1 {
//                //buy
//                parameter.updateValue(1, forKey: "type") // sale type 1 is Buy and type 2 is Rent
//                rentORBuy = 1
//            }else {
//                //rent
//                parameter.updateValue(2, forKey: "type") // sale type 1 is Buy and type 2 is Rent
//                parameter.updateValue(self.moving_inMyrequest, forKey: "moving_in")
//                parameter.updateValue(self.durationMyRequest, forKey: "moving_out_date")
//                rentORBuy = 2
//            }
//
//
//        }else if isFromMyProperty {
//            print("getrequestStatus Not FromSearch")
//            if typeOfProperty == 1 {
//                //buy
//                parameter.updateValue(1, forKey: "type") // sale type 1 is Buy and type 2 is Rent
//                rentORBuy = 1
//            }else {
//                //rent
//                parameter.updateValue(2, forKey: "type") // sale type 1 is Buy and type 2 is Rent
//                parameter.updateValue(self.moving_inMyrequest, forKey: "moving_in")
//                parameter.updateValue(self.durationMyRequest, forKey: "moving_out_date")
//                rentORBuy = 2
//            }
//        }
//
        print("requestAPIIIIIIIII ",url)
        print("requestAPIIIIIIIII-param",parameter)
        Webservices2.getMethodWith(url: url, parameter: parameter) { (isSucceeded, responseRrequest) in
            guard isSucceeded else {
                self.goBack()
                self.setUpInterfaceReviewStatus()
                return
                
            }
            
            
            guard let responseDataRrequest = responseRrequest["Data"] as? Json, !responseDataRrequest.isEmpty else {
                let message = responseRrequest["Message"] as? String ?? "Error"
                showBanner(message: message)
                self.goBack()
                return
            }
            
            
            self.resultRequestStatus = KXDetailsRequestStatusModel.getValue(fromObject: responseDataRrequest)
            self.passingConfirmationRequestID = self.resultRequestStatus?.response?.request_id ?? 0
            print(" *## Passing Request ID ",self.passingConfirmationRequestID)
            self.threadRequestStatus = .completed
            if self.isThreadsCompleted(){
                BaseThread.asyncMain {
                    self.setUpInterface()
                }

            }

            
        }
        
    }
    
    //MARK:GetChatRoomData
    func getChatRoomDetails(){
        let url = "getChatRoom"
        let parameter = ["owner_id":"89","property_id":""]
        Webservices.postMethod(url: url, parameter: parameter) { (isComplete, json) in
            
        }
    }
    
    private func getCountryData(){
        getCountryListWeb { (list) in
            self.countrySource = list
            self.threadUserReview = .completed
            if self.isThreadsCompleted(){
                BaseThread.asyncMain {
                    self.setUpInterface()

                }

            }

        }

    }

    
    
    private func getCountryName(ofId: Int) -> String{
        return self.countrySource.filter{ $0.id == ofId }.first?.name ?? ""
    }
    
    
    
    private func getDataWeb() {
        if isLoggedIn() {
            getPropertyDetail()
            getRequestStatus()
           
        }else{
            getPropertyDetail()
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        print("didReceiveMemoryWarning")
    
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        goBack()
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func favouriteTapped(_ sender: UIButton) {
        
        if isLoggedIn() {
            if resultPropertyDetails?.wishlist == 1 {
                deleteWishlist()
            }else {
                addToWishList()
            }
        }else {
            let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    private func deleteWishlist(){
        let url = "deleteWishlist"
        var param = Json()
        param.updateValue(Credentials.shared.userId, forKey: "user_id")
        param.updateValue(self.propertyId, forKey: "propertyid")

        //print(resultPropertyDetails!.wishlist_id)
        Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
            //print("deleted from wishlist")
            guard isSucceeded else { return }
            BaseThread.asyncMain {
                self.favouriteBTN.setImage(#imageLiteral(resourceName: "heartShadow"), for: [])
                self.resultPropertyDetails?.wishlist = 0
                var notificationInfo = Json()
                notificationInfo["property_id"] = self.propertyId
                notificationInfo["wish_list"] = 0
                notificationInfo["segue_type"] = self.detailSegueIsProperty
                notificationInfo["building_id"] = self.buildingId
                
                NotificationCenter.default.post(name: Notification.Name.searchSourceInfoChanged, object: nil, userInfo: notificationInfo)

            }
        }
    }
    
    private func addToWishList(){
        let url = "addtoWishlist"
        var param = Json()
        param.updateValue(propertyId, forKey: "property_id")
        param.updateValue(Credentials.shared.userId, forKey: "user_id")
        
        Webservices2.postMethod(url: url, parameter: param) { (isSucceeded, response) in
            //print("added to wishlist")
            guard isSucceeded else { return }
            
            BaseThread.asyncMain {
                self.favouriteBTN.setImage(#imageLiteral(resourceName: "heartFiledShadow"), for: [])
                self.resultPropertyDetails?.wishlist = 1
                var notificationInfo = Json()
                notificationInfo["property_id"] = self.propertyId
                notificationInfo["wish_list"] = 1
                notificationInfo["segue_type"] = self.detailSegueIsProperty
                notificationInfo["building_id"] = self.buildingId

                NotificationCenter.default.post(name: Notification.Name.searchSourceInfoChanged, object: nil, userInfo: notificationInfo)

//                self.getDataWeb()
            }
        }
        
    }
    
    
    
    @IBAction func slideMenuTapped(_ sender: UIButton) {
        showSideMenu()
        
    }
    //MARK:- Pdf Btns
    @IBAction func titleDeedTapped(_ sender: UIButton) {
        
        if isLoggedIn() {
            if Credentials.shared.userId == resultPropertyDetails!.property!.user_id.description {
                let pdflink = resultPropertyDetails?.property?.property_deeds[0].titleDeed
                let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
                vc.link = pdflink!
                vc.isLocal = false
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                guard let requestStatus = resultRequestStatus else {
                    showBanner(title: "Access Unauthorized ", message: "Please send request")
                    return
                }
                let currentStatus = requestStatus.response!.status
                if currentStatus == 1 {
                    let pdflink = resultPropertyDetails?.property?.property_deeds[0].titleDeed
                    let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
                    vc.link = pdflink!
                    vc.isLocal = false
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else {
                    showBanner(title: "Access Unauthorized ", message: "Please send request")
                    
                }

            }
            
        }else {
            showBanner(title: "Access Unauthorized ", message: "Please send request")
        }
        

    }
    
    
    
    @IBAction func brochureTapped(_ sender: UIButton) {

        if let prop = resultPropertyDetails?.property {
            
            if !prop.property_brochures.isEmpty {
                
                let link = prop.property_brochures[0].brochure
                if link.isNotEmpty {
                    let pdflink = link
                    let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
                    vc.link = pdflink
                    vc.isLocal = false
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            }
            
        }else {
            showBanner(message: "Brochure not available")
        }
        
        return
        

        
        if isLoggedIn() {
            if Credentials.shared.userId == resultPropertyDetails!.property!.user_id.description {
                let pdflink = resultPropertyDetails?.property?.property_brochures[0].brochure
                let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
                vc.link = pdflink!
                vc.isLocal = false
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                guard let requestStatus = resultRequestStatus else {
                    showBanner(title: "Access Unauthorized ", message: "Please send request")
                    return
                }
                let currentStatus = requestStatus.response!.status
                if currentStatus == 1 {
                    // chat request accepted
                    let pdflink = resultPropertyDetails?.property?.property_brochures[0].brochure
                    let vc = storyboardProperty?.instantiateViewController(withIdentifier: "KXWebViewController") as! KXWebViewController
                    vc.link = pdflink!
                    vc.isLocal = false
                    self.navigationController?.pushViewController(vc, animated: true)
                }else {
                    showBanner(title: "Access Unauthorized ", message: "Please send request")
                }
            }
        }else {
            showBanner(title: "Access Unauthorized ", message: "Please send request")
        }
        
        
        
    }
    
    @IBAction func sliderNextTapped(_ sender: UIButton) {
        if propertySlidingImages.images.count > 1 {
            propertySlidingImages.setCurrentPage(propertySlidingImages.pageControl.currentPage + 1, animated: true)
        }
    }
    
    @IBAction func sliderPreviousTapped(_ sender: UIButton) {
        if propertySlidingImages.images.count > 1 {
            propertySlidingImages.setCurrentPage(propertySlidingImages.pageControl.currentPage - 1, animated: true)

        }

    }
    
    @IBAction func callAssistanceTapped(_ sender: UIButton) {
        
        if isLoggedIn() {
            if Credentials.shared.userId != ownerID.description {
                guard let requestStatus = resultRequestStatus else {
                    showBanner(message: "Please request contact.")
                    return
                }
                let currentStatus = requestStatus.response!.status
                if currentStatus == 1 {
                    guard let ownerPhoneNumber = ownerMobileLBL.text else {
                        showBanner(message: "Contacts not available")
                        return
                    }
                    let alertController = UIAlertController(title: "Koloxo", message: "Do you want to call ? \n\(ownerPhoneNumber)", preferredStyle: .alert)
                    let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                        ownerPhoneNumber.makeACall()
                        alertController.dismiss(animated: true, completion: nil)
                    })
                    let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                        alertController.dismiss(animated: true, completion: nil)
                    })
                    alertController.addAction(yesPressed)
                    alertController.addAction(noPressed)
                    present(alertController, animated: true, completion: nil)
                }else{
                    showBanner(message: "Please request contact.")
                }
            }
        }else{
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func moreAboutPropertyTapped(_ sender: UIButton) {
        
        if isAbovePropertyMoreTapped {
            aboutPropertyLBL.numberOfLines = 3
            isAbovePropertyMoreTapped = false
            let attributeString = NSMutableAttributedString(string: "More", attributes: aboutMoreBTNAttributes)
            aboutPropertyMoreBTN.setAttributedTitle(attributeString, for: .normal)
        }else{
            aboutPropertyLBL.numberOfLines = 0
            isAbovePropertyMoreTapped = true
            let attributeString = NSMutableAttributedString(string: "Less", attributes: aboutMoreBTNAttributes)
            aboutPropertyMoreBTN.setAttributedTitle(attributeString, for: .normal)
        }
        
    }
    
    @IBAction func facilitiesMoreTapped(_ sender: UIButton) {
        if isFacilitiesMoreTapped {
            facilitiesMoreLBL.text = "More"
            heightFacilitiesCV.constant = 55.0
            isFacilitiesMoreTapped = false
            facilitiesMoreImage.transform = CGAffineTransform.identity
        }else{
            facilitiesMoreLBL.text = "Less"
            if facilitiesCV.contentSize.height >= CGFloat(55.0) {
                heightFacilitiesCV.constant = facilitiesCV.contentSize.height
            } else {
                heightFacilitiesCV.constant = 120
            }
            isFacilitiesMoreTapped = true
            facilitiesMoreImage.transform = facilitiesMoreImage.transform.rotated(by: .pi)
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func spacesMoreTapped(_ sender: UIButton) {
        if isSpacesMoreTapped {
            spacesMoreLBL.text = "More"
            heightSpacesCV.constant = 120.0
            isSpacesMoreTapped = false
            spacesMoreImage.transform = CGAffineTransform.identity
        }else{
            spacesMoreLBL.text = "Less"
            if spacesCV.contentSize.height >= CGFloat(55.0) {
                heightSpacesCV.constant = spacesCV.contentSize.height
            } else {
                heightSpacesCV.constant = 120
            }
            isSpacesMoreTapped = true
            spacesMoreImage.transform = facilitiesMoreImage.transform.rotated(by: .pi)
            
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func viewProfileTapped(_ sender: UIButton) {
        if isLoggedIn() {
            if Credentials.shared.userId != ownerID.description {
                guard let requestStatus = resultRequestStatus else { return }
                let currentStatus = requestStatus.response!.status
                if currentStatus == 1 {
                    let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXProfileEditVC") as! KXProfileEditVC
                    vc.passingUserID = "\(ownerID)"
                    vc.profileReviewType = .owner
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        //KXProfileEditVC
    }
    
    @IBAction func viewChatTapped(_ sender: UIButton) {
        if isLoggedIn() {
            guard let requestStatus = resultRequestStatus else { return }
            let currentStatus = requestStatus.response!.status
            if currentStatus == 3 {
                getRequestContactWeb()
            }else if currentStatus == 1 {
                if requestId != nil && requestId != 0 {
                    let vc = storyboardChat!.instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
                    vc.propertyID = self.propertyId
                    vc.propertyName = resultPropertyDetails?.property?.property_name ?? "Property"
                    vc.propertyOwnerId = ownerID
                    vc.isRentProperty = self.isRentProperty
                    
                    Webservices.postMethod(url: "getChatRoom", parameter: ["owner_id":"\(ownerID)","property_id":"\(propertyId)"], CompletionHandler: { (isComplete, json) in
                        if isComplete{
                            do{
                                if let trueData = json["Data"] as? [[String:Any]]{
                                    let data = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                                    let parsedData = try JSONDecoder().decode([ChatRoomData].self, from: data)
                                    
                                    let filteredArray = parsedData.filter({$0.request_id == self.requestId!})
                                    vc.channelID = filteredArray[0].id
                                    vc.channelName = filteredArray[0].channel
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }else{
                                    print("Failed conversion")
                                }
                            }catch{
                                print(error)
                            }
                        }

                    })
                    
                }
                
            }
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @IBAction func confirmDealTapped(_ sender: UIButton) {
        // submit offer
        if let btnTitle = sender.title(for: .normal), btnTitle == "My Property"{
            return
        }

        if isLoggedIn() {
            guard let requestStatus = resultRequestStatus else { return }
            let currentStatus = requestStatus.response!.status
            let dealStatus = requestStatus.response?.deal_confirmation ?? 10 // default status
            print("currentStatus",currentStatus,"DealStatus",requestStatus.response?.deal_confirmation as Any)
            if currentStatus == 3 {
                getRequestContactWeb()
            }else if currentStatus == 1 && (dealStatus == 10 || dealStatus == 2) {
                
                if requestId != nil && requestId != 0 {
                    let vc = self.storyboardSearch!.instantiateViewController(withIdentifier: "KXConfirmDealVC") as! KXConfirmDealVC
                    vc.isRentProperty = self.isRentProperty
                    vc.propertyId = self.propertyId
                    vc.requestId = self.requestId!
                    //vc.propertyData = self.propertyData
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    // Action when not available or almost available property tapped
    @objc private func availabilityLBLBottomTapped() {
        print("Showing available dates")
        let vc = storyboardBooking?.instantiateViewController(withIdentifier: "KXPreviousBookingsVC") as! KXPreviousBookingsVC
        vc.propertyID = propertyId.description
//        if let propertyDetails = resultPropertyDetails{
//            vc.availableDate = propertyDetails.date_available
//        }
        self.present(vc, animated: false, completion: nil)
    }
    
    private func getRequestContactWeb() {
        let url = "requestContact"
        var params = Json()
        params["user_id"] = Credentials.shared.userId
        params["property_id"] = self.propertyId
        params["device_type"] = Credentials.shared.deviceType
        params["device_token"] = Credentials.shared.deviceTocken
        
        if isFromSearch {
            if parameterContainerForRent.shared.saleType == 2 {
                if parameterContainerForRent.shared.movingInDate != "" {
                    params.updateValue(parameterContainerForRent.shared.movingInDate, forKey: "moving_in")
                }
                if parameterContainerForRent.shared.movingOutDate != "" {
                    params.updateValue(parameterContainerForRent.shared.movingOutDate, forKey: "duration")
                }
                
            }

        }

        print("Parameter:",params)
        Webservices2.postMethod(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else { return }
            print("Response",response)
            if let ErrorCode = response["ErrorCode"] as? String {
                if ErrorCode == "0" {
                    showBanner(title: "", message: "Request submitted", style: .success)
                    self.getDataWeb()
                  self.getRequestStatus()
                }else {
                    showBanner(message: "Request Failed.Try Again")
                }
            }
            
        }
        
        
    }
    
    @IBAction func requestContactTapped(_ sender: UIButton) {
        
      // getRequestContactWeb()
        
    }
    
    
    
    @IBAction func expandCollapseTVTapped(_ sender: BaseButton) {
        print("expandsss")
        let indexPath = sender.indexPath!
        guard let selectedCell = furnituresTV.cellForRow(at: indexPath) as? KXPropertyDetailsFurnitureTVC else { return }
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
        //updateContent(of: selectedCell, byIndexPath: selectedIndexPath!)
        furnituresTV.beginUpdates()
        self.rotateArrowIMG(of: selectedCell)
        furnituresTV.endUpdates()
        self.heightFurnitureTV.constant = self.furnituresTV.contentSize.height
    }
    
    private func rotateArrowIMG(of selectedCell: KXPropertyDetailsFurnitureTVC){
        let towardsDown = CGFloat(Double.pi/2)
        let towardsRight = CGFloat(-Double.pi/2)
        
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                if self.deselectedCell == nil {
                    /// tapping on the cell first time
                    selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                    self.isRotatedArrowIMG = true
                }else if self.deselectedCell == selectedCell {
                    /// tapping on already selected cell
                    if self.isRotatedArrowIMG {
                        selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsRight)
                        self.isRotatedArrowIMG = false
                    }else{
                        selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                        self.isRotatedArrowIMG = true
                    }
                }else{
                    /// tapping on other cell not already selected cell
                    selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                    guard let dCell = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
                    if self.isRotatedArrowIMG {
                        dCell.arrowImage.transform = dCell.arrowImage.transform.rotated(by: towardsRight)
                    }
                    self.isRotatedArrowIMG = true
                }
                self.deselectedCell = selectedCell
            })
        }
    }
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        
        if isLoggedIn() {
            let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)

        }
        
    }
    
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        
        if isLoggedIn() {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .chat
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }

        
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        
        if isLoggedIn() {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
            vc.segueType = .notification
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXLoginViewController") as! KXLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }

        
    }
    
    //MARK: Model Data
    /*
    "id": 147,
    "owner_id": 203,
    "property_id": 619,
    "request_id": 140,
    "channel": "Koloxo_203_619_140",
    "created_at": "2018-07-10 12:45:23",
    "updated_at": "2018-07-10 12:45:23",
    "property": {
    "id": 619,
    "property_name": "Akhil New for chat test"
    },
    "chat_room_members": [
    {
    "id": 162,
    "chat_room_id": 147,
    "customer_id": 203,
    "created_at": "2018-07-10 07:15:23",
    "updated_at": "2018-07-10 07:15:23",
    "users": {
    "first_name": "Akhil",
    "id": 203,
    "last_name": "Akhil",
    "image": "/storage/app/user_images/968980user_image.jpg"
    }
    },
 */
    struct ChatRoomData:Codable{
        let id:Int
        let channel:String
        let request_id:Int
    }
    
}




// table view data source
extension KXPropertyDetailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return furnitureSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
        return selectedIndexPath == indexPath ? (isExpandedCell ? 253 :  50) :  50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KXPropertyDetailsFurnitureTVC", for: indexPath) as? KXPropertyDetailsFurnitureTVC else { return UITableViewCell() }
        cell.expandCollapseBTN.indexPath = indexPath
        let tag = indexPath.row
        cell.furnituresCV.tag = tag
        let source = furnitureSource[tag]
        cell.titleLBL.text = source.category
        print(indexPath)
//        print("KXPropertyDetailsFurnitureTVC",source.property_furnitures.count,source.property_furnitures)
//        print("Property_Furniture",source.property_furnitures[0].description)
        //furnitureCVSource[indexPath.row] = source.property_furnitures
        furnitureCVSource.insert(source.property_furnitures, at: tag)
        cell.furnituresCV.reloadData()
        
        if furnitureCVSource[tag].count == 1 {
            cell.furnituesPC.isHidden = true
        } else {
            cell.furnituesPC.isHidden = false
        }
        cell.furnituesPC.numberOfPages = furnitureCVSource[tag].count
        
        cell.furnituesPC.currentPage = 0
        return cell
    }
    
}

// table view delegate
extension KXPropertyDetailsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectedTableCell")
    }
}

// collection view data source
extension KXPropertyDetailsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == facilitiesCV {
            
            if facilitiesSource.count > 3 {
                facilitiesMoreView.isHidden = false
            } else {
                facilitiesMoreView.isHidden = true
            }
            self.view.layoutIfNeeded()
            
            
            return facilitiesSource.count
        }else if collectionView ==  spacesCV {
            print(imageArray.count)
            if imageArray.count > 2 {
                spacesMoreView.isHidden = false
            } else {
                spacesMoreView.isHidden = true
            }
            self.view.layoutIfNeeded()
            return imageArray.count//spacesSource.count
        }else if collectionView == newsfeedsCV {
            return newsFeedSource.count
        }else {
            // furniture cv inside table view
            
            guard let _ = furnituresTV.dequeueReusableCell(withIdentifier: "KXPropertyDetailsFurnitureTVC") as? KXPropertyDetailsFurnitureTVC else { return 0 }
            
            
            
            return furnitureCVSource[collectionView.tag].count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == facilitiesCV {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsFacilitiesCVC", for: indexPath) as? KXPropertyDetailsFacilitiesCVC else { return UICollectionViewCell() }
            let source = facilitiesSource[indexPath.item]
            cell.titleLBL.text = source.facility?.facilities
            let itemID = source.facility?.id
          //  print(itemID)
            let _image = facilitiesImageSource.filter({$0.id == itemID}).first?.image ?? "forma1"
         //   print(facilitiesImageSource)
            let imageStr = cell.titleLBL.text ?? "forma1-4"
            cell.facilitiesImage.image = UIImage(named:imageStr)
            return cell
            
        }else if collectionView == spacesCV {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsSpacesCVC", for: indexPath) as? KXPropertyDetailsSpacesCVC else { return UICollectionViewCell() }
            
            //            let source = spacesSource[indexPath.item]
            //            cell.spaceTitleLBL.text = source.types?.type
            //            cell.spaceImage.tag = indexPath.item
            //            cell.spaceImage.kf.setImage(with: URL(string: baseImageURL2 + source.image), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            //MARK:- to imple
            let source = imageArray[indexPath.item]
            
            var typeOfImageName = String()
            var typeOfImage = [String]()
            for (key, value) in source {
                typeOfImageName = key
                typeOfImage = value
            }
            
            
            
            
            
            cell.spaceTitleLBL.text = typeOfImageName
            cell.spaceImage.tag = indexPath.item
            cell.spaceImage.kf.setImage(with: URL(string: baseImageURL2 + typeOfImage[0]), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            //
            //            cell.spaceImage.tag = indexPath.item
            //            cell.spaceImage.kf.setImage(with: URL(string: baseImageURL2 + source.image), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.connected(_:)))
            cell.spaceImage.isUserInteractionEnabled = true
            cell.spaceImage.addGestureRecognizer(tapGestureRecognizer)
            return cell
            
        }else if collectionView == newsfeedsCV {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsNewsFeedsCVC", for: indexPath) as? KXPropertyDetailsNewsFeedsCVC else { return UICollectionViewCell() }
            
            let source = newsFeedSource[indexPath.item]
            cell.titleLBL.text = source.title
            cell.detailsTextView.text = source.description
            cell.timeStampLBL.text = getDateString(from: source.created_at)
            print("newsfeedcell",cell.titleLBL.text as Any)
            return cell
            
        }else{
            // furniture list inside the tableview cell
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KXPropertyDetailsFurnituresCVC", for: indexPath) as? KXPropertyDetailsFurnituresCVC else { return UICollectionViewCell() }
            cell.tag = indexPath.item
            
            let source = furnitureCVSource[collectionView.tag][indexPath.item]
            cell.titleLBL.text = source.title
            cell.descriptionTXV.text = source.description
            
            return cell
            
        }
        
    }
    
    
}

extension KXPropertyDetailsVC {
    
    func getImageID() {
        
        if isFirst {
           // print(spacesSource.count)
            _ = spacesSource.map{$0.image}
            //print(a)
            var typeID = spacesSource.map{$0.types?.type}
            //print(typeID)
            
            
            typeID = uniqueElementsFrom(array: typeID as! [String])
            
            //print(typeID)
            
            
            
            //create image array
            for id in typeID {
                var temp = [String]()
                for i in 0..<spacesSource.count {
                    if spacesSource[i].types?.type == id {
                        temp.append(spacesSource[i].image)
                        print(temp)
                    }
                }
                imageArray.append([id! : temp])
                
            }
           // print(imageArray.count)
           // print(imageArray)
            isFirst = false
        }
    }
    
    func uniqueElementsFrom(array: [String]) -> [String] {
        //Create an empty Set to track unique items
        var set = Set<String>()
        let result = array.filter {
            guard !set.contains($0) else {
                //If the set already contains this object, return false
                //so we skip it
                return false
            }
            //Add this item to the set since it will now be in the array
            set.insert($0)
            //Return true so that filtered array will contain this item.
            return true
        }
        return result
    }
    
    
    @objc func connected(_ sender:UITapGestureRecognizer){
        //print("you tap image number")
        let destinationVC = self.storyboardSearch!.instantiateViewController(withIdentifier: "PhotoViewerVC") as!  PhotoViewerVC
        
        if let imageContainer = sender.view as? UIImageView {
            print(imageContainer.tag)
            let imageDictionary = imageArray[imageContainer.tag]
            
            var typeOfImage = [String]()
            for (_, value) in imageDictionary {
                typeOfImage = value
            }
            
            destinationVC.imageFrom = typeOfImage

            self.present(destinationVC, animated: true, completion: nil)
            
        }
        
    }
}
// collection view delegate
extension KXPropertyDetailsVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == facilitiesCV {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 3
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            let height =  55
            
            return CGSize(width: size, height: height)
            
        }else if collectionView == spacesCV {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 2
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(cellsCount))
            let height = 120
            
            return CGSize(width: size, height: height)
            
        }else{
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == newsfeedsCV {
            var visibleRect = CGRect()
            visibleRect.origin = newsfeedsCV.contentOffset
            visibleRect.size = newsfeedsCV.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = newsfeedsCV.indexPathForItem(at: visiblePoint) else { return }
            currentIndexPath = indexPath
            newsfeedsPC.currentPage = currentIndexPath.item
        }else{
            guard let _ = scrollView as? UICollectionView else { return }
            guard let cell = furnituresTV.cellForRow(at: IndexPath(row: scrollView.tag, section: 0)) as? KXPropertyDetailsFurnitureTVC else { return }
            var visibleRect = CGRect()
            visibleRect.origin = cell.furnituresCV.contentOffset
            visibleRect.size = cell.furnituresCV.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = cell.furnituresCV.indexPathForItem(at: visiblePoint) else { return }
            cell.furnituesPC.currentPage = indexPath.item
            
        }
    }
    
    @objc func updateNewsfeed(){
        newsFeedCount = newsFeedSource.count
        newsfeedsPC.numberOfPages = newsFeedCount
        guard newsFeedCount > 0 else { return }
        if newsFeedCount > currentIndexPath.item + 1{
            let nextIndexpath = IndexPath(item: currentIndexPath.item + 1, section: 0)
            newsfeedsCV.scrollToItem(at: nextIndexpath, at: .centeredHorizontally, animated: true)
            currentIndexPath = nextIndexpath
            newsfeedsPC.currentPage = currentIndexPath.item
        }else{
            currentIndexPath = IndexPath(item: 0, section: 0)
            newsfeedsCV.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: true)
            newsfeedsPC.currentPage = 0
        }
    }
    
    
}

struct FacilitiesImageSource{
    let name:String
    let image:String
    let id:Int
}

/*
 func blurViews(){
 if let response = resultRequestStatus?.response?.status{
 
 if response == 1{
 ratingReviewBlurView.isHidden = true
 ownerBlurView.isHidden = true
 separatorBlurView.isHidden = true
 }else{
 ratingReviewBlurView.isHidden = false
 ownerBlurView.isHidden = false
 separatorBlurView.isHidden = false
 }
 }else{
 ratingReviewBlurView.isHidden = false
 ownerBlurView.isHidden = false
 separatorBlurView.isHidden = false
 }
 }
"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."//
 */

