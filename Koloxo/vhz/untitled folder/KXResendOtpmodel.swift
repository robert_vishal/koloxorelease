//
//  KXResendOtpmodel.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 17/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
public class KXResendOtpmodel {
    public var id : Int = 0
    public var otp : String = ""
    
    
    public class func getValue(fromObject: Json?) -> KXResendOtpmodel {
        guard let fromObject = fromObject else { return KXResendOtpmodel() }
        return KXResendOtpmodel (object: fromObject)!
    }
 
    required public init?(object: Json) {
        id = object["id"] as? Int ?? 0
        otp = object["otp"] as? String ?? ""
    }
    
    private init(){
        
    }
}
