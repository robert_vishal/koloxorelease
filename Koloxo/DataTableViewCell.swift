//
//  DataTableViewCell.swift
//  collapasbleTbale
//
//  Created by admin on 27/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import DatePickerDialog

class DataTableViewCell: UITableViewCell, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var secondViewContent: UIView!
    @IBOutlet weak var firstViewContent: UIView!
    
    //    @IBOutlet weak var titile_Lbl: UILabel!
    
    //SendDataOutlets - Buy
    @IBOutlet weak var propertyTitleTFB: UITextField!
    
    @IBOutlet weak var propertyPriceTFB: UITextField!
    
    @IBOutlet var floorSizeTFB: UITextField!
    
    
    @IBOutlet var priceNegotiableYes: UIImageView!
    
    @IBOutlet var priceNegotiableNo: UIImageView!
    
    @IBOutlet var reputedProfileYes: UIImageView!
    
    @IBOutlet var reputedProfileNo: UIImageView!
    
    @IBOutlet weak var currencyBTNB: UIButton!
    
    
    @IBOutlet weak var requiredDepositsTFB: UITextField!
    
    @IBOutlet weak var propertyTypeLBLB: UILabel!
    
    @IBOutlet weak var ageOfBuildingLBLB: UILabel!
    
    @IBOutlet weak var elevatorsLBLB: UILabel!
    
    @IBOutlet weak var floorNumberLBLB: UILabel!
    
    //    @IBOutlet weak var buildingNameTFB: UITextField!
    
    @IBOutlet var bedroomLBLB: UILabel!
    
    @IBOutlet var bathroomLBLB: UILabel!
    
    @IBOutlet var buildingDescriptionB: UITextView!
    
    @IBOutlet var uploadedBrochureViewBuy: UIView!
    
    @IBOutlet var addBrochureViewBuy: BaseGradientView!
    @IBOutlet var uploadedDeedViewBuy: UIView!
    @IBOutlet var addDeedViewBuy: BaseGradientView!
    
    
    
    //Send Data Outlets - Rent
    
    @IBOutlet weak var currencyBTNR: UIButton!
    
    @IBOutlet weak var propertyTitleTFR: UITextField!
    
    @IBOutlet weak var propertyPriceTFR: UITextField!
    
    @IBOutlet var floorSizeTFR: UITextField!
    
    @IBOutlet var dateBTNRef: UIButton!
    
    
    @IBOutlet weak var requiredDepositsTFR: UITextField!
    
    @IBOutlet var priceNegotiableYesRent: UIImageView!
    
    @IBOutlet var priceNegotiableNoRent: UIImageView!
    
    @IBOutlet var reputedProfileYesRent: UIImageView!
    
    @IBOutlet var reputedProfileNoRent: UIImageView!
    
    @IBOutlet var brochureFileNameLBLR: UILabel!
    @IBOutlet var titledeedFileNameLBLR: UILabel!
    @IBOutlet var brochureFileNameLBLB: UILabel!
    @IBOutlet var titledeedFileNameLBLB: UILabel!
    @IBOutlet var buildingFloorViewB: UIView!
    @IBOutlet var buildingFloorViewR: UIView!
    @IBOutlet var elevatorViewR: UIView!
    @IBOutlet var elevatorViewB: UIView!
    
    @IBOutlet var priceHintLBLB: UILabel!
    @IBOutlet var priceHintLBLR: UILabel!
    @IBOutlet var depositHintLBLB: UILabel!
    @IBOutlet var depositHintLBLR: UILabel!

    @IBOutlet var buildingTypeLBLR: UILabel!
    @IBOutlet var waterHeatingLBLR: UILabel!
    @IBOutlet var energyTypeLBLR: UILabel!
    @IBOutlet var buildingTypeLBLB: UILabel!
    @IBOutlet var waterHeatingLBLB: UILabel!
    @IBOutlet var energyTypeLBLB: UILabel!

    
    @IBOutlet weak var propertyTypeLBLR: UILabel!
    
    @IBOutlet weak var ageOfBuildingLBLR: UILabel!
    
    @IBOutlet weak var elevatorsLBLR: UILabel!
    
    @IBOutlet weak var floorNumberLBLR: UILabel!
    
    //    @IBOutlet weak var buildingNameTFR: UITextField!
    
    @IBOutlet var bedroomLBLR: UILabel!
    
    @IBOutlet var bathroomLBLR: UILabel!
    
    @IBOutlet var buildingDescriptionR: UITextView!
    
    @IBOutlet var brochureUploadedViewRent: UIView!
    
    @IBOutlet var deedUploadedViewRent: UIView!
    
    @IBOutlet var uploadedBrochureIcon: UIView!
    
    @IBOutlet var uploadedDeedIcon: UIView!
    
    @IBOutlet var uploadedBrochureIconBuy: UIView!
    
    @IBOutlet var uploadedDeedIconBuy: UIView!
    
    
    @IBOutlet weak var brochureGradientVIewRent: BaseGradientView!
    
    @IBOutlet weak var deedGradientViewRent: BaseGradientView!
    
    @IBOutlet weak var deedGradientViewBuy: BaseGradientView!
    
    @IBOutlet weak var brochureGradientViewBuy: BaseGradientView!
    
    fileprivate var textViewPlaceholder = "Type a short description."

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buildingDescriptionB.delegate = self
        buildingDescriptionR.delegate = self
        requiredDepositsTFB.delegate = self
        requiredDepositsTFR.delegate = self
        propertyPriceTFR.delegate = self
        propertyPriceTFB.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textViewPlaceholder {
            textView.text = ""
            textView.textColor = UIColor.typingTextColor
        }else{
            if textView.text == nil || textView.text.isEmpty {
                textView.text = textViewPlaceholder
                textView.textColor = UIColor.textViewPlaceholderColor
            }else{
                textView.textColor = UIColor.typingTextColor
            }
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == nil || textView.text.isEmpty {
            textView.text = textViewPlaceholder
            textView.textColor = UIColor.typingTextColor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == requiredDepositsTFB || textField == requiredDepositsTFR || textField == propertyPriceTFB || textField == propertyPriceTFR{
            let text = textField.text as NSString?
            let newText = text?.replacingCharacters(in: range, with: string).replacingOccurrences(of: ",", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
            
            print("New Text :",newText as Any)
            if let _ = newText, let numericValue = Double(newText!){
                if textField == requiredDepositsTFR{
                    updateText(newText: newText!, numericValue: numericValue, textField: requiredDepositsTFR)
                }else if textField == requiredDepositsTFB{
                    updateText(newText: newText!, numericValue: numericValue, textField: requiredDepositsTFB)
                }else if textField == propertyPriceTFB{
                    updateText(newText: newText!, numericValue: numericValue, textField: propertyPriceTFB)
                }else if textField == propertyPriceTFR{
                    updateText(newText: newText!, numericValue: numericValue, textField: propertyPriceTFR)
                }
//                let currencyFormattedValue = numericValue.showPriceInDollar()
//                print("Currency Formatted Value : ",currencyFormattedValue)
//                //textField.text = nil
//                DispatchQueue.main.async{
//                    textField.text == currencyFormattedValue
//                }
            }
        }
        return true
    }
    
    func updateText(newText:String,numericValue:Double,textField:UITextField){
        let currencyFormattedValue = numericValue.showPriceInDollar()
        print("Currency Formatted Value : ",currencyFormattedValue)
        //textField.text = nil
        DispatchQueue.main.async{
            textField.text = currencyFormattedValue
        }
    }
    
    @IBAction func setAvailableDate(_ sender: UIButton) {
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                let dateStringDisplay = formatter.string(from: dt)
                sender.setTitle(dateStringDisplay, for: [])
                availableDateDisplayGB = dateStringDisplay
                formatter.dateFormat = "MM/dd/yyyy"
                let dateString = formatter.string(from: dt)
                availableDateGB = dateString
                
            }
        }
    }
    
    
}

