//
//  KXHistory.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 28/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import AAViewAnimator
import SideMenu

class KxHistoryTVC: UITableViewCell
{
    
    @IBOutlet var propertyName: UILabel!
    
    @IBOutlet var propertyAddress: UILabel!
    
    @IBOutlet var usdPrice: UILabel!
    
    @IBOutlet var propertyImage: UIImageView!
    
    @IBOutlet var rateAndReviewBTNRef: UIButton!
    @IBOutlet weak var propertyTypeView: UIView!
    @IBOutlet weak var propertyTypeLBL: UILabel!
    
    override func awakeFromNib() {
        propertyTypeView.transform = CGAffineTransform.identity
        propertyTypeView.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))

    }
    
    func configure(with data: MyRentalModel) {
        self.propertyName.text = data.property.propertyName
        self.propertyAddress.text = data.property.address
        self.usdPrice.text = data.property.price.showPriceInKilo()
        if let image = data.property.images.filter({$0.typeID == 6}).first{
            self.propertyImage.kf.setImage(with: URL(string: baseImageURL + image.image)!, placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }else if let imageAlter = data.property.images.map({$0.image}).first{
            self.propertyImage.kf.setImage(with: URL(string: baseImageURL + imageAlter)!, placeholder: #imageLiteral(resourceName: "buildingPlaceHolder"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
        }
        
    }
}

class KxHistoryMyPropTVC: UITableViewCell{
    
    @IBOutlet var propertyName: UILabel!
    
    @IBOutlet var propertyAddress: UILabel!
    
    @IBOutlet var pendingRequestsCount: UILabel!
    
    @IBOutlet var propertyImage: UIImageView!
    
    @IBOutlet var pendingRequestBTN: UIButton!
    
    @IBOutlet weak var propertyTypeView: UIView!
    
    @IBOutlet weak var propertyTypeLBL: UILabel!
    
    override func awakeFromNib() {
        propertyTypeView.transform = CGAffineTransform.identity
        propertyTypeView.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi * 0.25))
        
    }

    func configure(with data: MyPropertyModel){
        self.propertyName.text = data.propertyName
        self.propertyAddress.text = data.address
        //let reviewCount = (data.offersCount - data.ratingCount) < 0 ? 0 : data.offersCount - data.ratingCount
//        self.pendingRequestsCount.text =  reviewCount.description +  " pending reviews"
        if data.property_type == 1 {
            self.propertyTypeLBL.text = "SELL"
        }else {
            self.propertyTypeLBL.text = "RENT"
        }

        if data.requestsCount == 0 {
            self.pendingRequestsCount.text = "No people stayed yet"
            self.pendingRequestBTN.isUserInteractionEnabled = false
        }else {
            self.pendingRequestsCount.text = data.requestsCount.description +  " People stayed here"
            self.pendingRequestBTN.isUserInteractionEnabled = true
        }

        self.propertyImage.kf.setImage(with: URL(string: baseImageURL + data.image), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
    }
    
}

class KXHistory: KLBaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var movingview2: UIView!
    @IBOutlet var movingview1: UIView!
    @IBOutlet var RentView: UIView!
    @IBOutlet var PropertyView: UIView!
    
    @IBOutlet var myPropertyTable: UITableView!
    @IBOutlet var myRentalTable: UITableView!
    
    var myRentalBTN = 0
    var myPropertyBTN = 0
    
    var myRentalData:[MyRentalModel] = []
    var myPropertyData:[MyPropertyModel] = []
    var reviewQuestionsData:[ReviewQuestionModel] = []
    var myRentalDone:Bool = false
    var myPropertyDone:Bool = false
    var reviewQuestionsDone = false
    
    final var moving_inMyrequest: String = ""
    final var durationMyRequest: String = ""
    final var typeOfProperty: Int = 1
    final var propertyId: Int = 210
    
    
    
    
//    @IBOutlet var unreadChatCountLBL: UILabel!
//    @IBOutlet var notificationCountLBL: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        print("KXHistory")
        setUpWebcall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setUpNotificationLBL()
        //self.setUpChatCount()
    }
    
    
//    func setUpChatCount(){
//        DispatchQueue.main.async {
//            self.unreadChatCountLBL.layer.cornerRadius = 9
//            self.unreadChatCountLBL.clipsToBounds = true
//            //self.unreadChatCountLBL.layer.masksToBounds = true
//        }
//
//        if Chat.instance.unreadCount > 0{
//            unreadChatCountLBL.isHidden = false
//            unreadChatCountLBL.text = Chat.instance.unreadCount.description
//        }else{
//            unreadChatCountLBL.isHidden = true
//        }
//    }
//
//    func setUpNotificationLBL(){
//        DispatchQueue.main.async {
//            self.notificationCountLBL.layer.cornerRadius = 9
//            self.notificationCountLBL.layer.masksToBounds = true
//        }
//
//        if NotificationManager.shared.currentCount > 0{
//            self.notificationCountLBL.isHidden = false
//            self.notificationCountLBL.text = NotificationManager.shared.currentCount.description
//        }else{
//            self.notificationCountLBL.isHidden = true
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func slideTapped(_ sender: UIButton) {
        // sideMenuNavigation
        showSideMenu()
    }
    
    // Tab Actions
    
    @IBAction func tabHomeTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXHomeViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabAddPropertyTapped(_ sender: UIButton) {
        
        let vc = storyboardProperty!.instantiateViewController(withIdentifier: "KXPostPropertyViewController") as! KXPostPropertyViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabSearchTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .search
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabChatTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .chat
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tabNotificationTapped(_ sender: UIButton) {
        
        let vc = storyboardMain!.instantiateViewController(withIdentifier: "KXTabViewController") as! KXTabViewController
        vc.segueType = .notification
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

    
    @IBAction func myRentalTapped(_ sender: UIButton) {
        if(myRentalBTN == 1)
        {
            RentView.isHidden = false
            movingview1.isHidden = false
            
            PropertyView.isHidden = true
            movingview2.isHidden = true
            
            PropertyView.aa_animate(duration: 1, springDamping: .slight, animation:.fromRight ) { inAnimating in
                if inAnimating {
                    // View is animating
                    
                }
                else {
                    // View's animation is done
                }
            }
            myRentalBTN = 0
            myPropertyBTN = 0
            
            movingview1.aa_animate(duration: 1, springDamping: .slight, animation:.fromRight ) { inAnimating in
                if inAnimating {
                    // View is animating
                    
                }
                else {
                    // View's animation is done
                }
            }
        }
    }
    
    
    @IBAction func myPropertyTapped(_ sender: UIButton) {
        if(myPropertyBTN == 0)
        {
            RentView.isHidden = true
            movingview1.isHidden = true
            
            PropertyView.isHidden = false
            movingview2.isHidden = false
            RentView.aa_animate(duration: 0.8, springDamping: .slight, animation:.fromLeft ) { inAnimating in
                if inAnimating {
                    // View is animating
                    
                }
                else {
                    // View's animation is done
                }
            }
            
           myRentalBTN = 1
           myPropertyBTN = 1
            movingview2.aa_animate(duration: 0.8, springDamping: .slight, animation:.fromLeft ) { inAnimating in
                if inAnimating {
                    // View is animating
                    
                }
                else {
                    // View's animation is done
                }
            }
        }
    }
    

    
    //ShowPendingReviews
    @IBAction func showPendingReviews(_ sender: UIButton) {
        let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXHistoryPendingRequestsController") as! KXHistoryPendingRequestsController
        vc.questionSource = self.reviewQuestionsData.filter({$0.question_type == 2}) // rate the property
        vc.passingPropertyID = sender.tag
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    @IBAction func showReviewPopup(_ sender: UIButton) {
        let vc = storyboardMain?.instantiateViewController(withIdentifier: "KXRateUserPopUpController") as! KXRateUserPopUpController
        vc.questionsSource = self.reviewQuestionsData.filter({$0.question_type == 2}) // rate user
        vc.passingPropertyID = myRentalData[sender.tag].propertyID
        vc.toUserID = myRentalData[sender.tag].userID
       // vc.
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //MARK: SetUpWebCall
    func setUpWebcall(){
        
        //ReviewQuestions
        Webservices.getMethod(url: "getReviewQuestions?user_id=\(Credentials.shared.userId)") { (isComplete, json) in
            if isComplete{
                do{
                    if let trueData = json["Data"] as? [[String:Any]]{
                        let data = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                        let parsedData = try JSONDecoder().decode([ReviewQuestionModel].self, from: data)
                        self.reviewQuestionsData = parsedData
                        self.reviewQuestionsDone = true
                        if self.myRentalDone && self.myPropertyDone && self.reviewQuestionsDone{
                            BaseThread.asyncMain {
                                self.filterOutData()

                            }
                            
                        }

                        print("Succeess-getReviewQuestions")
                        // print(parsedData.map({$0.question}))
                    }else{
                        print("Failed conversion")
                    }
                }catch{
                    print(error)
                }
            }
            
        }
        //My Rental
        //\(Credentials.shared.userId)
        Webservices.getMethod(url: "propertyRequestsSent?user_id=\(Credentials.shared.userId)") { (isComplete, json) in
            if isComplete{
                do{
                    if let trueData = json["Data"] as? [[String:Any]]{
                        print("========")
                        print("propertyRequestsSent:",json["Data"] as AnyObject)
                        print("========")
                        let data = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                        let parsedData = try JSONDecoder().decode([MyRentalModel].self, from: data)
                        self.myRentalData = parsedData
                        self.myRentalDone = true
                        if self.myPropertyDone && self.myRentalDone && self.reviewQuestionsDone{
                            self.filterOutData()
                        }
                        print("Succeess-propertyRequestsSent")

                    }else{
                        print("Failed conversion")
                    }
                }catch{
                    print(error)
                }
            }
        }
        
        //My Properties
        Webservices.getMethod(url: "myAddedProperties?user_id=\(Credentials.shared.userId)&history=1") { (isComplete, json) in
            if isComplete{
                do{
                    if let trueData = json["Data"] as? [[String:Any]]{
                        print("========")
                        print("myAddedProperties:",json["Data"] as AnyObject)
                        print("========")

                        let data = try JSONSerialization.data(withJSONObject: trueData, options: .prettyPrinted)
                        let parsedData = try JSONDecoder().decode([MyPropertyModel].self, from: data)
                        self.myPropertyData = parsedData
                        self.myPropertyDone = true
                        if self.myRentalDone && self.myPropertyDone && self.reviewQuestionsDone{
                            self.filterOutData()

                        }
                        print("Succeess-myAddedProperties")
                    }else{
                        print("Failed conversion")
                    }
                }catch{
                    print(error)
                }
            }
        }


        
    }
    
    func filterOutData(){
        
        print("My Rental Count before",myRentalData.count)
        myRentalData = myRentalData.filter({$0.dealConfirmation == 1})
        print("My Rental Count after",myRentalData.count)
        //print("My Property Count before",myPropertyData.count)
        //myPropertyData = myPropertyData.filter({$0.requestsCount > 0})
        print("My Property Count after",myPropertyData.count)
        myPropertyTable.reloadData()
        myRentalTable.reloadData()
        myRentalTapped(UIButton())

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == myRentalTable{
            return myRentalData.count
        }else{
            return myPropertyData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == myRentalTable{
//            let cellRental = myRentalTable.dequeueReusableCell(withIdentifier: "KxHistoryTVC") as! KxHistoryTVC
//            cellRental.configure(with: myRentalData[indexPath.row])
//            return cellRental
            let cellRental = myRentalTable.dequeueReusableCell(withIdentifier: "KXHistoryExtendedTVC") as! KXHistoryExtendedTVC
            cellRental.reviewQuestionsData = self.reviewQuestionsData
            cellRental.configureCellWith(data: myRentalData[indexPath.row], controller: self)
            cellRental.cancelRequestBTN.tag = indexPath.row
            
            return cellRental

        }else{
            let cellProperty = myPropertyTable.dequeueReusableCell(withIdentifier: "KxHistoryMyPropTVC") as! KxHistoryMyPropTVC
            cellProperty.pendingRequestBTN.tag = myPropertyData[indexPath.row].propertyId
            cellProperty.configure(with: myPropertyData[indexPath.row])
            return cellProperty

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    

}


// Mark: - This used to set the cancel request status
////////// if null - > status = not requested for cancel, owner_status 1 -> requested by owner, else zero.
////////// tenent_status 1 -> requested by tenent, else zero.
////////// admin_status 1 -> admin approved the request, if it is zero, admin waiting for approval. 2-> rejected

//enum KXCancelDealRequestType: Int {
//    case notRequested
//    case tenantRequestPending
//    case tenantRequestRejected
//    case tenantRequestApproved // this will not list in history -> my booking because deal_confimation become zero (from one)
//}



//MARK:Models
struct MyRentalModel:Codable{
    let id: Int
    let dealConfirmation:Int
    let property:Property
    let userID:Int
    let propertyID:Int
    let movingIn:String?
    let movingOut:String?
    let rental_details: RentalDetails?
    
    private enum CodingKeys:String, CodingKey{
        case id, dealConfirmation = "deal_confirmation", property, userID = "user_id", propertyID = "property_id", movingIn = "moving_in", movingOut = "moving_out",rental_details
    }
    
    
    struct RentalDetails: Codable {
        let movingIn:String?
        let movingOut:String?
        let cancel_details: CancelDetails?
        
        private enum CodingKeys: String, CodingKey {
            case  movingIn = "moving_in", movingOut = "moving_out", cancel_details
        }
        
        struct CancelDetails: Codable {
            let id: Int
            let rental_id: Int
            let admin_status: Int
            let owner_status: Int
            let tenant_status: Int

            private enum CodingKeys: String, CodingKey {
                case  id, rental_id, admin_status, owner_status, tenant_status
            }
        }
        
    }
    
    struct Property:Codable{
        let propertyName:String
        let address:String?
        let price:Double
        let images:[Images]
        let propertyType:Int
        let user_id: Int
        
        private enum CodingKeys:String, CodingKey{
            case propertyName = "property_name",address,price = "usd_price", images = "property_images", propertyType = "property_type", user_id
        }
        
        struct Images:Codable{
            let id:Int
            let image:String
            let typeID:Int
            
            private enum CodingKeys:String, CodingKey{
                case id,image,typeID = "type_id"
            }
        }
    }
}

struct MyPropertyModel:Codable {
    let propertyId:Int
    let propertyName:String
    let address:String?
    let requestsCount:Int
    let offersCount:Int
    let ratingCount:Int
    let image:String
    let price:Double
    let property_type: Int
    
    private enum CodingKeys:String, CodingKey{
        case propertyName = "property_name", address, requestsCount = "property_requests_count", offersCount = "property_offers_count", ratingCount = "property_rating_count", image, propertyId = "id", price = "usd_price", property_type
    }
}


struct ReviewQuestionModel:Codable {
    let id:Int
    let question:String
    var rating:String? = "0"
    var question_type: Int?
    
    private enum CodingKeys: String, CodingKey{
        case id, question = "Question", rating, question_type
    }
}




