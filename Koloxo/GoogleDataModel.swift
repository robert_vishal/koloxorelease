//
//  GoogleDataModel.swift
//  Koloxo
//
//  Created by Appzoc on 05/06/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//



import Foundation
struct GoogleMapLocationParse : Codable {
    let results : [Results]
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case results = "results"
        case status = "status"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        results = (try values.decodeIfPresent([Results].self, forKey: .results))!
    //        status = try values.decodeIfPresent(String.self, forKey: .status)
    //    }
    
}


struct Address_components : Codable {
    let long_name : String?
    let short_name : String?
    let types : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case long_name = "long_name"
        case short_name = "short_name"
        case types = "types"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        long_name = try values.decodeIfPresent(String.self, forKey: .long_name)
    //        short_name = try values.decodeIfPresent(String.self, forKey: .short_name)
    //        types = try values.decodeIfPresent([String].self, forKey: .types)
    //    }
    
}


struct Bounds : Codable {
    let northeast : Northeast?
    let southwest : Southwest?
    
    enum CodingKeys: String, CodingKey {
        
        case northeast
        case southwest
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        northeast = try Northeast(from: decoder)
    //        southwest = try Southwest(from: decoder)
    //    }
    
}

struct Geometry : Codable {
    let bounds : Bounds?
    let location : Location
    let location_type : String?
    let viewport : Viewport?
    
    enum CodingKeys: String, CodingKey {
        
        case bounds
        case location = "location"
        case location_type = "location_type"
        case viewport
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        bounds = try Bounds(from: decoder)
    //        location = try Location(from: decoder)
    //        location_type = try values.decodeIfPresent(String.self, forKey: .location_type)
    //        viewport = try Viewport(from: decoder)
    //    }
    
}

struct Location : Codable {
    let lat : Double
    let lng : Double
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        lat = (try values.decodeIfPresent(Double.self, forKey: .lat))!
    //        lng = (try values.decodeIfPresent(Double.self, forKey: .lng))!
    //    }
    
}

struct Northeast : Codable {
    let lat : Double?
    let lng : Double?
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
    //        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
    //    }
    
}

struct Results : Codable {
    let address_components : [Address_components]?
    let formatted_address : String?
    let geometry : Geometry
    let place_id : String?
    let types : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case address_components = "address_components"
        case formatted_address = "formatted_address"
        case geometry = "geometry"
        case place_id = "place_id"
        case types = "types"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        address_components = try values.decodeIfPresent([Address_components].self, forKey: .address_components)
    //        formatted_address = try values.decodeIfPresent(String.self, forKey: .formatted_address)
    //        geometry = try Geometry(from: decoder)
    //        place_id = try values.decodeIfPresent(String.self, forKey: .place_id)
    //        types = try values.decodeIfPresent([String].self, forKey: .types)
    //    }
    
}

struct Southwest : Codable {
    let lat : Double
    let lng : Double
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
    //        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
    //    }
    
}

struct Viewport : Codable {
    let northeast : Northeast?
    let southwest : Southwest?
    
    enum CodingKeys: String, CodingKey {
        
        case northeast
        case southwest
    }
    
    //    init(from decoder: Decoder) throws {
    //        let values = try decoder.container(keyedBy: CodingKeys.self)
    //        northeast = try Northeast(from: decoder)
    //        southwest = try Southwest(from: decoder)
    //    }
    
}
