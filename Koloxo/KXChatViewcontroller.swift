//
//  KXChatViewcontroller.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 09/04/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import UIKit
import SideMenu
import AAViewAnimator

//
class KXChatMyPropertyModel {
    var propertyId:Int = 0
    var propertyName:String = ""
    var propertyLocation:String = ""
    var propertyImage:String = ""
    var propertyOwnerId: Int = 0
    var property_accepted_request_usersImages: [String] = []
    var unreadChat:Int = 0
    var property_type: Int = 1
    var numberOfChats:Int = 0
    var images:[String] = []
    init(){}
    
    init(object: Json){
        propertyId = object["id"] as? Int ?? 0
        propertyName = object["property_name"] as? String ?? ""
        propertyLocation = object["address"] as? String ?? ""
        propertyImage = object["image"] as? String ?? ""
        property_type = object["property_type"] as? Int ?? 1
        propertyOwnerId = object["user_id"] as? Int ?? 0
        numberOfChats = object["property_chats_count"] as? Int ?? 0
        
        if let tempImageStore = object["property_chat_room"] as? [[String:Any]]{
            images = getImages(from: tempImageStore)
            print(images)
        }
        
        if let acceptRequests = object["property_accepted_request"] as? JsonArray, !acceptRequests.isEmpty {
            for item in acceptRequests {
                if let imageItem = item["users"] as? Json {
                    property_accepted_request_usersImages.append(imageItem["image"] as? String ?? "")
                }
            }
        }
        
        
        
    }
    
    class func getValue(fromObject: Json?) -> KXChatMyPropertyModel{
        guard let object = fromObject else { return KXChatMyPropertyModel()}
        return KXChatMyPropertyModel(object: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXChatMyPropertyModel]{
        var modelArray: [KXChatMyPropertyModel] = []
        for item in fromArray {
            modelArray.append(KXChatMyPropertyModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
    fileprivate func getImages(from obj:[[String:Any]])->[String]{
        var imageArrayTemp:[String] = []
        for item in obj{
            if let chatRoomMembers = item["chat_room_members"] as? [[String:Any]]{
                if chatRoomMembers.count > 1,let users = (chatRoomMembers[1])["users"] as? [String:Any]{
                    imageArrayTemp.append(users["image"] as! String)
                }
            }
         }
        return imageArrayTemp
    }
    
}

class KXChatMyGeneralModel {
    var propertyId:Int = 0
    var propertyName:String = ""
    var propertyLocation:String = ""
    var propertyImage:String = ""
    var propertyOwnerId: Int = 0
    var unreadChat:Int = 0
    var chat_owner_ship: KXChatOwnerShip?
    var invitedChats:[KXInvitedChatRooms] = []
    
    //var property_type: Int = 1
    
    class KXChatOwnerShip {
        var id: Int = 0
        var owner_id: Int = 0
        var property_id: Int = 0
        var request_id: Int = 0
        var channel: String = ""
        
        init(){}
        init(object: Json){
            id = object["id"] as? Int ?? 0
            owner_id = object["owner_id"] as? Int ?? 0
            property_id = object["property_id"] as? Int ?? 0
            request_id = object["request_id"] as? Int ?? 0
            channel = object["channel"] as? String ?? ""
        }
        class func getValue(fromObject: Json?) -> KXChatOwnerShip {
            guard let object = fromObject else { return KXChatOwnerShip() }
            return KXChatOwnerShip(object: object)
        }
    }
 
    class KXInvitedChatRooms{
        var id:Int
        var ownerID:Int
        var propertyID:Int
        var requestID:Int
        var channel:String
        var image:String
        var name:String = ""
        
        init(json:Json){
            id = json["id"] as? Int ?? 0
            ownerID = json["owner_id"] as? Int ?? 0
            propertyID = json["property_id"] as? Int ?? 0
            requestID = json["request_id"] as? Int ?? 0
            channel = json["channel"] as? String ?? ""
            if let members = json["chat_room_members"] as? [[String:Any]]{
                if members.count > 1,let actualMember = members[1]["users_details"] as? [String:Any]{
                    image = actualMember["image"] as? String ?? "not found"
                    name = (actualMember["first_name"] as? String ?? "") + " " + (actualMember["last_name"] as? String ?? "")
                }else{
                    image = "not found"
                }
            }else{
                image = "not found"
            }
        }
        
    }
    
    init(){}
    
    init(object: Json){
        if let property = object["property"] as? Json {
            propertyOwnerId = property["user_id"] as? Int ?? 0
            propertyLocation = property["address"] as? String ?? ""
            propertyId = property["id"] as? Int ?? 0
            propertyName = property["property_name"] as? String ?? ""
            if let imageArray = property["property_images"] as? JsonArray {
                for item in imageArray {
                    if let typeId = item["type_id"] as? Int, typeId == 6, let imageURL = item["image"] as? String {
                        propertyImage = imageURL
                        break
                    }
                }
            }
            if let chatownership = object["chat_owner_ship"] as? Json {
                self.chat_owner_ship = KXChatOwnerShip.getValue(fromObject: chatownership)
            }
            if let invitedChats = object["char_rooms"] as? [Json]{
                print("Invited Chat Count : \(invitedChats.count)")
                for chat in invitedChats{
                    self.invitedChats.append(KXInvitedChatRooms.init(json: chat))
                }
            }
        }
    }
    
    class func getValue(fromObject: Json?) -> KXChatMyGeneralModel{
        guard let object = fromObject else { return KXChatMyGeneralModel()}
        return KXChatMyGeneralModel(object: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [KXChatMyGeneralModel]{
        var modelArray: [KXChatMyGeneralModel] = []
        for item in fromArray {
            modelArray.append(KXChatMyGeneralModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
}

class GeneralTableCell: UITableViewCell
{
    @IBOutlet weak var buildingImage: UIImageView!
    
    @IBOutlet weak var buildingName: UILabel!
    
    @IBOutlet weak var buildingLocation: UILabel!
    
    @IBOutlet weak var requestCount: UILabel!
    
    func configureWith(data:KXChatViewcontroller.RequestedUsers.MyGeneral){
        self.buildingImage.kf.setImage(with: URL(string:"\(baseImageURL)\(data.propertyImage)"), placeholder: nil,
                                       options: [.requestModifier(getImageAuthorization())],
                                       progressBlock: nil,
                                       completionHandler: nil)
        self.buildingName.text = data.propertyName
        self.buildingLocation.text = data.propertyLocation
        self.requestCount.text = "\(data.unreadChat)"
        
    }
}

class MyPropertiesTableCell:UITableViewCell{
   
    @IBOutlet weak var buildingImage: UIImageView!
    
    @IBOutlet weak var buildingName: UILabel!
    

    @IBOutlet weak var buildingLocation: UILabel!
    
    @IBOutlet weak var unreadCount: UILabel!
    
    @IBOutlet weak var requestCount: UILabel!
    
    @IBOutlet var profileImgOne: RoundedImage!
    
    @IBOutlet var profileImageTwo: RoundedImage!
    
    func configureWith(data:KXChatViewcontroller.RequestedUsers.MyProperties){
        self.buildingName.text = data.propertyName
        self.buildingLocation.text = data.propertyLocation
        self.unreadCount.text = "\(data.unreadChat)"
        self.requestCount.text = "\(data.requestsReceived)"
        self.buildingImage.kf.setImage(with: URL(string:"\(baseImageURL)\(data.propertyImage)"), placeholder: nil,
                         options: [.requestModifier(getImageAuthorization())],
                         progressBlock: nil,
                         completionHandler: nil)
    }
}


class KXChatViewcontroller: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet var movingview: UIView!
    @IBOutlet var mypropertyref: UIButton!
    @IBOutlet var mygeneralref: UIButton!
    var btn1 = 0
    var btn2 = 0
    @IBOutlet var generalView: UIView!
    @IBOutlet var propertyView: UIView!
    @IBOutlet var Property_moveScreen: UIView!
    @IBOutlet var general_moveScreen: UIView!
    @IBOutlet var chat_tab: UITableView!
    @IBOutlet var general_tab: UITableView!
    let desectedColor:UIColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.4)
    
    fileprivate var myPropertySource: [KXChatMyPropertyModel] = []
    fileprivate var myGeneralSource: [KXChatMyGeneralModel] = []
    fileprivate var propertyOwnerId: Int = 0
    fileprivate var isThreadPropertyFinished: Bool = false
    fileprivate var isThreadGeneralFinished: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//         guard isLoggedIn() else {return}
         //guard isLoadedTabSearchChatNotify == .chat else { return }
        NotificationCenter.default.addObserver(self, selector: #selector(currentTabSet), name: .chatTabActive, object: nil)
//         getDataMyPropertiesWeb()
//         getDataMyGeneralWeb()
    }
    
    @objc func currentTabSet(){
        guard isLoggedIn() else {return}
        getDataMyPropertiesWeb()
        getDataMyGeneralWeb()
    }
    
    fileprivate func setupInterface(){
        chat_tab.reloadData()
        general_tab.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      //  guard isLoadedTabSearchChatNotify == .chat else { return }
       // guard isLoggedIn() else {return}
      //  if isLoadedTabSearchChatNotify == .chat {
            /*  Property_moveScreen.isHidden = false
             general_moveScreen.isHidden = true
             view1.isHidden = false
             view2.isHidden = true
             self.generalView.alpha = 0.2
             self.propertyView.alpha = 1 */
            //addFillerData()
            
          //  getDataMyPropertiesWeb()
          //  getDataMyGeneralWeb()
     //   }
    }
    
    @IBAction func My_Property_Button(_ sender: Any) {
        
        general_moveScreen.isHidden = true
        Property_moveScreen.isHidden = false
        if(btn1 == 1)
        {
            view2.isHidden = true
            view1.isHidden = false
            view1.aa_animate(duration: 0.5, springDamping: .slight, animation:.fromRight ) { inAnimating in
                if inAnimating {

                }
                else {
                    // View's animation is done
                }
            }
            btn1 -= 1
            btn2 -= 1
            Property_moveScreen.aa_animate(duration: 0.6, springDamping: .slight, animation:.fromRight ) { inAnimating in
                if inAnimating {
                    self.generalView.alpha = 0.2
                    self.propertyView.alpha = 1
                }
                else {
                    // View's animation is done
                }
            }
        }
    }
    @IBAction func My_General_Button(_ sender: Any) {
        
        Property_moveScreen.isHidden = true
        general_moveScreen.isHidden = false
        if(btn2 == 0)
        {
            view2.isHidden = false
            view1.isHidden = true
            view2.aa_animate(duration: 0.5, springDamping: .slight, animation:.fromLeft ) { inAnimating in
                if inAnimating {
                    
                    
                    
                }
                else {
                    // View's animation is done
                }
            }
            btn1 += 1
            btn2 += 1
            general_moveScreen.aa_animate(duration: 0.6, springDamping: .slight, animation:.fromLeft ) { inAnimating in
                if inAnimating {
                    self.generalView.alpha = 1
                    self.propertyView.alpha = 0.2
                    
                }
                else {
                    // View's animation is done
                }
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == chat_tab{
            return myPropertySource.count
        }else{
            return myGeneralSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == chat_tab{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyPropertiesTableCell") as! MyPropertiesTableCell
            let data = myPropertySource[indexPath.row]
            cell.buildingName.text = data.propertyName
            cell.buildingLocation.text = data.propertyLocation
            if data.numberOfChats > 3{
                cell.requestCount.text = "3+"
            }else{
                cell.requestCount.text = "\(data.numberOfChats)"
            }
            if data.images.count > 1{
                cell.profileImgOne.kf.setImage(with: URL(string: "\(baseImageURL)\(data.images[1])"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            }
            if data.images.count > 0{
                cell.profileImageTwo.kf.setImage(with: URL(string: "\(baseImageURL)\(data.images[0])"), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
            }
            //cell.unreadCount.text = "\(data.unreadChat)"
            //cell.requestCount.text = "\(data.requestsReceived)"
            cell.buildingImage.kf.setImage(with: URL(string:baseImageURLDetail + data.propertyImage), placeholder: nil,
                                           options: [.requestModifier(getImageAuthorization())],
                                           progressBlock: nil,
                                           completionHandler: nil)
            print("UsersCount:",myPropertySource[indexPath.row].property_accepted_request_usersImages.count)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralTableCell") as! GeneralTableCell
            let data = myGeneralSource[indexPath.row]
            cell.buildingImage.kf.setImage(with: URL(string:baseImageURLDetail + data.propertyImage), placeholder: nil,
                                           options: [.requestModifier(getImageAuthorization())],
                                           progressBlock: nil,
                                           completionHandler: nil)
            cell.buildingName.text = data.propertyName
            cell.buildingLocation.text = data.propertyLocation
            
            if let chatData = data.chat_owner_ship{
                if let channelList = Chat.instance.chatClient?.channelsList(){
                    _ = channelList.channel(withSidOrUniqueName: chatData.channel, completion: { (result, channel) in
                        if let _ = channel{
                            channel!.getUnconsumedMessagesCount(completion: { (result, count) in
                                cell.requestCount.text = "\(count)"
                            })
                        }
                    })
                    
                }
            }
//            cell.requestCount.text = "\(data.unreadChat)"

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == chat_tab{
            return 134
        }
        else{
            return 101
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == chat_tab{
            let vc = self.storyboardChat!.instantiateViewController(withIdentifier: "KXChatMembersViewController") as! KXChatMembersViewController
            vc.propertyName = myPropertySource[indexPath.row].propertyName
            vc.passingPropertyID = myPropertySource[indexPath.row].propertyId
            vc.propertyOwnerId = myPropertySource[indexPath.row].propertyOwnerId
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            if myGeneralSource[indexPath.row].invitedChats.count > 1{
                let vc = self.storyboardMain?.instantiateViewController(withIdentifier: "KXInvitedChatController") as! KXInvitedChatController
                vc.dataSource = myGeneralSource[indexPath.row]
                vc.accessNav = self.navigationController
                self.navigationController?.present(vc, animated: true, completion: nil)
               // print(myGeneralSource[indexPath.row].invitedChats[1].channel)
            }else{
                let vc = self.storyboardChat!.instantiateViewController(withIdentifier: "KXChatDetailVC") as! KXChatDetailVC
                if let _ = myGeneralSource[indexPath.row].chat_owner_ship{
                    vc.channelName = myGeneralSource[indexPath.row].chat_owner_ship!.channel
                    vc.channelID = myGeneralSource[indexPath.row].chat_owner_ship!.id
                    vc.propertyID = myGeneralSource[indexPath.row].propertyId
                    vc.propertyOwnerId = myGeneralSource[indexPath.row].propertyOwnerId
                    vc.propertyName = myGeneralSource[indexPath.row].propertyName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    
    fileprivate func getDataMyPropertiesWeb(){
        let url = "myAddedProperties?user_id=" + Credentials.shared.userId
        Webservices2.getMethod(url: url) { (isSucceeded, response) in
            guard isSucceeded else { return }
           // guard let responseData = response["Data"] as? JsonArray, !responseData.isEmpty else { return }
            if let responseData = response["Data"] as? [[String:Any]]{
                let tempSource = KXChatMyPropertyModel.getValue(fromArray: responseData)
                self.myPropertySource = tempSource.filter({$0.numberOfChats > 0})
                self.isThreadPropertyFinished = true
                BaseThread.asyncMain {
                    if self.isThreadGeneralFinished && self.isThreadPropertyFinished {
                        self.setupInterface()
                        self.My_Property_Button(Any.self)
                    }
                }
            }
            
        }
    }
    
    fileprivate func getDataMyGeneralWeb(){
        let url = "propertyRequestsSent?user_id=" + Credentials.shared.userId
        
        Webservices2.getMethod(url: url) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let responseData = response["Data"] as? JsonArray, !responseData.isEmpty else { return }
            var responseDataStatusChecked: JsonArray = []
            for item in responseData {
                if let status = item["status"] as? Int, status == 1{
                    responseDataStatusChecked.append(item)
                }
            }
            let tempGeneralSource = KXChatMyGeneralModel.getValue(fromArray: responseDataStatusChecked)
            self.myGeneralSource = tempGeneralSource.filter({$0.chat_owner_ship != nil})
            self.isThreadGeneralFinished = true
            
            if let dawshCHat = self.myGeneralSource.filter({$0.propertyName == "Dawsh"}).first?.invitedChats{
                print("Count : ",dawshCHat.count)
                print("Image :",dawshCHat[0].image)
                print("Channel : ",dawshCHat[0].channel)
            }
            
            BaseThread.asyncMain {
                if self.isThreadPropertyFinished && self.isThreadGeneralFinished {
                    self.setupInterface()
                }
            }
        }
    }

    
    @IBAction func Humberger_menu(_ sender: Any) {
        showSideMenu()
    }
    
    @IBAction func Back_Button(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    /*
     "Data": {
     
     "myProperties":[
     {
     “propertyID”:59
     “propertyName”:”Dallas Cape Suite”,
     “propertyLocation”:”Madinat, Khalifa South, Dubai”
     “propertyImage”:”storage/img45.jpg”
     “requestsReceived”:45
     “unreadChat”:8
     }
     ],
     “myGeneral”:[
     {
     “propertyID”:59
     “propertyName”:”Dallas Cape Suite”,
     “propertyLocation”:”Madinat, Khalifa South, Dubai”
     “propertyImage”:”storage/img45.jpg”
     “unreadChat”:8
     “channel”:”Koloxo_193_59_120”
     
     }
     ]
     
     }
 */
    
    //MARK:- SetupWebcall
    
//    func setupWebCall(){
//        let unixTime = SecureSocket.getTimeStamp()
//        let tokenKey = SecureSocket.getToken(with: unixTime)
//
//        let param:[String:String] = ["userid":Credentials.shared.userId,"tokenkey":tokenKey,"unix_time":unixTime]
//        Webservices.getMethodWith(url: "chatPropertiesList", parameter: param) { (isComplete,json) in
//            let trueData = json["Data"] as? [String:Any]
//            let tempData = trueData!["requestedUsers"]
//            let decoder = JSONDecoder()
//            do{
//                let jsonData = try JSONSerialization.data(withJSONObject: tempData, options: .prettyPrinted)
//                let parsedData = try decoder.decode(RequestedUsers.self, from: jsonData)
//                self.myPropertySource = parsedData.myProperties
//                self.myGeneralSource = parsedData.myGeneral
//            }catch{
//                print(error)
//            }
//        }
//
//    }

    struct RequestedUsers:Codable{
        let myProperties:[MyProperties]
        let myGeneral:[MyGeneral]
        
        struct MyProperties:Codable {
            let propertyID:Int
            let propertyName:String
            let propertyLocation:String
            let propertyImage:String
            let requestsReceived:Int
            let unreadChat:Int
        }
        struct MyGeneral:Codable {
            let propertyID:Int
            let propertyName:String
            let propertyLocation:String
            let propertyImage:String
            let unreadChat:Int
            let channel:String
            let channelID:Int
        }
        
    }
    
    
}


//class KXChatMyPropertyUsersModel
//{
//    var id:Int = 0
//    var first_name:String = ""
//    var image:String = ""
//
//    init(){}
//
//    init(dictionary: Json) {
//        id = dictionary["id"] as? Int ?? 0
//        first_name = dictionary["first_name"] as? String ?? ""
//        image = dictionary["image"] as? String ?? ""
//    }
//
//    class func  getValue(fromObject: Json?) -> KXChatMyPropertyUsersModel {
//        guard let object = fromObject else { return KXChatMyPropertyUsersModel() }
//        return KXChatMyPropertyUsersModel(dictionary: object)
//    }
//
//    class func getValue(fromArray: JsonArray) -> [KXChatMyPropertyUsersModel]
//    {
//        var result:[KXChatMyPropertyUsersModel] = []
//        for item in fromArray
//        {
//            result.append(KXChatMyPropertyUsersModel.getValue(fromObject: item))
//        }
//        return result
//    }
//
//}

//    //MARK:- Add Filler Data
//    func addFillerData(){
//        for i in 0...2{
//            let propertyNames = ["Dallas Club","PoolSide House","Dallas Apartments"]
//            let mp = RequestedUsers.MyProperties(propertyID: i, propertyName: propertyNames[i], propertyLocation: "XYZ Street, ABC Colony", propertyImage: "public/koloxo-building_default.png", requestsReceived: 3+i, unreadChat: 5+i)
//            let mg = RequestedUsers.MyGeneral(propertyID: i, propertyName: propertyNames[i], propertyLocation: "XYZ Street, ABC Colony", propertyImage: "public/koloxo-building_default.png", unreadChat: 3+i, channel: "Koloxo_193_60_120", channelID: i)
//            myPropertiesSource.append(mp)
//            myGeneralSource.append(mg)
//        }
//        chat_tab.reloadData()
//        general_tab.reloadData()
//    }

