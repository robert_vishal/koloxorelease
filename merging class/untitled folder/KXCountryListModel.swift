//
//  KXCountryListModel.swift
//  Koloxo
//
//  Created by Appzoc-Macmini on 16/05/18.
//  Copyright © 2018 Appzoc-Macmini. All rights reserved.
//

import Foundation
public class KXCountryListModel {
    public var id : Int = 0
    public var country : String = ""
    public var currency: String = ""
    public var name : String = ""
  

    public class func getValue(fromObject: Json?) -> KXCountryListModel {
        guard let fromObject = fromObject else { return KXCountryListModel() }
        return KXCountryListModel(object: fromObject)!
    }
    public class func getValue(fromArray: [Json]?) -> [KXCountryListModel] {
        var modelArray : [KXCountryListModel] = []
        guard let fromArray = fromArray else { return modelArray }
        for item in fromArray{
            let object = KXCountryListModel.getValue(fromObject: item)
            modelArray.append(object)
        }
        
        
        return modelArray
    }
    required public init?(object: Json) {
        id = object["id"] as? Int ?? 0
        country = object["country"] as? String ?? ""
        currency = object["currency"] as? String ?? ""
        name = object["name"] as? String ?? ""
    }
    
    private init(){
        
    }
}
